/*********************************************************************
 * 版权所有 WJF
 *
 * 文件名称：
 * 文件标识：
 * 内容摘要：
 * 其它说明：
 * 当前版本：
 * 作    者： 王俊峰
 * 完成日期：
 **********************************************************************/

/***************************************************************************
 *                                文件引用
 ***************************************************************************/
#import "AppDelegate.h"

#import <QuartzCore/CATransform3D.h>

#import "LoginViewController.h"
#import "LoadingViewController.h"

//身份证
#import <ISIDReaderSDK/ISIDReaderSDK.h>
//行驶证
#import <ISVehicleLicenseSDK/ISVehicleLicenseSDK.h>
//驾驶证
#import <ISDrivingLicenseSDK/ISDrivingLicenseSDK.h>
//合格证
#import <ISVehicleCertificateSDK/ISVehicleCertificateSDK.h>

/***************************************************************************
 *                                 宏定义
 ***************************************************************************/
/* 移除欢迎页面 */
#define LOADING_TIME 1

/***************************************************************************
 *                                 常量
 ***************************************************************************/


/***************************************************************************
 *                                类型定义
 ***************************************************************************/


/***************************************************************************
 *                                全局变量
 ***************************************************************************/


/***************************************************************************
 *                                 原型
 ***************************************************************************/
@interface AppDelegate ()
{
    LoadingViewController *loadingViewController;
    LoginViewController *loginViewController;
}

@end


/***************************************************************************
 *                                类的实现
 ***************************************************************************/
@implementation AppDelegate

@synthesize devicetoken;

#pragma mark -
#pragma mark - didFinishLaunchingWithOptions
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    /* 加载界面 */
    [self loadLoadingView];
    
    //记录日志消息
    [self logMessageRecordBackground:application withLaunchingWithOptions:launchOptions];

    //OCR SDK 初始化
    [self ocrSdkInit];

    /* 初始化 清除缓存 */
    NSURLCache * cache = [NSURLCache sharedURLCache];
    [cache removeAllCachedResponses];
    [cache setDiskCapacity:0];
    [cache setMemoryCapacity:0];

    /*
    NSHTTPCookieStorage实现管理共享的cookie存储一个singleton对象（共享实例）
    这些cookies是所有的应用程序之间的共享和跨进程同步保存。
     */
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    [cookieStorage setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];

    int cacheSizeMemory = 8 * 1024 * 1024; // 8MB
    int cacheSizeDisk = 32 * 1024 * 1024; // 32MB
    NSURLCache* sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"cache"];
    [NSURLCache setSharedURLCache:sharedCache];

    //微信注册
    NSString *weixinAppId = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getWeixinAppId]];
    STRING_NIL_TO_NONE(weixinAppId);
    BOOL wxApi =  [WXApi registerApp:weixinAppId];

    if (wxApi)
    {
        NSLog(@"注册成功");
    }

    //注册通知
    #ifdef DEBUG

    #else
    
    MPPPushServer *pushServer = [MPPPushServer sharePushManager];
    [pushServer setDelegate:self];
    
    NSString *MPPPushServerAppCode = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getMPPPushServerAppCode]];
    STRING_NIL_TO_NONE(MPPPushServerAppCode);
    NSString *MPPPushServerPushServerIP = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getMPPPushServerPushServerIP]];
    STRING_NIL_TO_NONE(MPPPushServerPushServerIP);
    
    [MPPPushServer initWithOptions:launchOptions MPPAPPCode:MPPPushServerAppCode pushUrl:MPPPushServerPushServerIP];
    [MPPPushServer applicationWillEnterForeground:application];

    #endif

    //打开加密传输
    [UMConfigure setEncryptEnabled:YES];
    
    //设置打开日志
    #ifdef DEBUG
    [UMConfigure setLogEnabled:YES];
    #else
    [UMConfigure setLogEnabled:NO];
    #endif
    
    [UMConfigure initWithAppkey:UMENG_APPKEY channel:UMENG_CHANNELID];
    [UMErrorCatch initErrorCatch];
    [UMCommonLogManager setUpUMCommonLogManager];
    
    //太保用户行为分析
    [self initTalkingDataBackground:application withLaunchingWithOptions:launchOptions];
    
    //百度引擎初始化
    NSString *baiduMapAppKey = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getBaiduMapAppKey]];
    STRING_NIL_TO_NONE(baiduMapAppKey);
    [self BMKMapManagerInit:baiduMapAppKey];
    
    //开启定位
    [self startUpdatingLocation];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

#pragma mark -
#pragma mark - URL Scheme 配置
//当程序完成载入后调用
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return [WXApi handleOpenURL:url delegate:self];
}
//当程序完成载入，并且可能有额外的启动选项时被调用，建议用此方法来初始化应用程序
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [WXApi handleOpenURL:url delegate:self];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    #ifdef DEBUG
    
    #else
    @try {
        [MPPPushServer applicationDidEnterBackground:application];
    } @catch (NSException *exception) {
        
    }
    
    
    #endif
    
    //记录日志消息
    [self logMessageRecordBackground:application withLaunchingWithOptions:nil];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    UIApplication *app = [UIApplication sharedApplication];
    
    __block UIBackgroundTaskIdentifier bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (bgTask != UIBackgroundTaskInvalid)
            {
                bgTask = UIBackgroundTaskInvalid;
            }
        });
    }];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (bgTask != UIBackgroundTaskInvalid)
            {
                bgTask = UIBackgroundTaskInvalid;
            }
        });
    });
    
    #ifdef DEBUG
    
    #else
    
    @try {
        
        [MPPPushServer applicationWillEnterForeground:application];
        
    } @catch (NSException *exception) {
        
    }
    
    
    #endif

    
    //记录日志消息
    [self logMessageRecordBackground:application withLaunchingWithOptions:nil];
}

/**
 当有电话进来或者锁屏，这时你的应用程会挂起
 在这时，UIApplicationDelegate委托会收到通知，
 调用 applicationWillResignActive 方法，你可以重写这个方法，做挂起前的工作，比如关闭网络，保存数据。
 */
- (void)applicationWillResignActive:(UIApplication *)application
{
    //挂起
    [[[ClassFactory getInstance] getInfoHUD] hiddenHud];
    
    //记录日志消息
    [self logMessageRecordBackground:application withLaunchingWithOptions:nil];
}

/*当程序复原时
 另一个名为 applicationDidBecomeActive 委托方法会被调用，
 在此你可以通过之前挂起前保存的数据来恢复你的应用程序
 */
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    //复原
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    //记录日志消息
    [self logMessageRecordBackground:application withLaunchingWithOptions:nil];
}

/*当用户按下按钮，或者关机，程序都会被终止。
 当一个程序将要正常终止时会调用 applicationWillTerminate 方法。
 但是如果长主按钮强制退出，则不会调用该方法。
 这个方法该执行剩下的清理工作，比如所有的连接都能正常关闭，并在程序退出前执行任何其他的必要的工作
 */
- (void)applicationWillTerminate:(UIApplication *)application
{
    //终止
    
    //记录日志消息
    [self logMessageRecordBackground:application withLaunchingWithOptions:nil];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{

    NSLog(@"DeviceToken  %@",[[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""] stringByReplacingOccurrencesOfString: @">" withString: @""] stringByReplacingOccurrencesOfString: @" " withString: @""]);
    
    self.devicetoken=[NSString stringWithFormat:@"%@",[[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""] stringByReplacingOccurrencesOfString: @">" withString: @""] stringByReplacingOccurrencesOfString: @" " withString: @""]];

    #ifdef DEBUG
    
    #else
    @try {
        
        [MPPPushServer registerDeviceToken:deviceToken];
        
    } @catch (NSException *exception) {
        
    }
    
    #endif
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    //如果注册不成功，打印错误信息，可以在网上找到对应的解决方案
    //如果注册成功，可以删掉这个方法
    NSLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    #ifdef DEBUG
    
    #else
    
    @try {
        
        [MPPPushServer didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
        
    } @catch (NSException *exception) {
        
    }
    
    #endif
    
    completionHandler(UIBackgroundFetchResultNewData);
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo
{
    #ifdef DEBUG
    
    #else
    
    @try {
        
        [MPPPushServer handleRemoteNotification:userInfo];
        
    } @catch (NSException *exception) {
        
    }
    
    #endif
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)applicationd idRegisterForRemoteNotificationsWithDeviceToken:(NSData *)pToken
{
    NSLog(@"regisger success:%@",pToken);
    
    //注册成功，将deviceToken保存到应用服务器数据库中
}

- (void)application:(UIApplication *)applicationd idFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Registfail%@",error);
}

//在程序关闭的情况下改变applicationIconBadgeNumber的值
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    application.applicationIconBadgeNumber += 1;
}

#pragma mark -
#pragma mark - WXApiDelegate
/*! @brief 收到一个来自微信的请求，第三方应用程序处理完后调用sendResp向微信发送结果
 *
 * 收到一个来自微信的请求，异步处理完成后必须调用sendResp发送处理结果给微信。
 * 可能收到的请求有GetMessageFromWXReq、ShowMessageFromWXReq等。
 * @param req 具体请求内容，是自动释放的
 */
-(void)onReq:(BaseReq*)req
{
    if ([WXApiManager sharedManager] && [[[WXApiManager sharedManager] delegate ]respondsToSelector:@selector(onReq:)]) {
        [[[WXApiManager sharedManager] delegate] onReq:req];
    }
}

/*! @brief 发送一个sendReq后，收到微信的回应
 *
 * 收到一个来自微信的处理结果。调用一次sendReq后会收到onResp。
 * 可能收到的处理结果有SendMessageToWXResp、SendAuthResp等。
 * @param resp具体的回应内容，是自动释放的
 */
-(void)onResp:(BaseResp*)resp
{
    if ([WXApiManager sharedManager] && [[[WXApiManager sharedManager] delegate ]respondsToSelector:@selector(onResp:)]) {
        [[[WXApiManager sharedManager] delegate] onResp:resp];
    }
}

#pragma mark -
#pragma mark - 太保行为分析
-(void)initTalkingDataBackground:(UIApplication *)application withLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [TalkingData setSignalReportEnabled:YES];
    [TalkingData setLogEnabled:YES];
    
    NSString *serverType = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getServerType]];
    STRING_NIL_TO_NONE(serverType)
    
    NSString *appAnalyticsUrl = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getAppAnalyticsURL]];
    STRING_NIL_TO_NONE(appAnalyticsUrl)
    
    NSString *cloudControlUrl = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getCloudControlURL]];
    STRING_NIL_TO_NONE(cloudControlUrl)
    
    [TalkingData setAppAnalyticsUrl:appAnalyticsUrl];
    [TalkingData setCloudControlUrl:cloudControlUrl];
    [TalkingData sessionStarted:@"8E0A6741219742F6A844B5560D41FF03" withChannelId:serverType];
}

#pragma mark -
#pragma mark - 日志记录
-(void)logMessageRecordBackground:(UIApplication *)application withLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if (launchOptions != nil)
    {
        //初次启动系统 若是两次提交时间相隔1小时，则提交日志;
    }
    else
    {
//        return;
    }
    
    //获取设备UUID
    NSString *uuid = [NSString stringWithFormat:@"%@",[[[UIDevice currentDevice] identifierForVendor] UUIDString]];
    STRING_NIL_TO_NONE(uuid);
    
    /* 数据缓存 查询 */
    FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
    
    [fmdbSql queryWithTableName:LOG_TABLE_NAME complete:^(NSArray *array) {
        
        [self startUploadLogg:array];
        
    }];
}

-(void)startUploadLogg:(NSArray *)operateList
{
    if (operateList.count == 0)
    {
        return;
    }
    
    NSDictionary *requestobjDic = [NSDictionary dictionaryWithObjectsAndKeys:operateList,@"operateList", nil];
    
    [[[ClassFactory getInstance] getNetComm] logRecordAction:requestobjDic success:^(id JSON) {
        
        NSDictionary *requestResultDic = [[NSDictionary alloc] initWithDictionary:JSON];
        NSString *resultCode = [requestResultDic objectForKey:@"resultCode"];
        
        NSString *message = [NSString stringWithFormat:@"%@",[requestResultDic objectForKey:@"message"]];
        STRING_NIL_TO_NONE(message);
        
        if ([resultCode isEqualToString:@"1"])
        {
            /* 数据缓存 清空数据库 */
            FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
            
            [fmdbSql clearTable:LOG_TABLE_NAME complete:^(BOOL isSuccess) {
                
                if (isSuccess)
                {
                    NSLog(@"=====================上传日志/清空日志成功!");
                }
                else
                {
                    NSLog(@"=====================上传日志/清空日志成功!");
                }
            }];
        }
        else
        {
            NSLog(@"=====================上传日志失败!");
        }
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark -
#pragma mark - OCR SDK 初始化
- (void)ocrSdkInit
{
    /*================================== 身份证 ==================================*/
    NSString *cardReaderAppKey = @"3rUCQ58dBd5BgrDaTR6E7Qe8";
    NSString *cardReaderSubAppkey = nil;//reserved for future use
    /*================================== 身份证 ==================================*/
    [[ISCardReaderController sharedController] constructResourcesWithAppKey:cardReaderAppKey subAppkey:cardReaderSubAppkey finishHandler:^(ISOpenSDKStatus status) {
        if (ISOpenSDKStatusSuccess != status)
        {
            NSString *message = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLogic] getOpenSDKStatusMessage:status]];
            STRING_NIL_TO_NONE(message)
            
            NSLog(@"身份证 Auth failed:%ld  %@",(long)status,message);
        }
    }];

    /*================================== 行驶证 ==================================*/
    NSString *vehicleLicenseReaderAppKey = @"3rUCQ58dBd5BgrDaTR6E7Qe8";//this key just for test
    NSString *vehicleLicenseReaderSubAppkey = nil;//reserved for future use
    /*================================== 行驶证 ==================================*/
    [[ISVehicleLicenseReaderController sharedController] constructResourcesWithAppKey:vehicleLicenseReaderAppKey subAppkey:vehicleLicenseReaderSubAppkey finishHandler:^(ISOpenSDKStatus status) {
        if (status != ISOpenSDKStatusSuccess)
        {
            NSString *message = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLogic] getOpenSDKStatusMessage:status]];
            STRING_NIL_TO_NONE(message)
            
            NSLog(@"行驶证 Auth failed:%ld  %@",(long)status,message);
        }
    }];
    
    /*================================== 合格证 ==================================*/
    NSString *vehicleCertificateAppKey = @"3rUCQ58dBd5BgrDaTR6E7Qe8";//this key just for test
    NSString *vehicleCertificateSubAppkey = nil;//reserved for future use
    /*================================== 合格证 ==================================*/
    [[ISVehicleCertificateController sharedISOpenSDKController] constructResourcesWithAppKey:vehicleCertificateAppKey subAppkey:vehicleCertificateSubAppkey finishHandler:^(ISOpenSDKStatus status) {
        if (status != ISOpenSDKStatusSuccess)
        {
            NSString *message = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLogic] getOpenSDKStatusMessage:status]];
            STRING_NIL_TO_NONE(message)
            
            NSLog(@"合格证 Auth failed:%ld  %@",(long)status,message);
        }
    }];
    
    /*================================== 驾驶证 ==================================*/
    NSString *drivingLicenseAppKey = @"3rUCQ58dBd5BgrDaTR6E7Qe8";
    NSString *drivingLicenseSubAppkey = nil;//reserved for future use
    /*================================== 驾驶证 ==================================*/
    [[ISDrivingLicenseReaderController sharedController] constructResourcesWithAppKey:drivingLicenseAppKey subAppkey:drivingLicenseSubAppkey finishHandler:^(ISOpenSDKStatus status) {
        if (ISOpenSDKStatusSuccess != status)
        {
            NSString *message = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLogic] getOpenSDKStatusMessage:status]];
            STRING_NIL_TO_NONE(message)

            NSLog(@"驾驶证 Auth failed:%ld  %@", (long)status,message);
        }
    }];
}

#pragma mark -
#pragma mark - MPPCallBackDelegate
/* 透传消息(接收从极光透传过来的消息，不经过苹果服务器) */
-(void)transparentReceiveTitle:(NSString *)title content:(NSString *)content
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:content delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    
    [alert show];
}
/* 通知消息处理 只针对iOS10 app处于前台 处于后台 走此代理方法接收通知*/
-(void)pushMessageCallBackResult:(UNNotificationContent *)content
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:content.title message:content.body delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    
    [alert show];
}
/* 注册失败 极光返回错误信息*/
-(void)pushRegisterErrorMessage:(NSString *)error
{
    
}
/* 绑定接口失败  registedID获取不到 手机推送权限关闭走此代理方法*/
-(void)pushIphoneNotAllowedNotification
{
    
}

#pragma mark -
#pragma mark - 网络监测
// 程序启动器，启动网络监视
- (void)applicationDidFinishLaunching:(UIApplication *)application
{
    // 设置网络检测的站点
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    hostReach = [Reachability reachabilityWithHostname:@"www.baidu.com"];
    hostReach.reachableOnWWAN = NO;
    
    [hostReach startNotifier];
}

// 通知网络状态
- (void)reachabilityChanged:(NSNotification *)note
{
    Reachability *curReach = [note object];
    
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    NetworkStatus status = [curReach currentReachabilityStatus];
    
    if (status == NotReachable)
    {
        [[[ClassFactory getInstance] getInfoHUD] showHud:@"当前网络不可用,请检查网络偏好设置!"];
    }
    else
    {
        //网络可用
    }
}



#pragma mark -
#pragma mark -  百度地图
- (void)BMKMapManagerInit:(NSString*)key
{
    // 要使用百度地图，请先启动BaiduMapManager
    BMKMapManager *mapManager = [[BMKMapManager alloc]init];
    
    /**
     *百度地图SDK所有接口均支持百度坐标（BD09）和国测局坐标（GCJ02），用此方法设置您使用的坐标类型.
     *默认是BD09（BMK_COORDTYPE_BD09LL）坐标.
     *如果需要使用GCJ02坐标，需要设置CoordinateType为：BMK_COORDTYPE_COMMON.
     */
    if ([BMKMapManager setCoordinateTypeUsedInBaiduMapSDK:BMK_COORDTYPE_BD09LL]) {
        NSLog(@"经纬度类型设置成功");
    } else {
        NSLog(@"经纬度类型设置失败");
    }
    BOOL ret = [mapManager start:key generalDelegate:self];
    if (!ret) {
        NSLog(@"manager start failed!");
    }
    else
    {
        NSLog(@"manager start success!");
    }
}
#pragma mark -
#pragma mark - BMKGeneralDelegate

/**
 *返回网络错误
 *@param iError 错误号
 */
- (void)onGetNetworkState:(int)iError
{
    NSLog(@"BMKGeneralDelegate:%d",iError);
}

/**
 *返回授权验证错误
 *@param iError 错误号 : BMKErrorPermissionCheckFailure 验证失败
 */
- (void)onGetPermissionState:(int)iError
{
    NSLog(@"BMKGeneralDelegate:%d",iError);
}

#pragma mark -
#pragma mark -  定位
- (void)startUpdatingLocation
{
    locationManager = [[CLLocationManager alloc] init];
    
    //判断是否启动了定位服务
    if (![CLLocationManager locationServicesEnabled])
    {
        NSLog(@"定位服务没有开启,请设置打开");
        return;
    }
    //如果用户还没有决定是否使用定位服务
    if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined){
        //向用户请求授权
        [locationManager requestWhenInUseAuthorization];
        
        return;
    }
    //设置委托对象,当定位服务发生事件时通知委托
    locationManager.delegate = self;
    
    // 设置定位精度
    // kCLLocationAccuracyNearestTenMeters:精度10米
    // kCLLocationAccuracyHundredMeters:精度100 米
    // kCLLocationAccuracyKilometer:精度1000 米
    // kCLLocationAccuracyThreeKilometers:精度3000米
    // kCLLocationAccuracyBest:设备使用电池供电时候最高的精度
    // kCLLocationAccuracyBestForNavigation:导航情况下最高精度，一般要有外接电源时才能使用
    //定位的精度:定位的误差在多少M的范围之内
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    //定位的频率:位置超出多少米定位一次
    locationManager.distanceFilter = 10.0;
    
    //开始定位
    [locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations firstObject];
    
    CLLocationCoordinate2D coord = location.coordinate;//经纬度
    //保存经纬度信息
    [[[ClassFactory getInstance] getLogic] setCoord:location.coordinate];
    //location.altitude; //海拔
    //location.course;//航向
    //location.speed;//行走速度
    NSLog(@"经度:%f, 纬度:%f, 海拔:%f, 航向:%f, 速度:%f", coord.longitude, coord.latitude, location.altitude, location.course, location.speed);
    
    
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    
    [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error){
        
        if (placemarks.count>0) {
            
            CLPlacemark *placeMark = placemarks.firstObject;
    
            NSString *country = [NSString stringWithFormat:@"%@",placeMark.country];
            STRING_NIL_TO_NONE(country);
            NSString *locality = [NSString stringWithFormat:@"%@",placeMark.locality];
            STRING_NIL_TO_NONE(locality);
            NSString *subLocality = [NSString stringWithFormat:@"%@",placeMark.subLocality];
            STRING_NIL_TO_NONE(subLocality);
            NSString *thoroughfare = [NSString stringWithFormat:@"%@",placeMark.thoroughfare];
            STRING_NIL_TO_NONE(thoroughfare);
            NSString *name = [NSString stringWithFormat:@"%@",placeMark.name];
            STRING_NIL_TO_NONE(name);
            
            NSString *address = [NSString stringWithFormat:@"%@%@%@%@%@",country,locality,subLocality,thoroughfare,name];
            STRING_NIL_TO_NONE(address);
            NSLog(@"%@",address);
            
            [[[ClassFactory getInstance] getLocalCfg] setAddress:address];
        }
    }];

    //友盟定位统计
    [MobClick setLatitude:coord.latitude longitude:coord.longitude];
    
    //太保定位统计
    [TalkingData setLatitude:coord.latitude longitude:coord.longitude];
}

#pragma mark -
#pragma mark - 内存警告
-  (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    [[[ClassFactory getInstance] getNetComm] clearTheCache];
}

#pragma mark -
#pragma mark - 添加页面
/***********************************************************************
 * 方法名称： loadLoadingView
 * 功能描述： 添加欢迎页面
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (void)loadLoadingView
{
    loadingViewController = [[LoadingViewController alloc] init];
    [self.window setRootViewController:loadingViewController];
}

/***********************************************************************
 * 方法名称： loadLoginView
 * 功能描述： 添加欢迎页面
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (void)loadLoginView
{
    loginViewController = [[LoginViewController alloc] init];
    
    NSString *loginViewStartPage = [NSString stringWithFormat:@"%@/thb/app/login.html?%@",[[[ClassFactory getInstance] getLocalCfg] getHttpServerAddr],[[[ClassFactory getInstance] getLocalCfg] getVersionCode]];
    [loginViewController setStartPage:loginViewStartPage];
    [loginViewController setBatteryColor:HEXCOLOR(0X0C5DAA)];
    
    UICustomNavigationController *loginViewNav = [[UICustomNavigationController alloc] initWithRootViewController:loginViewController];
    [self.window setRootViewController:loginViewNav];
}


@end
