/*********************************************************************
 * 版权所有 WJF
 *
 * 文件名称： 
 * 文件标识：
 * 内容摘要：
 * 其它说明：
 * 当前版本：
 * 作    者： 王俊峰
 * 完成日期： 
 ***********************************************************************/


/***************************************************************************
 *                                文件引用
 ***************************************************************************/
#import <UIKit/UIKit.h>

/***************************************************************************
 *                                 类引用
 ***************************************************************************/

#import "ViewController.h"

#import <Reachability/Reachability.h>

#import <BaiduMapAPI_Base/BMKBaseComponent.h>
#import <CoreLocation/CoreLocation.h>

/***************************************************************************
 *                                 三方库
 ***************************************************************************/

#import "WXApi.h"
/***************************************************************************
 *                                 宏定义
 ***************************************************************************/

//友盟
#define UMENG_APPKEY @"580d5b8c4ad1567046001a09"
#define UMENG_SECRET @"太保CRM"
#define UMENG_CHANNELID @"太保CRM"
//微信
//SIT
//#define WeChatkey @"wx9050a6e5cc2f34ec"
//PRD
//#define WeChatkey @"wx32efd01823e64327"

#define UMSYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define _IPHONE80_ 80000

/***************************************************************************
 *                                 常量
 ***************************************************************************/


/***************************************************************************
 *                                类型定义
 ***************************************************************************/


/***************************************************************************
 *                                 类定义
 ***************************************************************************/

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate,MPPCallBackDelegate,BMKGeneralDelegate,WXApiDelegate>
{
    //主页面
    ViewController *viewController;
    
    //网络监测
    Reachability *hostReach;
    
    //实例变量
    CLLocationManager *locationManager;
}

//MAMapView VisibleMapRect
@property (strong, nonatomic) NSString *orginX_str;
@property (strong, nonatomic) NSString *orginY_str;
@property (strong, nonatomic) NSString *sizeW_str;
@property (strong, nonatomic) NSString *sizeH_str;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *appId;
@property (strong, nonatomic) NSString *channelId;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *devicetoken;

/* 添加 LoadingView */
- (void)loadLoadingView;

/* 添加 LoginView */
- (void)loadLoginView;

@end
