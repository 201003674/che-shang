//
//  CodeStandard.h
//  IPadFramework
//
//  Created by allianture on 13-8-2.
//  Copyright (c) 2013年 allianture. All rights reserved.

//coolfeizy@sina.com
//Cpic1234

#import <Foundation/Foundation.h>

@interface CodeStandard : NSObject
{
    /* 其他类可调用参数 在这里声明 */
    NSString *productCodeStr;/* 产品code */
    
}
@property (strong, nonatomic) NSString *productCodeStr;
/*
 获取产品列表
 入参：productCodeStr：产品code
 出参：产品列表数组
 */
- (NSArray *)getProductList:(NSString *)productCodeStr;


@end
