//
//  InfoHUD.m
//  GUOHUALIFE
//
//  Created by allianture on 13-2-1.
//  Copyright (c) 2013年 zte. All rights reserved.
//

#import "InfoHUD.h"
#import "BDKNotifyHUD.h"
#import "AppDelegate.h"

#import <QuartzCore/QuartzCore.h>


#define HUD_Y 20                /* 距离中心位置调整 */

#define SHOWHUDTIME  1.0f       /* 显示时间 */



@interface InfoHUD ()

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) BDKNotifyHUD *notify;
@property (strong, nonatomic) NSString *imageName;
@property (strong, nonatomic) NSString *notificationText;
@property (strong, nonatomic) UIView *rootView;
@property (strong, nonatomic) AppDelegate *appdelegate;

@end

@implementation InfoHUD


- (id)init
{
    if (self = [super init])
    {
        self.appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    }
    return self;
}


- (void)dealloc
{
//    [super dealloc];
}


#pragma -
#pragma mark - 显示和隐藏hud
/* 设置需要显示的view */
- (void)setRootView:(UIView *)rootview
{
    self.rootView = rootview;
}


/* 显示hud */
- (void)showHud:(NSString *)showStr
{
    self.notify.image = nil;
    self.notify.text = showStr;
    
    [self displayNotification];
}


/* 隐藏hud */
- (void)hiddenHud
{
    if (self.notify.isAnimating)
    {
        [self.notify removeFromSuperview];
        self.notify.isAnimating = NO;
    }
}

#pragma -
#pragma mark - 添加处理
- (BDKNotifyHUD *)notify
{
    if (_notify != nil)
    {
        return _notify;
    }
    _notify = [BDKNotifyHUD notifyHUDWithImage:[UIImage imageNamed:self.imageName] text:self.notificationText];
    
    _notify.center = CGPointMake(self.appdelegate.window.center.x, self.appdelegate.window.center.y - HUD_Y);
    return _notify;
}



- (void)displayNotification
{
    if (self.notify.isAnimating)
    {
        return;
    }
    
    [self.appdelegate.window addSubview:self.notify];
    
    [self.notify presentWithDuration:SHOWHUDTIME speed:0.5f inView:self.appdelegate.window completion:^{
        
        [self.notify removeFromSuperview];
        
    }];
}


@end
