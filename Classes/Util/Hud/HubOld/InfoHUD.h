//
//  InfoHUD.h
//  GUOHUALIFE
//
//  Created by allianture on 13-2-1.
//  Copyright (c) 2013年 zte. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InfoHUD : NSObject


/* 显示hud */
- (void)showHud:(NSString *)showStr;

/* 隐藏hud */
- (void)hiddenHud;


@end
