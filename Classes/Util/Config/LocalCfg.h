/*********************************************************************
 * 版权所有 LK
 *
 * 文件名称： 
 * 文件标识：
 * 内容摘要：配置文件，把一些基本信息存放到文件中
 * 其它说明：
 * 当前版本：
 * 作   者：林科
 * 完成日期： 
 **********************************************************************/

/*************************************************************************** 
 *                                文件引用 
 ***************************************************************************/ 
#import <Foundation/Foundation.h>

/*************************************************************************** 
 *                                 类引用 
 ***************************************************************************/ 

#import <Reachability/Reachability.h>

#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

/*************************************************************************** 
 *                                 宏定义 
 ***************************************************************************/ 
#define MENU_FILE_NAME                @"menuFileName.plist"



/*************************************************************************** 
 *                                 常量 
 ***************************************************************************/ 


/*************************************************************************** 
 *                                类型定义 
 ***************************************************************************/ 


/*************************************************************************** 
 *                                 类定义
 ***************************************************************************/

@interface LocalCfg : NSObject 
{
    //FileOperation *fileOp;
    NSMutableDictionary *m_pCfgData;
}
@property(strong, nonatomic) NSString *physicsAddress;

/********************* Http相关接口 *************************/

/* 获取服务器用户相关地址 */
- (NSString *)getHttpServerUserAddr;

/* 获取服务器太享贷相关地址 */
- (NSString *)getHttpServerBookAddr;

/* 获取服务器太好创相关地址 */
- (NSString *)getHttpThcServerAddr;

/* 获取服务器电销客户经理相关地址 */
- (NSString *)getHttpKhjlServerAddr;

/* 获取下载Web界面的URL */
- (NSString *)getWebDownURL;

/* 获取系统环境类型 */
- (NSString *)getServerType;

/* 获取APP 更新服务器地址 */
- (NSString *)getHttpDownLoadURL;

- (NSString *)getHttpServerAddr;

- (NSString *)getHttpImageServerAddr;

- (NSString *)getEpgImageServerAddr;

- (NSString *)getHttpServerAddrFixString;

- (NSString *)getHttpUserAgent;

- (NSString *)getHttpExpiredTime;

- (NSStringEncoding)getHttpDataCodeFormatType;

- (NSString *)getVersionCode;

- (void)setVersionCode:(NSString *)strVersion;

/* 获取平台类型 */
- (NSString *)getPlatType;

- (NSString *)getLocalHttpRequest;

- (NSString *)getProjectType;

- (NSString *)getUserToken;

/* 微信 AppId */
- (NSString *)getWeixinAppId;

/* 微信 AppSecret */
- (NSString *)getWexinAppSecret;

/* QQ AppId */
- (NSString *)getQQAppId;

/* 获取UUID */
- (NSString *)getUUID;

/* 获取是否加密 */
- (BOOL)getEncrypt;

/* 获取密钥 */
- (NSString *)getEncryptKey;

/* 获取设定缓存大小 */
- (NSString *)getCacheSize;

/* 获取手势密码权限 */
- (BOOL)getGesturePassword;

/* 设置手势密码 */
- (void)setGesturePasswordInKeyChain:(NSString *)keychain;

/* 获取手势密码 */
- (NSString *)getGesturePasswordInKeyChain;

/* 配置服务器 图片 */
- (void)setImageHeadUrl:(NSString *)imageHeadUrl;

- (NSString *)getImageHeadUrl;

/* 配置服务器 报表 */
- (void)setReportServer:(NSString *)reportServer;

- (NSString *)getReportServer;

/* 设置报表维度 */
- (void)setBaseReportDimensions:(NSArray *)baseReportDimensions;

- (NSArray *)getBaseReportDimensions;

/* 获取网络类型 */
- (NSString *)getNetworkType;

/* 获取网络运营商 */
- (NSString *)getNetworkName;

/* 获取IP地址 */
- (NSString *)getIPAddress;

/* 获取mac地址 */
- (NSString *)getMacAddress;

/* 获取地址 */
- (void)setAddress:(NSString *)address;
- (NSString *)getAddress;

/* 法人客户地址 */
- (NSString *)getHttpLegalServerAddr;

/* 百度地图BaiduMapAppKey */
- (NSString *)getBaiduMapAppKey;

/* 获取推送AppCode */
- (NSString *)getMPPPushServerAppCode;

/* 获取推送PushServerIP */
- (NSString *)getMPPPushServerPushServerIP;

/* 获取行为分析URL */
- (NSString *)getAppAnalyticsURL;

/* 获取行为分析云控制地址 */
- (NSString *)getCloudControlURL;

/* 获取百度轨迹识别授权地址 */
- (NSString *)getBaiDuCaAppKey;

/* 获取服务器时间戳 */
- (NSString *)getHttpServerTimeStamp;

@end

