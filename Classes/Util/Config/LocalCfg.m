/*********************************************************************
 * 版权所有 LK
 *
 * 文件名称： 
 * 文件标识：
 * 内容摘要： 配置文件，把一些基本信息存放到文件中
 * 其它说明：
 * 当前版本：
 * 作    者： 林科
 * 完成日期： 
 **********************************************************************/


/*************************************************************************** 
 *                                文件引用 
 ***************************************************************************/ 
#import "LocalCfg.h"

#import "UIDevice+IdentifierAddition.h"
/*************************************************************************** 
 *                                 宏定义 
 ***************************************************************************/ 


/*************************************************************************** 
 *                                 常量 
 ***************************************************************************/ 


/*************************************************************************** 
 *                                类型定义 
 ***************************************************************************/ 


/*************************************************************************** 
 *                                全局变量 
 ***************************************************************************/ 


/*************************************************************************** 
 *                                 原型 
 ***************************************************************************/ 
@interface LocalCfg(private)

/* 获取固定配置文件路径 */
- (NSString *)getBundleCfgFilePath;

/* 解析文件 */
- (void)parseFile;

@end

/*************************************************************************** 
 *                                类特性
 ***************************************************************************/ 
@implementation LocalCfg


/*************************************************************************** 
 *                                类的实现 
 ***************************************************************************/ 

/***********************************************************************
 * 方法名称： init
 * 功能描述： 初始化
 * 输入参数： 
 * 输出参数： 
 * 返 回 值： 
 * 其它说明： 
 ***********************************************************************/
- (id)init
{
    if (self = [super init])
    {
        /* 解析文件 */
        [self parseFile];
    }
    
    return self;
}

- (NSString *)getServerType
{
    NSString *serverType = [NSString stringWithFormat:@"%@",[m_pCfgData objectForKey:@"CurrentServerType"]];
    STRING_NIL_TO_NONE(serverType)
    
    return serverType;
}


/***********************************************************************
 * 方法名称： getHttpServerUserAddr
 * 功能描述： 获取服务器用户相关地址
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (NSString *)getHttpServerUserAddr
{
    NSString *serverType = [NSString stringWithFormat:@"%@",[m_pCfgData objectForKey:@"CurrentServerType"]];
    STRING_NIL_TO_NONE(serverType)
    
    return [[m_pCfgData objectForKey:@"HttpServerUserAddr"] objectForKey:serverType];
}

/***********************************************************************
 * 方法名称： getHttpServerBookAddr
 * 功能描述： 获取服务器书城相关地址
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (NSString *)getHttpServerBookAddr
{
    NSString *serverType = [NSString stringWithFormat:@"%@",[m_pCfgData objectForKey:@"CurrentServerType"]];
    STRING_NIL_TO_NONE(serverType)
    
    return [[m_pCfgData objectForKey:@"HttpServerBookAddr"] objectForKey:serverType];
}

/***********************************************************************
 * 方法名称： getHttpThcServerAddr
 * 功能描述： 获取服务器书城相关地址
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (NSString *)getHttpThcServerAddr
{
    NSString *serverType = [NSString stringWithFormat:@"%@",[m_pCfgData objectForKey:@"CurrentServerType"]];
    STRING_NIL_TO_NONE(serverType)
    
    return [[m_pCfgData objectForKey:@"HttpThcServerAddr"] objectForKey:serverType];
}

/***********************************************************************
 * 方法名称： getHttpHKhjlServerAddr
 * 功能描述： 获取服务器书城相关地址
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (NSString *)getHttpKhjlServerAddr
{
    NSString *serverType = [NSString stringWithFormat:@"%@",[m_pCfgData objectForKey:@"CurrentServerType"]];
    STRING_NIL_TO_NONE(serverType)
    
    return [[m_pCfgData objectForKey:@"HttpKhjlServerAddr"] objectForKey:serverType];
}


/***********************************************************************
 * 方法名称： getWebDownURL
 * 功能描述： 获取下载Web界面的URL
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (NSString *)getWebDownURL
{
    return [m_pCfgData objectForKey:@"WebDownURL"];
}

/***********************************************************************
 * 方法名称： getHttpServerAddr
 * 功能描述： 获取服务器地址 
 * 输出参数： 
 * 返 回 值：
 * 其它说明： 
 ***********************************************************************/
- (NSString *)getHttpServerAddr
{
    NSString *serverType = [NSString stringWithFormat:@"%@",[m_pCfgData objectForKey:@"CurrentServerType"]];
    STRING_NIL_TO_NONE(serverType)
    
    return [[m_pCfgData objectForKey:@"HttpServerAddr"] objectForKey:serverType];
}

/***********************************************************************
 * 方法名称： getHttpServerTimeStamp
 * 功能描述： 获取服务器时间戳
 * 输出参数：
 * 返 回 值：
 * 其它说明：刷新前端缓存
 ***********************************************************************/
- (NSString *)getHttpServerTimeStamp
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    
    return [formatter stringFromDate:[NSDate date]];
}



/***********************************************************************
 * 方法名称： getHttpDownLoadURL
 * 功能描述： 获取APP更新服务器地址
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
/* 获取APP 更新服务器地址 */
- (NSString *)getHttpDownLoadURL
{
    return [m_pCfgData objectForKey:@"HttpDownLoadURL"];
}

/***********************************************************************
 * 方法名称： getEpgImageServerAddr
 * 功能描述： 获取图片服务器地址 
 * 输出参数： 
 * 返 回 值：
 * 其它说明： 
 ***********************************************************************/
- (NSString *)getEpgImageServerAddr
{
    return [m_pCfgData objectForKey:@"EpgImageServerAddr"];
}


/***********************************************************************
 * 方法名称： getHttpServerAddrFixString
 * 功能描述： 获取服务器地址中固定字符串 
 * 输出参数： 
 * 返 回 值：
 * 其它说明： 
 ***********************************************************************/
- (NSString *)getHttpServerAddrFixString
{
    return [m_pCfgData objectForKey:@"EpgServerAddrFixString"];
}


/***********************************************************************
 * 方法名称： getHttpUserAgent
 * 功能描述： 用户代理 
 * 输出参数： 
 * 返 回 值：
 * 其它说明： 
 ***********************************************************************/
- (NSString *)getHttpUserAgent
{
    return [m_pCfgData objectForKey:@"HttpUserAgent"];
}


/***********************************************************************
 * 方法名称： getHttpExpiredTime
 * 功能描述： Http通讯超时时间
 * 输入参数： 
 * 输出参数： 
 * 返 回 值：
 * 其它说明： 
 ***********************************************************************/
- (NSString *)getHttpExpiredTime
{
    return [m_pCfgData objectForKey:@"HttpExpiredTime"];
}

/***********************************************************************
 * 方法名称： getHttpLegalServerAddr
 * 功能描述： 法人客户地址
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (NSString *)getHttpLegalServerAddr
{
    return [m_pCfgData objectForKey:@"HttpLegalServerAddr"];
}




/***********************************************************************
 * 方法名称： getHttpDataCodeFormatType
 * 功能描述： 获取Http数据编码格式类型
 * 输入参数： 
 * 输出参数： 
 * 返 回 值： 
 * 其它说明： 编码格式有GBK, UTF-8
 ***********************************************************************/
- (NSStringEncoding)getHttpDataCodeFormatType
{
    NSString *strType = [m_pCfgData objectForKey:@"HttpDataCodeFormatType"];
    
    if ([strType compare:@"GBK" options:NSCaseInsensitiveSearch] == NSOrderedSame)
    {
        return CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    }
    else
    {
        return NSUTF8StringEncoding;
    }
}


/***********************************************************************
 * 方法名称： getVersionCode
 * 功能描述： 获取版本号
 * 输入参数： 
 * 输出参数： 
 * 返 回 值： 
 * 其它说明： 
 ***********************************************************************/
- (NSString *)getVersionCode
{
    NSString *serverType = [NSString stringWithFormat:@"%@",[m_pCfgData objectForKey:@"CurrentServerType"]];
    STRING_NIL_TO_NONE(serverType)
    
    return [[m_pCfgData objectForKey:@"VersionCode"] objectForKey:serverType];
}


/***********************************************************************
 * 方法名称： setVersionCode
 * 功能描述： 设定版本号
 * 输入参数： 
 * 输出参数： 
 * 返 回 值： 
 * 其它说明： 
 ***********************************************************************/
- (void)setVersionCode:(NSString *)strVersion
{
	[m_pCfgData setObject:strVersion forKey:@"VersionCode"];
}


/***********************************************************************
 * 方法名称： getPlatType
 * 功能描述： 获取平台类型
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (NSString *)getPlatType
{
    return ([m_pCfgData objectForKey:@"PlatType"]);
}

/***********************************************************************
 * 方法名称： getProjectType
 * 功能描述： 获取项目类型
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (NSString *)getProjectType
{
    return ([m_pCfgData objectForKey:@"ProjectType"]);
}

/***********************************************************************
 * 方法名称： getLocalHttpRequest
 * 功能描述： 获取是否使用本地模拟数据 1：使用  0：不使用
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明： 该接口服务于本地模拟服务器，因为本地模拟服务器只支持标准格式
 ***********************************************************************/
- (NSString *)getLocalHttpRequest
{
    return ([m_pCfgData objectForKey:@"LocalHttpRequest"]);
}

/***********************************************************************
 * 方法名称： getProjectType
 * 功能描述： 获取个信贷中台地址
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (NSString *)getHttpImageServerAddr
{
    return ([m_pCfgData objectForKey:@"HttpImageServerAddr"]);
}

/***********************************************************************
 * 方法名称： getWeixinAppId
 * 功能描述： 获取 微信 AppId
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明： 该接口服务于本地模拟服务器，因为本地模拟服务器只支持标准格式
 ***********************************************************************/
- (NSString *)getWeixinAppId
{
    NSString *serverType = [NSString stringWithFormat:@"%@",[m_pCfgData objectForKey:@"CurrentServerType"]];
    STRING_NIL_TO_NONE(serverType)
    
    return [[m_pCfgData objectForKey:@"WeixinAppId"] objectForKey:serverType];
}

/* 微信 AppSecret */
- (NSString *)getWexinAppSecret
{
    NSString *serverType = [NSString stringWithFormat:@"%@",[m_pCfgData objectForKey:@"CurrentServerType"]];
    STRING_NIL_TO_NONE(serverType)
    
    return [[m_pCfgData objectForKey:@"WexinAppSecret"] objectForKey:serverType];
}

/***********************************************************************
 * 方法名称： getQQAppId
 * 功能描述： 获取QQ  AppId
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明： 该接口服务于本地模拟服务器，因为本地模拟服务器只支持标准格式
 ***********************************************************************/
- (NSString *)getQQAppId
{
    return ([m_pCfgData objectForKey:@"QQAppId"]);
}

/***********************************************************************
 * 方法名称： getGesturePassword
 * 功能描述： 获取是否支持手势密码解锁
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (BOOL)getGesturePassword
{
    return ([[m_pCfgData objectForKey:@"GesturePassword"] intValue]);
}

/***********************************************************************
 * 方法名称： getUUID
 * 功能描述： 获取UUID
 * 输入参数：
 * 输出参数：
 * 返 回 值： UUID
 * 其它说明：
 ***********************************************************************/
- (NSString *)getUUID
{
    return [self getInfoFromKeychain:@"com.game.userinfo" Account:@"uuid"];
}

/***********************************************************************
 * 方法名称： getUserToken
 * 功能描述： 获取userToken
 * 输入参数：
 * 输出参数：
 * 返 回 值： userToken
 * 其它说明：getHttpServerAddr
 ***********************************************************************/
- (NSString *)getUserToken
{
    return ([m_pCfgData objectForKey:@"userToken"]);
}

/***********************************************************************
 * 方法名称：setGesturePasswordInKeyChain
 * 功能描述：写入手势密码信息
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (void)setGesturePasswordInKeyChain:(NSString *)keychain
{
//    [SSKeychain setPassword:keychain forService:@"com.game.gesturePassword" account:@"gesturePassword"];
}

/***********************************************************************
 * 方法名称：getGesturePasswordInKeyChain
 * 功能描述：获取手势密码信息
 * 输入参数：
 * 输出参数：
 * 返 回 值：手势密码
 * 其它说明：
 ***********************************************************************/
- (NSString *)getGesturePasswordInKeyChain
{
//    NSString *keychain = [NSString stringWithFormat:@"%@",[SSKeychain passwordForService:@"com.game.gesturePassword" account:@"gesturePassword"]];
//    STRING_NIL_TO_NONE(keychain);
//    
//    return keychain;
}

/***********************************************************************
 * 方法名称：setInfoFromKeychain
 * 功能描述：写入钥匙串信息
 * 输入参数：
 * 输出参数：
 * 返 回 值：钥匙串信息
 * 其它说明：
 ***********************************************************************/
- (void)setInfoFromKeychain:(NSString *)service Account:(NSString *)account WithKeyChain:(NSString *)keychain
{
//    NSString *retrieveuuid = [NSString stringWithFormat:@"%@",[SSKeychain passwordForService:service account:account]];
//    STRING_NIL_TO_NONE(retrieveuuid);
//    
//    if ( retrieveuuid == nil || [retrieveuuid isEqualToString:@""])
//    {
//        CFUUIDRef uuid = CFUUIDCreate(NULL);
//        assert(uuid != NULL);
//        CFStringRef uuidStr = CFUUIDCreateString(NULL, uuid);
//        retrieveuuid = [NSString stringWithFormat:@"%@", uuidStr];
//
//        [SSKeychain setPassword:keychain forService:service account:account];
//    }
//    else
//    {
//        NSLog(@"keychain exist!");
//    }
}

/***********************************************************************
 * 方法名称：getInfoFromKeychain
 * 功能描述：获取钥匙串信息
 * 输入参数：
 * 输出参数：
 * 返 回 值：钥匙串信息
 * 其它说明：
 ***********************************************************************/
- (NSString *)getInfoFromKeychain:(NSString *)service Account:(NSString *)account
{
//    NSString *info = @"";
//	NSString *retrieveuuid = [NSString stringWithFormat:@"%@",[SSKeychain passwordForService:service account:account]];
//    STRING_NIL_TO_NONE(retrieveuuid);
//    
//	if ( retrieveuuid == nil || [retrieveuuid isEqualToString:@""])
//	{
//		CFUUIDRef uuid = CFUUIDCreate(NULL);
//		assert(uuid != NULL);
//		CFStringRef uuidStr = CFUUIDCreateString(NULL, uuid);
//		retrieveuuid = [NSString stringWithFormat:@"%@", uuidStr];
//        /**
//         KeyChainItemWrapper会缓存读入数据，但不会缓存写出的数据。
//         向钥匙串中写数据的成本很高，所以我们不会频繁写入。
//         钥匙串不适合用来存储经常改变的敏感数据。
//         */
//		[SSKeychain setPassword:retrieveuuid forService:service account:account];
//	}
//    
//	if (retrieveuuid != nil)
//	{
//		info = [NSString stringWithFormat:@"%s",[retrieveuuid UTF8String]];
//	}
//	else
//	{
//		info = @"null";
//	}
//    
//	return info;
}

/***********************************************************************
 * 方法名称： getEncrypt
 * 功能描述： 获取 是否加密
 * 输入参数：
 * 输出参数：
 * 返 回 值： 是否加密
 * 其它说明：
 ***********************************************************************/
- (BOOL)getEncrypt
{
    return ([[m_pCfgData objectForKey:@"Encrypt"] intValue]); 
}


/***********************************************************************
 * 方法名称： getEncryptKey
 * 功能描述： 获取加密密钥
 * 输入参数：
 * 输出参数：
 * 返 回 值： 加密密钥
 * 其它说明：
 ***********************************************************************/
- (NSString *)getEncryptKey
{
    return ([m_pCfgData objectForKey:@"EncryptKey"]);
}

/***********************************************************************
 * 方法名称： getCacheSize
 * 功能描述： 获取缓存大小
 * 输入参数：
 * 输出参数：
 * 返 回 值： 是否缓存
 * 其它说明：
 ***********************************************************************/
- (NSString *)getCacheSize
{
    return ([m_pCfgData objectForKey:@"CacheSize"]);
}

/***********************************************************************
 * 方法名称： setImageHeadUrl
 * 功能描述： 设置图片服务器
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (void)setImageHeadUrl:(NSString *)imageHeadUrl
{
    [m_pCfgData setObject:imageHeadUrl forKey:@"imageHeadUrl"];
}

- (NSString *)getImageHeadUrl
{
    return ([m_pCfgData objectForKey:@"imageHeadUrl"]);
}

/***********************************************************************
 * 方法名称： setReportServer
 * 功能描述： 设置报表服务器
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (void)setReportServer:(NSString *)reportServer
{
    [m_pCfgData setObject:reportServer forKey:@"reportServer"];
}

- (NSString *)getReportServer
{
    return ([m_pCfgData objectForKey:@"reportServer"]);
}

/***********************************************************************
 * 方法名称： setBaseReportDimensions
 * 功能描述： 设置报表维度
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (void)setBaseReportDimensions:(NSArray *)baseReportDimensions
{
    [m_pCfgData setObject:baseReportDimensions forKey:@"baseReportDimensions"];
}

- (NSArray *)getBaseReportDimensions
{
    return ([m_pCfgData objectForKey:@"baseReportDimensions"]);
}


- (NSString *)getBaiduMapAppKey
{
//    dev T2B1eODSyV3AIqWG6Rkw8nKlKUW1P1Uz
//    uat sOoaotU210dbMX07HotoSkQwRtybPIAw
//    sit byYxkuAzV9GsXRO7G6Dxcgg9joXNoMrL
//    prd LsuQ8ujESAecyfaTkPT2vzpa1PG5GTCH
    
    NSString *bundleIdentifier = [NSString stringWithFormat:@"%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]];
    STRING_NIL_TO_NONE(bundleIdentifier);
    if ([bundleIdentifier isEqualToString:@"com.cpic.cxydcrmdev"])
    {
        return @"T2B1eODSyV3AIqWG6Rkw8nKlKUW1P1Uz";
    }
    else if ([bundleIdentifier isEqualToString:@"com.cpic.cxydcrmuat"])
    {
        return @"sOoaotU210dbMX07HotoSkQwRtybPIAw";
    }
    else if ([bundleIdentifier isEqualToString:@"com.cpic.cxydcrmsit"])
    {
        return @"byYxkuAzV9GsXRO7G6Dxcgg9joXNoMrL";
    }
    else
    {
        return @"LsuQ8ujESAecyfaTkPT2vzpa1PG5GTCH";
    }
}

/* 获取推送AppCode */
- (NSString *)getMPPPushServerAppCode
{
    NSString *serverType = [NSString stringWithFormat:@"%@",[m_pCfgData objectForKey:@"CurrentServerType"]];
    STRING_NIL_TO_NONE(serverType)
    
    return [[m_pCfgData objectForKey:@"MPPPushServerAppCode"] objectForKey:serverType];
}

/* 获取推送PushServerIP */
- (NSString *)getMPPPushServerPushServerIP
{
    NSString *serverType = [NSString stringWithFormat:@"%@",[m_pCfgData objectForKey:@"CurrentServerType"]];
    STRING_NIL_TO_NONE(serverType)
    
    return [[m_pCfgData objectForKey:@"MPPPushServerPushServerIP"] objectForKey:serverType];
}

/* 获取行为分析URL */
- (NSString *)getAppAnalyticsURL
{
    NSString *serverType = [NSString stringWithFormat:@"%@",[m_pCfgData objectForKey:@"CurrentServerType"]];
    STRING_NIL_TO_NONE(serverType)
    
    return [[m_pCfgData objectForKey:@"AppAnalyticsURL"] objectForKey:serverType];
}

/* 获取行为分析云控制地址 */
- (NSString *)getCloudControlURL
{
    NSString *serverType = [NSString stringWithFormat:@"%@",[m_pCfgData objectForKey:@"CurrentServerType"]];
    STRING_NIL_TO_NONE(serverType)
    
    return [[m_pCfgData objectForKey:@"CloudControlURL"] objectForKey:serverType];
}

/* 获取百度轨迹识别授权地址 */
- (NSString *)getBaiDuCaAppKey
{
    NSString *serverType = [NSString stringWithFormat:@"%@",[m_pCfgData objectForKey:@"CurrentServerType"]];
    STRING_NIL_TO_NONE(serverType)
    
    return [[m_pCfgData objectForKey:@"BaiDuCaAppKey"] objectForKey:serverType];
}

/* 获取网络类型 */
- (NSString *)getNetworkType
{
    NSString *netconnType = @"";
    
    Reachability *reach = [Reachability reachabilityWithHostname:@"www.apple.com"];
    
    switch (reach.currentReachabilityStatus)
    {
        case NotReachable:// 没有网络
        {
            netconnType = @"no network";
        }
            break;
            
        case ReachableViaWiFi:// Wifi
        {
            netconnType = @"Wifi";
        }
            break;
            
        case ReachableViaWWAN:// 手机自带网络
        {
            // 获取手机网络类型
            CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
            
            NSString *currentStatus = info.currentRadioAccessTechnology;
            
            if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyGPRS"]) {
                
                netconnType = @"GPRS";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyEdge"]) {
                
                netconnType = @"2.75G EDGE";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyWCDMA"]){
                
                netconnType = @"3G";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyHSDPA"]){
                
                netconnType = @"3.5G HSDPA";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyHSUPA"]){
                
                netconnType = @"3.5G HSUPA";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyCDMA1x"]){
                
                netconnType = @"2G";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyCDMAEVDORev0"]){
                
                netconnType = @"3G";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyCDMAEVDORevA"]){
                
                netconnType = @"3G";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyCDMAEVDORevB"]){
                
                netconnType = @"3G";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyeHRPD"]){
                
                netconnType = @"HRPD";
            }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyLTE"]){
                
                netconnType = @"4G";
            }
        }
            break;
            
        default:
            break;
    }
    
    return netconnType;
}

/* 获取网络运营商 */
- (NSString *)getNetworkName
{
    CTTelephonyNetworkInfo *telephonyInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [telephonyInfo subscriberCellularProvider];
    
    NSString *currentCountry = [NSString stringWithFormat:@"%@",[carrier carrierName]];
    STRING_NIL_TO_NONE(currentCountry);
    
    return currentCountry;
}

/* 获取地址 */
- (void)setAddress:(NSString *)address
{
    self.physicsAddress = [NSString stringWithFormat:@"%@",address];
}
- (NSString *)getAddress
{
    return self.physicsAddress;
}

/* 获取IP地址 */
- (NSString *)getIPAddress
{
    NSString *address = @"error";
//    struct ifaddrs *interfaces = NULL;
//    struct ifaddrs *temp_addr = NULL;
//    int success = 0;
//    // retrieve the current interfaces - returns 0 on success
//    success = getifaddrs(&interfaces);
//    if (success == 0) {
//        // Loop through linked list of interfaces
//        temp_addr = interfaces;
//        while(temp_addr != NULL) {
//            if(temp_addr->ifa_addr->sa_family == AF_INET) {
//                // Check if interface is en0 which is the wifi connection on the iPhone
//                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
//                    // Get NSString from C String
//                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
//                }
//            }
//            temp_addr = temp_addr->ifa_next;
//        }
//    }
//    // Free memory
//    freeifaddrs(interfaces);
    return address;
}

/* 获取mac地址 */
- (NSString *)getMacAddress
{
    NSString *macAddress = [NSString stringWithFormat:@"%@",[[UIDevice currentDevice] uniqueGlobalDeviceIdentifier]];
    STRING_NIL_TO_NONE(macAddress);
    
    return macAddress;
}

#pragma -
#pragma mark - 文件读写
/* 获取路径 */
- (NSString *)dataUDNFilePath
{
	NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
	NSString *documentDirectory=[paths objectAtIndex:0];
	return [documentDirectory stringByAppendingPathComponent:MENU_FILE_NAME];
}


#pragma mark -
#pragma mark Private Method

/***********************************************************************
 * 方法名称： parseFile
 * 功能描述： 解析文件的内容
 * 输入参数： 
 * 输出参数： 
 * 返 回 值：
 * 其它说明： 
 ***********************************************************************/
- (void)parseFile
{
	if ([[NSFileManager defaultManager] fileExistsAtPath:[self getBundleCfgFilePath]])
	{
	    NSString *strPathFile = [self getBundleCfgFilePath];
		m_pCfgData = [[NSMutableDictionary alloc] initWithContentsOfFile:strPathFile];
	}
	else
	{
		NSLog(@"error:LocalCfg-->parseFile fail");
	}
}

/***********************************************************************
 * 方法名称： getBundleCfgFilePath
 * 功能描述： 获取固定配置文件路径
 * 输入参数： 
 * 输出参数： 
 * 返 回 值： (NSString *)类型，文件路径
 * 其它说明： 
 ***********************************************************************/
- (NSString *)getBundleCfgFilePath
{
    NSString *cfgFileName = @"LocalCfg";
    
    NSString *bundleIdentifier = [NSString stringWithFormat:@"%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]];
    STRING_NIL_TO_NONE(bundleIdentifier);
    
	NSString *strPath = [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] pathForResource:cfgFileName ofType:@"plist"]];
    STRING_NIL_TO_NONE(strPath);
    
    return strPath;
}


@end

