/*********************************************************************
 * 版权所有 LK
 * 
 * 文件名称： 
 * 文件标识： 
 * 内容摘要： 网络相关通用接口，网络数据统一从该接口获取
 * 其它说明： 
 * 当前版本： 
 * 作   者： 林科
 * 完成日期： 
 **********************************************************************/


/*************************************************************************** 
 *                                文件引用 
 ***************************************************************************/ 
#import <Foundation/Foundation.h>


/*************************************************************************** 
 *                                 类引用 
 ***************************************************************************/ 

#import "FMDBSqlite.h"

/*************************************************************************** 
 *                                 宏定义 
 ***************************************************************************/ 


/*************************************************************************** 
 *                                 常量 
 ***************************************************************************/ 


/*************************************************************************** 
 *                                类型定义 
 ***************************************************************************/ 


/*************************************************************************** 
 *                                 类定义
 ***************************************************************************/
@protocol NetCommDelegate;

@interface NetComm : NSObject 
{
    id <NetCommDelegate> netDelegate;
    
}
#pragma -
#pragma mark - *********************** 中韩OA接口 ***********************

#pragma mark -
#pragma mark -  应用程序接口
- (void)getAppInfoRequestSuccess:(void (^)(id JSON))success
    failure:(void (^)(NSError *error))failure;

- (void)ensureUploadImageList:(NSDictionary *)requestobjDic
                      success:(void (^)(id JSON))success
                      failure:(void (^)(NSError *error))failure;

- (void)getCrmMenuFromCache:(NSDictionary *)requestobjDic
                      success:(void (^)(id JSON))success
                      failure:(void (^)(NSError *error))failure;

- (void)getTaskAndActivityCount:(NSDictionary *)requestobjDic
                      success:(void (^)(id JSON))success
                      failure:(void (^)(NSError *error))failure;

- (void)creditInquire:(NSDictionary *)requestobjDic
              success:(void (^)(id JSON))success
              failure:(void (^)(NSError *error))failure;

- (void)startFaceRecognitionAction:(NSDictionary *)requestobjDic
                           success:(void (^)(id JSON))success
                           failure:(void (^)(NSError *error))failure;

- (void)logRecordAction:(NSDictionary *)requestobjDic
                success:(void (^)(id JSON))success
                failure:(void (^)(NSError *error))failure;

- (void)getWeChatQrCode:(NSString *)strUrl withParmars:(NSDictionary *)parmars
                success:(void (^)(id JSON))success
                failure:(void (^)(NSError *error))failure;

@end







