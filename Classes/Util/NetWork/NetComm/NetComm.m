/*********************************************************************
 * 版权所有 LK
 * 
 * 文件名称： 
 * 文件标识： 
 * 内容摘要： 网络相关通用接口，网络数据统一从该接口获取
 * 其它说明： 
 * 当前版本： 
 * 作   者： 林科
 * 完成日期： 
 **********************************************************************/


/*************************************************************************** 
 *                                文件引用 
 ***************************************************************************/ 
#import "NetComm.h"

/*************************************************************************** 
 *                                 宏定义 
 ***************************************************************************/ 


/*************************************************************************** 
 *                                 常量 
 ***************************************************************************/ 


/*************************************************************************** 
 *                                类型定义 
 ***************************************************************************/ 


/*************************************************************************** 
 *                                全局变量 
 ***************************************************************************/ 


/*************************************************************************** 
 *                                 原型 
 ***************************************************************************/ 
@interface NetComm ()
{
    
}

@end



/*************************************************************************** 
 *                                类特性
 ***************************************************************************/

@implementation NetComm


/*************************************************************************** 
 *                                类的实现 
 ***************************************************************************/

/***********************************************************************
 * 方法名称： init
 * 功能描述： 对象初始化
 * 输入参数：        
 * 输出参数： 
 * 返 回 值： 
 * 其它说明： 必须预先初始化ColumnConfig对象
 ***********************************************************************/
- (id)init
{
	if (self = [super init])
    {
        
    }
    return self;
}


#pragma mark -
#pragma mark Dealloc
/***********************************************************************
 * 方法名称： dealloc 
 * 功能描述： 当对象引用计数为0时调用该方法 释放相应对象指针
 * 输入参数： 
 * 输出参数： 
 * 返 回 值： 空 
 * 其它说明： 
 ***********************************************************************/
- (void)dealloc 
{    
    
}

#pragma -
#pragma mark - *********************** 中韩OA接口 ***********************

#pragma mark -
#pragma mark -  应用程序接口
- (void)getAppInfoRequestSuccess:(void (^)(id JSON))success
                         failure:(void (^)(NSError *error))failure
{
    NSMutableString *strUrl = [NSMutableString stringWithFormat:@"%@/",
							   [[[ClassFactory getInstance] getLocalCfg] getHttpServerAddr]];
    [strUrl appendFormat:@"crm-http/crm/getCrmVersionInfo.do"];
    
    NSString *platType = [[[ClassFactory getInstance] getLocalCfg] getPlatType];
    STRING_NIL_TO_NONE(platType);
    NSString *projectType = [[[ClassFactory getInstance] getLocalCfg] getProjectType];
    STRING_NIL_TO_NONE(projectType);
    NSString *version = [[[ClassFactory getInstance] getLocalCfg] getVersionCode];
    STRING_NIL_TO_NONE(version)
//    NSString *userToken = [[[ClassFactory getInstance] getLocalCfg] getUserToken];
//    STRING_NIL_TO_NONE(userToken);

    NSDictionary *requestObjectDic = [NSDictionary dictionaryWithObjectsAndKeys:@"111",@"1111",@"ios",@"plateType",nil];
    
    NSDictionary *parmars = [NSDictionary dictionaryWithObjectsAndKeys:
                             projectType,@"projectType",
                             platType, @"platType",
                             version,@"version",
                             @"M",@"appType",
                             @"phone",@"clientType",
                             requestObjectDic,@"requestObject",
                             nil];
    
    [self postRequestWithUrl:strUrl path:strUrl parameters:parmars success:^(id JSON) {
        
        if (success) {
            success(JSON);
        }
    } failure:^(NSError *error, id JSON) {
        
        if (failure) {
            failure(error);
        }
    }];
}

//确认提交影像接口
- (void)ensureUploadImageList:(NSDictionary *)requestobjDic
                      success:(void (^)(id JSON))success
                      failure:(void (^)(NSError *error))failure
{
    NSString *strUrl = [NSString stringWithFormat:@"%@",[requestobjDic objectForKey:@"url"]];
    STRING_NIL_TO_NONE(strUrl);
    
    NSString *platType = [[[ClassFactory getInstance] getLocalCfg] getPlatType];
    STRING_NIL_TO_NONE(platType);
    NSString *projectType = [[[ClassFactory getInstance] getLocalCfg] getProjectType];
    STRING_NIL_TO_NONE(projectType);
    NSString *version = [[[ClassFactory getInstance] getLocalCfg] getVersionCode];
    STRING_NIL_TO_NONE(version)
    NSString *userToken = [[[ClassFactory getInstance] getLocalCfg] getUserToken];
    STRING_NIL_TO_NONE(userToken);

    NSArray *dataList = [NSArray arrayWithArray:[requestobjDic objectForKey:@"dataList"]];
    
    NSDictionary *userInfoDic = [NSDictionary dictionaryWithDictionary:[[[ClassFactory getInstance] getNetComm] getObject:USER_PATH_KAY]];
    NSDictionary *personInfo = [NSDictionary dictionaryWithDictionary:[userInfoDic objectForKey:@"personInfo"]];
    
    //员工代码
    NSString *agentCode = [NSString stringWithFormat:@"%@",[requestobjDic objectForKey:@"agentCode"]];
    STRING_NIL_TO_NONE(agentCode);
    if(STRING_ISNIL(agentCode))
    {
        agentCode = [NSString stringWithFormat:@"%@",[personInfo objectForKey:@"psId"]];
        STRING_NIL_TO_NONE(agentCode);
    }
        
    //员工名称
    NSString *agentName = [NSString stringWithFormat:@"%@",[requestobjDic objectForKey:@"agentName"]];
    STRING_NIL_TO_NONE(agentName);
    if(STRING_ISNIL(agentName))
    {
        agentName = [NSString stringWithFormat:@"%@",[personInfo objectForKey:@"psName"]];
        STRING_NIL_TO_NONE(agentName);
    }
    
    //分公司代码
    NSString *unitCode = [NSString stringWithFormat:@"%@",[requestobjDic objectForKey:@"unitCode"]];
    STRING_NIL_TO_NONE(unitCode);
    if(STRING_ISNIL(unitCode))
    {
        unitCode = [NSString stringWithFormat:@"%@",[userInfoDic objectForKey:@"unitCode"]];
        STRING_NIL_TO_NONE(unitCode);
    }
    
    //分公司名称
    NSString *unitName = [NSString stringWithFormat:@"%@",[requestobjDic objectForKey:@"unitName"]];
    STRING_NIL_TO_NONE(unitName);
    if(STRING_ISNIL(unitName))
    {
        unitName = [NSString stringWithFormat:@"%@",[userInfoDic objectForKey:@"unitName"]];
        STRING_NIL_TO_NONE(unitName);
    }
    
    //是否上传pdf
    NSString *uploadPdf = [NSString stringWithFormat:@"%@",[requestobjDic objectForKey:@"uploadPdf"]];
    STRING_NIL_TO_NONE(uploadPdf);
    
    //pdf 名称
    NSString *pdfName = [NSString stringWithFormat:@"%@",[requestobjDic objectForKey:@"pdfName"]];
    STRING_NIL_TO_NONE(pdfName);
    
    //任务申请号
    NSString *requestNo = [NSString stringWithFormat:@"%@",[requestobjDic objectForKey:@"requestNo"]];
    STRING_NIL_TO_NONE(requestNo);
    
    NSDictionary *requestObjectDic = [NSDictionary dictionaryWithObjectsAndKeys:requestNo,@"requestNo",agentCode,@"agentCode",agentName,@"agentName",unitCode,@"unitCode",unitName,@"unitName",pdfName,@"pdfName",uploadPdf,@"uploadPdf",dataList,@"dataList",nil];
    
    NSDictionary *parmars = [NSDictionary dictionaryWithObjectsAndKeys:
                             projectType,@"projectType",
                             platType, @"platType",
                             version,@"version",
                             @"M",@"appType",
                             @"phone",@"clientType",
                             requestObjectDic,@"requestObject",
                             nil];
       
    [self postRequestWithUrl:strUrl path:strUrl parameters:parmars timeOut:60 success:^(id JSON) {
        
        if (success) {
            success(JSON);
        }
    } failure:^(NSError *error, id JSON) {
        
        if (failure) {
            failure(error);
        }
    }];
}

//获取CRM菜单接口
- (void)getCrmMenuFromCache:(NSDictionary *)requestobjDic
                    success:(void (^)(id JSON))success
                    failure:(void (^)(NSError *error))failure
{
    NSMutableString *strUrl = [NSMutableString stringWithFormat:@"%@/",
                               [[[ClassFactory getInstance] getLocalCfg] getHttpServerAddr]];
    [strUrl appendFormat:@"crm-http/crm/getCrmMenuFromCache.do"];
    
    NSString *platType = [[[ClassFactory getInstance] getLocalCfg] getPlatType];
    STRING_NIL_TO_NONE(platType);
    NSString *projectType = [[[ClassFactory getInstance] getLocalCfg] getProjectType];
    STRING_NIL_TO_NONE(projectType);
    NSString *version = [[[ClassFactory getInstance] getLocalCfg] getVersionCode];
    STRING_NIL_TO_NONE(version)
    NSString *userToken = [[[ClassFactory getInstance] getLocalCfg] getUserToken];
    STRING_NIL_TO_NONE(userToken);
    
    NSDictionary *userInfoDic = [NSDictionary dictionaryWithDictionary:[[[ClassFactory getInstance] getNetComm] getObject:USER_PATH_KAY]];
    NSDictionary *personInfo = [NSDictionary dictionaryWithDictionary:[userInfoDic objectForKey:@"personInfo"]];
    
    //分公司代码
    NSString *unitCode = [NSString stringWithFormat:@"%@",[userInfoDic objectForKey:@"unitCode"]];
    STRING_NIL_TO_NONE(unitCode);
    //分公司代码
    NSArray *roleCode = [NSArray arrayWithArray:[personInfo objectForKey:@"roleCode"]];

    
    NSDictionary *requestObjectDic = [NSDictionary dictionaryWithObjectsAndKeys:unitCode,@"roleOrg",roleCode,@"roleCode",nil];
    
    NSDictionary *parmars = [NSDictionary dictionaryWithObjectsAndKeys:
                             projectType,@"projectType",
                             platType, @"platType",
                             version,@"version",
                             @"M",@"appType",
                             @"phone",@"clientType",
                             requestObjectDic,@"requestObject",
                             nil];
    
    [self postRequestWithUrl:strUrl path:strUrl parameters:parmars success:^(id JSON) {
        
        if (success) {
            success(JSON);
        }
    } failure:^(NSError *error, id JSON) {
        
        if (failure) {
            failure(error);
        }
    }];
}


//获取消息数量
- (void)getTaskAndActivityCount:(NSDictionary *)requestobjDic
                        success:(void (^)(id JSON))success
                        failure:(void (^)(NSError *error))failure
{
    NSMutableString *strUrl = [NSMutableString stringWithFormat:@"%@/",
                               [[[ClassFactory getInstance] getLocalCfg] getHttpServerAddr]];
    [strUrl appendFormat:@"crm-http/crm/getTaskAndActivityCount.do"];
    
//    NSString *platType = [[[ClassFactory getInstance] getLocalCfg] getPlatType];
//    STRING_NIL_TO_NONE(platType);
//    NSString *projectType = [[[ClassFactory getInstance] getLocalCfg] getProjectType];
//    STRING_NIL_TO_NONE(projectType);
//    NSString *version = [[[ClassFactory getInstance] getLocalCfg] getVersionCode];
//    STRING_NIL_TO_NONE(version)
//    NSString *userToken = [[[ClassFactory getInstance] getLocalCfg] getUserToken];
//    STRING_NIL_TO_NONE(userToken);
    
    NSDictionary *userInfoDic = [NSDictionary dictionaryWithDictionary:[[[ClassFactory getInstance] getNetComm] getObject:USER_PATH_KAY]];
    
    NSDictionary *personInfo = [NSDictionary dictionaryWithDictionary:[userInfoDic objectForKey:@"personInfo"]];
 
    //分公司代码
    NSString *unitCode = [NSString stringWithFormat:@"%@",[userInfoDic objectForKey:@"unitCode"]];
    STRING_NIL_TO_NONE(unitCode);
    //
    NSString *agentCode = [NSString stringWithFormat:@"%@",[userInfoDic objectForKey:@"userCode"]];
    STRING_NIL_TO_NONE(agentCode);
    
    //分公司代码
    NSArray *roleCode = [NSArray arrayWithArray:[personInfo objectForKey:@"roleCode"]];
    
    NSDictionary *requestObjectDic = [NSDictionary dictionaryWithObjectsAndKeys:unitCode,@"unitCode",agentCode,@"agentCode",roleCode,@"roleCode",nil];
    
    NSDictionary *parmars = [NSDictionary dictionaryWithObjectsAndKeys:
//                             projectType,@"projectType",
//                             platType, @"platType",
//                             version,@"version",
                             @"M",@"appType",
                             @"phone",@"clientType",
                             requestObjectDic,@"requestObject",
                             nil];
    
    [self postRequestWithUrl:strUrl path:strUrl parameters:parmars success:^(id JSON) {
        
        if (success) {
            success(JSON);
        }
    } failure:^(NSError *error, id JSON) {
        
        if (failure) {
            failure(error);
        }
    }];
}

//征信查询
- (void)creditInquire:(NSDictionary *)requestobjDic
                        success:(void (^)(id JSON))success
                        failure:(void (^)(NSError *error))failure
{
    NSMutableString *strUrl = [NSMutableString stringWithFormat:@"%@/",
                               [[[ClassFactory getInstance] getLocalCfg] getHttpServerAddr]];
    [strUrl appendFormat:@"crm-http/crm/getTaskAndActivityCount.do"];
    
    NSString *platType = [[[ClassFactory getInstance] getLocalCfg] getPlatType];
    STRING_NIL_TO_NONE(platType);
    NSString *projectType = [[[ClassFactory getInstance] getLocalCfg] getProjectType];
    STRING_NIL_TO_NONE(projectType);
    NSString *version = [[[ClassFactory getInstance] getLocalCfg] getVersionCode];
    STRING_NIL_TO_NONE(version)
    NSString *userToken = [[[ClassFactory getInstance] getLocalCfg] getUserToken];
    STRING_NIL_TO_NONE(userToken);
    
    NSDictionary *userInfoDic = [NSDictionary dictionaryWithDictionary:[[[ClassFactory getInstance] getNetComm] getObject:USER_PATH_KAY]];
    
    NSDictionary *personInfo = [NSDictionary dictionaryWithDictionary:[userInfoDic objectForKey:@"personInfo"]];
    
    //分公司代码
    NSString *unitCode = [NSString stringWithFormat:@"%@",[userInfoDic objectForKey:@"unitCode"]];
    STRING_NIL_TO_NONE(unitCode);
    //
    NSString *agentCode = [NSString stringWithFormat:@"%@",[userInfoDic objectForKey:@"userCode"]];
    STRING_NIL_TO_NONE(agentCode);
    
    //分公司代码
    NSArray *roleCode = [NSArray arrayWithArray:[personInfo objectForKey:@"roleCode"]];
    
    NSDictionary *requestObjectDic = [NSDictionary dictionaryWithObjectsAndKeys:unitCode,@"unitCode",agentCode,@"agentCode",roleCode,@"roleCode",nil];
    
    NSDictionary *parmars = [NSDictionary dictionaryWithObjectsAndKeys:
                             //                             projectType,@"projectType",
                             //                             platType, @"platType",
                             //                             version,@"version",
                             @"M",@"appType",
                             @"phone",@"clientType",
                             requestObjectDic,@"requestObject",
                             nil];
    
    [self postRequestWithUrl:strUrl path:strUrl parameters:parmars success:^(id JSON) {
        
        if (success) {
            success(JSON);
        }
    } failure:^(NSError *error, id JSON) {
        
        if (failure) {
            failure(error);
        }
    }];
}

#pragma mark -
#pragma mark - 人脸识别 
- (void)startFaceRecognitionAction:(NSDictionary *)requestobjDic
                      success:(void (^)(id JSON))success
                      failure:(void (^)(NSError *error))failure
{
    NSString *strUrl = [NSString stringWithFormat:@"%@",[requestobjDic objectForKey:@"url"]];
    STRING_NIL_TO_NONE(strUrl);
    
    NSString *platType = [[[ClassFactory getInstance] getLocalCfg] getPlatType];
    STRING_NIL_TO_NONE(platType);
    NSString *projectType = [[[ClassFactory getInstance] getLocalCfg] getProjectType];
    STRING_NIL_TO_NONE(projectType);
    NSString *version = [[[ClassFactory getInstance] getLocalCfg] getVersionCode];
    STRING_NIL_TO_NONE(version)
    NSString *userToken = [[[ClassFactory getInstance] getLocalCfg] getUserToken];
    STRING_NIL_TO_NONE(userToken);
    
    NSDictionary *parmars = [NSDictionary dictionaryWithObjectsAndKeys:
                             projectType,@"projectType",
                             platType, @"platType",
                             version,@"version",
                             @"M",@"appType",
                             @"phone",@"clientType",
                             requestobjDic,@"requestObject",
                             nil];
    
    [self postRequestWithUrl:strUrl path:strUrl parameters:parmars success:^(id JSON) {
        
        if (success) {
            success(JSON);
        }
    } failure:^(NSError *error, id JSON) {
        
        if (failure) {
            failure(error);
        }
    }];
}

#pragma mark -
#pragma mark - 日志记录
- (void)logRecordAction:(NSDictionary *)requestobjDic
                           success:(void (^)(id JSON))success
                           failure:(void (^)(NSError *error))failure
{
    NSMutableString *strUrl = [NSMutableString stringWithFormat:@"%@/",
                               [[[ClassFactory getInstance] getLocalCfg] getHttpServerBookAddr]];
    [strUrl appendFormat:@"cpic-cxcim-web/crm/saveOperatingRecord.do?"];
    
    NSString *platType = [[[ClassFactory getInstance] getLocalCfg] getPlatType];
    STRING_NIL_TO_NONE(platType);
    NSString *projectType = [[[ClassFactory getInstance] getLocalCfg] getProjectType];
    STRING_NIL_TO_NONE(projectType);
    NSString *version = [[[ClassFactory getInstance] getLocalCfg] getVersionCode];
    STRING_NIL_TO_NONE(version)
    NSString *userToken = [[[ClassFactory getInstance] getLocalCfg] getUserToken];
    STRING_NIL_TO_NONE(userToken);
    
    NSDictionary *parmars = [NSDictionary dictionaryWithObjectsAndKeys:
                             projectType,@"projectType",
                             platType, @"platType",
                             version,@"version",
                             @"M",@"appType",
                             @"phone",@"clientType",
                             requestobjDic,@"requestObject",
                             nil];
    
    [self postRequestWithUrl:strUrl path:strUrl parameters:parmars timeOut:120 success:^(id JSON) {
        
        if (success) {
            success(JSON);
        }
    } failure:^(NSError *error, id JSON) {
        
        if (failure) {
            failure(error);
        }
    }];
}

#pragma mark -
#pragma mark - 生成二维码
- (void)getWeChatQrCode:(NSString *)strUrl withParmars:(NSDictionary *)parmars
                success:(void (^)(id JSON))success
                failure:(void (^)(NSError *error))failure
{
    AFHTTPSessionManager *operationManager = [AFHTTPSessionManager manager];
    
    /* 发送json数据 */
    operationManager.requestSerializer = [AFJSONRequestSerializer serializer];
    /* 响应json数据 */
    operationManager.responseSerializer  = [AFJSONResponseSerializer serializer];
    
    // 设置超时时间
    [operationManager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    [operationManager.requestSerializer setTimeoutInterval:120];
    [operationManager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    
    /* 设置响应内容格式  经常因为服务器返回的格式不是标准json而出错 服务器可能返回text/html text/plain 作为json */
    operationManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", @"text/plain",nil];
    
    /* 数据请求 */
    [operationManager POST:strUrl parameters:parmars constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success)
        {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure){
            failure (error);
        }
        
        NSString *errorInfo = [NSString stringWithFormat:@"%@",[error localizedDescription]];
        STRING_NIL_TO_NONE(errorInfo);
        
        [[[ClassFactory getInstance] getInfoHUD] showHud:errorInfo];
    }];
    
    
}

@end


