//
//  NSObject+NetComm_Object.h
//  IOSFramework
//
//  Created by 林科 on 15/11/16.
//  Copyright © 2015年 allianture. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (NetComm_Object)

#pragma mark -
#pragma mark - 信息保存
- (void)saveObject:(id)object Path:(NSString *)path;

- (id)getObject:(NSString *)path;


#pragma mark -
#pragma mark -加密
-(NSString *)GetEncrypt:(NSString *)string;

#pragma mark -
#pragma mark - 解密
-(id)GetDecrypt:(id)string;


#pragma mark -
#pragma mark - 

@end
