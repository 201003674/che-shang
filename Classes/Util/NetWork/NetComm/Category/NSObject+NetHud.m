//
//  NSObject+NetHud.m
//  IOSFramework
//
//  Created by 林科 on 15/11/16.
//  Copyright © 2015年 allianture. All rights reserved.
//
#import "ProgressHUD.h"

#import "NSObject+NetHud.h"

@implementation NSObject (NetHud)

#pragma -
#pragma mark - 显示隐藏hud
/*
 hudStr：传空"",则显示“加载中...”
 不显示内容则传 nil
 */
- (void)showHud:(NSString *)hudStr
{
    if ([hudStr isEqualToString:@""])
    {
        hudStr = NSLocalizedString(@"Loading", @"加载中... 状态");
    }

    [ProgressHUD showInfo:hudStr];
}

- (void)hiddenHud
{
    [ProgressHUD dismiss];
}

@end
