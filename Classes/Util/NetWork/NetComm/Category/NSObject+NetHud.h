//
//  NSObject+NetHud.h
//  IOSFramework
//
//  Created by 林科 on 15/11/16.
//  Copyright © 2015年 allianture. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (NetHud)

/* 显示 网络层hud */
- (void)showHud:(NSString *)hudStr;

/* 隐藏 网络hud */
- (void)hiddenHud;

@end
