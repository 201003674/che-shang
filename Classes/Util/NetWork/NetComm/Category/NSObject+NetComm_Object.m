//
//  NSObject+NetComm_Object.m
//  IOSFramework
//
//  Created by 林科 on 15/11/16.
//  Copyright © 2015年 allianture. All rights reserved.
//

#import "ThreeDES.h"

#import "NSObject+NetComm_Object.h"

@implementation NSObject (NetComm_Object)

#pragma mark -
#pragma mark - 信息保存
- (void)saveObject:(id)object Path:(NSString *)path
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:path];
    
    if (object == nil)
    {
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:path];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (id)getObject:(NSString *)path
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:path];
}

#pragma mark -
#pragma mark - 加密解密
/**
 *	@brief	加密
 *
 *	@param 	string 	加密字段
 *
 *	@return	加密结果
 */
-(NSString *)GetEncrypt:(NSString *)string
{
    //是否加密
    BOOL Encrypt = [[[ClassFactory getInstance] getLocalCfg] getEncrypt];
    
    //获取加密密钥
    NSString *EncryptKey = [[[ClassFactory getInstance] getLocalCfg] getEncryptKey];
    
    NSString *parJSONString = @"";
    
    if (Encrypt)
    {
        //执行加密
        parJSONString = [ThreeDES TripleDES:string encryptOrDecrypt:kCCEncrypt key:EncryptKey];
    }
    else
    {
        parJSONString = string;
    }
    return parJSONString;
}


/**
 *	@brief	解密
 *
 *	@param 	string 	解密字段
 *
 *	@return	解密结果
 */
-(id)GetDecrypt:(id)string
{
    //是否加密
    BOOL Encrypt = [[[ClassFactory getInstance] getLocalCfg] getEncrypt];
    
    //获取加密密钥
    NSString *EncryptKey = [[[ClassFactory getInstance] getLocalCfg] getEncryptKey];
    
    id JSON = @"";
    if (Encrypt)
    {
        JSON = [ThreeDES TripleDES:string encryptOrDecrypt:kCCDecrypt key:EncryptKey];
    }
    else
    {
        JSON = string;
    }
    return JSON;
}

@end
