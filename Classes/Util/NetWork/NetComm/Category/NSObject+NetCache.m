//
//  NSObject+NetCache.m
//  IOSFramework
//
//  Created by 林科 on 15/11/16.
//  Copyright © 2015年 allianture. All rights reserved.
//

#import "NSObject+NetCache.h"

@implementation NSObject (NetCache)

#pragma mark -
#pragma mark - 清除缓存
- (void)clearCaches:(NSArray *)typeArr
{  
    dispatch_async(
                   dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
                   , ^{
                       for (NSDictionary *dic in typeArr)
                       {
                           NSString *checked = [NSString stringWithFormat:@"%@",[dic objectForKey:@"checked"]];
                           STRING_NIL_TO_NONE(checked);
                           
                           NSString *itemType = [NSString stringWithFormat:@"%@",[dic objectForKey:@"itemType"]];
                           STRING_NIL_TO_NONE(itemType);
                           
                           if ([checked isEqualToString:@"ture"])
                           {
                               if ([itemType isEqualToString:@"0"])
                               {
                                   //输入记录

                               }
                               else if ([itemType isEqualToString:@"1"])
                               {
                                   //历史记录
                                   FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
                                   
                                   [fmdbSql eraseHttpTable];
                               }
                               else if ([itemType isEqualToString:@"2"])
                               {
                                   //账号密码
                               }
                               else if ([itemType isEqualToString:@"3"])
                               {
                                   //缓存文件
                                   NSFileManager *fileManager = [NSFileManager defaultManager];
                                   
                                   NSString *fileFinal = [NSString stringWithFormat:@"%@",DocumentsDirectory];
                                   
                                   BOOL isDir = FALSE;
                                   
                                   BOOL isDirExist = [fileManager fileExistsAtPath:fileFinal isDirectory:&isDir];
                                   
                                   if(isDirExist)
                                   {
                                       [fileManager removeItemAtPath:fileFinal error:nil];
                                   }
                                   else
                                   {
                                       //
                                   }
                               }
                               else if ([itemType isEqualToString:@"4"])
                               {
                                   //Cookies
                                   NSString *cachPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask, YES) objectAtIndex:0];
                                   
                                   NSArray *files = [[NSFileManager defaultManager] subpathsAtPath:cachPath];
                                   
                                   for (NSString *p in files)
                                   {
                                       NSError *error;
                                       
                                       NSString *path = [cachPath stringByAppendingPathComponent:p];
                                       
                                       if ([[NSFileManager defaultManager] fileExistsAtPath:path])
                                       {
                                           [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
                                       }
                                   }
                               }
                               else
                               {
                                   
                               }
                           }
                           else
                           {
                               
                           }
                       }
                       
                       [self performSelectorOnMainThread:@selector(clearCacheSuccess) withObject:nil waitUntilDone:YES];
                       
                   });
}

- (void)clearTheCache
{
    dispatch_async(
                   
                   dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
                   , ^{
                       NSString *cachPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask, YES) objectAtIndex:0];
                       
                       NSArray *files = [[NSFileManager defaultManager] subpathsAtPath:cachPath];
                       
                       for (NSString *p in files)
                       {
                           NSError *error;
                           
                           NSString *path = [cachPath stringByAppendingPathComponent:p];
                           
                           if ([[NSFileManager defaultManager] fileExistsAtPath:path])
                           {
                               [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
                           }
                       }
                   });
}

-(void)clearCacheSuccess
{
    [[[ClassFactory getInstance] getInfoHUD] showHud:@"清理成功"];
}

- (void)clearTheWebCache
{
    //清除cookies
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    for (NSHTTPCookie *cookie in storage.cookies)
    {
        [storage deleteCookie:cookie];
    }
    
    //清除WEB 缓存
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    NSURLCache * cache = [NSURLCache sharedURLCache];
    [cache removeAllCachedResponses];
    [cache setDiskCapacity:0];
    [cache setMemoryCapacity:0];
}

#pragma mark -
#pragma mark - 文件大小
//遍历文件夹获得文件夹大小，返回多少M
- (NSString *)folderSizeAtPath:(NSString*)folderPath
{
    NSFileManager *manager = [NSFileManager defaultManager];
    
    if (![manager fileExistsAtPath:folderPath])
    {
        return @"0 B";
    }
    else
    {
        NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
        
        NSString *fileName = nil;
        
        long long folderSize = 0;
        
        while ((fileName = [childFilesEnumerator nextObject]) != nil)
        {
            NSString *fileAbsolutePath = [NSString stringWithFormat:@"%@",[folderPath stringByAppendingPathComponent:fileName]];
            
            folderSize += [self fileSizeAtPath:fileAbsolutePath];
        }
        
        if (folderSize >= 1024.0*1024.0*1024.0)
        {
            return [NSString stringWithFormat:@"%.2f G",folderSize/(1024.0*1024.0*1024.0)];
        }
        else if (folderSize >= 1024.0*1024.0)
        {
            return [NSString stringWithFormat:@"%.2f M",folderSize/(1024.0*1024.0)];
        }
        else if (folderSize >= 1024.0)
        {
            return [NSString stringWithFormat:@"%.2f KB",folderSize/(1024.0)];
        }
        else
        {
            return [NSString stringWithFormat:@"%.2f B",folderSize/(1024.0*1024.0)];
        }
        return @"0 B";
    }
}
//单个文件的大小
- (long long ) fileSizeAtPath:(NSString*)filePath
{
    NSFileManager *manager = [NSFileManager defaultManager];
    
    if ([manager fileExistsAtPath:filePath])
    {
        NSDictionary *dic = [NSDictionary dictionaryWithDictionary:[manager attributesOfItemAtPath:filePath error:nil]];
        
        return dic.fileSize;
    }
    else
    {
        //
    }
    return 0;
}


@end
