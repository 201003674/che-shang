//
//  NSObject+NetCache.h
//  IOSFramework
//
//  Created by 林科 on 15/11/16.
//  Copyright © 2015年 allianture. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (NetCache)

#pragma mark -
#pragma mark - 清除缓存
- (void)clearTheCache;

- (void)clearCaches:(NSArray *)typeArr;

#pragma mark -
#pragma mark - 文件大小
- (NSString *)folderSizeAtPath:(NSString*)folderPath;

- (long long )fileSizeAtPath:(NSString*)filePath;

#pragma mark -
#pragma mark - 清除web缓存

- (void)clearTheWebCache;

@end
