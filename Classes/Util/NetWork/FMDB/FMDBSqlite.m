//
//  FMDBSqlite.m
//  IPadFramework
//
//  Created by allianture on 13-4-27.
//  Copyright (c) 2013年 allianture. All rights reserved.
//
//表名：NetworkCacheTable      字段：REQUESTURL     RESPONSEOBJECT    UPDATETIME      都是TEXT类型
///* 表名 */
#define SQLNAME @"SinokoreaLife.s3db"     /* 默认数据库名 */
#define HTTPTABLE @"NetworkCacheTable"              /* http网络数据缓存表名 */
#define HTTP_KEY_URL @"REQUESTURL"                  /* http网络缓存表 key */
#define HTTP_KEY_JSON @"RESPONSEOBJECT"
#define HTTP_KEY_UPDATETIME  @"UPDATETIME"

@implementation FMDBSqlite

- (id)init
{
    if (self = [super init])
    {
        if (!self.queue)
        {
            [self creatDatabase];
        }
    }
    return self;
}

#pragma mark -
#pragma mark - 基本操作

//数据库文件地址
- (NSString *)databaseFilePath
{
    /* document文件 */
    NSArray *filePath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [filePath objectAtIndex:0];
    
    NSString *dbFilePath = [documentPath stringByAppendingPathComponent:SQLNAME];
    
    return dbFilePath;
}

//创建数据库的操作
- (void)creatDatabase
{
    self.queue = [FMDatabaseQueue databaseQueueWithPath:[self databaseFilePath]];
}

/* 判断表是否存在 */
- (BOOL)tableExists:(NSString *)tableName
{
    __block BOOL result = NO;
    
    [self.queue inDatabase:^(FMDatabase *db){
        
        result = [db tableExists:tableName];
        
    }];
    
    return result;
}


//创建表 sqlStr = [NSString stringWithFormat:@"CREATE TABLE %@ (%@ text, %@ text) ",HTTPTABLE, HTTP_KEY_URL, HTTP_KEY_JSON]
- (void)creatTable:(NSString *)tableName sqlStr:(NSString *)sqlStr
{
    //先判断数据库是否存在，如果不存在，创建数据库
    if (!self.queue)
    {
        [self creatDatabase];
    }
    
    //判断数据库中是否已经存在这个表，如果不存在则创建该表
    if(![self tableExists:tableName])
    {
        [self.queue inDatabase:^(FMDatabase *db){
            
            //判断数据库是否已经打开，如果没有打开，提示失败
            if (![db open])
            {
                NSLog(@"数据库打开失败");
                return;
            }
            
            //为数据库设置缓存，提高查询效率
            [db setShouldCacheStatements:YES];
            
            [db executeUpdate:sqlStr];
            NSLog(@" HTTPTABLE 创建完成");
            
        }];
    }
}

/* 创建表 */
- (void)creatHttpSqlTable
{
    NSString *sqlStr = [NSString stringWithFormat:@"CREATE TABLE %@ (%@ text, %@ text) ",HTTPTABLE, HTTP_KEY_URL, HTTP_KEY_JSON];
    
    [self creatTable:HTTPTABLE sqlStr:sqlStr];
}


/* 删除表 */
- (BOOL)deleteTable:(NSString *)tableName
{
    //先判断数据库是否存在，如果不存在，创建数据库
    if (!self.queue)
    {
        [self creatDatabase];
    }
    __block BOOL result = NO;
    //判断数据库中是否已经存在这个表，如果不存在则创建该表
    if(![self tableExists:tableName])
    {
        [self.queue inDatabase:^(FMDatabase *db){
            
            //判断数据库是否已经打开，如果没有打开，提示失败
            if (![db open])
            {
                NSLog(@"数据库打开失败");
                return;
            }
            
            //为数据库设置缓存，提高查询效率
            [db setShouldCacheStatements:YES];
            
            NSString *sqlstr = [NSString stringWithFormat:@"DROP TABLE %@", tableName];
            
            result = [db executeUpdate:sqlstr];
            
        }];
    }
    return result;
}


/* 清除表内容 */
- (BOOL)eraseTable:(NSString *)tableName
{
    //先判断数据库是否存在，如果不存在，创建数据库
    if (!self.queue)
    {
        [self creatDatabase];
    }
    __block BOOL result = NO;
    //判断数据库中是否已经存在这个表，如果不存在则创建该表
    if(![self tableExists:tableName])
    {
        [self.queue inDatabase:^(FMDatabase *db){
            
            //判断数据库是否已经打开，如果没有打开，提示失败
            if (![db open])
            {
                NSLog(@"数据库打开失败");
                return;
            }
            
            //为数据库设置缓存，提高查询效率
            [db setShouldCacheStatements:YES];
            
            NSString *sqlstr = [NSString stringWithFormat:@"DELETE FROM %@", tableName];
            
            result = [db executeUpdate:sqlstr];
            
        }];
    }
    return result;
}


#pragma -
#pragma mark - 网络缓存
/* http数据缓存 表数据查询  */
- (void)selectHttpTable:(NSString *)urlStr
                success:(void (^)(NSString *jsonString))success
                failure:(void (^)(NSString *error))failure
{
    [self.queue inDatabase:^(FMDatabase *db) {
        
        /* 数据缓存 */
        [db setShouldCacheStatements:YES];
        
        /* 语句 */
        NSString *selectStr = [NSString stringWithFormat:@"SELECT * FROM %@  WHERE %@ = '%@'", HTTPTABLE, HTTP_KEY_URL, urlStr];
        
        FMResultSet *rs = [db executeQuery:selectStr];
        
        if ([rs next])
        {
            NSString *jsonStr = [rs stringForColumn:HTTP_KEY_JSON];
            
            if (success)
            {
                success(jsonStr);
            }
            
            return;
        }
        else
        {
            if (failure)
            {
                failure(@"NO Data !");
            }
            
            return;
        }
        
        
    }];
}



/* http数据缓存 新增或更新数据 */
- (void)updateHttpTable:(NSString *)urlStr
              valueJson:(NSString *)jsonStr
                success:(void (^)(NSString *success))success
                failure:(void (^)(NSString *error))failure
{
    [self.queue inDatabase:^(FMDatabase *db) {
        
        /* 语句 */
        NSString *selectStr = [NSString stringWithFormat:@"SELECT * FROM %@  WHERE %@ = '%@'", HTTPTABLE, HTTP_KEY_URL, urlStr];
        FMResultSet *rs = [db executeQuery:selectStr];
        
        BOOL result;
        
        if ([rs next])
        {
            /* 语句 */
            result = [db executeUpdate:[NSString stringWithFormat:@"UPDATE %@ SET %@ = ? WHERE %@ = ?", HTTPTABLE, HTTP_KEY_JSON, HTTP_KEY_URL], jsonStr, urlStr];
        }
        else
        {
            /* 语句 */
            result = [db executeUpdate:[NSString stringWithFormat:@"INSERT INTO %@ (%@, %@) VALUES (?,?)", HTTPTABLE, HTTP_KEY_URL, HTTP_KEY_JSON], urlStr, jsonStr];
        }
        
        if (result)
        {
            success(@"insert or updata success!");
            
            NSLog(@"写入数据库成功!");
        }
        else
        {
            failure(@"insert or updata failure!");
            NSLog(@"写入数据库失败!");
        }
        
    }];
}


/* http数据缓存 清除数据 */
- (void)eraseHttpTable
{
    [self eraseTable:HTTPTABLE];
}

#pragma mark -
#pragma mark - FMDBSqliteHelper
/**
 数据库中是否存在表
 */
-(void)isExistWithTableName:(NSString*)name complete:(void (^)(BOOL isExist))complete{
    if (name==nil){
        NSLog(@"表名不能为空!");
        if (complete) {
            complete(NO);
        }
        return;
    }
    __block BOOL result;
    [self.queue inDatabase:^(FMDatabase *db){
        result = [db tableExists:name];
    }];
    if (complete) {
        complete(result);
    }
}

/**
 默认建立主键id
 创建表(如果存在则不创建) keys 数据存放要求@[字段名称1,字段名称2]
 */
-(void)createTableWithTableName:(NSString*)name keys:(NSArray*)keys complete:(void (^)(BOOL isSuccess))complete{
    if (name == nil) {
        NSLog(@"表名不能为空!");
        if (complete) {
            complete(NO);
        }
        return;
    }else if (keys == nil){
        NSLog(@"字段数组不能为空!");
        if (complete) {
            complete(NO);
        }
        return;
    }else;
    
    //创表
    __block BOOL result;
    [self.queue inDatabase:^(FMDatabase *db) {
        NSString* header = [NSString stringWithFormat:@"create table if not exists %@ (id integer primary key autoincrement",name];//,name text,age integer);
        NSMutableString* sql = [[NSMutableString alloc] init];
        [sql appendString:header];
        
        for(int i=0;i<keys.count;i++){
            [sql appendFormat:@",%@ text",keys[i]];
            if (i == (keys.count-1)) {
                [sql appendString:@");"];
            }
        }
        result = [db executeUpdate:sql];
    }];
    if (complete){
        complete(result);
    }
}
/**
 插入值
 */
-(void)insertIntoTableName:(NSString*)name Dict:(NSDictionary*)dict complete:(void (^)(BOOL isSuccess))complete
{
    //判断数据库中是否已经存在这个表，如果不存在则创建该表
    if(![self tableExists:name])
    {
        [self createTableWithTableName:name keys:dict.allKeys complete:^(BOOL isSuccess) {
            
        }];
    }
    
    if (name == nil)
    {
        NSLog(@"表名不能为空!");
        if (complete) {
            complete(NO);
        }
        return;
    }
    else if (dict == nil)
    {
        NSLog(@"插入值字典不能为空!");
        if (complete) {
            complete(NO);
        }
        return;
    }
    else
    {
        __block BOOL result;
        [self.queue inDatabase:^(FMDatabase *db) {
            NSArray* keys = dict.allKeys;
            NSArray* values = dict.allValues;
            NSMutableString* SQL = [[NSMutableString alloc] init];
            [SQL appendFormat:@"insert into %@(",name];
            for(int i=0;i<keys.count;i++){
                [SQL appendFormat:@"%@",keys[i]];
                if(i == (keys.count-1)){
                    [SQL appendString:@") "];
                }else{
                    [SQL appendString:@","];
                }
            }
            [SQL appendString:@"values("];
            for(int i=0;i<values.count;i++){
                [SQL appendString:@"?"];
                if(i == (keys.count-1)){
                    [SQL appendString:@");"];
                }else{
                    [SQL appendString:@","];
                }
            }
            result = [db executeUpdate:SQL withArgumentsInArray:values];
            NSLog(@"插入 -- %d",result);
        }];
        
        
        if (complete) {
            complete(result);
        }
    }
}
/**
 根据条件查询字段
 */
-(void)queryWithTableName:(NSString*)name keys:(NSArray*)keys where:(NSArray*)where complete:(void (^)(NSArray* array))complete
{
    //判断数据库中是否已经存在这个表，如果不存在则创建该表    
    if (name == nil)
    {
        if (complete) {
            complete(nil);
        }
        return;
    }
    
    //判断数据库中是否已经存在这个表，如果不存在则创建该表
    if(![self tableExists:name])
    {
        [self createTableWithTableName:name keys:keys complete:^(BOOL isSuccess) {
            
            if (isSuccess) {
                NSLog(@"%@ 表不存在c，创建成功;",name);
            }
        }];
    }
    
    __block NSMutableArray* arrM = [[NSMutableArray alloc] init];
    [self.queue inDatabase:^(FMDatabase *db) {
        NSMutableString* SQL = [[NSMutableString alloc] init];
        [SQL appendString:@"select"];
        if ((keys!=nil)&&(keys.count>0)) {
            [SQL appendString:@" "];
            for(int i=0;i<keys.count;i++){
                [SQL appendFormat:@"%@",keys[i]];
                if (i != (keys.count-1)) {
                    [SQL appendString:@","];
                }
            }
        }else{
            [SQL appendString:@" *"];
        }
        [SQL appendFormat:@" from %@",name];
        
        if ((where!=nil) && (where.count>0)){
            if(!(where.count%3)){
                [SQL appendString:@" where "];
                for(int i=0;i<where.count;i+=3){
                    [SQL appendFormat:@"%@%@'%@'",where[i],where[i+1],where[i+2]];
                    if (i != (where.count-3)) {
                        [SQL appendString:@" and "];
                    }
                }
            }else{
                NSLog(@"条件数组错误!");
            }
        }
        // 1.查询数据
        FMResultSet *rs = [db executeQuery:SQL];
        // 2.遍历结果集
        while (rs.next) {
            NSMutableDictionary* dictM = [[NSMutableDictionary alloc] init];
            for (int i=0;i<[[[rs columnNameToIndexMap] allKeys] count];i++) {
                dictM[[rs columnNameForIndex:i]] = [rs objectForColumnIndex:i];
            }
            [arrM addObject:dictM];
        }
    }];
    if (complete) {
        complete(arrM);
    }
	NSLog(@"查询 -- %@",arrM);
}

/**
 全部查询
 */
-(void)queryWithTableName:(NSString*)name complete:(void (^)(NSArray* array))complete{
    if (name==nil)
    {
        if (complete)
        {
            complete(nil);
        }
        return;
    }
    
    if(![self tableExists:name])
    {
        return;
    }
    
    __block NSMutableArray* arrM = [[NSMutableArray alloc] init];
    [self.queue inDatabase:^(FMDatabase *db) {
        NSString* SQL = [NSString stringWithFormat:@"select * from %@",name];
        // 1.查询数据
        FMResultSet *rs = [db executeQuery:SQL];
        // 2.遍历结果集
        while (rs.next) {
            NSMutableDictionary* dictM = [[NSMutableDictionary alloc] init];
            for (int i=0;i<[[[rs columnNameToIndexMap] allKeys] count];i++) {
                dictM[[rs columnNameForIndex:i]] = [rs objectForColumnIndex:i];
            }
            [arrM addObject:dictM];
        }
    }];
    if (complete) {
        complete(arrM);
    }
	NSLog(@"查询 -- %@",arrM);
}

/**
 根据key更新value
 */
-(void)updateWithTableName:(NSString*)name valueDict:(NSDictionary*)valueDict where:(NSArray*)where complete:(void (^)(BOOL isSuccess))complete{
    if (name == nil) {
        NSLog(@"表名不能为空!");
        if (complete) {
            complete(NO);
        }
        return;
    }
    __block BOOL result;
    [self.queue inDatabase:^(FMDatabase *db) {
        NSMutableString* SQL = [[NSMutableString alloc] init];
        [SQL appendFormat:@"update %@ set ",name];
        for(int i=0;i<valueDict.allKeys.count;i++){
            [SQL appendFormat:@"%@='%@'",valueDict.allKeys[i],valueDict[valueDict.allKeys[i]]];
            if (i != (valueDict.allKeys.count-1)) {
                [SQL appendString:@","];
            }
        }
        if ((where!=nil) && (where.count>0)){
            if(!(where.count%3)){
                [SQL appendString:@" where "];
                for(int i=0;i<where.count;i+=3){
                    [SQL appendFormat:@"%@%@'%@'",where[i],where[i+1],where[i+2]];
                    if (i != (where.count-3)) {
                        [SQL appendString:@" and "];
                    }
                }
            }else{
                NSLog(@"条件数组格式错误!");
            }
        }
        result = [db executeUpdate:SQL];
//        NSLog(@"更新:  %@",SQL);
    }];
    if (complete) {
        complete(result);
    }
}

/**
 删除
 */
-(void)deleteWithTableName:(NSString*)name where:(NSArray*)where complete:(void (^)(BOOL isSuccess))complete{
    if (name == nil) {
        NSLog(@"表名不能为空!");
        if (complete) {
            complete(NO);
        }
        return;
    }else if (where==nil || (where.count%3)){
        NSLog(@"条件数组错误!");
        if (complete) {
            complete(NO);
        }
        return;
    }else;
    __block BOOL result;
    [self.queue inDatabase:^(FMDatabase *db) {
        NSMutableString* SQL = [[NSMutableString alloc] init];
        [SQL appendFormat:@"delete from %@ where ",name];
        for(int i=0;i<where.count;i+=3){
            [SQL appendFormat:@"%@%@'%@'",where[i],where[i+1],where[i+2]];
            if (i != (where.count-3)) {
                [SQL appendString:@" and "];
            }
        }
        result = [db executeUpdate:SQL];
    }];
    if (complete){
        complete(result);
    }
}
/**
 根据表名删除表格全部内容
 */
-(void )clearTable:(NSString *)name complete:(void (^)(BOOL isSuccess))complete{
    if (name==nil){
        NSLog(@"表名不能为空!");
        if (complete) {
            complete(NO);
        }
        return;
    }
    __block BOOL result;
    [self.queue inDatabase:^(FMDatabase *db) {
        NSString* SQL = [NSString stringWithFormat:@"delete from %@",name];
        result = [db executeUpdate:SQL];
    }];
    if (complete) {
        complete(result);
    }
}

/**
 删除表
 */
-(void)dropTable:(NSString*)name complete:(void (^)(BOOL isSuccess))complete{
    if (name==nil){
        NSLog(@"表名不能为空!");
        if (complete) {
            complete(NO);
        }
        return;
    }
    __block BOOL result;
    [self.queue inDatabase:^(FMDatabase *db) {
        NSString* SQL = [NSString stringWithFormat:@"drop table %@",name];
        result = [db executeUpdate:SQL];
    }];
    if (complete) {
        complete(result);
    }
}

/**
 存储一个对象
 */
-(void)saveObject:(id)object complete:(void (^)(BOOL isSuccess))complete{
    NSMutableDictionary* dictM = [NSMutableDictionary dictionary];
    unsigned int numIvars; //成员变量个数
    Ivar *vars = class_copyIvarList([object class], &numIvars);
    NSString *key=nil;
    for(int i = 0; i < numIvars; i++) {
        Ivar thisIvar = vars[i];
        key = [NSString stringWithUTF8String:ivar_getName(thisIvar)];  //获取成员变量的名字
        key = [key substringFromIndex:1];
        [dictM setObject:[object valueForKey:key] forKey:key];
        //NSLog(@"variable name :%@ = %@", key,[object valueForKey:key]);
        //key = [NSString stringWithUTF8String:ivar_getTypeEncoding(thisIvar)]; //获取成员变量的数据类型
        //NSLog(@"variable type :%@", key);
    }
    free(vars);
    //检查是否建立了跟对象相对应的数据表
    NSString* tableName = [NSString stringWithFormat:@"%@",[object class]];
    [self isExistWithTableName:tableName complete:^(BOOL isExist) {
        if (!isExist){//如果不存在就新建
            [self createTableWithTableName:tableName keys:dictM.allKeys complete:^(BOOL isSuccess) {
                if (isSuccess) {
                    NSLog(@"建表成功 第一次建立 %@ 对应的表",tableName);
                }
            }];
        }
    }];
    [self insertIntoTableName:tableName Dict:dictM complete:complete];
}
/**
 查询全部对象
 */
-(void)queryAllObject:(__unsafe_unretained Class)cla complete:(void (^)(NSArray* array))complete{
    //检查是否建立了跟对象相对应的数据表
    NSString* tableName = [NSString stringWithFormat:@"%@",cla];
    
    [self isExistWithTableName:tableName complete:^(BOOL isExist) {
        if (!isExist){//如果不存在就返回空
            if (complete) {
                complete(nil);
            }
        }else{
            [self queryWithTableName:tableName complete:^(NSArray *array) {
                NSMutableArray* arrM = [NSMutableArray array];
                for(NSDictionary* dict in array){
                    id claObj = [cla mj_objectWithKeyValues:dict];
                    [arrM addObject:claObj];
                }
                if (complete) {
                    complete(arrM);
                }
            }];
        }
    }];
}
/**
 根据条件查询某个对象
 keys存放的是要查询的哪些key,为nil时代表查询全部
 where形式 @[@"key",@"=",@"value",@"key",@">=",@"value"]
 */
-(void)queryObjectWithClass:(__unsafe_unretained Class)cla keys:(NSArray*)keys where:(NSArray*)where complete:(void (^)(NSArray* array))complete{
    //检查是否建立了跟对象相对应的数据表
    NSString* tableName = [NSString stringWithFormat:@"%@",cla];
    
    [self isExistWithTableName:tableName complete:^(BOOL isExist) {
        if (!isExist){//如果不存在就返回空
            if (complete) {
                complete(nil);
            }
        }else{
            [self queryWithTableName:tableName keys:keys where:where complete:^(NSArray *array) {
                NSMutableArray* arrM = [NSMutableArray array];
                for(NSDictionary* dict in array){
                    id claObj = [cla mj_objectWithKeyValues:dict];
                    [arrM addObject:claObj];
                }
                if (complete) {
                    complete(arrM);
                }
            }];
        }
    }];
}
/**
 根据条件改变对象的值
 valueDict 存放的是key和value
 where数组的形式 @[@"key",@"=",@"value",@"key",@">=",@"value"]
 */
-(void)updateWithClass:(__unsafe_unretained Class)cla valueDict:(NSDictionary*)valueDict where:(NSArray*)where complete:(void (^)(BOOL isSuccess))complete{
    NSString* tableName = [NSString stringWithFormat:@"%@",cla];
    [self isExistWithTableName:tableName complete:^(BOOL isExist) {
        if (!isExist){//如果不存在就返回NO
            if (complete) {
                complete(NO);
            }
        }else{
            [self updateWithTableName:tableName valueDict:valueDict where:where complete:complete];
        }
    }];
}
/**
 cla代表对应的类
 根据条件删除对象表中的对象数据
 where形式 @[@"key",@"=",@"value",@"key",@">=",@"value"],where要非空
 */
-(void)deleteWithClass:(__unsafe_unretained Class)cla where:(NSArray*)where complete:(void (^)(BOOL isSuccess))complete{
    NSString* tableName = [NSString stringWithFormat:@"%@",cla];
    [self isExistWithTableName:tableName complete:^(BOOL isExist) {
        if (!isExist){//如果不存在就返回NO
            if (complete) {
                complete(NO);
            }
        }else{
            [self deleteWithTableName:tableName where:where complete:complete];
        }
    }];
    
}
/**
 根据类删除此类所有表数据
 */
-(void)clearWithClass:(__unsafe_unretained Class)cla complete:(void (^)(BOOL isSuccess))complete{
    NSString* tableName = [NSString stringWithFormat:@"%@",cla];
    [self isExistWithTableName:tableName complete:^(BOOL isExist) {
        if (!isExist){//如果不存在就返回NO
            if (complete) {
                complete(NO);
            }
        }else{
            [self clearTable:tableName complete:complete];
        }
    }];
}
/**
 根据类,删除这个类的表
 */
-(void)dropWithClass:(__unsafe_unretained Class)cla complete:(void (^)(BOOL isSuccess))complete{
    NSString* tableName = [NSString stringWithFormat:@"%@",cla];
    [self isExistWithTableName:tableName complete:^(BOOL isExist) {
        if (!isExist){//如果不存在就返回NO
            if (complete) {
                complete(NO);
            }
        }else{
            [self dropTable:tableName complete:complete];
        }
    }];
}

@end
