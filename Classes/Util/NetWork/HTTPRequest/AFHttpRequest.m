/*********************************************************************
 * 版权所有 LK
 *
 * 文件名称：
 * 文件标识：
 * 内容摘要：
 * 其它说明：
 * 当前版本：
 * 作    者： 林科
 * 完成日期：
 **********************************************************************/

/***************************************************************************
 *                                文件引用
 ***************************************************************************/
#import "AfHttpRequest.h"

/***************************************************************************
 *                                 宏定义
 ***************************************************************************/


/***************************************************************************
 *                                 常量
 ***************************************************************************/


/***************************************************************************
 *                                类型定义
 ***************************************************************************/


/***************************************************************************
 *                                全局变量
 ***************************************************************************/


/***************************************************************************
 *                                 原型
 ***************************************************************************/
@interface afHttpRequest ()


@end


/***************************************************************************
 *                                类的实现
 ***************************************************************************/
@implementation afHttpRequest


- (id)init
{
    if (self = [super init])
    {
        
    }
    return self;
}

@end




@implementation NSObject (afHttpRequest_object)

#pragma mark -
#pragma mark - GET请求
/***********************************************************************
 * 方法名称： getRequestWithUrl
 * 功能描述： GET请求方式
 * 输入参数： urlStr:请求的url
 * 输出参数：
 * 返 回 值：
 * 其它说明：GET请求，JSON解析, 不设定超时时长
 ***********************************************************************/
- (void)getRequestWithUrl:(NSString *)urlStr
                     path:(NSString *)strUrl
               parameters:(NSDictionary *)parameters
                  success:(void (^)(id JSON))success
                  failure:(void (^)(NSError *error, id JSON))failure;
{
    NSInteger timeOutInt = 0;
    
    timeOutInt = [[[[ClassFactory getInstance] getLocalCfg] getHttpExpiredTime] intValue];
    
    [self getRequestWithUrl:urlStr path:strUrl
                 parameters:parameters timeOut:timeOutInt success:success failure:failure ];
}


/***********************************************************************
 * 方法名称： getRequestWithUrl
 * 功能描述： GET请求方式
 * 输入参数： urlStr:请求的url timeOutInt:请求超时时长
 * 输出参数：
 * 返 回 值：
 * 其它说明：GET请求，JSON解析, 设定超时时长
 ***********************************************************************/
- (void)getRequestWithUrl:(NSString *)urlStr
                     path:(NSString *)strUrl
               parameters:(NSDictionary *)parameters
                  timeOut:(NSInteger )timeOutInt
                  success:(void (^)(id JSON))success
                  failure:(void (^)(NSError *error, id JSON))failure
;
{
    if (STRING_ISNIL(strUrl))
    {
        return;
    }
    
    //网络类型判断
    [self ConnectNetWork];
    
    NSString *JsonString = [NSString stringWithFormat:@"%@",[parameters JSONString]];
    //加密处理
    NSString *parJSONString = [[[ClassFactory getInstance] getNetComm] GetEncrypt:JsonString];
    
    urlStr = [NSString stringWithFormat:@"%@requestMessage=%@",strUrl,parJSONString];
    urlStr = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    /* 是否使用本地缓存数据 */
    NSString *localHttpStr = [[[ClassFactory getInstance] getLocalCfg] getLocalHttpRequest];
    
    if ([localHttpStr isEqualToString:@"1"])
    {
        /* 数据缓存 查询 */
        FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
        [fmdbSql selectHttpTable:urlStr success:^(NSString *jsonString) {
            //数据库查询成功
            if (success)
            {
                success([jsonString objectFromJSONString]);
            }
        } failure:^(NSString *error) {

            //数据库查询失败
            if (failure)
            {
                failure(nil,error);
            }
        }];
    }
    else
    {
        //不使用缓存 或则 强制刷新缓存
    }
    //判断是否联网
    BOOL isConnect = [self isConnectNetWork];
    
    if (isConnect)
    {
        //网络获取数据
        [self getRequestWithUrl:urlStr parameters:parameters timeOut:timeOutInt success:^(id JSON) {
                if (success)
                {
                    success(JSON);
                }
            
        } failure:^(NSError *error, id JSON) {
            
            if (failure)
            {
                failure (JSON, error);
            }
            
        }];
    }
    else
    {
        failure(nil,nil);
        
        //隐藏 loading
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
    }
}

/***********************************************************************
 * 方法名称： getRequestWithUrl
 * 功能描述： GET请求方式
 * 输入参数： urlStr:请求的url timeOutInt:请求超时时长
 * 输出参数：
 * 返 回 值：
 * 其它说明：GET请求，JSON解析, 设定超时时长
 ***********************************************************************/
- (void)getRequestWithUrl:(NSString *)urlStr
               parameters:(NSDictionary *)parameters
                  timeOut:(NSInteger )timeOutInt
                  success:(void (^)(id JSON))success
                  failure:(void (^)(NSError *error, id JSON))failure
{
    AFHTTPSessionManager *operationManager = [AFHTTPSessionManager manager];
    //发送json数据
    operationManager.requestSerializer = [AFJSONRequestSerializer serializer];
    //响应json数据
    operationManager.responseSerializer  = [AFJSONResponseSerializer serializer];
    //请求时长
    operationManager.requestSerializer.timeoutInterval = timeOutInt;
    
    [operationManager.requestSerializer setValue:@"123" forHTTPHeaderField:@"x-access-id"];
    [operationManager.requestSerializer setValue:@"123" forHTTPHeaderField:@"x-signature"];
    
    //设置响应内容格式  经常因为服务器返回的格式不是标准json而出错 服务器可能返回text/html text/plain 作为json
    operationManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", @"text/plain",nil];
    
    /* 日志打印 */
    [Log logWithSessionManager:operationManager WithRequestBean:parameters];
    
    [operationManager GET:urlStr parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success)
        {
            /* 返回 网络请求 */
            success(responseObject);
            
            NSDictionary *requestResultDic = [[NSDictionary alloc] initWithDictionary:responseObject];
            NSString *resultCode = [NSString stringWithFormat:@"%@",[requestResultDic objectForKey:@"resultCode"]];
            STRING_NIL_TO_NONE(resultCode);
            
            /* 是否使用本地缓存数据 */
            NSString *localHttpStr = [[[ClassFactory getInstance] getLocalCfg] getLocalHttpRequest];
            
            if ([resultCode isEqualToString:@"1"] && [localHttpStr isEqualToString:@"1"])
            {
                /* 数据缓存 更新 */
                FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
                
                [fmdbSql updateHttpTable:urlStr valueJson:[responseObject JSONString] success:^(NSString *success) {
                    
                } failure:^(NSString *error) {
                    
                }];
            }
            else
            {
                
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure){
            failure (error,task.response);
        }
        
        NSString *errorInfo = [NSString stringWithFormat:@"%@",[error localizedDescription]];
        STRING_NIL_TO_NONE(errorInfo);
        
        [[[ClassFactory getInstance] getInfoHUD] showHud:errorInfo];
        
    }];
}

/***********************************************************************
 * 方法名称： getXMLRequestWithUrl
 * 功能描述： GET请求方式（XML数据）
 * 输入参数： urlStr:请求的url
 * 输出参数：
 * 返 回 值：
 * 其它说明：GET请求，XML解析, 不设定超时时长
 ***********************************************************************/
- (void)getXMLRequestWithUrl:(NSString *)urlStr
                        path:(NSString *)strUrl
                  parameters:(NSDictionary *)parameters
                     success:(void (^)(id JSON))success
                     failure:(void (^)(NSError *error))failure
;
{
    NSInteger timeOutInt = 0;
    timeOutInt = [[[[ClassFactory getInstance] getLocalCfg] getHttpExpiredTime] intValue];
    
    [self getXMLRequestWithUrl:urlStr path:strUrl
                    parameters:parameters timeOut:timeOutInt success:success failure:failure ];
}


/***********************************************************************
 * 方法名称： getXMLRequestWithUrl
 * 功能描述： GET请求方式（XML数据）
 * 输入参数： urlStr:请求的url timeOutInt:请求超时时长
 * 输出参数：
 * 返 回 值：
 * 其它说明：GET请求，XML解析, 设定超时时长
 ***********************************************************************/
- (void)getXMLRequestWithUrl:(NSString *)urlStr
                        path:(NSString *)strUrl
                  parameters:(NSDictionary *)parameters
                     timeOut:(NSInteger )timeOutInt
                     success:(void (^)(id JSON))success
                     failure:(void (^)(NSError *error))failure
;
{
    if (STRING_ISNIL(strUrl))
    {
        NSLog(@"url is nil !");
        return;
    }
    NSString *JsonString = [NSString stringWithFormat:@"%@",[parameters JSONString]];
    NSString *parJSONString = [[[ClassFactory getInstance] getNetComm] GetEncrypt:JsonString];
    
    urlStr = [NSString stringWithFormat:@"%@requestMessage=%@",strUrl,parJSONString];
    
    /* 是否使用本地缓存数据 */
    NSString *localHttpStr = [[[ClassFactory getInstance] getLocalCfg] getLocalHttpRequest];
    if ([localHttpStr isEqualToString:@"1"])
    {
        /* 数据缓存 查询 */
        FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
        [fmdbSql selectHttpTable:urlStr success:^(NSString *jsonString) {
            if (success) {
                success(jsonString);
            }
        } failure:^(NSString *error) {
            if (failure) {
                failure((NSError *)error);
            }
        }];
    }
    
    /* 数据传值 */
    urlStr = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    /* 数据请求 */
    AFHTTPSessionManager *operationManagerXML = [AFHTTPSessionManager manager];
    
    //请求响应时长
    operationManagerXML.requestSerializer.timeoutInterval = timeOutInt;
    //请求的数据格式是JSON
    operationManagerXML.requestSerializer = [AFJSONRequestSerializer serializer];
    // 返回的数据格式是XML
    operationManagerXML.responseSerializer = [AFXMLParserResponseSerializer serializer];
    
    [operationManagerXML GET:strUrl parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success)
        {
            NSDictionary *tempDic = [XMLReader dictionaryForXMLData:responseObject error:nil];
            
            /* 数据缓存 更新 */
            FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
            [fmdbSql updateHttpTable:urlStr valueJson:[tempDic JSONString] success:^(NSString *succ) {
                
                
            }failure:^(NSString *error) {
                
            }];
            
            success (tempDic);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure){
            
            failure (error);
        }
    }];
}




#pragma mark -
#pragma mark - POST请求
/***********************************************************************
 * 方法名称： postRequestWithUrl
 * 功能描述： POST请求方式
 * 输入参数： urlStr:请求的url
 path:请求头
 parameters:请求消息体
 * 输出参数：
 * 返 回 值：
 * 其它说明：POST请求，JSON解析, 不设定超时时长
 ***********************************************************************/
- (void)postRequestWithUrl:(NSString *)urlStr
                      path:(NSString *)path
                parameters:(NSDictionary *)parameters
                   success:(void (^)(id JSON))success
                   failure:(void (^)(NSError *error, id JSON))failure
;
{
    NSInteger timeOutInt = 0;
    timeOutInt = [[[[ClassFactory getInstance] getLocalCfg] getHttpExpiredTime] intValue];
    
    [self postRequestWithUrl:urlStr path:path parameters:parameters timeOut:timeOutInt success:success failure:failure ];
}


/***********************************************************************
 * 方法名称： postRequestWithUrl
 * 功能描述： POST请求方式
 * 输入参数： urlStr:请求的url path:请求头 parameters:请求消息体 timeOutInt:请求超时时长
 * 输出参数：
 * 返 回 值：
 * 其它说明：POST请求，JSON解析, 设定超时时长
 ***********************************************************************/
- (void)postRequestWithUrl:(NSString *)urlStr
                      path:(NSString *)path
                parameters:(NSDictionary *)parameters
                   timeOut:(NSInteger )timeOutInt
                   success:(void (^)(id JSON))success
                   failure:(void (^)(NSError *error, id JSON))failure
;
{
    if (STRING_ISNIL(path))
    {
        return;
    }
    
    /* 是否使用本地缓存数据 */
    NSString *localHttpStr = [[[ClassFactory getInstance] getLocalCfg] getLocalHttpRequest];
    
    /* 数据传值 */
    urlStr = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    if ([localHttpStr isEqualToString:@"1"])
    {
        /* 数据缓存 查询 */
        FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
        
        [fmdbSql selectHttpTable:urlStr success:^(NSString *jsonString) {
            
            if (success)
            {
                success([jsonString objectFromJSONString]);
                
                return;
            }
        } failure:^(NSString *error) {
            
        }];
    }
    
    /* POST 请求 */
    [self postRequestWithUrl:urlStr parameters:parameters timeOut:timeOutInt success:^(id JSON) {
        
        if (success) {
            success(JSON);
        }
    } failure:^(NSError *error, id JSON) {
        
        if (failure) {
            failure(error,JSON);
        }
    }];
}

/***********************************************************************
 * 方法名称： postRequestWithUrl
 * 功能描述： POST请求方式
 * 输入参数： urlStr:请求的url timeOutInt:请求超时时长
 * 输出参数：
 * 返 回 值：
 * 其它说明：POST请求，JSON解析, 设定超时时长
 ***********************************************************************/
- (void)postRequestWithUrl:(NSString *)urlStr
                parameters:(NSDictionary *)parameters
                   timeOut:(NSInteger )timeOutInt
                   success:(void (^)(id JSON))success
                   failure:(void (^)(NSError *error, id JSON))failure
{
    AFSessionManager *operationManager = [AFSessionManager manager];
    
    /* 发送json数据 */
    operationManager.requestSerializer = [AFJSONRequestSerializer serializer];
    /* 响应json数据 */
    operationManager.responseSerializer  = [AFJSONResponseSerializer serializer];
    
    // 设置超时时间
    [operationManager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    [operationManager.requestSerializer setTimeoutInterval:timeOutInt];
    [operationManager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    /* 设置响应内容格式  经常因为服务器返回的格式不是标准json而出错 服务器可能返回text/html text/plain 作为json */
    operationManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", @"text/plain",nil];
    
    /* 日志打印 */
    [Log logWithSessionManager:operationManager WithRequestBean:parameters];
    
    /* 数据请求 */
    [operationManager POST:urlStr parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success)
        {
            /* 返回 网络请求 */
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure){
            failure (error,task.response);
        }
        
        NSString *errorInfo = [NSString stringWithFormat:@"%@",[error localizedDescription]];
        STRING_NIL_TO_NONE(errorInfo);
        
        [[[ClassFactory getInstance] getInfoHUD] showHud:errorInfo];
    }];
}

/***********************************************************************
 * 方法名称： postXMLRequestWithUrl
 * 功能描述： POST请求方式（XML数据）
 * 输入参数： urlStr:请求的url
 path:请求头
 parameters:请求消息体
 * 输出参数：
 * 返 回 值：
 * 其它说明：POST请求，XML解析, 不设定超时时长
 ***********************************************************************/
- (void)postXMLRequestWithUrl:(NSString *)urlStr
                         path:(NSString *)path
                   parameters:(NSDictionary *)parameters
                      success:(void (^)(id JSON))success
                      failure:(void (^)(NSError *error))failure
;
{
    NSInteger timeOutInt = 0;
    timeOutInt = [[[[ClassFactory getInstance] getLocalCfg] getHttpExpiredTime] intValue];
    
    [self postXMLRequestWithUrl:urlStr path:path parameters:parameters timeOut:timeOutInt success:success failure:failure ];
}


/***********************************************************************
 * 方法名称： postXMLRequestWithUrl
 * 功能描述： POST请求方式（XML数据）
 * 输入参数： urlStr:请求的url path:请求头 parameters:请求消息体 timeOutInt:请求超时时长
 * 输出参数：
 * 返 回 值：
 * 其它说明：POST请求，JSON解析, 设定超时时长
 ***********************************************************************/
- (void)postXMLRequestWithUrl:(NSString *)urlStr
                         path:(NSString *)path
                   parameters:(NSDictionary *)parameters
                      timeOut:(NSInteger )timeOutInt
                      success:(void (^)(id JSON))success
                      failure:(void (^)(NSError *error))failure
;
{
    if (STRING_ISNIL(urlStr))
    {
        NSLog(@"url is nil !");
        return;
    }
    
    
    /* 是否使用本地缓存数据 */
    NSString *localHttpStr = [[[ClassFactory getInstance] getLocalCfg] getLocalHttpRequest];
    if ([localHttpStr isEqualToString:@"1"])
    {
        /* 数据缓存 查询 */
        NSString *sqlUrl = [NSString stringWithFormat:@"%@/%@/%@",urlStr,path,[parameters JSONString]];
        
        FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
        [fmdbSql selectHttpTable:sqlUrl success:^(NSString *jsonString) {
            if (success) {
                success(jsonString);
            }
        } failure:^(NSString *error) {
            if (failure) {
                failure((NSError *)error);
            }
        }];
    }
    
    /* 数据传值 */
    urlStr = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    /* 数据请求 */
    AFHTTPSessionManager *operationManagerXML = [AFHTTPSessionManager manager];
    
    /* 请求响应时长 */
    operationManagerXML.requestSerializer.timeoutInterval = timeOutInt;
    /* 请求的数据格式是JSON */
    operationManagerXML.requestSerializer = [AFJSONRequestSerializer serializer];
    /* 返回的数据格式是XML */
    operationManagerXML.responseSerializer = [AFXMLParserResponseSerializer serializer];
    
    /* allowInvalidCertificates 定义了客户端是否信任非法证书 */
    [operationManagerXML.securityPolicy setAllowInvalidCertificates:YES];
    
    /* 请求数据 */
    [operationManagerXML POST:urlStr parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success)
        {
            NSDictionary *tempDic = [XMLReader dictionaryForXMLData:responseObject error:nil];
            
            /* 数据缓存 更新 */
            FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
            [fmdbSql updateHttpTable:urlStr valueJson:[tempDic JSONString] success:^(NSString *succ) {
                
                
            }failure:^(NSString *error) {
                
            }];
            
            success (tempDic);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure){
            
            failure (error);
        }
    }];
}

/***********************************************************************
 * 方法名称： postSoapRequestWithUrl
 * 功能描述： POST请求方式（Soap）
 * 输入参数： urlStr:请求的url
 path:请求头
 parameters:请求消息体
 * 输出参数：
 * 返 回 值：
 * 其它说明：POST请求，Soap解析, 不设定超时时长
 ***********************************************************************/
- (void)postSoapRequestWithUrl:(NSString *)urlStr
                          path:(NSString *)path
                    parameters:(NSDictionary *)parameters
                       success:(void (^)(id JSON))success
                       failure:(void (^)(NSError *error))failure;
{
    NSInteger timeOutInt = 0;
    timeOutInt = [[[[ClassFactory getInstance] getLocalCfg] getHttpExpiredTime] intValue];
    
    [self postSoapRequestWithUrl:urlStr path:path parameters:parameters timeOut:timeOutInt success:success failure:failure ];
}


/***********************************************************************
 * 方法名称： postSoapRequestWithUrl
 * 功能描述： POST请求方式（Soap）
 * 输入参数： urlStr:请求的url path:请求头 parameters:请求消息体 timeOutInt:请求超时时长
 * 输出参数：
 * 返 回 值：
 * 其它说明：POST请求，Soap解析, 设定超时时长
 ***********************************************************************/
- (void)postSoapRequestWithUrl:(NSString *)urlStr
                          path:(NSString *)path
                    parameters:(NSDictionary *)parameters
                       timeOut:(NSInteger )timeOutInt
                       success:(void (^)(id JSON))success
                       failure:(void (^)(NSError *error))failure;
{
//    if (STRING_ISNIL(urlStr))
//    {
//        NSLog(@"url is nil !");
//        return;
//    }
//    
//    
//    /* 是否使用本地缓存数据 */
//    NSString *localHttpStr = [[[ClassFactory getInstance] getLocalCfg] getLocalHttpRequest];
//    if ([localHttpStr isEqualToString:@"1"])
//    {
//        /* 数据缓存 查询 */
//        NSString *sqlUrl = [NSString stringWithFormat:@"%@/%@/%@",urlStr,path,[parameters JSONString]];
//        
//        FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
//        [fmdbSql selectHttpTable:sqlUrl success:^(NSString *jsonString) {
//            if (success) {
//                success(jsonString);
//            }
//        } failure:^(NSString *error) {
//            if (failure) {
//                failure((NSError *)error);
//            }
//        }];
//    }
//    
//    NSArray *keyArr = [parameters allKeys];
//    NSMutableString *parmerStrM = [NSMutableString string];
//    for (NSInteger i = 0; i < keyArr.count; i++)
//    {
//        [parmerStrM appendFormat:@"<%@>%@</%@>", [keyArr objectAtIndex:i], [parameters objectForKey:[keyArr objectAtIndex:i]], [keyArr objectAtIndex:i]];
//    }
//    
//    NSString *soapMsg = [NSString stringWithFormat:
//                         @"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
//                         "<soap12:Envelope "
//                         "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
//                         "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" "
//                         "xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">"
//                         "<soap12:Body>"
//                         "<%@ xmlns=\"http://tempuri.org/\">"
//                         @"%@"
//                         "</%@>"
//                         "</soap12:Body>"
//                         "</soap12:Envelope>", path, parmerStrM, path];
//    
//    urlStr = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
//    
//    /* soap 消息长度 */
//    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMsg length]];
//    
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    manager.responseSerializer = [[AFHTTPResponseSerializer alloc] init];
//    [manager.requestSerializer setValue:@"application/soap+xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
//    [manager.requestSerializer setValue:msgLength forHTTPHeaderField:@"Content-Length"];
//    [manager.requestSerializer setTimeoutInterval:timeOutInt];
//    
//    /* 根据上面的URL创建一个请求 */
//    NSMutableURLRequest *request = [manager.requestSerializer requestWithMethod:@"POST" URLString:urlStr parameters:parameters error:nil];
//    /* 将SOAP消息加到请求中 */
//    [request setHTTPBody: [soapMsg dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    AFHTTPRequestSerializer *operation = [AFHTTPRequestSerializer r]
//    
//    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        if (success){
//            /* 提取数据 */
//            NSDictionary *tempDic = [XMLReader dictionaryForXMLData:responseObject error:nil];
//            NSDictionary *tempDicOne = [tempDic objectForKey:@"soap:Envelope"];
//            NSDictionary *tempDicTwo = [tempDicOne objectForKey:@"soap:Body"];
//            NSString *tempKeyThree = [NSString stringWithFormat:@"%@Response", path];
//            NSDictionary *tempDicThree = [tempDicTwo objectForKey:tempKeyThree];
//            NSString *tempKeyFour = [NSString stringWithFormat:@"%@Result", path];
//            NSDictionary *tempDicFour = [tempDicThree objectForKey:tempKeyFour];
//            
//            /* 数据缓存 更新 */
//            NSString *sqlUrl = [NSString stringWithFormat:@"%@/%@/%@",urlStr,path,[parameters JSONString]];
//            
//            FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
//            [fmdbSql updateHttpTable:sqlUrl valueJson:[tempDic JSONString] success:^(NSString *succ) {
//            } failure:^(NSString *error) {
//            }];
//            
//            success (tempDicFour);
//        }
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//        if (failure) {
//            failure (error);
//        }
//    }];
//    [manager.operationQueue addOperation:operation];
}

#pragma mark -
#pragma mark - 判断是否联网
-(void)ConnectNetWork
{
    //判断是否联网
    BOOL isConnect = [self isConnectNetWork];
    
    if (!isConnect)
        
    {
        UIAlertView *myalert = [[UIAlertView alloc] initWithTitle:nil message:@"请检查网络设置" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        myalert.tag = 0x9999;
        [myalert show];

        
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"])
        {
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"];
            
            NSLog(@"first launch再次程序启动");
        }
        else
        {
            NSLog(@"second launch再次程序启动");
        }
        return;
    }
    else
    {
        
    }
}
//获取网络类型
-(BOOL)isConnectNetWork
{
    BOOL isExistenceNetwork = YES;
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    
    switch ([reachability currentReachabilityStatus])
    {
        case NotReachable:
            isExistenceNetwork=NO;
            NSLog(@"没有网络");
            break;
        case ReachableViaWWAN:
            isExistenceNetwork=YES;
            NSLog(@"正在使用3G网络");
            break;
        case ReachableViaWiFi:
            isExistenceNetwork=YES;
            NSLog(@"正在使用wifi网络");
            break;
    }
    return isExistenceNetwork;
}
//弹出WiFi
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 0X9999)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
    else
    {
        
    }
}

#pragma mark -
#pragma mark -

@end



