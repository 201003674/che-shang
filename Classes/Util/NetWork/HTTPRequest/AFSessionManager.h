//
//  AFSessionManager.h
//  IOSFramework
//
//  Created by 林科 on 2018/9/21.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

NS_ASSUME_NONNULL_BEGIN

@interface AFSessionManager : AFHTTPSessionManager
{
    
}

@property(nonatomic, strong) NSString *targetOnlyStr;

@end

NS_ASSUME_NONNULL_END
