//
//  ReachabilityHudView.h
//  IOSFramework
//
//  Created by allianture on 13-7-25.
//  Copyright (c) 2013年 allianture. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReachabilityHudView : NSObject



/*
 设置一个网络监控的提示
 bannerSupView:提示框会在该页面展示
 */
- (void)initReachablityHud:(UIView *)bannerSupView;

@end


@interface NSObject (ReachablityHud)



@end