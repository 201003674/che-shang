//
//  ReachabilityHudView.m
//  IOSFramework
//
//  Created by allianture on 13-7-25.
//  Copyright (c) 2013年 allianture. All rights reserved.
//

#import "ReachabilityHudView.h"

#import <Reachability/Reachability.h>


#define BANNERVIEW_WIDTH 320        /* bannerView坐标 */
#define BANNERVIEW_HEIGHT 40




@interface ReachabilityHudView ()
{
    UIView *bannerView;         /* 提示框 */
    
    UIView *bannerSuperView;    /* 需要显示该提示的view */
    
    BOOL isFinished;            /* 是否已经停止 */
}
@end


@implementation ReachabilityHudView


- (id)init
{
	if (self = [super init])
    {
        
    }
    return self;
}


/* 
 设置一个网络监控的提示
 bannerSupView:提示框会在该页面展示
 */
- (void)initReachablityHud:(UIView *)bannerSupView
{
    bannerSuperView = bannerSupView;
    isFinished = YES;
    
    /* 注册网络状态通知 */
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    
    /* Block方式获取网络状态 */
    Reachability * reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    reach.reachableBlock = ^(Reachability * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //  ULog(@"Block Says Reachable");
            
        });
    };
    
    reach.unreachableBlock = ^(Reachability * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //  ULog(@"Block Says Unreachable");
            
        });
    };
    
    [reach startNotifier];
    
}


/* 网络变化通知 */
-(void)reachabilityChanged:(NSNotification*)note
{
    Reachability * reach = [note object];
    
    if([reach isReachable])
    {
        // ULog(@"Notification Says Reachable");
        if ([reach isReachableViaWiFi])
        {
          //  ULog(@" WIFI");
        }
        else if ([reach isReachableViaWWAN])
        {
          //  ULog(@" 3G");
        }
        
    }
    else
    {
        // ULog(@"Notification Says Unreachable");
    }
    
    [self performSelector:@selector(showBanner) withObject:nil afterDelay:0.1];
    // [self performSelector:@selector(hideBanner) withObject:nil afterDelay:0.1];
}


- (void)showBanner
{
    if (isFinished == NO)
    {
        return;
    }
    
    if (bannerView == nil)
    {
        bannerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - BANNERVIEW_HEIGHT, BANNERVIEW_WIDTH, BANNERVIEW_HEIGHT)];
        bannerView.backgroundColor = [UIColor redColor];
    }
    [bannerSuperView addSubview:bannerView];
    bannerView.hidden = NO;
    
    isFinished = NO;
    
    [UIView animateWithDuration:0.7
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         bannerView.frame = CGRectMake(0, 0, 320, 40);
                         
                     }
                     completion:^(BOOL finished) {
                         
                         [self cancel];
                         
                         [self performSelector:@selector(hideBanner) withObject:nil afterDelay:1.5];
                         
                     }];
}


- (void)hideBanner
{
    if (bannerView == nil)
    {
        return;
    }
    
    [UIView animateWithDuration:0.7
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         bannerView.frame = CGRectMake(0, -40, 320, 40);
                         
                     }
                     completion:^(BOOL finished) {
                         
                         [self cancel];
                         
                         isFinished = YES;
                         
                         bannerView.hidden = YES;
                         [bannerView removeFromSuperview];
                     }];
}


- (void)cancel
{
    
    [ReachabilityHudView cancelPreviousPerformRequestsWithTarget:self selector:@selector(showBanner) object:nil];
    [ReachabilityHudView cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideBanner) object:nil];
}




@end


@implementation NSObject (ReachablityHud)



@end
