//
//  FileDownLoadAFRequest.h
//  IOSFramework
//
//  Created by tiannu on 13-9-11.
//  Copyright (c) 2013年 allianture. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^FileDownLoadProgressDataBlock)(float percentDone);

@interface FileDownLoadAFRequestQueue : NSOperationQueue
{
    NSMutableArray *operationArrM;
}

// 添加一个下载线程
- (void)addFileDownOperationWithFileName:(NSString *)tempFileName
                              WithFolder:(NSString *)tempFolderName
                                 withURL:(NSString *)tempURL
                          withOnlyTarget:(NSString *)tempTarget
                             withSuccess:(void (^)(id JSON))success
                                withFail:(void (^)(NSError *error))fail
                        progessDataBlock:(FileDownLoadProgressDataBlock)tempBlock;

// 取消特定的下载请求
- (void)cancelSpecifyDownOperationWithTarget:(NSString *)tempTarget;

/* 暂停特定的下载请求 */
- (void)pauseSpecifyDownOperationWithTarget:(NSString *)tempTarget;

/* 恢复暂停的特定的下载请求 */
- (void)resumeSpecifyDownOperationWithTarget:(NSString *)tempTarget;

// 停止所有的请求
- (void)stopAllDownLoad;

// 开始所有的请求
- (void)startAllDownLoad;

@end
