//
//  FileDownLoadAFRequest.m
//  IOSFramework
//
//  Created by tiannu on 13-9-11.
//  Copyright (c) 2013年 allianture. All rights reserved.
//

#import "FileDownLoadAFRequestQueue.h"

@implementation FileDownLoadAFRequestQueue

- (id)init
{
    if (self = [super init])
    {
        self.maxConcurrentOperationCount = 20;
    }
    return self;
}

/***********************************************************************
 * 方法名称： addFileDownOperationWithFileName
 * 功能描述： 添加一个下载线程
 * 输入参数： tempFileName:存储的文件名
            tempFolderName:文件所在文件夹名称
            tempURL:请求的url
            tempTarget:文件的唯一标识
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (void)addFileDownOperationWithFileName:(NSString *)tempFileName
                              WithFolder:(NSString *)tempFolderName
                                 withURL:(NSString *)tempURL
                          withOnlyTarget:(NSString *)tempTarget
                             withSuccess:(void (^)(id JSON))success
                                withFail:(void (^)(NSError *error))fail
                        progessDataBlock:(FileDownLoadProgressDataBlock)tempBlock
{
    // URL
    if (tempURL == nil)
    {
        return;
    }
    
    tempURL = [tempURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    STRING_NIL_TO_NONE(tempURL);
    
    // 超时时间
    NSInteger timeOutInt = 60*10;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:tempURL] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:timeOutInt];
    
    //配置下载文件 位置
    NSString *finialFilePath = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLogic] creatFileDownLoadDocuments:tempFolderName WithFileName:tempFileName]];
    STRING_NIL_TO_NONE(finialFilePath);
    
    /* 创建网络下载对象 */
    AFSessionManager *operation = [AFSessionManager manager];
    
    // 设置文件的唯一标识
    operation.targetOnlyStr = tempTarget;
    
    NSURLSessionDownloadTask *session = [operation downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {

        // 配置下载 百分比
        float percentDone = downloadProgress.completedUnitCount/downloadProgress.totalUnitCount;
        
        NSLog(@"================= %f =================",percentDone);
        
        tempBlock(percentDone);
        
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        
        //设定下载位置
        return [NSURL fileURLWithPath:finialFilePath];
        
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        
        if (error)
        {
            fail(error);
        }
        else
        {
            success(response);
        }
        
        NSLog(@"Successfully downloaded file to %@", filePath);
        
    }];
    
    [session resume];
}

///***********************************************************************
// * 方法名称： cancelSpecifyDownOperation
// * 功能描述： 取消特定的下载请求
// * 输入参数： tempTarget:文件的唯一标识
// * 输出参数：
// * 返 回 值：
// * 其它说明：
// ***********************************************************************/
//- (void)cancelSpecifyDownOperationWithTarget:(NSString *)tempTarget
//{
//    if (STRING_ISNIL(tempTarget))
//    {
//        return;
//    }
//
//    for (AFHTTPRequestOperation *tempOperation in self.operations)
//    {
//        if ([tempOperation.targetOnlyStr isEqualToString:tempTarget])
//        {
//            if (tempOperation.isFinished == NO)
//            {
//                [tempOperation cancel];
//
//                return;
//            }
//        }
//    }
//}
//
///***********************************************************************
// * 方法名称： pauseSpecifyDownOperationWithTarget
// * 功能描述： 暂停特定的下载请求
// * 输入参数： tempTarget:文件的唯一标识
// * 输出参数：
// * 返 回 值：
// * 其它说明：
// ***********************************************************************/
//- (void)pauseSpecifyDownOperationWithTarget:(NSString *)tempTarget
//{
//    if (STRING_ISNIL(tempTarget))
//    {
//        return;
//    }
//
//    for (AFHTTPRequestOperation *tempOperation in self.operations)
//    {
//        if ([tempOperation.targetOnlyStr isEqualToString:tempTarget])
//        {
//            if (tempOperation.isFinished == NO) {
//                [tempOperation pause];
//
//                return;
//            }
//        }
//    }
//}
//
///***********************************************************************
// * 方法名称： resumeSpecifyDownOperationWithTarget
// * 功能描述： 恢复暂停的特定的下载请求
// * 输入参数： tempTarget:文件的唯一标识
// * 输出参数：
// * 返 回 值：
// * 其它说明：
// ***********************************************************************/
//- (void)resumeSpecifyDownOperationWithTarget:(NSString *)tempTarget
//{
//    if (STRING_ISNIL(tempTarget))
//    {
//        return;
//    }
//
//    for (AFHTTPRequestOperation *tempOperation in self.operations)
//    {
//        if ([tempOperation.targetOnlyStr isEqualToString:tempTarget])
//        {
//            if (tempOperation.isPaused)
//            {
//                [tempOperation resume];
//
//                return;
//            }
//        }
//    }
//}
//
///***********************************************************************
// * 方法名称： stopAllDownLoad
// * 功能描述： 停止所有的请求
// * 输入参数：
// * 输出参数：
// * 返 回 值：
// * 其它说明：
// ***********************************************************************/
//- (void)stopAllDownLoad
//{
//    for (AFHTTPRequestOperation *tempOperation in self.operations)
//    {
//        // 暂停执行中的线程
//        if (tempOperation.isExecuting) {
//            [tempOperation pause];
//        }
//    }
//
//    // 暂停队列
//    [self setSuspended:YES];
//}
//
///***********************************************************************
// * 方法名称： startAllDownLoad
// * 功能描述： 开始所有的请求
// * 输入参数：
// * 输出参数：
// * 返 回 值：
// * 其它说明：
// ***********************************************************************/
//- (void)startAllDownLoad
//{
//    for (AFHTTPRequestOperation *tempOperation in self.operations)
//    {
//        // 启动暂停的执行中的线程
//        if (tempOperation.isPaused) {
//            [tempOperation resume];
//        }
//    }
//
//    // 重新开启队列
//    [self setSuspended:NO];
//}

@end
