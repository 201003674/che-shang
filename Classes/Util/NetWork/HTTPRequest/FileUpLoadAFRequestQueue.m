//
//  FileUpLoadAFRequestQueue.m
//  IOSFramework
//
//  Created by 林科 on 14-11-26.
//  Copyright (c) 2014年 allianture. All rights reserved.
//

#import "FileUpLoadAFRequestQueue.h"

@implementation FileUpLoadAFRequestQueue

- (id)init
{
    if (self = [super init])
    {
        self.operationQueue.maxConcurrentOperationCount = 200;
    }
    
    return self;
}

/***********************************************************************
 * 方法名称： addFileUpOperationWithFileName
 * 功能描述： 添加一个上传线程
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (void)addFileUpOperationWithFileName:(NSString *)tempFileName
                        parameters:(NSDictionary *)requestobjDic
                          withFileData:(NSData *)fileData
                        withOnlyTarget:(NSString *)tempTarget
                           withSuccess:(void (^)(id JSON))success
                              withFail:(void (^)(NSError *error))fail
                      progessDataBlock:(FileUpLoadProgressDataBlock)tempBlock
{    
    NSString *strUrl = [NSString stringWithFormat:@"%@",[requestobjDic objectForKey:@"url"]];
    STRING_NIL_TO_NONE(strUrl);
    
    NSString *platType = [[[ClassFactory getInstance] getLocalCfg] getPlatType];
    STRING_NIL_TO_NONE(platType);
    NSString *version = [[[ClassFactory getInstance] getLocalCfg]getVersionCode];
    STRING_NIL_TO_NONE(version);
    NSString *projectType = [[[ClassFactory getInstance] getLocalCfg] getProjectType];
    STRING_NIL_TO_NONE(projectType);
    NSString *userToken = [[[ClassFactory getInstance] getLocalCfg] getUserToken];
    STRING_NIL_TO_NONE(userToken);
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                projectType,@"projectType",
                                platType,@"platType",
                                version,@"version",
                                userToken,@"userToken",
                                requestobjDic,@"requestObject",
                                nil];
 
    NSString *urlstring = [NSString stringWithFormat:@"%@requestMessage=%@",strUrl,[parameters JSONString]];
    STRING_NIL_TO_NONE(urlstring);
    
    urlstring = [urlstring stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFSessionManager *manager = [AFSessionManager manager];
    
    //发送格式
    
    //接受格式
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //进程标签
    manager.targetOnlyStr = tempTarget;
    
    [manager POST:urlstring parameters:requestobjDic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        /*
         此方法参数
         1. 要上传的[二进制数据]
         2. 对应网站上[upload.php中]处理文件的[字段"file"]
         3. 要保存在服务器上的[文件名]
         4. 上传文件的[mimeType]
         
         */
        
        [Log logWithSessionManager:manager WithRequestBean:requestobjDic];
        
        [formData appendPartWithFileData:fileData name:@"file" fileName:tempFileName mimeType:@"image/png"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        // 配置上传 百分比
        float percentDone = uploadProgress.completedUnitCount/uploadProgress.totalUnitCount;
        
        NSLog(@"百分比 ====================%f",percentDone);
        
        tempBlock(percentDone);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
              
        if (success)
        {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (fail)
        {
            fail(error);
        }
        
        NSString *errorInfo = [NSString stringWithFormat:@"%@",error.localizedDescription];
        STRING_NIL_TO_NONE(errorInfo);
        
        [[[ClassFactory getInstance] getInfoHUD] showHud:errorInfo];
    }];
}

/***********************************************************************
 * 方法名称： addFileUpOperationWithFileName
 * 功能描述： 添加一个上传线程 支持多张上传
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (void)addFileUpOperationWithFileMutArr:(NSArray *)tempFileMutArr
                            parameters:(NSDictionary *)requestobjDic
                        withOnlyTarget:(NSString *)tempTarget
                           withSuccess:(void (^)(id JSON))success
                              withFail:(void (^)(NSError *error))fail
                      progessDataBlock:(FileUpLoadProgressDataBlock)tempBlock
{
    NSString *strUrl = [NSString stringWithFormat:@"%@",[requestobjDic objectForKey:@"url"]];
    STRING_NIL_TO_NONE(strUrl);
    
    NSString *platType = [[[ClassFactory getInstance] getLocalCfg] getPlatType];
    STRING_NIL_TO_NONE(platType);
    NSString *version = [[[ClassFactory getInstance] getLocalCfg]getVersionCode];
    STRING_NIL_TO_NONE(version);
    NSString *projectType = [[[ClassFactory getInstance] getLocalCfg] getProjectType];
    STRING_NIL_TO_NONE(projectType);
    NSString *userToken = [[[ClassFactory getInstance] getLocalCfg] getUserToken];
    STRING_NIL_TO_NONE(userToken);
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                projectType,@"projectType",
                                platType,@"platType",
                                version,@"version",
                                userToken,@"userToken",
                                requestobjDic,@"requestObject",
                                nil];
    
    NSString *urlstring = [NSString stringWithFormat:@"%@requestMessage=%@",strUrl,[parameters JSONString]];
    STRING_NIL_TO_NONE(urlstring);
    
    urlstring = [urlstring stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFSessionManager *operationManager = [AFSessionManager manager];
    
    //发送格式
    [operationManager.requestSerializer setHTTPShouldHandleCookies:YES];
    
    NSString *CookieString = @"";
    NSHTTPCookieStorage *cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    for (NSHTTPCookie *cookie in [cookies cookiesForURL:[NSURL URLWithString:urlstring]])
    {
        CookieString = [NSString stringWithFormat:@"%@%@=%@;",CookieString,cookie.name,cookie.value];
        STRING_NIL_TO_NONE(CookieString);
    }
    [operationManager.requestSerializer setValue:CookieString forHTTPHeaderField:@"Cookie"];
    
    //设置响应内容格式  经常因为服务器返回的格式不是标准json而出错 服务器可能返回text/html text/plain 作为json
    [operationManager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", @"text/plain",@"application/javascript",nil]];
    //进程标签
    [operationManager setTargetOnlyStr:tempTarget];
    
    [operationManager POST:urlstring parameters:requestobjDic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        /*
         此方法参数
         1. 要上传的[二进制数据]
         2. 对应网站上[upload.php中]处理文件的[字段"file"]
         3. 要保存在服务器上的[文件名]
         4. 上传文件的[mimeType]
         
         */
        for (imageUploadModel *imageModel in tempFileMutArr)
        {
            NSData *imageData = UIImageJPEGRepresentation(imageModel.image, 1.0);
            
            //进行图片像素限制压缩 320万
            if (imageModel.image.size.width * imageModel.image.size.height > MAX_PIXEL)
            {
                UIImage *finialImage = [UIImage compressImage:imageModel.image toPixel:MAX_PIXEL];
                
                NSLog(@"压缩=======width:%f height:%f  %@",finialImage.size.width,finialImage.size.height,imageModel.imageName);
                
                imageData = UIImageJPEGRepresentation(finialImage, 1.0);
            }
            else
            {
                NSLog(@"不压缩=======width:%f height:%f   %@",imageModel.image.size.width,imageModel.image.size.height,imageModel.imageName);
                
                imageData = UIImageJPEGRepresentation(imageModel.image, 1.0);
            }
            
            [Log logWithSessionManager:operationManager WithRequestBean:requestobjDic];
            
            [formData appendPartWithFileData:imageData name:imageModel.imageName fileName:imageModel.imageName mimeType:@"image/png"];
        }
        
        //车E保大保单
        NSString *policyID = [NSString stringWithFormat:@"%@",[requestobjDic objectForKey:@"policyID"]];
        STRING_NIL_TO_NONE(policyID);
        [formData appendPartWithFormData:[policyID dataUsingEncoding:NSUTF8StringEncoding] name:@"policyID"];
        
        //图片批次
        NSString *imgIndex = [NSString stringWithFormat:@"%@",[requestobjDic objectForKey:@"imgIndex"]];
        STRING_NIL_TO_NONE(imgIndex);
        [formData appendPartWithFormData:[imgIndex dataUsingEncoding:NSUTF8StringEncoding] name:@"imgIndex"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        // 配置上传 百分比
        float percentDone = uploadProgress.completedUnitCount/uploadProgress.totalUnitCount;
        
        NSLog(@"百分比 ====================%f",percentDone);
        
        tempBlock(percentDone);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success)
        {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (fail)
        {
            fail(error);
        }
        
        NSString *errorInfo = [NSString stringWithFormat:@"%@",error.localizedDescription];
        STRING_NIL_TO_NONE(errorInfo);
        
        [[[ClassFactory getInstance] getInfoHUD] showHud:errorInfo];
    }];
}

/***********************************************************************
 * 方法名称： cancelSpecifyUpOperationWithTarget
 * 功能描述： 取消特定的上传请求
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (void)cancelSpecifyUpOperationWithTarget:(NSString *)tempTarget
{
    if (STRING_ISNIL(tempTarget))
    {
        return;
    }
    
    [self.uploadTasks enumerateObjectsUsingBlock:^(NSURLSessionUploadTask * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([obj.taskDescription isEqualToString:tempTarget])
        {
            [obj cancel];
            return;
        }
    }];
}

/***********************************************************************
 * 方法名称： pauseSpecifyUpOperationWithTarget
 * 功能描述： 暂停特定的上传请求
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (void)pauseSpecifyUpOperationWithTarget:(NSString *)tempTarget
{
    if (STRING_ISNIL(tempTarget))
    {
        return;
    }
    
    [self.uploadTasks enumerateObjectsUsingBlock:^(NSURLSessionUploadTask * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([obj.taskDescription isEqualToString:tempTarget])
        {
            [obj suspend];
            return;
        }
    }];
}

/***********************************************************************
 * 方法名称： resumeSpecifyUpOperationWithTarget
 * 功能描述： 恢复暂停的特定的上传请求
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (void)resumeSpecifyUpOperationWithTarget:(NSString *)tempTarget
{
    if (STRING_ISNIL(tempTarget))
    {
        return;
    }
    
    
    [self.uploadTasks enumerateObjectsUsingBlock:^(NSURLSessionUploadTask * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([obj.taskDescription isEqualToString:tempTarget])
        {
            [obj resume];
            return;
        }
    }];
}

/***********************************************************************
 * 方法名称： stopAllUpLoad
 * 功能描述： 停止所有的请求
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (void)stopAllUpLoad
{
    [self.uploadTasks enumerateObjectsUsingBlock:^(NSURLSessionUploadTask * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [obj suspend];
        
        return;
    }];
}

/***********************************************************************
 * 方法名称： startAllUpLoad
 * 功能描述： 开始所有的请求
 * 输入参数：
 * 输出参数：
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (void)startAllUpLoad
{
    [self.uploadTasks enumerateObjectsUsingBlock:^(NSURLSessionUploadTask * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [obj resume];
        
        return;
    }];
}

@end
