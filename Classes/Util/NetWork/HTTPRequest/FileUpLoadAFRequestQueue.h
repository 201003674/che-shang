//
//  FileUpLoadAFRequestQueue.h
//  IOSFramework
//
//  Created by 林科 on 14-11-26.
//  Copyright (c) 2014年 allianture. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^FileUpLoadProgressDataBlock)(float percentDone);

#import "AFSessionManager.h"
@interface FileUpLoadAFRequestQueue : AFSessionManager
{
    
}

// 添加一个上传线程
- (void)addFileUpOperationWithFileName:(NSString *)tempFileName
                            parameters:(NSDictionary *)requestobjDic
                             withFileData:(NSData *)fileData
                        withOnlyTarget:(NSString *)tempTarget
                           withSuccess:(void (^)(id JSON))success
                              withFail:(void (^)(NSError *error))fail
                      progessDataBlock:(FileUpLoadProgressDataBlock)tempBlock;

//添加多张上传线程
- (void)addFileUpOperationWithFileMutArr:(NSArray *)tempFileMutArr
                              parameters:(NSDictionary *)requestobjDic
                          withOnlyTarget:(NSString *)tempTarget
                             withSuccess:(void (^)(id JSON))success
                                withFail:(void (^)(NSError *error))fail
                        progessDataBlock:(FileUpLoadProgressDataBlock)tempBlock;

// 取消特定的上传请求
- (void)cancelSpecifyUpOperationWithTarget:(NSString *)tempTarget;

/* 暂停特定的上传请求 */
- (void)pauseSpecifyUpOperationWithTarget:(NSString *)tempTarget;

/* 恢复暂停的特定的上传请求 */
- (void)resumeSpecifyUpOperationWithTarget:(NSString *)tempTarget;

// 停止所有的请求
- (void)stopAllUpLoad;

// 开始所有的请求
- (void)startAllUpLoad;


@end
