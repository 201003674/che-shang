/*********************************************************************
 * 版权所有 LK
 *
 * 文件名称：
 * 文件标识：
 * 内容摘要：网络请求类 调用AFNetworking
 * 其它说明：
 * 当前版本：
 * 作   者：林科
 * 完成日期：
 **********************************************************************/


/***************************************************************************
 *                                文件引用
 ***************************************************************************/
#import <Foundation/Foundation.h>


/***************************************************************************
 *                                 类引用
 ***************************************************************************/
#import <Reachability/Reachability.h>

#import "FMDBSqlite.h"
#import "XMLReader.h"

#import "ClassFactory.h"
#import "UICustomDefine.h"
#import "JSONKit.h"
#import "AFSessionManager.h"

/***************************************************************************
 *                                 宏定义
 ***************************************************************************/


/***************************************************************************
 *                                 常量
 ***************************************************************************/


/***************************************************************************
 *                                类型定义
 ***************************************************************************/


/***************************************************************************
 *                                 类定义
 ***************************************************************************/
@interface afHttpRequest : NSObject
{
    AFSessionManager *operationManager;
}

@end


@interface NSObject (afHttpRequest_object)


/* GET请求方式 不设置超时时长请求 */
- (void)getRequestWithUrl:(NSString *)urlStr
                     path:(NSString *)strUrl
               parameters:(NSDictionary *)parameters
                  success:(void (^)(id JSON))success
                  failure:(void (^)(NSError *error, id JSON))failure;

/* GET请求方式 设置超时时长请求 */
- (void)getRequestWithUrl:(NSString *)urlStr
                     path:(NSString *)strUrl
               parameters:(NSDictionary *)parameters
                  timeOut:(NSInteger )timeOutInt
                  success:(void (^)(id JSON))success
                  failure:(void (^)(NSError *error, id JSON))failure;

/* GET请求方式（XML数据），不设置超时 */
- (void)getXMLRequestWithUrl:(NSString *)urlStr
                        path:(NSString *)strUrl
                  parameters:(NSDictionary *)parameters
                     success:(void (^)(id JSON))success
                     failure:(void (^)(NSError *error))failure;

/* GET请求方式（XML数据），设置超时*/
- (void)getXMLRequestWithUrl:(NSString *)urlStr
                        path:(NSString *)strUrl
                  parameters:(NSDictionary *)parameters
                     timeOut:(NSInteger )timeOutInt
                     success:(void (^)(id JSON))success
                     failure:(void (^)(NSError *error))failure;


/* POST请求方式 不设置超时时长请求*/
- (void)postRequestWithUrl:(NSString *)urlStr
                      path:(NSString *)path
                parameters:(NSDictionary *)parameters
                   success:(void (^)(id JSON))success
                   failure:(void (^)(NSError *error, id JSON))failure;


/* POST请求方式 设置超时时长请求*/
- (void)postRequestWithUrl:(NSString *)urlStr
                      path:(NSString *)path
                parameters:(NSDictionary *)parameters
                   timeOut:(NSInteger )timeOutInt
                   success:(void (^)(id JSON))success
                   failure:(void (^)(NSError *error, id JSON))failure;

/* POST请求方式（XML数据），不设置超时 */
- (void)postXMLRequestWithUrl:(NSString *)urlStr
                         path:(NSString *)path
                   parameters:(NSDictionary *)parameters
                      success:(void (^)(id JSON))success
                      failure:(void (^)(NSError *error))failure;

/* POST请求方式（XML数据），设置超时 */
- (void)postXMLRequestWithUrl:(NSString *)urlStr
                         path:(NSString *)path
                   parameters:(NSDictionary *)parameters
                      timeOut:(NSInteger )timeOutInt
                      success:(void (^)(id JSON))success
                      failure:(void (^)(NSError *error))failure;

/* POST请求方式（Soap），不设置超时 */
- (void)postSoapRequestWithUrl:(NSString *)urlStr
                          path:(NSString *)path
                    parameters:(NSDictionary *)parameters
                       success:(void (^)(id JSON))success
                       failure:(void (^)(NSError *error))failure;

/* POST请求方式（Soap），设置超时*/
- (void)postSoapRequestWithUrl:(NSString *)urlStr
                          path:(NSString *)path
                    parameters:(NSDictionary *)parameters
                       timeOut:(NSInteger )timeOutInt
                       success:(void (^)(id JSON))success
                       failure:(void (^)(NSError *error))failure;

@end




