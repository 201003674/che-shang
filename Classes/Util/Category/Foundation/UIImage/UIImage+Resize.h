// UIImage+Resize.h
// Created by Trevor Harmon on 8/5/09.
// Free for personal or commercial use, with or without modification.
// No warranty is expressed or implied.

// Extends the UIImage class to support resizing/cropping
@interface UIImage (Resize)
- (UIImage *)croppedImage:(CGRect)bounds;
- (UIImage *)thumbnailImage:(NSInteger)thumbnailSize
          transparentBorder:(NSUInteger)borderSize
               cornerRadius:(NSUInteger)cornerRadius
       interpolationQuality:(CGInterpolationQuality)quality;
- (UIImage *)resizedImage:(CGSize)newSize
     interpolationQuality:(CGInterpolationQuality)quality;
- (UIImage *)resizedImageWithContentMode:(UIViewContentMode)contentMode
                                  bounds:(CGSize)bounds
                    interpolationQuality:(CGInterpolationQuality)quality;

/*!
 *
 *  压缩图片至目标尺寸
 *
 *  @param sourceImage 源图片
 *  @param targetWidth 图片最终尺寸的宽
 *
 *  @return 返回按照源图片的宽、高比例压缩至目标宽、高的图片
 */
+ (UIImage *)compressImage:(UIImage *)sourceImage toTargetWidth:(CGFloat)targetWidth;

+ (UIImage *)scaleFromImage:(UIImage *)image;

/*!
 *  @author 林科, 15-12-01 16:12:01
 *
 *  压缩图片至目标大小
 *
 *  @param image 源图片
 *  @param maxLength 图片阈值大小
 *
 *  @return 返回按照源图片的宽、高比例压缩至目标宽、高的图片
 */
+ (UIImage *)compressImage:(UIImage *)image toByte:(NSUInteger)maxLength;

/*!
 *  @author 林科, 15-12-01 16:12:01
 *
 *  压缩图片至目标大小
 *
 *  @param image 源图片
 *  @param maxPixel 图片像素阈值大小
 *
 *  @return 返回按照源图片的宽、高比例压缩至目标宽、高的图片
 */
+ (UIImage *)compressImage:(UIImage *)sourceImage toPixel:(NSUInteger)maxPixel;

@end
