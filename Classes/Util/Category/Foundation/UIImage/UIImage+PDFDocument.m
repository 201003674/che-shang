//
//  UIImage+PDFDocument.m
//  IOSFramework
//
//  Created by 林科 on 2017/10/19.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import "UIImage+PDFDocument.h"

@implementation UIImage (PDFDocument)

CGContextRef CreateARGBBitmapContext (size_t pixelsWide, size_t pixelsHigh)
{
    CGContextRef    context = NULL;
    CGColorSpaceRef colorSpace;
    void *          bitmapData;
    int             bitmapByteCount;
    int             bitmapBytesPerRow;
    
    // Get image width, height. We’ll use the entire image.
    //  size_t pixelsWide = CGImageGetWidth(inImage);
    //  size_t pixelsHigh = CGImageGetHeight(inImage);
    
    // Declare the number of bytes per row. Each pixel in the bitmap in this
    // example is represented by 4 bytes; 8 bits each of red, green, blue, and
    // alpha.
    bitmapBytesPerRow   = (pixelsWide * 4);
    bitmapByteCount     = (bitmapBytesPerRow * pixelsHigh);
    
    // Use the generic RGB color space.
    //colorSpace = CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB);
    colorSpace = CGColorSpaceCreateDeviceRGB();
    if (colorSpace == NULL)
    {
        fprintf(stderr, "Error allocating color space\n");
        return NULL;
    }
    
    // Allocate memory for image data. This is the destination in memory
    // where any drawing to the bitmap context will be rendered.
    if (sizeof(bitmapData))
    {
        //NSLog(@"size %d",bitmapData);
        //free(bitmapData);
    }
    bitmapData = malloc( bitmapByteCount );
    if (bitmapData == NULL)
    {
        fprintf (stderr, "Memory not allocated!");
        CGColorSpaceRelease( colorSpace );
        return NULL;
    }
    
    // Create the bitmap context. We want pre-multiplied ARGB, 8-bits
    // per component. Regardless of what the source image format is
    // (CMYK, Grayscale, and so on) it will be converted over to the format
    // specified here by CGBitmapContextCreate.
    context = CGBitmapContextCreate (bitmapData,
                                     pixelsWide,
                                     pixelsHigh,
                                     8,      // bits per component
                                     bitmapBytesPerRow,
                                     colorSpace,
                                     kCGImageAlphaPremultipliedFirst);
    if (context == NULL)
    {
        free (bitmapData);
        fprintf (stderr, "Context not created!");
    }
    
    // Make sure and release colorspace before returning
    CGColorSpaceRelease( colorSpace );
    
    return context;
}

CGImageRef PDFPageToCGImage(size_t pageNumber, CGPDFDocumentRef document)

{
    CGPDFPageRef    page;
    CGRect        pageSize;
    CGContextRef    outContext;
    CGImageRef    ThePDFImage;
    //CGAffineTransform ctm;
    page = CGPDFDocumentGetPage (document, pageNumber);
    if(page)
    {
        pageSize = CGPDFPageGetBoxRect(page, kCGPDFMediaBox);
        
        outContext= CreateARGBBitmapContext (pageSize.size.width, pageSize.size.height);
        if(outContext)
        {
            CGContextDrawPDFPage(outContext, page);
            ThePDFImage= CGBitmapContextCreateImage(outContext);
            int *buffer;
            
            buffer = CGBitmapContextGetData(outContext);
            //NSLog(@"%d",buffer);
            free(buffer);
            
            CGContextRelease(outContext);
            CGPDFPageRelease(page);
            return ThePDFImage;
        }
    }
    return NULL;
} 

@end
