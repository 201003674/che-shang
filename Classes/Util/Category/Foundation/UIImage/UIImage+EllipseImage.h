//
//  UIImage+EllipseImage.h
//  IOSFramework
//
//  Created by 林科 on 15/12/14.
//  Copyright © 2015年 allianture. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (EllipseImage)

+ (UIImage *)ellipseImage:(UIImage *)image withInset:(CGFloat)inset withBorderWidth:(CGFloat)width withBorderColor:(UIColor*)color;

@end
