//
//  UIImage+PDFDocument.h
//  IOSFramework
//
//  Created by 林科 on 2017/10/19.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (PDFDocument)

CGImageRef PDFPageToCGImage(size_t pageNumber, CGPDFDocumentRef document);


@end
