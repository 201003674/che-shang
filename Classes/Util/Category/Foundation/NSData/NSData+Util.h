//
//  NSData+Util.h
//  IOSFramework
//
//  Created by 林科 on 2019/5/20.
//  Copyright © 2019 allianture. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSData (Util)

+ (NSData *)compressImage:(UIImage *)image toByte:(NSUInteger)maxLength;

@end

NS_ASSUME_NONNULL_END
