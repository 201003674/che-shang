//
//  NSData+Util.m
//  IOSFramework
//
//  Created by 林科 on 2019/5/20.
//  Copyright © 2019 allianture. All rights reserved.
//

#import "NSData+Util.h"

@implementation NSData (Util)

/*!
 *  @author 林科, 15-12-01 16:12:01
 *
 *  压缩图片至目标大小
 *
 *  @param image 源图片
 *  @param maxLength 图片阈值大小
 *
 *  @return 返回按照源图片的宽、高比例压缩至目标宽、高的图片
 */
+ (NSData *)compressImage:(UIImage *)image toByte:(NSUInteger)maxLength
{
    // Compress by quality
    CGFloat compression = 1;
    NSData *data = UIImageJPEGRepresentation(image, compression);
    NSLog(@"开始压缩%lu",(unsigned long)data.length);
    if (data.length < maxLength)
    {
        return data;
    }
    
    CGFloat max = 1, min = 0;
    
    while (data.length > maxLength)
    {
        compression = (max + min) / 2;
        data = UIImageJPEGRepresentation(image, compression);
        
        if (data.length < maxLength * 0.9)
        {
            min = compression;
        }
        else if (data.length > maxLength)
        {
            max = compression;
        }
        else
        {
            break;
        }
    }
    NSLog(@"质量压缩后%lu",(unsigned long)data.length);
    return data;
}

@end
