//
//  NSString+JSONCategories.h
//  IOSFramework
//
//  Created by 林科 on 2017/6/21.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (JSONCategories)

-(id)JSONValue;

@end
