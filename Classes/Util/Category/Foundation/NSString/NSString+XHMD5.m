//
//  NSString+XHMD5.m
//  XHImageViewer
//
//  Created by 曾 宪华 on 14-2-18.
//  Copyright (c) 2014年 曾宪华 开发团队(http://iyilunba.com ) 本人QQ:543413507 本人QQ群（142557668）. All rights reserved.
//

#import "NSString+XHMD5.h"
#import <CommonCrypto/CommonCrypto.h>

@implementation NSString (XHMD5)

- (NSString *)MD5Hash {
    if(self.length == 0) {
        return nil;
    }
    
	const char *cStr = [self UTF8String];
	unsigned char result[16];
	CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
    
	return [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],result[12], result[13], result[14], result[15]];
}

- (NSString *)shortUrl
{
    NSArray *chars = [[NSArray alloc] initWithObjects:@"a" , @"b" , @"c" , @"d" , @"e" , @"f" , @"g" , @"h" ,
                      
                      @"i" , @"j" , @"k" , @"l" , @"m" , @"n" , @"o" , @"p" , @"q" , @"r" , @"s" , @"t" ,
                      
                      @"u" , @"v" , @"w" , @"x" , @"y" , @"z" , @"0" , @"1" , @"2" , @"3" , @"4" , @"5" ,
                      
                      @"6" , @"7" , @"8" , @"9" , @"A" , @"B" , @"C" , @"D" , @"E" , @"F" , @"G" , @"H" ,
                      
                      @"I" , @"J" , @"K" , @"L" , @"M" , @"N" , @"O" , @"P" , @"Q" , @"R" , @"S" , @"T" ,
                      
                      @"U" , @"V" , @"W" , @"X" , @"Y" , @"Z", nil];
    
    NSString *key = @"1234567890";
    
    NSString *hex = [NSString stringWithFormat:@"%@",[[key stringByAppendingFormat:@"%@",self] MD5Hash]];
    
    NSMutableArray *resUrl = [[NSMutableArray alloc] initWithCapacity:4];
    
    for (int i=0; i<4; i++) {
        
        // 把加密字符按照 8 位一组 16 进制与 0x3FFFFFFF 进行位与运算
        
        NSString *sTempSubString = [hex substringWithRange:NSMakeRange(i*8, 8)];
        
        
        
        // 这里需要使用 long 型来转换，因为 Inteper只能处理 31 位 , 首位为符号位 , 如果不用 long ，则会越界
        
        long longOfTemp;
        
        sscanf([sTempSubString cStringUsingEncoding:NSASCIIStringEncoding], "%lx", &longOfTemp);
        
        long lHexLong = 0x3FFFFFFF & longOfTemp;
        
        NSString *outChars = @"";
        
        for (int j=0; j<6; j++) {
            
            // 把得到的值与 0x0000003D 进行位与运算，取得字符数组 chars 索引
            
            long index = 0x0000003D & lHexLong;
            
            // 把取得的字符相加
            
            outChars = [outChars stringByAppendingFormat:@"%@",[chars objectAtIndex:(int)index]];
            
            // 每次循环按位右移 5 位
            
            lHexLong = lHexLong >> 5;
            
        }
        
        // 把字符串存入对应索引的输出数组
        
        [resUrl insertObject:outChars atIndex:i];
        
    }
    
    return [resUrl objectAtIndex:0];//这里可以返回任意一个元素作为短链接（0，1，2，3）
    
}

@end
