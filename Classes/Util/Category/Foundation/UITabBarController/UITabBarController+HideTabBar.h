//
//  UITabBarController+HideTabBar.h
//  IOSFramework
//
//  Created by 林科 on 2017/9/20.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBarController (HideTabBar)

- (void)hideTabBarAnimated:(BOOL)animated;

- (void)showTabBarAnimated:(BOOL)animated;

@end
