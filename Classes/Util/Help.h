/*
asihttprequest
 
 需要添类库：CFNetwork.framework,SystemConfiguration.framework, MobileCoreServices.framework,CoreGraphics.framework和libz.1.2.3.dylib
 
  使用ASIHTTPRequest xcode编译提示找不到"libxml/HTMLparser.h",解决方法如下:
 1>.在xcode中左边选中项目的root节点,在中间编辑区的搜索框中输入"header search paths",
 双击Header Search Paths项,点击加号增加一项并输入"${SDK_DIR}/usr/include/libxml2"。
 2>.再次在搜索框中输入"other linker flags",双击Other Linker Flags项,
 点击加号增加一项并输入"-lxml2",点击done按钮结束。
 
 
 
 队列
 self.netWorkQueue  = [[ASINetworkQueue alloc] init];
 [self.netWorkQueue setDelegate:self];
 [self.netWorkQueue reset];
 [self.netWorkQueue setShouldCancelAllRequestsOnFailure:NO];
 //[self.netWorkQueue setShowAccurateProgress:YES];
 [self.netWorkQueue go];
 
 请求
 NSURL *url = [NSURL URLWithString:@"http://www.weather.com.cn/data/sk/101010100.html"];
 ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
 [request setDelegate:self];
 
 //[request startAsynchronous];
 
 [self.netWorkQueue addOperation:request];
 
 - (void)requestFinished:(ASIHTTPRequest *)request;
 - (void)requestFailed:(ASIHTTPRequest *)request;
 */


/*  
 AFNetworking框架
 
 在使用AFNetworking框架时会看到有如下告警:
 #warning SystemConfiguration framework not found in project, or not included in precompiled header. Network reachability functionality will not be available.
 #warning MobileCoreServices framework not found in project, or not included in precompiled header. Automatic MIME type detection when uploading files in multipart requests will not be available.
 AFNetworking可选的依赖SystemConfiguration框架来进行网络可达性监测,依赖MobileCoreServices对上传文件进行MIME类型检测。如果没有包含这两个框架或者没有在预编译头文件中包括就会有上述告警.
 解决方法为:
 在项目中增加SystemConfiguration和MobileCoreServices框架(对应Mac OS X，则为CoreServices).
 在项目的预编译头文件Prefix.pch中增加对应的头文件
 #import <SystemConfiguration/SystemConfiguration.h>
 #import <MobileCoreServices/MobileCoreServices.h>
 //or #import <CoreServices/CoreServices> for Mac OS X
 
 
 
 */


/*
 系统版本判定
 
 #if __IPHONE_OS_VERSION_MAX_ALLOWED < __IPHONE_6_0
 
 #else
 
 #endif
 */


/*
 jsonKit解析json
 
 NSDictionary *jsonDic = [responseData objectFromJSONData];
 
 */


/* 
 禁用arc：-fno-objc-arc 使用arc：-fobjc-arc
 
 arc判断
 
 #if __has_feature(objc_arc)
 return hud;
 #else
 return [hud autorelease];
 #endif
 
 */


/*
 显示tabbar：[[NSNotificationCenter defaultCenter] postNotificationName:@"showCustomTabBar" object:nil];
 隐藏tabbar：[[NSNotificationCenter defaultCenter] postNotificationName:@"hideCustomTabBar" object:nil];
 */


/* 
 NSLog
 
 1、如何自定义NSLog呢？
 直接在工程的XXX_Prefix.pch中加入以下语句(就相当于在全局中定义了)
 #define NSLog NSLog(@”#%s##%d#”,strrchr(__FILE__,’/'),__LINE__);
 NSLog 例如NSLog(@” debug function”);
 打印出来如下 #/main.mm##30# begin main function
 
 2、如何在发行版release中屏蔽NSLog？
 方法1: 直接在工程的XXX_Prefix.pch中加入以下语句(就相当于在全局中定义了) #define NSLog //NSLog
 方法2: 打开工程 Project—>Edit Project Editting(选着build)选项—>找到Preprocessor Macros 设置其值为DEBUG=1
 
 
 简单介绍以下几个宏：
 1) __VA_ARGS__ 是一个可变参数的宏，这个可变参数的宏是新的C99规范中新增的，目前似乎只有gcc支持（VC6.0的编译器不支持）。宏前面加上##的作用在于，当可变参数的个数为0时，这里的##起到把前面多余的","去掉,否则会编译出错。
 2) __FILE__ 宏在预编译时会替换成当前的源文件名
 3) __LINE__宏在预编译时会替换成当前的行号
 4) __FUNCTION__宏在预编译时会替换成当前的函数名称
 1.重新定义系统的NSLog，__OPTIMIZE__ 是release 默认会加的宏
 #ifndef __OPTIMIZE__
 #define NSLog(...) NSLog(__VA_ARGS__)
 #else
 #define NSLog(...){}
 #endif
 2.直接自己写#define,当release版本的时候把#define 注释掉即可
 #define IOS_DEBUG
 #ifdef IOS_DEBUG
 #define NSLog(...) NSLog(__VA_ARGS__)
 #endif
 3.
 #ifdef DEBUG
 # define DLog(format, ...) NSLog((@"[文件名:%s]" "[函数名:%s]" "[行号:%d]" format), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
 #else
 # define DLog(...)
 #endif
 这种方式需要修改项目的配置，使得在debug编译的时候，编译DLog的宏，产生详细的日志信息，而release的时候，不产生任何控制台输出。
 相比而言，还是第一种比较方便。
 
 */


/*
 判断网络相关用法
 
 [[NSNotificationCenter defaultCenter] addObserver:self
 selector:@selector(reachabilityChanged:)
 name:kNewReachabilityChangedNotification
 object:nil];
 
 Reachability * reach = [Reachability reachabilityWithHostname:@"www.google.com"];
 
 reach.reachableBlock = ^(Reachability * reachability)
 {
 dispatch_async(dispatch_get_main_queue(), ^{
 blockLabel.text = @"Block Says Reachable";
 });
 };
 
 reach.unreachableBlock = ^(Reachability * reachability)
 {
 dispatch_async(dispatch_get_main_queue(), ^{
 blockLabel.text = @"Block Says Unreachable";
 });
 };
 
 [reach startNotifier];
 
 
 */


/*
 HUD使用
 
 MBProgressHUD *HUD;
 
 - (IBAction)showSimple:(id)sender {
 // The hud will dispable all input on the view (use the higest view possible in the view hierarchy)
 HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
 [self.navigationController.view addSubview:HUD];
 
 // Regiser for HUD callbacks so we can remove it from the window at the right time
 HUD.delegate = self;
 
 // Show the HUD while the provided method executes in a new thread
 [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
 }
 
 - (IBAction)showWithLabel:(id)sender {
 
 HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
 [self.navigationController.view addSubview:HUD];
 
 HUD.delegate = self;
 HUD.labelText = @"Loading";
 
 [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
 }
 
 - (IBAction)showWithDetailsLabel:(id)sender {
 
 HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
 [self.navigationController.view addSubview:HUD];
 
 HUD.delegate = self;
 HUD.labelText = @"Loading";
 HUD.detailsLabelText = @"updating data";
 HUD.square = YES;
 
 [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
 }
 
 - (IBAction)showWithLabelDeterminate:(id)sender {
 
 HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
 [self.navigationController.view addSubview:HUD];
 
 // Set determinate mode
 HUD.mode = MBProgressHUDModeDeterminate;
 
 HUD.delegate = self;
 HUD.labelText = @"Loading";
 
 // myProgressTask uses the HUD instance to update progress
 [HUD showWhileExecuting:@selector(myProgressTask) onTarget:self withObject:nil animated:YES];
 }
 
 - (IBAction)showWithCustomView:(id)sender {
 
 HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
 [self.navigationController.view addSubview:HUD];
 
 // The sample image is based on the work by http://www.pixelpressicons.com, http://creativecommons.org/licenses/by/2.5/ca/
 // Make the customViews 37 by 37 pixels for best results (those are the bounds of the build-in progress indicators)
 HUD.customView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]] autorelease];
 
 // Set custom view mode
 HUD.mode = MBProgressHUDModeCustomView;
 
 HUD.delegate = self;
 HUD.labelText = @"Completed";
 
 [HUD show:YES];
 [HUD hide:YES afterDelay:3];
 }
 
 - (IBAction)showWithLabelMixed:(id)sender {
 
 HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
 [self.navigationController.view addSubview:HUD];
 
 HUD.delegate = self;
 HUD.labelText = @"Connecting";
 HUD.minSize = CGSizeMake(135.f, 135.f);
 
 [HUD showWhileExecuting:@selector(myMixedTask) onTarget:self withObject:nil animated:YES];
 }
 
 - (IBAction)showUsingBlocks:(id)sender {
 #ifdef __BLOCKS__
 // No need to retain (just a local variable)
 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
 hud.labelText = @"Loading";
 
 dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
 // Do a taks in the background
 [self myTask];
 // Hide the HUD in the main tread
 dispatch_async(dispatch_get_main_queue(), ^{
 [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
 });
 });
 #endif
 }
 
 - (IBAction)showOnWindow:(id)sender {
 // The hud will dispable all input on the window
 HUD = [[MBProgressHUD alloc] initWithView:self.view.window];
 [self.view.window addSubview:HUD];
 
 HUD.delegate = self;
 HUD.labelText = @"Loading";
 
 [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
 }
 
 - (IBAction)showURL:(id)sender {
 NSURL *URL = [NSURL URLWithString:@"https://github.com/matej/MBProgressHUD/zipball/master"];
 NSURLRequest *request = [NSURLRequest requestWithURL:URL];
 
 NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
 [connection start];
 [connection release];
 
 HUD = [[MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES] retain];
 }
 
 
 - (IBAction)showWithGradient:(id)sender {
 
 HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
 [self.navigationController.view addSubview:HUD];
 
 HUD.dimBackground = YES;
 
 // Regiser for HUD callbacks so we can remove it from the window at the right time
 HUD.delegate = self;
 
 // Show the HUD while the provided method executes in a new thread
 [HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
 }
 
 
 */





/*
 键盘遮挡处理方法
 
 使用方法：
 
 将IQKeyboardManager 和 IQSegmentedNextPrevious类文件加进项目中。在AppDelegate文件中写下以下一行代码：
 [IQKeyBoardManager installKeyboardManager];
 搞定！
 也可以开启或者关闭keyboard avoiding功能：
 [IQKeyBoardManager enableKeyboardManger];
 [IQKeyBoardManager disableKeyboardManager];
 
 */


/* 联系人 
 
 contactsView = [[ContactsViewController alloc] init];
 [self.navigationController pushViewController:contactsView animated:YES];
 
 */


//使用方法：

//1. 压缩:ZipArchive可以压缩多个文件,只需要把文件一一addFileToZip即可.
//
//ZipArchive* zip = [[ZipArchive alloc] init];
//NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//NSString *documentpath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
//NSString* l_zipfile = [documentpath stringByAppendingString:@"/test.zip"] ;
//
//NSString* image1 = [documentpath stringByAppendingString:@"/image1.jpg"] ;
//NSString* image2 = [documentpath stringByAppendingString:@"/image2.jpg"] ;
//
//BOOL ret = [zip CreateZipFile2:l_zipfile];
//ret = [zip addFileToZip:image1 newname:@"image1.jpg"];
//ret = [zip addFileToZip:image2 newname:@"image2.jpg"];
//if( ![zip CloseZipFile2] )
//{
//    l_zipfile = @"";
//}
//[zip release];


//2. 解压缩:
//
//ZipArchive* zip = [[ZipArchive alloc] init];
//NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//NSString *documentpath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
//
//NSString* l_zipfile = [documentpath stringByAppendingString:@"/test.zip"] ;
//NSString* unzipto = [documentpath stringByAppendingString:@"/test"] ;
//if( [zip UnzipOpenFile:l_zipfile] )
//{
//    BOOL ret = [zip UnzipFileTo:unzipto overWrite:YES];
//    if( NO==ret )
//    {
//    }
//    [zip UnzipCloseFile];
//}
//[zip release];


//自定义密码键盘
//*************************使用说明
/*
 
 CDKeyBoard *cdkey = [[CDKeyBoard alloc]initWithFrame:CGRectMake(0, 0, keyBoard_w, keyBoard_h)];
 
 
 cdkey.textfield = textfield;
 cdkey.typeKeyBoard = 1; //设置键盘类型  纯数字 和 字母数字
 
 
 */

/* 文件管理 
 
 FileManagement *filem = [FileManagement alloc]init];
 具体看FileManagement 里面说明 对文件的创建和删 等操作
 
 
 
 _emojiLabel = [[MLEmojiLabel alloc]initWithFrame:(CGRect)];
 _emojiLabel.numberOfLines = 0;
 _emojiLabel.font = [UIFont systemFontOfSize:14.0f];
 NSLog(@"%f",_emojiLabel.font.lineHeight);
 _emojiLabel.emojiDelegate = self;
 //        _emojiLabel.textAlignment = NSTextAlignmentCenter;
 _emojiLabel.backgroundColor = [UIColor clearColor];
 _emojiLabel.lineBreakMode = NSLineBreakByCharWrapping;
 _emojiLabel.isNeedAtAndPoundSign = YES;
 
 //        _emojiLabel.disableThreeCommon = YES;
 //        _emojiLabel.disableEmoji = YES;
 
// [_emojiLabel setEmojiText:@"撒萨撒旦撒旦撒达说https://github.com/molon/MLEmojiLabel阿苏打@撒旦 哈哈哈哈#撒asd#撒旦撒电话18120136012邮箱dudl@qq.com旦旦/:dsad旦/::)sss/::~啊是大三的/::B/::|/:8-)/::</::$/::X/::Z/::'(/::-|/::@/::P/::D/::O/::(/::+/:--b/::Q/::T/:,@P/:,@-D/::d/:,@o/::g/:|-)/::!/::L/::>/::,@/:,@f/::-S/:?/:,@x/:,@@/::8/:,@!/:!!!/:xx/:bye/:wipe/:dig/:handclap/:&-(/:B-)/:<@/:@>/::-O/:>-|/:P-(/::'|/:X-)/::*/:@x/:8*/:pd/:<W>/:beer/:basketb/:oo/:coffee/:eat/:pig/:rose/:fade/:showlovesssff/:heart/:break/:cake/:li/:bome/:kn/:footb/:ladybug/:shit/:moon/:sun/:gift/:hug/:strong/:weak/:share/:v/:@)/:jj/:@@/:bad/:lvu/:no/:ok/:love/:<L>/:jump/:shake/:<O>/:circle/:kotow/:turn/:skip/:oY链接:http://baidu.com拉了dudl@qq.com"];



//PhotoViewController  拍照 相册
/*
 
 PhotoViewController 创建后直接调用即可
 
 
 */


/*
 
 quick dialog
 QRootElement *root = [SampleDataBuilder create];
 ViewController *quickviewcontroller = [[ViewController
 alloc]initWithRoot:root];
 
 + (QRootElement *)create {
 QRootElement *root = [[QRootElement alloc] init];
 root.grouped = YES;
 root.title = @"QuickForms!";
 QSection *sectionSamples = [[QSection alloc] init];
 sectionSamples.headerView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"quickdialog"]];
 
 
 
 
 [sectionSamples addElement:[self createWithInitDefault]];
 //若以根元素section的容器为元素表示 此cell可点击进入 展示另外表单
 
 [root addSection:sectionSamples];
 
 
 
 等 详见百度
 
 
 return root;
 }

 
 
 
 
 
 
 */



/*  MONActivityIndicatorView  圆圈progress
 
 MONActivityIndicatorView *indicatorView = [[MONActivityIndicatorView alloc] init];
 
 indicatorView.numberOfCircles = 5;
 indicatorView.radius = 7;
 indicatorView.internalSpacing = 3;
 indicatorView.center = self.view.center;
 [indicatorView startAnimating];
 [self.view addSubview:indicatorView];
 [NSTimer scheduledTimerWithTimeInterval:7 target:indicatorView selector:@selector(stopAnimating) userInfo:nil repeats:NO];
 [NSTimer scheduledTimerWithTimeInterval:9 target:indicatorView selector:@selector(startAnimating) userInfo:nil repeats:NO];
 */

/*  二维码扫描
QrcodeViewController *QrcodeView = [[QrcodeViewController alloc]init];
QrcodeView.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
[self.view addSubview:QrcodeView.view];
[self addChildViewController:QrcodeView];
 
 
 */

