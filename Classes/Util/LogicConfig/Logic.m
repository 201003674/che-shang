//
//  Logic.m
//  IOSFramework
//
//  Created by 林科 on 14-10-22.
//  Copyright (c) 2014年 allianture. All rights reserved.
//

#include <sys/types.h>
#include <sys/sysctl.h>

#import "Logic.h"

@implementation Logic

- (id)init
{
    self = [super init];
    
    if (self) {
        self.loginSuccess = NO;
        
        self.networkStatus = YES;
        
        self.mutArr = [NSMutableArray arrayWithCapacity:10];
        
        self.encryTarData = [NSData data];
        
        self.encryArrLFImage = [NSMutableArray array];
    }
    
    return self;
}

/***********************************************************************
 * 方法名称：    getAffixFinialFile
 * 功能描述：    保存图片到指定文件夹 并返回图片的最终路径
 * 输入参数：    文件名 图片信息
 * 输出参数：    文件 路径
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (NSString *)getAffixFinialFile:(UIImage *)tempImage WithName:(NSString *)imageName
{
    NSData *imageData = UIImagePNGRepresentation(tempImage);
    
    // 判断存放附件文件夹是否存在,不存在则创建对应文件夹
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL isDir = FALSE;
    //创建子 文件下载 文件夹
    NSString *filePath = [NSString stringWithFormat:@"%@",FileDocPath(@"附件上传")];
    
    BOOL isDirExist = [fileManager fileExistsAtPath:filePath isDirectory:&isDir];
    
    NSString *fullPathToFile = [filePath stringByAppendingPathComponent:imageName];
    
    if(!(isDirExist && isDir))
    {
        BOOL bCreateDir = [fileManager createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:nil];
        
        if(!bCreateDir)
        {
            NSLog(@"Create Audio Directory Failed.");
            return nil;
        }
        else
        {
            NSLog(@"Create Audio Directory success!  %@",fullPathToFile);
            [self imageWriteToFile:fullPathToFile withObj:imageData];
            return fullPathToFile;
        }
    }
    else
    {
        NSLog(@"file exists! %@",fullPathToFile);
        [self imageWriteToFile:fullPathToFile withObj:imageData];
        return fullPathToFile;
    }
    return nil;
}

- (void)imageWriteToFile:(NSString *)path withObj:(NSData *)data
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL isDir = FALSE;
    
    BOOL isDirExist = [fileManager fileExistsAtPath:path isDirectory:&isDir];
    
    if(!(isDirExist && isDir))
    {
        [data writeToFile:path atomically:NO];
    }
    else
    {
        NSLog(@"file exists!");
    }
}

/***********************************************************************
 * 方法名称：    addImageMutArr
 * 功能描述：    添加图片并返回是否成功
 * 输入参数：    文件ID
 * 输出参数：    标记
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (BOOL)addImageToMutArr:(NSDictionary *)dic WithIndex:(NSString *)index;
{
    NSInteger cout = self.mutArr.count;
    
    NSInteger defaultIndex = index.integerValue;
    
    if (defaultIndex > cout || defaultIndex <= 0)
    {
        [[[[ClassFactory getInstance] getLogic] mutArr] addObject:dic];
    }
    else
    {
        [[[[ClassFactory getInstance] getLogic] mutArr] insertObject:dic atIndex:defaultIndex-1];
    }
    
    if (self.mutArr.count > cout)
    {
        return YES;
    }
    return NO;
}

/***********************************************************************
 * 方法名称：    getImageName
 * 功能描述：    随机生成图片名
 * 输入参数：    无
 * 输出参数：    图片名
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (NSString *)getImageName
{
    NSDateFormatter *creatDateFormatter = [[NSDateFormatter alloc] init];
    [creatDateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    
    NSString *dateFormatterStr = [NSString stringWithFormat:@"%@",[creatDateFormatter stringFromDate:[NSDate date]]];
    
    NSString *userNameStr = [NSString stringWithFormat:@"%@_%u", dateFormatterStr,(arc4random() % 501) + 500];
    STRING_NIL_TO_NONE(userNameStr);
    
    return userNameStr;
}

- (NSString *)getNowTimeString:(NSString *)dateFormat
{
    //当前日期
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    NSString *createDate = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]];
    
    return createDate;
}
/***********************************************************************
 * 方法名称：    getOpenSDKStatusMessage
 * 功能描述：    解析和合OCR返回message
 * 输入参数：    状态码
 * 输出参数：    状态message
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
- (NSString *)getOpenSDKStatusMessage:(NSInteger)ISOpenSDKStatus
{
    switch (ISOpenSDKStatus) {
        case -1:
            return @"SDK未授权!";
            break;
        case 0:
            return @"SDK授权成功!";
            break;
        case 204:
            return @"SDK无法获取授权信息,可能是网络问题。SDK第一次联网验证时,没有网络连接,导致没有验证通过!";
            break;
        case 100:
            return @"当前设备 ID 错误!";
            break;
        case 101:
            return @"APP BundleID错误,授权APPKEY与绑定的BundleID不匹配!";
            break;
        case 102:
            return @"APPKEY错误,传递的APPKEY填写错误!";
            break;
        case 103:
            return @"超过时间限制,授权的APPKEY超出使用时间限制!";
            break;
        case 104:
            return @"达到设备上限,授权的APPKEY使用设备数量达到限制!";
            break;
        case 105:
            return @"达到识别次数上限,授权的APPKEY使用次数达到限制!";
            break;
        case 106:
            return @"错误的subappkey,暂时无用!";
            break;
        case 202:
            return @"其他未知的错误!";
            break;
        case 203:
            return @"服务器错误,第一次联网验证时,因服务器问题,没有验证通过!";
            break;
        case 205:
            return @"初始化资源失败,暂时无用!";
            break;
        case 206:
            return @"错误的SDK库版本!";
            break;
        default:
            return [NSString stringWithFormat:@"错误码:%ld,请您联系我们的技术支持进行解决!",ISOpenSDKStatus];
            break;
    }
}

/***********************************************************************
 * 方法名称：    creatFileDownDocm
 * 功能描述：    创建存放下载的文件的文件夹
 * 输入参数：    下载文件所处在的流程名 文件类型
 * 输出参数：
 * 返 回 值：   下载附件所在位置
 * 其它说明：
 ***********************************************************************/
- (NSString *)creatFileDownLoadDocuments:(NSString *)foldName WithFileName:(NSString *)tempFileName
{
    // 创建父文件下载 文件夹
    NSArray *filesArr = [NSArray arrayWithObjects:FILE_ATTACHMENT, nil];
    
    // 判断存放附件文件夹是否存在，不存在则创建对应文件夹
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    BOOL isDir = FALSE;
    
    for (NSInteger i = 0 ; i < [filesArr count] ; i ++ )
    {
        NSString *fileFinal = [NSString stringWithFormat:@"%@",FileDocPath([filesArr objectAtIndex:i])];
        
        BOOL isDirExist = [fileManager fileExistsAtPath:fileFinal isDirectory:&isDir];
        
        if(!(isDirExist && isDir))
        {
            
            BOOL bCreateDir = [fileManager createDirectoryAtPath:fileFinal withIntermediateDirectories:YES attributes:nil error:nil];
            
            if(!bCreateDir)
            {
                NSLog(@"Create Audio Directory Failed.");
            }
            
            NSLog(@"%@",fileFinal);
        }
    }
    
    //判断下载文件是否存在
    NSString *filePath = [NSString stringWithFormat:@"%@",FilePath(FILE_ATTACHMENT,tempFileName)];
    
    BOOL isDirExist = [fileManager fileExistsAtPath:filePath isDirectory:&isDir];
    
    if(!(isDirExist && isDir))
    {
        NSLog(@"file is not exists");
    }
    else
    {
        NSLog(@"file exists");
    }
    
    return filePath;
}

@end
