//
//  Log.h
//  IOSFramework
//
//  Created by 林科 on 2018/6/11.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Log : NSObject
{
    
}
+ (void)logWithSessionManager:(AFSessionManager *)sessionManager WithRequestBean:(NSDictionary *)requestBean ;
+ (void)logWithSessionManager:(AFSessionManager *)sessionManager WithRequestBean:(NSDictionary *)requestBean json:(id)responseJSON;
+ (void)logWithSessionManager:(AFSessionManager *)sessionManager WithContent:(NSString *)logContent;

@end
