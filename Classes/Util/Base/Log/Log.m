//
//  Log.m
//  IOSFramework
//
//  Created by 林科 on 2018/6/11.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import "Log.h"

@implementation Log

+ (void)logWithSessionManager:(AFSessionManager *)sessionManager WithRequestBean:(NSDictionary *)requestBean
{
    NSMutableString *logString = [NSMutableString stringWithString:@"\n\n"];
    [logString appendFormat:@"**************************************************************\n"];
    [logString appendFormat:@"*                       Request Start                        *\n"];
    [logString appendFormat:@"**************************************************************\n\n"];
    [logString appendFormat:@"请求:\n%@?%@\n\n", sessionManager.requestSerializer.description, sessionManager.requestSerializer.description];
    [logString appendFormat:@"CookieStorage:\n%@", [NSHTTPCookieStorage sharedHTTPCookieStorage]];
    [logString appendFormat:@"%@", [requestBean mj_keyValues]];
    [logString appendFormat:@"\n\n"];
    [logString appendFormat:@"**************************************************************\n"];
    [logString appendFormat:@"*                         Request End                        *\n"];
    [logString appendFormat:@"**************************************************************\n\n"];
    
    NSLog(@"%@", logString);
}

+ (void)logWithSessionManager:(AFSessionManager *)sessionManager WithRequestBean:(NSDictionary *)requestBean json:(id)responseJSON
{
    NSMutableString *logString = [NSMutableString stringWithString:@"\n\n"];
    [logString appendFormat:@"==============================================================\n"];
    [logString appendFormat:@"=                        API Response                        =\n"];
    [logString appendFormat:@"==============================================================\n\n"];
    [logString appendFormat:@"URL: %@\n\n", sessionManager.requestSerializer.description];
    [logString appendFormat:@"%@", responseJSON];
    [logString appendFormat:@"\n\n"];
    [logString appendFormat:@"==============================================================\n"];
    [logString appendFormat:@"=                        Response End                        =\n"];
    [logString appendFormat:@"==============================================================\n\n\n"];
    
    NSLog(@"%@", logString);
}

+ (void)logWithSessionManager:(AFSessionManager *)sessionManager WithContent:(NSString *)logContent
{
    NSMutableString *logStr = [NSMutableString stringWithString:@"\n\n"];
    [logStr appendFormat:@"################################################################\n"];
    [logStr appendFormat:@"#                             Error                            #\n"];
    [logStr appendFormat:@"################################################################\n\n"];
    [logStr appendFormat:@"%@\n\n", logContent];
    [logStr appendFormat:@"################################################################\n\n\n"];
    
    NSLog(@"%@", logStr);
}

@end
