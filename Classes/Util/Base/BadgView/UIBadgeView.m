//
//  UIBadgeView.m
//  IOSFramework
//
//  Created by allianture on 14-2-26.
//  Copyright (c) 2014年 allianture. All rights reserved.
//

#import "UIBadgeView.h"
#import "JSBadgeView.h"

@interface UIBadgeView ()
{
    JSBadgeView *badgeView;
}
@end

@implementation UIBadgeView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        
        badgeView = [[JSBadgeView alloc] initWithParentView:self alignment:JSBadgeViewAlignmentCenter];
    }
    return self;
}


/* 创建通知类型badge */
- (id)initWithFrame:(CGRect)frame notificationName:(NSString *)nameStr
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        
        badgeView = [[JSBadgeView alloc] initWithParentView:self alignment:JSBadgeViewAlignmentCenter];
        
        if (STRING_ISNOTNIL(nameStr))
        {
            /* 通知 */
            [[NSNotificationCenter defaultCenter] removeObserver:self name:nameStr object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(changebadgeValue:)
                                                         name:  nameStr
                                                       object: nil];
        }
        
    }
    return self;
}


/* 修改badgevalue
 [[NSNotificationCenter defaultCenter] postNotificationName:@"" object:nil];
 
 userInfoDic = [NSDictionary dictionaryWithObjectsAndKeys:
 @"10", @"value",
 @"1", @"type",
 nil];
 
 type: 0：在当前基础上减少value
       1：在当前基础上增加value
       2：设置当前值为value
 */
#if 0
- (void)loginSuccess:(NSNotification *)notification
{
    NSDictionary *userInfoDic = notification.userInfo;
    NSString *typeStr = [userInfoDic objectForKey:@"type"];
    NSString *valueStr = [userInfoDic objectForKey:@"value"];
    
    NSInteger norInt = [badgeView.badgeText intValue];
    NSInteger valueInt = [valueStr intValue];
    
    NSString *badgevalueStr = nil;
    
    if ([typeStr isEqualToString:@"0"])
    {
        //在当前基础上减少value
        if (norInt-valueInt>0)
        {
            badgevalueStr = [NSString stringWithFormat:@"%d",norInt-valueInt];
        }
    }
    else if ([typeStr isEqualToString:@"1"])
    {
        //在当前基础上增加value
        badgevalueStr = [NSString stringWithFormat:@"%d",norInt+valueInt];
        
    }
    else if ([typeStr isEqualToString:@"2"])
    {
        //设置当前值为value
        badgevalueStr = valueStr;
    }
    
    [self setBadgeValue:badgevalueStr];
}
#endif

/* 设置显示数字 */
- (void)setBadgeValue:(NSString *)valueStr
{
    if (STRING_ISNIL(valueStr))
    {
        badgeView.hidden = YES;
    }
    badgeView.hidden = NO;
    
    badgeView.badgeText = valueStr;
}



@end
