//
//  UIBadgeView.h
//  IOSFramework
//
//  Created by allianture on 14-2-26.
//  Copyright (c) 2014年 allianture. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBadgeView : UIView


/* 设置显示数字 */
- (void)setBadgeValue:(NSString *)valueStr;


/* 创建通知类型badge */
- (id)initWithFrame:(CGRect)frame notificationName:(NSString *)nameStr;


@end
