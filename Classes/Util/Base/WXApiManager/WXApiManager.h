//
//  WXApiManager.h
//  IOSFramework
//
//  Created by 林科 on 2019/5/27.
//  Copyright © 2019 allianture. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WXApi.h"

@protocol WXApiManagerDelegate <NSObject>
@required

@optional
-(void)onReq:(BaseReq*)req;
-(void)onResp:(BaseResp*)resp;
@end

@interface WXApiManager : NSObject<WXApiDelegate>

@property(nonatomic, weak) id <WXApiManagerDelegate> delegate;

+ (instancetype)sharedManager;

@end
