//
//  UICustomNavigationController.m
//  IOSFramework
//
//  Created by allianture on 13-3-22.
//  Copyright (c) 2013年 allianture. All rights reserved.
//

#import "UICustomNavigationController.h"

/* 返回按钮 */
#define COMM_BACK_NOR @"COMM_Back_Nor@2x"
#define COMM_BACK_HIG @"COMM_Back_Hig@2x"

#define BACKTITLE_COLOR_NOR 0xF6F6F6
#define BACKTITLE_COLOR_HIG 0xF6F6F6

#define BACK_FRAME_X 0
#define BACK_FRAME_Y 0
#define BACK_FRAME_WIDTH 39
#define BACK_FRAME_HEIGHT 44
#define BACKTITLE_FONT 14

#define BACKTITLE_LEFT 5
#define BACK_TAG 999

#define SHEET_TAG 2000

@interface UICustomNavigationController ()
{
    
}
@end

@implementation UICustomNavigationController
@synthesize backButtonType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        //修改导航栏图片
        if ([[[UIDevice currentDevice] systemVersion] intValue] >= 5.0)
        {
            if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)
            {
//                [self.navigationBar setBackgroundImage:navBgImage forBarPosition:UIBarPositionTop barMetrics:UIBarMetricsDefault];
                
                [self.navigationBar setTranslucent:YES];
            }
            else
            {
//                [self.navigationBar setBackgroundImage:navigationBgImae forBarMetrics:UIBarMetricsDefault];
                
                [self.navigationBar setTranslucent:NO];
            }
        }
        else
        {
            //
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /* 允许返回手势 */
    self.enableBackGesture = YES;

    //修改导航背景
    [self.navigationBar setBarTintColor:HEXCOLOR(0X0C5DAA)];
    
    /* 标题颜色 */
    NSDictionary *dictText = [NSDictionary dictionaryWithObjectsAndKeys:
                              HEXCOLOR(0XFFFFFF), NSForegroundColorAttributeName,
                              [UIFont systemFontOfSize:18],NSFontAttributeName,nil] ;
    
    [self.navigationBar setTitleTextAttributes:dictText];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - 
#pragma mark - 推进
/* 推进 */
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (viewController == nil)
    {
        return;
    }
    
    [super pushViewController:viewController animated:animated];
    
    if (viewController.navigationItem.leftBarButtonItem == nil && [self.viewControllers count] > 1)
    {
        viewController.navigationItem.leftBarButtonItem = [self createBackButton];
    }
}


/* 返回按钮 */
- (UIBarButtonItem*)createBackButton
{
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    backButton.showsTouchWhenHighlighted = YES;
    //float width = self.navigationItem.title.length * 20;
    backButton.frame = CGRectMake(BACK_FRAME_X, BACK_FRAME_Y, BACK_FRAME_WIDTH, BACK_FRAME_HEIGHT);
    [backButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:COMM_BACK_NOR ofType:@"png"]] forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:COMM_BACK_HIG ofType:@"png"]] forState:UIControlStateHighlighted];
    //[backButton setImageEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 40-14-3)];
    //[backButton setTitleEdgeInsets:UIEdgeInsetsMake(0, BACKTITLE_LEFT, 0, 0)];
    // [backButton setTitle:self.navigationItem.title forState:UIControlStateNormal];
    // [backButton setTitle:@"返回" forState:UIControlStateNormal];
    backButton.titleLabel.font = [UIFont systemFontOfSize:BACKTITLE_FONT];
    // [backButton setTitleColor:HEXCOLOR(BACKTITLE_COLOR_NOR) forState:UIControlStateNormal];
    // [backButton setTitleColor:HEXCOLOR(BACKTITLE_COLOR_NOR) forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(popself) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    barButtonItem.style = UIBarButtonItemStylePlain;
    barButtonItem.tag = BACK_TAG;
    return barButtonItem;
}


/* 返回 */
-(void)popself
{
    [self popViewControllerAnimated:YES];
}
/* presentViewController 的返回按钮操作 */
- (void)presentBackBtn:(UIViewController *)navigationC
{
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //float width = self.navigationItem.title.length * 20;
    backButton.frame = CGRectMake(BACK_FRAME_X, BACK_FRAME_Y, BACK_FRAME_WIDTH, BACK_FRAME_HEIGHT);
    [backButton setImage:IMAGE_NAMED(COMM_BACK_NOR) forState:UIControlStateNormal];
    [backButton setImage:IMAGE_NAMED(COMM_BACK_HIG) forState:UIControlStateHighlighted];
    //[backButton setImageEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 40-14-3)];
    //[backButton setTitleEdgeInsets:UIEdgeInsetsMake(0, BACKTITLE_LEFT, 0, 0)];
    // [backButton setTitle:self.navigationItem.title forState:UIControlStateNormal];
    // [backButton setTitle:@"返回" forState:UIControlStateNormal];
    backButton.titleLabel.font = [UIFont systemFontOfSize:BACKTITLE_FONT];
    // [backButton setTitleColor:HEXCOLOR(BACKTITLE_COLOR_NOR) forState:UIControlStateNormal];
    // [backButton setTitleColor:HEXCOLOR(BACKTITLE_COLOR_NOR) forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    barButtonItem.style = UIBarButtonItemStylePlain;
    barButtonItem.tag = BACK_TAG;
    navigationC.navigationItem.leftBarButtonItem = barButtonItem;
}

- (void)dismissViewController
{  
    [self dismissViewControllerAnimated:YES completion:^{
    
        
    }];
}

@end
