//
//  UICustomNavigationController.h
//  IOSFramework
//
//  Created by allianture on 13-3-22.
//  Copyright (c) 2013年 allianture. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UINavigationController+YRBackGesture.h"

#define foo4random() (1.0 * (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX)

typedef NS_ENUM(NSInteger, UIBackButtonType) {
    UIBackButtonTypeCustom = 0,             // 默认 <-
    UIBackButtonTypeBlack,                  // 黑色按钮 <-
    UIBackButtonTypeClose,                  // 关闭按钮 ×
};

typedef NS_ENUM(NSInteger, UINavigationType) {
    UINavTypeNormal = 0,             // 默认 <-
};

@interface UICustomNavigationController : UINavigationController
{
    UIBackButtonType backButtonType;       /* 返回按钮类型 */
}
@property (nonatomic, assign) NSInteger *navTag;
@property (nonatomic, assign) UIBackButtonType backButtonType;


/* presentViewController 的返回按钮操作 */
- (void)presentBackBtn:(UIViewController *)navigationC;


@end
