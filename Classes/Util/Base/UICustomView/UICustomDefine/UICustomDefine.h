//
//                       .::::.
//                     .::::::::.
//                    ::::::::::: 
//                ..:::::::::::'
//              '::::::::::::'
//                .::::::::::
//           '::::::::::::::..
//                ..::::::::::::.
//              ``::::::::::::::::  Hello Boy!!!
//               ::::``:::::::::'        .:::.
//              ::::'   ':::::'       .::::::::.
//            .::::'      ::::     .:::::::'::::.
//           .:::'       :::::  .:::::::::' ':::::.
//          .::'        :::::.:::::::::'      ':::::.
//         .::'         ::::::::::::::'         ``::::.
//     ...:::           ::::::::::::'              ``::.
//    ```` ':.          ':::::::::'                  ::::..
//                       '.:::::'                    ':'````..
//
//
#pragma mark -
#pragma mark -

/* 验证是否为iphone5  */
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

//当前设备的屏幕宽度
#define SCREEN_WIDTH   [[UIScreen mainScreen] bounds].size.width

//当前设备的屏幕高度
#define SCREEN_HEIGHT   [[UIScreen mainScreen] bounds].size.height

/* 获取屏幕尺寸 用于兼容iphone5 */
#define WINDOWSCREEN_WIDTH      SCREEN_WIDTH
#define WINDOWSCREEN_HEIGHT     SCREEN_HEIGHT

/* ios7尺寸 */
#define VIEWSCREEN_IOS7_Y  ((IOS_VERSION_7==1) ? 64:0)

/* 获取显示屏幕的尺寸 去除电池条高度 */
#define VIEWSCREEN_WIDTH        SCREEN_WIDTH

#define VIEWSCREEN_HEIGHT       ((IOS_VERSION_7==1)?(SCREEN_HEIGHT - 64):(SCREEN_HEIGHT - 44))

//系统字体大小
#define SYSTEM_FONT 12.0

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

/* 是不是IPhone5 */
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

//16进制color 使用方法：HEXCOLOR(0XFFFFFF)
#define HEXCOLOR(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]

/* 安全释放变量 release + nil */
#define RELEASE_SAFELY(__POINTER) { [__POINTER release]; __POINTER = nil; }

/* 防止空变量 nil -> @"" */
#define STRING_NIL_TO_NONE(__POINTER) if (__POINTER == nil || [__POINTER isEqualToString:@"(null)"]){__POINTER = @"";}

/* 物理地址防空 */
#define STRING_ADDRESS_NIL_TO_NONE(__POINTER) if (__POINTER == nil || [__POINTER isEqualToString:@"(null)"] || [__POINTER isEqualToString:@""]){__POINTER = @"用户没有授权访问地址!";}

#define STRING_ADDRESS_UNABLE_NONE(__POINTER) if (__POINTER == nil || [__POINTER isEqualToString:@"(null)"] || [__POINTER isEqualToString:@""]){__POINTER = @"无法获取当前地址信息!";}

/* 判定字符串是否为空 */
#define STRING_ISNIL(__POINTER) (__POINTER == nil || [__POINTER isEqualToString:@""])?YES:NO
#define STRING_ISNOTNIL(__POINTER) (__POINTER == nil || [__POINTER isEqualToString:@""])?NO:YES

/* 打印BOOL值 */
#define NSLOG_BOOL(_bool) { NSLog(@"_bool = %@",(_bool == YES)?@"YES":@"NO"); } 

/* 自定义NSLog */
#ifdef DEBUG
#define NSLog NSLog(@"[%s] [%s] [%d] ",strrchr(__FILE__,'/'), __FUNCTION__, __LINE__);NSLog
#else
#define NSLog(...)
#endif

/* 弹出alert形式NSLog */
#ifdef DEBUG
#define ULog(fmt, ...)  { UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%s\n [Line %d] ", __PRETTY_FUNCTION__, __LINE__] message:[NSString stringWithFormat:fmt, ##__VA_ARGS__]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]; [alert show]; }
#else
#define ULog(...)
#endif

/* 设备类型和方向 */
#define UI_USER_INTERFACE_IDIOM() ([[UIDevice currentDevice] respondsToSelector:@selector(userInterfaceIdiom)] ? [[UIDevice currentDevice] userInterfaceIdiom] : UIUserInterfaceIdiomPhone)

#define UIDeviceOrientationIsPortrait(orientation)  ((orientation) == UIDeviceOrientationPortrait || (orientation) == UIDeviceOrientationPortraitUpsideDown)
#define UIDeviceOrientationIsLandscape(orientation) ((orientation) == UIDeviceOrientationLandscapeLeft || (orientation) == UIDeviceOrientationLandscapeRight)

/* performSelector ARC警告问题
 
 如果没有返回结果，可以直接按如下方式调用：
 SuppressPerformSelectorLeakWarning([_target performSelector:_action withObject:self]);
 
 如果要返回结果，可以按如下方式调用:
 id result;SuppressPerformSelectorLeakWarning( result =[_target performSelector:_action withObject:self]);
 */
#define SuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)
/*判断BOOL类型值 */
#define BOOLTYPE(IPHONESTR) (IPHONESTR == YES)? YES:NO

/* 判断字符串是否为空 如果为空 则置为@"" */
#define ISSTRINGCLASS(IPHONESTR) [[IPHONESTR class] isSubclassOfClass:[NSString class]] ? YES:NO

#define ISNILSTRING(IPHONESTR)  (ISSTRINGCLASS(IPHONESTR) == YES) ? ((IPHONESTR == nil || [IPHONESTR isEqualToString:@""]) ? ((IPHONESTR = @""), YES):NO):YES
#define ISNOTNILSTR(IPHONESTR) (ISNILSTRING(IPHONESTR) == YES) ? NO:YES

/* 判断记住密码 字符串 */
#define ISNILORNO(IPHONESTR) (IPHONESTR == nil || [IPHONESTR isEqualToString:@""] || [IPHONESTR isEqualToString:@"NO"] || [IPHONESTR isEqualToString:@"(null)"]) ? YES:NO

/* 非空 字典 */
#define ISNOTNILDIC(IPHONEDIC) ([IPHONEDIC isKindOfClass:[NSDictionary class]] && IPHONEDIC != nil && IPHONEDIC.allKeys.count != 0 && IPHONEDIC.allValues.count != 0)? YES:NO

/* 空 字典 */
#define ISNILDIC(IPHONEDIC) ([IPHONEDIC isKindOfClass:[NSDictionary class]] && IPHONEDIC != nil && IPHONEDIC.allKeys.count != 0 && IPHONEDIC.allValues.count != 0)? NO:YES

/* 非空 数组 */
#define ISNOTNILARR(IPHONEARR) ([IPHONEARR isKindOfClass:[NSArray class]] && IPHONEARR != nil && IPHONEARR.count != 0)? YES:NO

/* 获取系统版本号 */
#define IOS_VERSION [[UIDevice currentDevice].systemVersion floatValue]

#define IOS_VERSION_5 (IOS_VERSION < 6.0)? 1:0
#define IOS_VERSION_6 (IOS_VERSION >= 6.0 && IOS_VERSION<7.0)? 1:0
#define IOS_VERSION_7 (IOS_VERSION >= 7.0)? 1:0
#define IOS_VERSION_8 (IOS_VERSION >= 8.0)? 1:0
/* 判断是不是IOS 9*/
#define IOS_VERSION_9 (IOS_VERSION >= 9.0)? 1:0

/* IOS 7 CGRECT 方法 */
#define IsIOS7 ([[[[UIDevice currentDevice] systemVersion] substringToIndex:1] floatValue] >= 7.0)
#define CGRECT_NO_NAV(x,y,w,h) CGRectMake((x), (y+(IsIOS7?20:0)), (w), (h))
#define CGRECT_HAVE_NAV(x,y,w,h) CGRectMake((x), (y+(IsIOS7?64:0)), (w), (h))

/* 判断屏幕 */
#define IS_RETINA (SCREEN_HEIGHT == 480) ? 1:0

/* 系统版本号判定 是否为6.0以上版本 */
#define DEVICEVALUE6 ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0)? 1:0

/* 5.0和6.0两种系统api兼容  ( (DEVICEVALUE6==1)? NSTextAlignmentCenter : UITextAlignmentCenter )*/
/* 文本类 居左、中、右 */
#ifdef DEVICEVALUE6
# define TEXT_ALIGN_LEFT NSTextAlignmentLeft
# define TEXT_ALIGN_CENTER NSTextAlignmentCenter
# define TEXT_ALIGN_RIGHT NSTextAlignmentRight
#else
# define TEXT_ALIGN_LEFT NSTextAlignmentLeft
# define TEXT_ALIGN_CENTER NSTextAlignmentCenter
# define TEXT_ALIGN_RIGHT UITextAlignmentRight
#endif

/* UILineBreakModeWordWrap  NSLineBreakByCharWrapping  */
#ifdef DEVICEVALUE6
# define LINE_BREAKMODE_WORDWRAP NSLineBreakByCharWrapping
#else
# define LINE_BREAKMODE_WORDWRAP UILineBreakModeWordWrap
#endif

/* NSLineBreakByCharWrapping  NSLineBreakByCharWrapping  */
#ifdef DEVICEVALUE6
# define LINE_BREAKMODE_CHARACTERWRAP NSLineBreakByCharWrapping
#else
# define LINE_BREAKMODE_CHARACTERWRAP NSLineBreakByCharWrapping
#endif

//区分模拟器和真机
#if TARGET_OS_IPHONE
//iPhone Device
#endif

#if TARGET_IPHONE_SIMULATOR
//iPhone Simulator
#endif

//ARC
#if __has_feature(objc_arc)
//compiling with ARC
#else
// compiling without ARC
#endif


// Time
#define TT_MINUTE 60
#define TT_HOUR   (60 * TT_MINUTE)
#define TT_DAY    (24 * TT_HOUR)
#define TT_5_DAYS (5 * TT_DAY)
#define TT_WEEK   (7 * TT_DAY)
#define TT_MONTH  (30.5 * TT_DAY)
#define TT_YEAR   (365 * TT_DAY)

/* 判定系统版本 */
#if __IPHONE_OS_VERSION_MAX_ALLOWED == __IPHONE_6_0

#else

#endif



//----------------------图片----------------------------

//读取本地图片
#define LOADIMAGE(file,ext) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:file ofType:ext]]

//定义UIImage对象

#define IMAGE_NAMED(A) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:A ofType:@"png"]]

//定义UIImage对象
#define IMAGENAMED(_pointer) [UIImage imageNamed:_pointer]

//定义图片像素压缩阈值
#define MAX_PIXEL 3200000

//----------------------图片----------------------------

/* 整体列表 */
#define TABLEVIEW_X 0
#define TABLEVIEW_Y 0
#define TABLEVIEW_WIDTH 320
#define TABLEVIEW_HEIGHT VIEWSCREEN_HEIGHT

/* 下载文件路径 */
#define DocumentsDirectory [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES) lastObject]
#define FileDocPath(file) [DocumentsDirectory stringByAppendingPathComponent:file]

#define FilePath(file,fileName) [FileDocPath(file) stringByAppendingPathComponent:fileName]

//tabBar的高度
#define TABBAR_HEIGHT 49

//操作栏高度
#define BOTTOM_BAR_HEIGHT 40

/* 背景色灰色 */
#define COMM_BACKGROUND_COLOR       [UIColor colorWithRed:0.9451 green:0.9451 blue:0.9451 alpha:1]

/* 字体默认颜色 */
#define TEXT_COLOR	 [UIColor colorWithRed:87.0/255.0 green:108.0/255.0 blue:137.0/255.0 alpha:1.0]

/* tableView 的背景颜色 */
#define TABLEVIEW_BACKGROUND_COLOR [UIColor colorWithPatternImage:[UIImage imageNamed:@"Product_background"]]

#define PRODUCT_ORDER_COLOR [UIColor colorWithPatternImage:[UIImage imageNamed:@"Product_order_footView"]]

/* 轮播图 */
#define PAGECONTROL_X 0
#define PAGECONTROL_Y 0
#define PAGECONTROL_WIDTH SCREEN_WIDTH
#define PAGECONTROL_HEIGHT 180

/* 用户信息钥匙串 */
#define COM_USERNAME_KEYCHAIN @"com.game.username"
#define COM_USERPASS_KEYCHAIN @"com.game.userpass"
#define COM_REMEMBER_KEYCHAIN @"com.game.remember"

/* 用户信息 路径 */
#define USER_PATH_KAY     @"user_path_key"

/* 用户信息 路径 */
#define USER_PATH_INFO    @"user_path_info"

/* 太好创 路径 */
#define THC_USER_PATH_KAY     @"thc_user_path_key"

/* 页面标题定义 */
#define LOGIN_TITLE @"登录"

#define TAB1_TITLE @"消息"
#define TAB2_TITLE @"客户"
#define TAB3_TITLE @"应用"
#define TAB4_TITLE @"我"

#define LOG_TABLE_NAME @"LOG_TABLE_NAME"
#define UPLOAD_TABLE_NAME @"UPLOAD_TABLE_NAME"

#define FILE_ATTACHMENT @"CRM"
#define FILE_ATTACHMENT_DOWN @"移动CRM/附件下载地址"
#define FILE_ATTACHMENT_UNZIP @"移动CRM/附件解压地址"


//高德地图API——KEY
#define  API_KEY   @"5de8d91c45eb00e0d00349df26078ead"

//设备信息
#define  DEVICE_INFO   @"deviceInfo"
//文件操作
#define  FILE_UPLOAD   @"fileUpload"
#define  FILE_DOWNLOAD   @"fileDownload"
#define  FILE_ABORT   @"fileAbort"
//APP存在判断
#define  CHECK_APP   @"check"
#define  START_APP   @"start"
//相机
#define  TAKE_PICTURE       @"takePicture"
#define  TAKE_ORCPICTURE   @"tackOcrPicture"
#define  TAKE_QRCODE   @"takeQRCode"
//手势设置
#define  EDIT_GESTURE   @"editGesture"
#define  VERIFY_GESTURE   @"verifyGesture"
//系统定位
#define  SYSTEM_LOCATION   @"bdLocation"
//支付
#define  ALIPAY   @"alipay"

//高德
#define  GD_LOCATION   @"gdLocation"
#define  POI_KEYWORD_SEARCH   @"PoiKeywordSearch"
#define  POI_AROUND_SEACH   @"PoiAroundSearch"
#define  ROUTE_KEYWORD   @"RouteKeyword"
#define  GET_DRAIVER_NAV_PATHKEYWOERD   @"getDriverNaviPathByKeyword"
#define  POINTS_MAP   @"PointsMap"

//签名
#define  SIGN_INIT      @"signInit"
#define  SIGN           @"sign"
#define  SIGN_MULTI     @"signMulti"
#define  NAV_HEIGHT     44
