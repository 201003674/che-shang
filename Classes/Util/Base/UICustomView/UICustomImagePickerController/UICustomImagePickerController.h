//
//  UICustomImagePickerController.h
//  IOSFramework
//
//  Created by 林科 on 2017/4/9.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Cordova/CDV.h>
@interface UICustomImagePickerController : UIImagePickerController
{
    
}
//Cordova 参数
@property(nonatomic, strong) CDVInvokedUrlCommand *command;

@end
