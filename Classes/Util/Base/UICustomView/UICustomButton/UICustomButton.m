//
//  UICustomButton.m
//  IOSFramework
//
//  Created by allianture on 13-9-10.
//  Copyright (c) 2013年 allianture. All rights reserved.
//

#import "UICustomButton.h"

@implementation UICustomButton

#pragma mark -
#pragma mark -
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
}

#pragma mark -
#pragma mark -
/**
 *	@brief	按钮初始化
 *
 *	@param 	frame 	控件大小
 *	@param 	title 	标题
 *	@param 	fontSize 	字体大小
 *	@param 	target 	方法委托
 *	@param 	selector 	执行方法
 *	@param 	imageNormal 	普通图片
 *	@param 	imageHighlighted 	高亮图片
 */
-(void)createButtonWithFrame:(CGRect)frame Title:(NSString *)title TitleColor:(UIColor *)titleColor Font:(CGFloat)fontSize Target:(id)target Selector:(SEL)selector ImageNormal:(NSString *)imageNormal ImageHighlighted:(NSString *)imageHighlighted
{
    [self setFrame:frame];
    
    UIImage *newImage = [UIImage imageNamed:imageNormal];
    [self setImage:newImage forState:UIControlStateNormal];
    
    UIImage *newPressedImage = [UIImage imageNamed:imageHighlighted];
    [self setImage:newPressedImage forState:UIControlStateHighlighted];
    
    [self setTitle:title forState:UIControlStateNormal];
    [self setTitleColor:titleColor forState:UIControlStateNormal];
    
    self.titleLabel.font = [UIFont systemFontOfSize:fontSize];
    
    [self addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
}
-(void)createButtonWithFrame:(CGRect)frame Title:(NSString *)title TitleColor:(UIColor *)titleColor Font:(CGFloat)fontSize Target:(id)target Selector:(SEL)selector{
    [self setFrame:frame];
    [self setTitle:title forState:UIControlStateNormal];
    [self setTitleColor:titleColor forState:UIControlStateNormal];
    self.titleLabel.font = [UIFont systemFontOfSize:fontSize];
    [self addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
}
-(void)createButtonWithFrame:(CGRect)frame Title:(NSString *)title TitleColor:(UIColor *)titleColor Font:(CGFloat)fontSize Target:(id)target Selector:(SEL)selector BackgroundImageNormal:(NSString *)imageNormal BackgroundImageHighlighted:(NSString *)imageHighlighted
{
    [self setFrame:frame];
    
    UIImage *newImage = [UIImage imageNamed:imageNormal];
    [self setBackgroundImage:newImage forState:UIControlStateNormal];
    
    UIImage *newPressedImage = [UIImage imageNamed:imageHighlighted];
    [self setBackgroundImage:newPressedImage forState:UIControlStateHighlighted];
    
    [self setTitle:title forState:UIControlStateNormal];
    [self setTitleColor:titleColor forState:UIControlStateNormal];
    
    self.titleLabel.font = [UIFont systemFontOfSize:fontSize];
    
    [self addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
}
-(void)createButtonWithFrame:(CGRect)frame Title:(NSString *)title TitleColor:(UIColor *)titleColor Font:(CGFloat)fontSize Target:(id)target Selector:(SEL)selector ImageNormal:(NSString *)imageNormal ImageSelected:(NSString *)imageSelected
{

    [self setFrame:frame];
    
    UIImage *newImage = [UIImage imageNamed:imageNormal];
    [self setImage:newImage forState:UIControlStateNormal];
    
    UIImage *newPressedImage = [UIImage imageNamed:imageSelected];
    [self setImage:newPressedImage forState:UIControlStateSelected];
    
    [self setTitle:title forState:UIControlStateNormal];
    [self setTitleColor:titleColor forState:UIControlStateNormal];
    
    self.titleLabel.font = [UIFont systemFontOfSize:fontSize];
    
    [self addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
