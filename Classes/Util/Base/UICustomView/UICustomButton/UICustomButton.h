//
//  UICustomButton.h
//  IOSFramework
//
//  Created by allianture on 13-9-10.
//  Copyright (c) 2013年 allianture. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICustomButton : UIButton
{
    
}
@property (strong, nonatomic) NSString *btnInfoString;

@property (strong, nonatomic) NSDictionary *btnInfoDic;


- (void)createButtonWithFrame:(CGRect)frame Title:(NSString *)title TitleColor:(UIColor *)titleColor Font:(CGFloat)fontSize Target:(id)target Selector:(SEL)selector ImageNormal:(NSString *)imageNormal ImageHighlighted:(NSString *)imageHighlighted;

-(void)createButtonWithFrame:(CGRect)frame Title:(NSString *)title TitleColor:(UIColor *)titleColor Font:(CGFloat)fontSize Target:(id)target Selector:(SEL)selector BackgroundImageNormal:(NSString *)imageNormal BackgroundImageHighlighted:(NSString *)imageHighlighted;

@end
