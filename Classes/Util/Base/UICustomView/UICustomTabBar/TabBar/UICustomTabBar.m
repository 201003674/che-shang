
#import "UICustomTabBar.h"


@implementation UICustomTabBar
@synthesize buttonItems;
@synthesize tabDelegate;
@synthesize backgroundImage;
@synthesize selectItemIndex;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        
        buttonMutArr = [[NSMutableArray alloc] init];
        
    }
    return self;
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    /* tabbar背景 */
    if (backgroundImageView == nil)
    {
        backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 49)];
    }
    backgroundImageView.image = backgroundImage;
    [self addSubview:backgroundImageView];
    
    NSInteger itemsCount = [buttonItems count];
    float itemWidth = self.frame.size.width / itemsCount;

    if ([buttonItems count] <= 0) {
        return;
    }
    
    /* 点击按钮移动背景 */
    if (tabbarBgImageView == nil)
    {
        tabbarBgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, itemWidth, 49)];
    }
    [self addSubview:tabbarBgImageView];
    tabbarBgImageView.image = [UIImage imageNamed:@"TabBarPress.png"];
    
    for (NSInteger i = 0; i < itemsCount; i++) {
        id item = [buttonItems objectAtIndex:i];
        
        if ([@"UICustomTabBarItem" isEqualToString:NSStringFromClass([item class])])
        {
            UICustomTabBarItem *tempItem = (UICustomTabBarItem *)item;
            
            UIButton *itemBtn = (UIButton *)[self viewWithTag:i + 100];
            
            BOOL isExist = itemBtn ? YES : NO;
            
            if (!isExist)
            {
                itemBtn = [[UIButton alloc] initWithFrame:CGRectMake(i * itemWidth, 0, itemWidth, 49)];
                
                /* 加入数组保存 */
                [buttonMutArr addObject:itemBtn];
                
                itemBtn.tag = tempItem.intTag + 100;
                [itemBtn setBackgroundImage:tempItem.itemImage forState:UIControlStateNormal];
                [itemBtn setBackgroundImage:tempItem.itemImageHighlight forState:UIControlStateHighlighted];
                [itemBtn setBackgroundImage:tempItem.itemImageHighlight forState:UIControlStateSelected];
                [itemBtn setTitle:tempItem.strTitle forState:UIControlStateNormal];
                
                itemBtn.titleEdgeInsets = UIEdgeInsetsMake(25, 0, 0, 0);
                itemBtn.titleLabel.font = [UIFont systemFontOfSize:TABTITLE_FONT];
                UIColor *colorNormal = HEXCOLOR(TABTITLE_COLOR_NOR);
                [itemBtn setTitleColor:colorNormal forState:UIControlStateNormal];
                
                UIColor *colorSel = HEXCOLOR(TABTITLE_COLOR_HIG);
                [itemBtn setTitleColor:colorSel forState:UIControlStateSelected];
                
                [itemBtn addTarget:self action:@selector(itemBtnClick:) forControlEvents:UIControlEventTouchDown];
                [self addSubview:itemBtn];
                [itemBtn release];
            }
            
            if (selectItemIndex == i + 100) {
                itemBtn.selected = YES;
                lastBtn = itemBtn;
                itemBtn.userInteractionEnabled = NO;
            }
        }
    }
}


- (IBAction)itemBtnClick:(id)sender{
    UIButton *tempBtn = (UIButton *)sender;
    if (lastBtn != nil) {
        lastBtn.selected = NO;
        
        /* 上次选中的tabbar按钮 */
        lastBtn.userInteractionEnabled = YES;
        NSInteger lastBtnTag = lastBtn.tag - 100;
        NSString *norStr = [NSString stringWithFormat:@"TabBarBg%d0.png",lastBtnTag + 1];
        UIColor *colorNormal = HEXCOLOR(TABTITLE_COLOR_NOR);
        [lastBtn setTitleColor:colorNormal forState:UIControlStateNormal];
        UIColor *colorSel = HEXCOLOR(TABTITLE_COLOR_HIG);
        [lastBtn setTitleColor:colorSel forState:UIControlStateSelected];
        [lastBtn setBackgroundImage:[UIImage imageNamed:norStr] forState:UIControlStateNormal];
    }
    
    /*  本次选中的tabbar按钮 */
    //tempBtn.selected = YES;
    tempBtn.userInteractionEnabled = NO;
    selectItemIndex = tempBtn.tag;
    NSInteger btnTag = tempBtn.tag - 100;
    
    NSString *imageStr = [NSString stringWithFormat:@"TabBarBg%d1.png",btnTag + 1];
    [tempBtn setBackgroundImage:[UIImage imageNamed:imageStr] forState:UIControlStateNormal];
    UIColor *colorNormal = HEXCOLOR(TABTITLE_COLOR_HIG);
    [tempBtn setTitleColor:colorNormal forState:UIControlStateNormal];
    UIColor *colorSel = HEXCOLOR(TABTITLE_COLOR_NOR);
    [tempBtn setTitleColor:colorSel forState:UIControlStateSelected];
    
    [tabDelegate custTabBarDidSelectItemIndex:btnTag];
    
    lastBtn = tempBtn;
    
    /* 移动按钮 */
    [self slideTabBarBg:tempBtn];
}


- (void)setSelectItemIndex:(NSInteger)selectItemIndexT
{
    selectItemIndex = selectItemIndexT + 100;
}


- (void)setindex:(NSInteger)selectItemIndexT
{    
    UIButton *tempBtn = (UIButton *)[buttonMutArr objectAtIndex:selectItemIndexT];
    
    [self itemBtnClick:tempBtn];
}


/* tabbar按钮背景移动 */
- (void)slideTabBarBg:(UIButton *)button
{
    [UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.20];
	[UIView setAnimationDelegate:self];
    tabbarBgImageView.center = button.center;
	[UIView commitAnimations];
}


- (void)dealloc
{
    [buttonItems release];
    
    [buttonMutArr release];
    buttonMutArr = nil;
    
    RELEASE_SAFELY(backgroundImageView);
    RELEASE_SAFELY(tabbarBgImageView);
    
    [super dealloc];
}

@end


