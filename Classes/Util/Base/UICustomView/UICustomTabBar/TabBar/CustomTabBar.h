//
//  CustomTabBar.h
//  jsnxMobileBank
//
//  Created by zhanhaoru on 13-7-3.
//  Copyright (c) 2013年 p&c. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CustomTabBar_W 320.0
#define CustomTabBar_H 49.0
#define CustomTabBar_Default_Tag 1010
#define CustomTabBAr_Label_To_Btn_Tag 100
#define CustomTabBar_Item_Image_EdgeInsets UIEdgeInsetsMake(0.0, 0.0, 14.0, 0.0)
#define CustomTabBar_Item_Title_Color_Normal HEXCOLOR(0X696969)
#define CustomTabBar_Item_Title_Color_Selected HEXCOLOR(0XF2A609)

/*按钮的坐标大小*/
#define CustomTabBar_Item_Y 1.5
#define CustomTabBar_Item_H (CustomTabBar_H - 1.5)

/* 文字label坐标大小 */
#define CustomTabBar_Item_Label_Y 31.5
#define CustomTabBar_Item_Label_H 15
#define CustomTabBar_Item_Label_Font [UIFont systemFontOfSize:10.0]

@protocol CustomTabBarDelegate;

@interface CustomTabBar : UIView
{
    UIImageView *bg_ImageView;  // 背景图片
}

@property (nonatomic, retain) NSArray *defaultItemImages;
// 未选中默认图片
@property (nonatomic, retain) NSArray *selectedItemImages;
// 选中的图片
@property (nonatomic, retain) NSArray *defaultItemImages_Background;
// 未选中默认图片背景图片
@property (nonatomic, retain) NSArray *selectedItemImages_Background;
// 选中的图片背景图片
@property (nonatomic, retain) NSArray *titleItems;
// 所有文字
@property (nonatomic, retain) NSMutableArray *barBtnItems;
// 存放按钮数组
@property (nonatomic, retain) NSString *bgTabBar_imageName;
// tabBar背景图片
@property (nonatomic, assign) id<CustomTabBarDelegate> delegate;
//CustomTabBarDelegate
@property (nonatomic, retain) NSString *shadowItemBgImageStr;
// 半透明的光标图片名(设置为nil则不显示)
@property (nonatomic, retain) UIImageView *shadowItemBgImageV;
// 半透明的光标
@property (nonatomic, assign) NSInteger selectedTag;
// 选中的光标
@property (nonatomic, assign) BOOL moveShadowBgImageV;
// 是否移动半透明层
@property (nonatomic, strong) NSMutableArray *viewControllersMutArr;
//viewControllersMutArr

- (void)setSelectedItemTag:(NSInteger)tempTag;
// 选中指定的item
@end

@protocol CustomTabBarDelegate <NSObject>

- (void)tabBar:(CustomTabBar *)customTabBar didSelectItemTag:(NSInteger)itemTag;

@end
