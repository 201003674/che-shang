//
//  CustomTabItem.m
//  GUOHUALIFE
//
//  Created by zte- s on 12-11-28.
//  Copyright (c) 2012年 zte. All rights reserved.
//

#import "UICustomTabBarItem.h"

@implementation UICustomTabBarItem

@synthesize strTitle;
@synthesize itemImage;
@synthesize intTag;
@synthesize itemImageHighlight;

- (id)initWithTitle:(NSString *)title image:(UIImage *)image highlightImage:(UIImage *)hightImage tag:(NSInteger)tag{
    if (self = [super init]) {
        self.strTitle = title;
        self.itemImage = image;
        self.intTag = tag;
        self.itemImageHighlight = hightImage;
    }
    
    return self;
}

//- (void)dealloc{
//    [strTitle release];
//    [itemImage release];
//    [itemImageHighlight release];
//    
//    [super dealloc];
//}

@end
