//
//  CustomTabBar.m
//  jsnxMobileBank
//
//  Created by zhanhaoru on 13-7-3.
//  Copyright (c) 2013年 p&c. All rights reserved.
//

#import "CustomTabBar.h"

@implementation CustomTabBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        bg_ImageView = [[UIImageView alloc] initWithFrame:self.bounds];
        [self addSubview:bg_ImageView];
        
        _shadowItemBgImageV = [[UIImageView alloc] init];
        _shadowItemBgImageV.backgroundColor = [UIColor clearColor];
        [self addSubview:_shadowItemBgImageV];
        
        _barBtnItems = [[NSMutableArray alloc] init];
        
        
        /* 切换tabbar的通知 */
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"changeTableBar" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(changeTableBar:)
                                                     name: @"changeTableBar"
                                                   object: nil];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // 加载背景图片
    if (_bgTabBar_imageName && bg_ImageView)
    {
        bg_ImageView.image = [UIImage imageNamed:_bgTabBar_imageName];
    }
    
    // item的总个数
    NSInteger tempItemTotal = _titleItems.count;
    
    // 每个item的宽度
    float tempEveryItem_W = CustomTabBar_W / tempItemTotal;
    
    // 半透明的光标
    if (_shadowItemBgImageV && _shadowItemBgImageStr) {
        if (_moveShadowBgImageV) {
            [UIView beginAnimations:nil context:nil];
            
            _shadowItemBgImageV.frame = CGRectMake(tempEveryItem_W * _selectedTag, 0.0, tempEveryItem_W, CustomTabBar_H);
            
            [UIView commitAnimations];
        }
        else
        {
            _shadowItemBgImageV.frame = CGRectMake(tempEveryItem_W * _selectedTag, 0.0, tempEveryItem_W, CustomTabBar_H);
        }
        
        _shadowItemBgImageV.image = [UIImage imageNamed:_shadowItemBgImageStr];
    }
    
    // 移除之前的items
    for (UIView *idView in _barBtnItems) {
        [idView removeFromSuperview];
    }
    
    // 添加items
    for (NSInteger i = 0; i < tempItemTotal; i++) {
        UIView *tempView = nil;
        if (_barBtnItems == nil || _barBtnItems.count < tempItemTotal) {
            tempView = [[[UIView alloc] initWithFrame:CGRectMake(tempEveryItem_W * i, 0.0, tempEveryItem_W, CustomTabBar_H)] autorelease];
            tempView.backgroundColor = [UIColor clearColor];
            [self insertSubview:tempView atIndex:1];
            
            [_barBtnItems addObject:tempView];
            
            UIButton *tempItem = [UIButton buttonWithType:UIButtonTypeCustom];
            tempItem.backgroundColor = [UIColor clearColor];
            tempItem.frame = CGRectMake(0.0, CustomTabBar_Item_Y, tempEveryItem_W, CustomTabBar_Item_H);
            tempItem.tag = CustomTabBar_Default_Tag + i;
            [tempItem setImage:[UIImage imageNamed:[_defaultItemImages objectAtIndex:i]] forState:UIControlStateNormal];
            [tempItem setImage:[UIImage imageNamed:[_defaultItemImages objectAtIndex:i]] forState:UIControlStateHighlighted];
            [tempItem setImage:[UIImage imageNamed:[_selectedItemImages objectAtIndex:i]] forState:UIControlStateSelected];
            [tempItem setBackgroundImage:[UIImage imageNamed:[_defaultItemImages_Background objectAtIndex:i]] forState:UIControlStateNormal];
            [tempItem setBackgroundImage:[UIImage imageNamed:[_selectedItemImages_Background objectAtIndex:i]] forState:UIControlStateHighlighted];
            [tempItem setBackgroundImage:[UIImage imageNamed:[_selectedItemImages_Background objectAtIndex:i]] forState:UIControlStateSelected];
            [tempItem addTarget:self action:@selector(tabBtn:) forControlEvents:UIControlEventTouchDown];
            [tempItem setImageEdgeInsets:CustomTabBar_Item_Image_EdgeInsets];
            [tempView addSubview:tempItem];
            
            // 文字显示
            UILabel *tempItemLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0.0, CustomTabBar_Item_Label_Y, tempEveryItem_W, CustomTabBar_Item_Label_H)] autorelease];
            tempItemLabel.backgroundColor = [UIColor clearColor];
            tempItemLabel.text = [_titleItems objectAtIndex:i];
            tempItemLabel.tag = CustomTabBar_Default_Tag + i + CustomTabBAr_Label_To_Btn_Tag;
            tempItemLabel.font = CustomTabBar_Item_Label_Font;
            tempItemLabel.textAlignment = NSTextAlignmentCenter;
            tempItemLabel.textColor = CustomTabBar_Item_Title_Color_Normal;
            [tempView addSubview:tempItemLabel];
            
            tempItem.selected = NO;
            if (self.selectedTag == i) {
                tempItem.selected = YES;
                tempItemLabel.textColor = CustomTabBar_Item_Title_Color_Selected;
            }
        }
        else {
            tempView = [_barBtnItems objectAtIndex:i];
            [self insertSubview:tempView atIndex:1];
            
            UIButton *tempItemBtn = (UIButton *)[tempView viewWithTag:CustomTabBar_Default_Tag + i];
            tempItemBtn.selected = NO;
            
            UILabel *tempLabel = (UILabel *)[tempView viewWithTag:CustomTabBar_Default_Tag + i + CustomTabBAr_Label_To_Btn_Tag];
            tempLabel.textColor = CustomTabBar_Item_Title_Color_Normal;
            
            if (self.selectedTag == i) {
                tempItemBtn.selected = YES;
                
                
                tempLabel.textColor = CustomTabBar_Item_Title_Color_Selected;
            }
        }
    }
}

// 选中指定的item
- (void)setSelectedItemTag:(NSInteger)tempTag
{
    _selectedTag = tempTag;
    [self layoutSubviews];
}

- (IBAction)tabBtn:(id)sender
{
    UIButton *tempBtn = (UIButton *)sender;
    NSInteger tempTag = tempBtn.tag - CustomTabBar_Default_Tag;
    
    _selectedTag = tempTag;
    [self layoutSubviews];
    
    [self.delegate tabBar:self didSelectItemTag:tempTag];
}



/* 切换tabbar */
- (void)changeTableBar:(NSNotification *)notifiction
{
    NSDictionary *notInfoDic = notifiction.userInfo;
    NSInteger itemTag = [[notInfoDic objectForKey:@"type"] intValue];
    
    /* type：0-财富分析案例  2-产品列表  3-研究咨询 4-动态活动 */
    
    if (itemTag <= 4 && itemTag >=0 )
    {
        UIView *tempView = [_barBtnItems objectAtIndex:itemTag];
        [self insertSubview:tempView atIndex:1];
        
        UIButton *tempItemBtn = (UIButton *)[tempView viewWithTag:CustomTabBar_Default_Tag + itemTag];
        
        [self tabBtn:tempItemBtn];
        
        UICustomNavigationController *nav = (UICustomNavigationController *)[self.viewControllersMutArr objectAtIndex:itemTag];
        [nav popToRootViewControllerAnimated:NO];
        
       // [self showTabBar];
    }
}


- (void)dealloc {
    [_defaultItemImages release];
    [_selectedItemImages release];
    [_titleItems release];
    [_bgTabBar_imageName release];
    [bg_ImageView release];
    [_shadowItemBgImageStr release];
    [_shadowItemBgImageV release];
    [_barBtnItems release];
    
    [super dealloc];
}

@end
