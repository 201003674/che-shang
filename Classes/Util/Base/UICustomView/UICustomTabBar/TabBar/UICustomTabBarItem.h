//
//  CustomTabItem.h
//  GUOHUALIFE
//
//  Created by zte- s on 12-11-28.
//  Copyright (c) 2012年 zte. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UICustomTabBarItem : NSObject
{
    NSString *strTitle;
    UIImage *itemImage;
    UIImage *itemImageHighlight;
    NSInteger intTag;
}

@property (nonatomic, retain) NSString *strTitle;
@property (nonatomic, retain) UIImage *itemImage;
@property (nonatomic, retain) UIImage *itemImageHighlight;
@property (nonatomic, assign) NSInteger intTag;

- (id)initWithTitle:(NSString *)title image:(UIImage *)image highlightImage:(UIImage *)hightImage tag:(NSInteger)tag;

@end
