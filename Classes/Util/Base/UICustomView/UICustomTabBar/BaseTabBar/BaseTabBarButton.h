

#import <UIKit/UIKit.h>

@interface BaseTabBarButton : UIButton

@property (nonatomic, strong) UITabBarItem *item;

@end
