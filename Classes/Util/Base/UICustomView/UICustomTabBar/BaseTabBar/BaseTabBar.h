
#import <UIKit/UIKit.h>

@class BaseTabBar;

@protocol BaseTabBarDelegate <NSObject>

@optional

- (void)tabBar:(BaseTabBar *)tabBar didSelectedButtonFrom:(NSUInteger)from to:(NSUInteger)to;

@end

@interface BaseTabBar : UIView

- (void)addTabBarButtonWithItem:(UITabBarItem *)item;

@property (nonatomic, weak) id<BaseTabBarDelegate> delegate;

@end
