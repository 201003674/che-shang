//
//  UICustomTabBarController.h
//  IOSFramework
//
//  Created by 林科 on 14-9-16.
//  Copyright (c) 2014年 allianture. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICustomTabBarController : UITabBarController
{
    
}

- (void)setupChildViewController:(UIViewController *)childVc title:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName;

@end
