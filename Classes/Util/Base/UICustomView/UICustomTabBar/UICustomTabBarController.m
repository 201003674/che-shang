//
//  UICustomTabBarController.m
//  IOSFramework
//
//  Created by 林科 on 14-9-16.
//  Copyright (c) 2014年 allianture. All rights reserved.
//

#import "UICustomTabBarController.h"

@interface UICustomTabBarController ()

@end

@implementation UICustomTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:HEXCOLOR(0X696969) ,NSFontAttributeName:[UIFont systemFontOfSize:12.0f]} forState:UIControlStateNormal];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:0 green:91/255.0 blue:172/255.0 alpha:1.0] ,NSFontAttributeName:[UIFont systemFontOfSize:12.0f]} forState:UIControlStateSelected];
    
    /* 注册 badgevalue 通知 */
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"changebadgeValue" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changebadgeValue:)
                                                 name:@"changebadgeValue"
                                               object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/**
 *  初始化一个子控制器
 *
 *  @param childVc           需要初始化的子控制器
 *  @param title             标题
 *  @param imageName         图标
 *  @param selectedImageName 选中的图标
 */
- (void)setupChildViewController:(UIViewController *)childVc title:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName
{ 
    // 设置控制器的属性
    childVc.title = title;
    // 设置图标
    childVc.tabBarItem.image = [UIImage imageNamed:imageName];
    // 设置选中的图标
    UIImage *selectedImage = [UIImage imageNamed:selectedImageName];
//    UIEdgeInsets imageInedgeInsets;
    if (IOS_VERSION_7)
    {
//        imageInedgeInsets  = UIEdgeInsetsMake(5.0, 0, -5.0, 0);
        childVc.tabBarItem.selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    else
    {
//        imageInedgeInsets = UIEdgeInsetsMake(0, 0, -5.0, 0);
        childVc.tabBarItem.selectedImage = selectedImage;
    }
//    [childVc.tabBarItem setImageInsets:imageInedgeInsets];
    
    childVc.title = title;
}


#pragma mark -
#pragma mark - changebadgeValue
- (void)changebadgeValue:(NSNotification*)notification
{
    //通过这个获取到传递的对象
    NSDictionary *badgeValueDic = [NSDictionary dictionaryWithDictionary:[notification object]];
    
    NSString *badgeValue = [NSString stringWithFormat:@"%@",[badgeValueDic objectForKey:@"badgeValue"]];
    STRING_NIL_TO_NONE(badgeValue);
    
    NSString *index = [NSString stringWithFormat:@"%@",[badgeValueDic objectForKey:@"index"]];
    STRING_NIL_TO_NONE(index);
    
    [self changebadgeValue:index WithBadgeValue:badgeValue];
}

- (void)changebadgeValue:(NSString *)index WithBadgeValue:(NSString *)badgeValue
{
    if (index.integerValue > self.viewControllers.count || self.viewControllers.count == 0)
    {
        return;
    }
    
    UICustomNavigationController *selectVC = (UICustomNavigationController *)[self.viewControllers objectAtIndex:index.integerValue];
    
    if(badgeValue.integerValue == 0)
    {
        return;
    }
    else  if (badgeValue.integerValue >= 100)
    {
        [selectVC.tabBarItem setBadgeValue:@"99+"];
    }
    else
    {
        [selectVC.tabBarItem setBadgeValue:badgeValue];
    }
}

#pragma mark -
#pragma mark -

@end
