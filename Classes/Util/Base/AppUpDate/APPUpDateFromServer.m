

#import "APPUpDateFromServer.h"
#import "AppDelegate.h"

@interface APPUpDateFromServer ()
{
    NSString *APPIDStr;
    NSMutableDictionary *updateMutDic;
    
    BOOL isMustUpdate;
}
@end


@implementation APPUpDateFromServer
@synthesize isLoading;
@synthesize sender_id;


- (id)init
{
    if (self == [super init])
    {
        updateMutDic = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}


#pragma -
#pragma mark - 软件版本更新
- (void)clickAppVer
{
    [[[ClassFactory getInstance] getNetComm] getAppInfoRequestSuccess:^(id JSON) {
        
        updateMutDic = [NSMutableDictionary dictionaryWithDictionary:JSON];
        
        NSString *resultCode = [NSString stringWithFormat:@"%@",[updateMutDic objectForKey:@"resultCode"]];
        STRING_NIL_TO_NONE(resultCode);
        if ([resultCode isEqualToString:@"1"])
        {
            /* 请求成功 */
            NSDictionary *responseObjectDic = [NSDictionary dictionaryWithDictionary:[[updateMutDic objectForKey:@"responseObject"] objectForKey:@"crmVersionInfoEO"]];
            
            NSString *platNewVer = [NSString stringWithFormat:@"%@",[responseObjectDic objectForKey:@"maxVersion"]];
            STRING_NIL_TO_NONE(platNewVer);
            NSString *platMinVer = [NSString stringWithFormat:@"%@",[responseObjectDic objectForKey:@"minVersion"]];
            STRING_NIL_TO_NONE(platMinVer);
            
            /* app版本号 */
            NSString *versionCodeStr = [[[ClassFactory getInstance] getLocalCfg] getVersionCode];
            STRING_NIL_TO_NONE(versionCodeStr);
            
            /* 是否必须更新 */
            NSComparisonResult mustComRes = [platMinVer compare:versionCodeStr options:NSCaseInsensitiveSearch];
            
            /* 后台返回强制跟新字段 */
            NSString *forceUpdateStr =[NSString stringWithFormat:@"%@",[responseObjectDic objectForKey:@"forceUpdate"]];
            STRING_NIL_TO_NONE(forceUpdateStr);
            /*更新内容*/
            NSString *updateContentStr=[NSString stringWithFormat:@"%@",[responseObjectDic objectForKey:@"remark"]];
            STRING_NIL_TO_NONE(updateContentStr);
            
            if (mustComRes > 0 || [forceUpdateStr isEqualToString:@"1"])
            {
                isMustUpdate = YES;
            }
            else
            {
                isMustUpdate = NO;
            }
            
            /* 最新版本号 */
            NSComparisonResult comResult = [platNewVer compare:versionCodeStr options:NSCaseInsensitiveSearch];
            
            if (comResult > 0)
            {
                /* 更新 */
                NSString *infoStr = [NSString stringWithFormat:@"当前版本V%@，可更新至最新版本V%@",versionCodeStr,platNewVer];
                
                [self alertUpdate:infoStr withUpdateContent:updateContentStr];
                
            }
            else if (comResult <= 0)
            {
                if (sender_id != nil)
                {
                    /* 取消更新 */
                    [self updateCancle];
                }
                else
                {
                    /* 已经是最新版 */
                    [[[ClassFactory getInstance] getInfoHUD] showHud:[NSString stringWithFormat:@"当前版本V%@已经是最新版本",versionCodeStr]];
                }
                
            }
        }
        else
        {
            /* 请求失败 */
            NSString *errMessage = [NSString stringWithFormat:@"%@",[updateMutDic objectForKey:@"errorMessage"]];
            STRING_NIL_TO_NONE(errMessage);
            
            /* 取消更新 */
            [self updateCancle];
        }
        
        
    } failure:^(NSError *error) {
        /* 取消更新 */
        
        [self updateCancle];
    }];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != [alertView cancelButtonIndex])
    {
        
        [self confirmUpdate];
    }
    else
    {
        /* 取消更新 */
        [self updateCancle];
        
    }
    
}

-(void)alertUpdate:(NSString *)strContent withUpdateContent:(NSString *)content
{
    
    BOOL isAlertUpdateShowed = NO;
    if (!isAlertUpdateShowed)
    {
        isAlertUpdateShowed=YES;
        
        UIAlertView *av = nil;
        
        if (isMustUpdate)
        {
            av = [[UIAlertView alloc] initWithTitle:@"升级提示"
                                            message:strContent
                                           delegate:self
                                  cancelButtonTitle:nil
                                  otherButtonTitles:@"马上更新",nil];
        }
        else
        {
            
            av = [[UIAlertView alloc] initWithTitle:@"升级提示"
                                            message:strContent
                                           delegate:self
                                  cancelButtonTitle:@"暂不更新"
                                  otherButtonTitles:@"马上更新",nil];
        }
        
        [av show];
    }
}
- (void)confirmUpdate
{
    NSDictionary *responseObjectDic = [NSDictionary dictionaryWithDictionary:[[updateMutDic objectForKey:@"responseObject"] objectForKey:@"crmVersionInfoEO"]];
    
    NSString *platDownloadUrl = [NSString stringWithFormat:@"%@",[responseObjectDic objectForKey:@"downloadUrl"]];
    STRING_NIL_TO_NONE(platDownloadUrl);
    
    if (platDownloadUrl == nil || [platDownloadUrl isEqualToString:@""])
    {
        /* 取消更新 */
        [self updateCancle];
    }
    
    /* 清空WEB缓存 */
    [[[ClassFactory getInstance] getLogic] clearTheWebCache];
    
    /* APP退出动画 */
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [UIView beginAnimations:@"exit" context:nil];
    [UIView setAnimationDuration:2.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:app.window cache:NO];
    [UIView setAnimationDidStopSelector:@selector(animationFinished:finished:context:)];
    app.window.bounds = CGRectMake(0, 0, 0, 0);
    app.window.alpha = 0.0f;
    [UIView commitAnimations];
}

/* 取消更新 */
- (void)updateCancle
{
    NSDictionary *userInfoDic = [NSDictionary dictionaryWithDictionary:[[updateMutDic objectForKey:@"responseObject"] objectForKey:@"appInfo"]];
    
    /* 取消更新 */
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateCancle" object:nil userInfo:userInfoDic];
}

//退出APP代码
- (void)animationFinished:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    if ([animationID compare:@"exit"] == 0)
    {
        NSDictionary *responseObjectDic = [NSDictionary dictionaryWithDictionary:[[updateMutDic objectForKey:@"responseObject"] objectForKey:@"crmVersionInfoEO"]];
        
        NSString *platDownloadUrl = [NSString stringWithFormat:@"%@",[responseObjectDic objectForKey:@"downloadUrl"]];
        STRING_NIL_TO_NONE(platDownloadUrl);
        
        /* 更新 */
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:platDownloadUrl]];
        
        /* 退出 */
        exit(0);
    }
}


@end
