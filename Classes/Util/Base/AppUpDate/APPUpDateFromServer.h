//
//  APPUpDateFromServer.h
//  GUOHUALIFE
//
//  Created by allianture on 13-6-25.
//  Copyright (c) 2013年 zte. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APPUpDateFromServer : NSObject
{
    BOOL isLoading;
    __unsafe_unretained id sender_id;
}

@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) id sender_id;

- (void)clickAppVer;


@end
