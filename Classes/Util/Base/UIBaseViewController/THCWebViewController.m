//
//  THCWebViewController.m
//  IOSFramework
//
//  Created by 林科 on 2018/8/27.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import "THCWebViewController.h"

@implementation THCWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setDelegate:self];
    
    [self setShowsToolBar:YES];
    
    //右侧分享按钮
    if (self.isShowRightButton)
    {
        UICustomButton *rightButton = [UICustomButton buttonWithType:UIButtonTypeCustom];
        
        [rightButton createButtonWithFrame:CGRectMake(SCREEN_WIDTH - 70,100, 50, 50) Title:@"分享" TitleColor:HEXCOLOR(0X99999) Font:12.0 Target:self Selector:@selector(leftButtonShare:) ImageNormal:@"" ImageHighlighted:@""];
        
        UIBarButtonItem* rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
        [self.navigationItem setRightBarButtonItem:rightItem];
    }
}
#pragma mark -
#pragma mark -
- (void)leftButtonShare:(UIButton *)button
{
    //分享title
    NSString *title = [NSString stringWithFormat:@"%@",self.webView.title];
    STRING_NIL_TO_NONE(title)
    
    //分享URL
    NSString *webpageUrl = [NSString stringWithFormat:@"%@",self.URL];
    STRING_NIL_TO_NONE(webpageUrl)
    
    BOOL success = [self WXApiHtmlShare:title WithDescription:@"" WithWebpageUrl:webpageUrl];
    
    if (success)
    {
        [[[ClassFactory getInstance] getInfoHUD] showHud:@"分享成功!"];
    }
    else
    {
        [[[ClassFactory getInstance] getInfoHUD] showHud:@"分享失败"];
    }
}

#pragma mark -
#pragma mark - 微信Html分享
- (BOOL)WXApiHtmlShare:(NSString *)title WithDescription:(NSString *)description WithWebpageUrl:(NSString *)webpageUrl
{
    // 0 type 1 title 2 description 3 webpageUrl
    //创建发送对象实例
    SendMessageToWXReq *sendReq = [[SendMessageToWXReq alloc] init];
    sendReq.bText = NO;//不使用文本信息
    sendReq.scene = 0;//0 = 好友列表 1 = 朋友圈 2 = 收藏
    
    //创建分享内容对象
    WXMediaMessage *urlMessage = [WXMediaMessage message];
    
    STRING_NIL_TO_NONE(title);
    urlMessage.title = title;//分享标题
    
    STRING_NIL_TO_NONE(description);
    urlMessage.description = description;//分享描述
    
    //分享图片,使用SDK的setThumbImage方法可压缩图片大小
    [urlMessage setThumbImage:[UIImage imageNamed:@"thcWaterMask.png"]];
    
    //创建多媒体对象
    WXWebpageObject *webObj = [WXWebpageObject object];
    
    STRING_NIL_TO_NONE(webpageUrl);
    webObj.webpageUrl = webpageUrl;//分享链接
    
    //完成发送对象实例
    urlMessage.mediaObject = webObj;
    sendReq.message = urlMessage;
    
    //发送分享信息
    return [WXApi sendReq:sendReq];
}

#pragma mark -
#pragma mark - viewWillAppear
- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

/// Called when web view will go back.
///
/// @param webViewController a web view controller.
- (void)webViewControllerWillGoBack:(AXWebViewController *)webViewController
{
    
}
/// Called when web view will go forward.
///
/// @param webViewController a web view controller.
- (void)webViewControllerWillGoForward:(AXWebViewController *)webViewController
{
    
}
/// Called when web view will reload.
///
/// @param webViewController a web view controller.
- (void)webViewControllerWillReload:(AXWebViewController *)webViewController
{
    
}
/// Called when web view will stop load.
///
/// @param webViewController a web view controller.
- (void)webViewControllerWillStop:(AXWebViewController *)webViewController
{
    
}
/// Called when web view did start loading.
///
/// @param webViewController a web view controller.
- (void)webViewControllerDidStartLoad:(AXWebViewController *)webViewController
{
    
}
/// Called when web view did finish loading.
///
/// @param webViewController a web view controller.
- (void)webViewControllerDidFinishLoad:(AXWebViewController *)webViewController
{
    
}
/// Called when web viw did fail loading.
///
/// @param webViewController a web view controller.
///
/// @param error a failed loading error.
- (void)webViewController:(AXWebViewController *)webViewController didFailLoadWithError:(NSError *)error
{
    
}

@end
