//
//  THCWebViewController.h
//  IOSFramework
//
//  Created by 林科 on 2018/8/27.
//  Copyright © 2018年 allianture. All rights reserved.
//
#import "WXApi.h"
#import "UIImage+Resize.h"

#import <AXWebViewController/AXWebViewController.h>

@interface THCWebViewController : AXWebViewController<AXWebViewControllerDelegate>
{
    
}

@property(assign, nonatomic) BOOL isShowRightButton;

@end
