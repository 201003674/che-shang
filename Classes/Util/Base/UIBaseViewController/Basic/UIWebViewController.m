//
//  UIWebViewController.m
//  IOSFramework
//
//  Created by allianture on 13-10-24.
//  Copyright (c) 2013年 allianture. All rights reserved.
//

#import "UIWebViewController.h"

@implementation UIWebViewController

#pragma mark -
#pragma mark - viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //底部背景色
    [self.view setBackgroundColor:HEXCOLOR(0XFFFFFF)];
    [self.webView setBackgroundColor:HEXCOLOR(0XFFFFFF)];
}

#pragma mark -
#pragma mark - UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView*)webView
{
    [super webViewDidStartLoad:webView];
    
    [[[ClassFactory getInstance] getNetComm] showHud:nil];
    
    NSString *documentTitle = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    STRING_ISNOTNIL(documentTitle);
    
    ///** 自动页面时长统计, 开始记录某个页面展示时长.
    // 使用方法：必须配对调用beginLogPageView:和endLogPageView:两个函数来完成自动统计，若只调用某一个函数不会生成有效数据。
    // 在该页面展示时调用beginLogPageView:，当退出该页面时调用endLogPageView:
    // @param pageName 统计的页面名称.
    // @return void.
    // */
    [MobClick beginLogPageView:documentTitle];
    [TalkingData trackPageBegin:documentTitle];
}

- (void)webViewDidFinishLoad:(UIWebView*)webView
{
    [super webViewDidFinishLoad:webView];
    
    [[[ClassFactory getInstance] getNetComm] hiddenHud];
    
    NSString *documentTitle = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    STRING_ISNOTNIL(documentTitle);
    
//    if ([documentTitle containsString:@"404 Not Found"])
//    {
//        //页面加载失败
//        NSString *path = [[NSBundle mainBundle] pathForResource:@"error" ofType:@"html" inDirectory:@"ErrorImages"];
//
//        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]]];
//    }
    
    [MobClick endLogPageView:documentTitle];
    
    [TalkingData trackPageEnd:documentTitle];
}

- (void)webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error
{
    [super webView:webView didFailLoadWithError:error];
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithDictionary:error.userInfo];
    NSString *failingURLStr = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"NSErrorFailingURLStringKey"]];
    STRING_NIL_TO_NONE(failingURLStr)
    
    NSString *message = [NSString stringWithFormat:@"%@", [error localizedDescription]];
    STRING_NIL_TO_NONE(message);
    [[[ClassFactory getInstance] getInfoHUD] showHud:message];
    

//    if([failingURLStr containsString:self.startPage])
//    {
//        //网络加载失败
//        NSString *path = [[NSBundle mainBundle] pathForResource:@"error_nonetwork" ofType:@"html" inDirectory:@"ErrorImages"];
//
//        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]]];
//    }

    [[[ClassFactory getInstance] getNetComm] hiddenHud];
}

#pragma mark -
#pragma mark - shouldStartLoadWithRequest
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *requestString = [NSString stringWithFormat:@"%@",[[[request URL]absoluteString] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    STRING_NIL_TO_NONE(requestString)

//    if ([requestString containsString:@"404 Not Found"])
//    {
//        //页面加载404
//        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.startPage]]];
//
//        return NO;
//    }
//    if ([requestString containsString:@"ERROR_NO_NETWORK"])
//    {
//        //网络加载失败
//        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.startPage]]];
//
//        return NO;
//    }
//    else
//    {
//
//    }
    
    return [super webView:webView shouldStartLoadWithRequest:request navigationType:navigationType];
}

#pragma mark -
#pragma mark -

@end




