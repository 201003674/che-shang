//
//  PlistManager.h
//  IOSFramework
//
//  Created by 林科 on 2017/4/9.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *const kInvalidFilenameMessage = @"Invalid filename; must be of at least 1 character.";
static NSString *const kIncompatibleMessage = @"Data provided is not compatible to be saved. To troubleshoot you can:\n1. Take a look at your data and your plist's root object\n2.Make sure your objects conform to the NSCoding protocol and/or are of these types: NSArray, NSDictionary, NSString, NSData, NSDate, NSNumber.";

@interface PlistManager : NSObject
{
    
}

@property (readonly, nonatomic) BOOL isEmpty;

+(PlistManager *)sharedInstance;

// create file
-(BOOL)createFile:(NSString *)fileName;
-(BOOL)createFile:(NSString *)fileName filePath:(NSString*)path;
-(BOOL)createFile:(NSString *)fileName dataToFile:(NSMutableArray*)data;
-(BOOL)createFile:(NSString *)fileName filePath:(NSString*)path
       dataToFile:(NSMutableArray *)data;


// write to file
-(BOOL)writeToFile:(NSString*)fileName dataToFile:(NSMutableArray*)data;
-(BOOL)writeToFile:(NSString*)fileName dataToFile:(NSMutableArray*)data
          filePath:(NSString*)path;

// read from file
-(NSMutableArray*)readFromFile:(NSString*)fileName;
-(NSMutableArray*)readFromFile:(NSString*)fileName filePath:(NSString*)path;
-(NSMutableArray*)readFromResourcePlist:(NSString*)namePlist;

// output file names are on the way "./document/path"
-(NSArray*)checkFileFromFolder:(NSString*)path;

// delete file
-(BOOL)deleteFile:(NSString*)filePath;

@end
