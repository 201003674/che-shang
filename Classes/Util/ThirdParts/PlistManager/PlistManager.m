//
//  PlistManager.m
//  IOSFramework
//
//  Created by 林科 on 2017/4/9.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import "PlistManager.h"

@implementation PlistManager

static PlistManager *instance = nil;

+(PlistManager *)sharedInstance
{
    if (instance == nil)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            instance = [[PlistManager alloc] init];
        });
    }
    return instance;
}

- (id)init
{
    if (self = [super init])
    {
        
    }
    return self;
}


// create file in root folder (./document/)
-(BOOL)createFile:(NSString *)fileName

{
    NSArray *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [documentPath objectAtIndex:0];
    
    NSString *newPlistFile = [documentFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist", fileName]];
    
    NSMutableArray * array = [[NSMutableArray alloc]init];
    
    BOOL success = [array writeToFile:newPlistFile atomically:YES];
    
    return success;
}

// create file in custom folder (./document/path/)
-(BOOL)createFile:(NSString *)fileName filePath:(NSString *)path

{
    NSArray *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [documentPath objectAtIndex:0];
    
    NSString * pathFolder = [NSString stringWithFormat:@"%@/%@/", documentFolder,path];
    
    NSString *newPlistFile = [documentFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.plist",path,fileName]];
    
    NSFileManager *defaultManager = [NSFileManager defaultManager];
    
    if (![defaultManager fileExistsAtPath:path])
        [defaultManager createDirectoryAtPath:pathFolder withIntermediateDirectories:YES attributes:nil error:nil];
    
    NSMutableArray * array = [[NSMutableArray alloc]init];
    
    BOOL success = [array writeToFile:newPlistFile atomically:YES];
    
    return success;
}

//  create file with data in root folder (./document/)
-(BOOL)createFile:(NSString *)fileName dataToFile:(NSMutableArray*)data
{
    NSArray *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [documentPath objectAtIndex:0];
    
    NSString *newPlistFile = [documentFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist", fileName]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:newPlistFile])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:newPlistFile withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    BOOL success = [data writeToFile:newPlistFile atomically:YES];
    
    return success;
}

//  create file with data in custom folder (./document/path)
-(BOOL)createFile:(NSString *)fileName filePath:(NSString *)path dataToFile:(NSMutableArray *)data

{
    NSArray *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [documentPath objectAtIndex:0];
    
    NSString * pathFolder = [NSString stringWithFormat:@"%@/%@/", documentFolder,path];
    
    NSString *newPlistFile = [documentFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.plist",path,fileName]];
  
    if (![[NSFileManager defaultManager] fileExistsAtPath:newPlistFile])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:pathFolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    BOOL success = [data writeToFile:newPlistFile atomically:YES];
    
    return success;
}


// write to file
-(BOOL)writeToFile:(NSString *)fileName dataToFile:(NSMutableArray *)data
{
    BOOL isCompatible = [self isDataCompatibleToSave:data];
    
    if (!isCompatible)
    {
        return NO;
    }
    
    NSString *path = [NSString stringWithFormat:@"%@.plist",FileDocPath(fileName)];
    STRING_NIL_TO_NONE(path)
    
    return [data writeToFile:path atomically:YES];
}

// write to file on the path
-(BOOL)writeToFile:(NSString *)fileName dataToFile:(NSMutableArray *)data filePath:(NSString *)path
{
    NSArray *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [documentPath objectAtIndex:0];
    
    NSString * pathFolder = [NSString stringWithFormat:@"%@/%@/", documentFolder,path];
    
    NSString *newPlistFile = [documentFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.plist",path,fileName]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:pathFolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    return [data writeToFile:newPlistFile atomically:YES];
}


// read from file
-(NSMutableArray*)readFromFile:(NSString *)fileName
{
    NSString *path = [NSString stringWithFormat:@"%@.plist",FileDocPath(fileName)];
    STRING_NIL_TO_NONE(path)
    
    NSMutableArray *arr = [NSMutableArray arrayWithContentsOfFile:path];
    
    return arr;
}

// read from file in root folder (.plist created in the project root)
-(NSMutableArray*)readFromResourcePlist:(NSString*)namePlist
{
    NSString *path = [[NSBundle mainBundle] pathForResource:
                      namePlist ofType:@"plist"];
    NSMutableArray *array = [[NSMutableArray alloc] initWithContentsOfFile:path];
    for (NSString *str in array)
    {
        NSLog(@"--%@", str);
    }
    
    return array;
}

// read from file on the path
-(NSMutableArray*)readFromFile:(NSString *)fileName filePath:(NSString *)path
{
    NSArray *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [documentPath objectAtIndex:0];
    
    NSString * pathFolder = [NSString stringWithFormat:@"%@/%@/", documentFolder,path];
    
    NSString *plistPath = [documentFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.plist",path,fileName]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:pathFolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    NSMutableArray *dict = [NSMutableArray arrayWithContentsOfFile:plistPath];
    
    return dict;
    
}


// output file names are on the way "./document/path"
-(NSArray*)checkFileFromFolder:(NSString *)path
{
    NSArray *documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentFolder = [documentPath objectAtIndex:0];
    
    
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[NSString stringWithFormat:@"%@/%@/",documentFolder,path] error:nil];
    
    for (int i = 0; i<[dirContents count ]; i--) {
        NSLog(@"document  = %@",[dirContents objectAtIndex:i]);
    }
    
    return dirContents;
    
}

// delete file
-(BOOL)deleteFile:(NSString *)filePath
{
    NSString *path = [NSString stringWithFormat:@"%@.plist",FileDocPath(filePath)];
    STRING_NIL_TO_NONE(path)
      
    NSError *error;
    
    if ([[NSFileManager defaultManager] removeItemAtPath:path error:&error] != YES)
    {
        NSLog(@"Unable to delete file: %@", [error localizedDescription]);
        
        return NO;
    }
    return YES;
}

- (BOOL)isDataCompatibleToSave:(id)data
{
    BOOL isCompatible = [NSPropertyListSerialization propertyList:data isValidForFormat:NSPropertyListXMLFormat_v1_0];
    NSAssert(isCompatible, kIncompatibleMessage);
    
    return isCompatible;
}

@end
