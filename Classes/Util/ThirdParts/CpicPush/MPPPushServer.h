/*
 *  注意:修改build settings->other linker flag :-all_load
 *      修改info.plist->添加字典App Transport Security Settings:{Allow Arbitrary Loads:YES}
 *
 *
 */
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
@protocol MPPCallBackDelegate <NSObject>

@optional
/* 透传消息(接收从极光透传过来的消息，不经过苹果服务器) */
-(void)transparentReceiveTitle:(NSString *)title content:(NSString *)content;
/* 通知消息处理 只针对iOS10 app处于前台 处于后台 走此代理方法接收通知*/
-(void)pushMessageCallBackResult:(UNNotificationContent *)content;
/* 注册失败 极光返回错误信息*/
-(void)pushRegisterErrorMessage:(NSString *)error;
/* 绑定接口失败  registedID获取不到 手机推送权限关闭走此代理方法*/
-(void)pushIphoneNotAllowedNotification;
@end


@interface MPPPushServer : NSObject
//透传 通知代理
@property(nonatomic,weak)id<MPPCallBackDelegate>delegate;
/**
 *
 *  此实例化方法只在接收透传通知时候使用 mppPush.delegate=self;
 *
 */
+(instancetype)sharePushManager;
/**
 *  初始化
 *
 *  @param launchOptions 从AppDelegate.m 传入
 *  code   随机生成的数，后台人员提供     在PushServerConfig中配置
 *  url    后台人员提供的接口地址        在PushServerConfig中配置
 */
+(void)initWithOptions:(NSDictionary *)launchOptions MPPAPPCode:(NSString *)code pushUrl:(NSString *)url;
/**
 *  绑定  一定要放在初始化之后
 *  tags name  都不能为空,tags是标签数组,name是用户名
 */
+(void)bindPushWithTags:(NSArray *)tagArray userName:(NSString *)name;
/**
 *  解绑
 *  yes:成功  no:失败
 */
+(void)delBindPushWithCallBack:(void (^)(BOOL isSucess))block;
/**
 *
 *  @param deviceToken 设备DeviceToken
 *
 */
+(void)registerDeviceToken:(NSData *)deviceToken;
/**
 *
 *  @param userInfo 处理收到的APNS消息
 *
 */
+(void)handleRemoteNotification:(NSDictionary *)userInfo;
/**
 *  接收到通知消息
 *
 *  @param userInfo          收到推送字典
 *  @param completionHandler
 */
+(void)didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;
/**
 *  消除角标
 *
 *  @param application 程序将要进入前台
 */
+ (void)applicationWillEnterForeground:(UIApplication *)application;
/**
 *  消除角标
 *
 *  @param application 程序进入后台
 */
+ (void)applicationDidEnterBackground:(UIApplication *)application;
/**
 *  判断手机设置是否允许推送
 *
 *  YES :允许 NO:不允许
 */
+(BOOL)isAllowedNotification;
/**
 *  读取日志
 */
+(NSString *)readFromLogTxt;
/**
 *  获取注册的id
 */
+(NSString *)registedID;
/*******************************************可选项**************************************************/
/**
 *  新增标签
 *
 *  @param tagArray 标签数组
 *  @param block    返回值
 */
+(void)addTags:(NSArray *)tagArray;

/**
 *  删除标签
 *
 *  @param tags  需删除标签值
 */
+(void)delTags:(NSString *)tags;
/**
 *  从标签移除用户
 *
 *  @param tags  需移除标签值
 */
+(void)removeFromUsertags:(NSString *)tags;
/**
 *  更新别名
 *
 *  @param alias 极光需设置此值
 */
+(void)resetAlias:(NSString *)alias;
/**
 *  查询标签
 *
 *  @param block 返回值
 */
+(void)searchTagsWithcallback:(void (^)(id result, id error))block;

/******************************************测试方法***********************************************************/
/**
 *  平台推送
 *
 *  @param messgae                       
                                            key
                                    accountCode(string)
                                    priority(int)         推送优先级
                                    msgType(int)
                                    title(string)
                                    msgContent(string)    推送消息内容
                                    platform(int)         平台
                                    sendTime(string)      推送时间        格式 yyyy-MM-dd HH:mm:ss
                                    msgExpires(int)       消息离线保留时间
 
 *  @param block   返回值
 */
+(void)pushPlatFormMessage:(NSMutableDictionary *)messgae;
/**
 *  用户推送 列如
 dic[@"appCode"]=@"0a55e77c31e842c9a9707ef8fb6f5606";
 dic[@"accountCode"]=@"mam";
 dic[@"encryptFlag"]=@1;
 dic[@"msgType"]=@0;
 dic[@"title"]=@"中国太保";
 dic[@"msgContent"]=@"内容";
 dic[@"platform"]=@4;
 dic[@"sendTime"]=nil;
 dic[@"msgExpires"]=@10000;
 dic[@"userArray"]=@[[UserEntity sharedUser].LoginName];
 dic[@"receiptFlag"]=@1;
 *
 */
+(void)pushUsersMessage:(NSMutableDictionary *)message;
/**
 *  单标签推送
 *
 */
+(void)pushOneTagMessage:(NSMutableDictionary *)message;
/**
 *  组合标签推送
 *
 */
+(void)combinationTags:(NSMutableDictionary *)message;
/**
 *  单用户推送回执
 *  rec 为1 表示回执,0表示不回执
 */
+(void)usercallbackWithReceiptId:(NSString *)rec callback:(void (^)(id result, NSError *error))block;
/**
 *  分页推送历史查询接口
 */
+(void)searchListWithRequestMessage:(NSDictionary *)message callback:(void (^)(id result, NSError *error))block;
/**
 *  设置角标
 */
+ (BOOL)setBadge:(NSInteger)value;
@end
