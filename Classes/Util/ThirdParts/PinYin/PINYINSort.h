//
//  PINYINSort.h
//  转发人
//
//  Created by 上海耸智 on 14-9-9.
//  Copyright (c) 2014年 tianping. All rights reserved.
//


#import "POAPinyin.h"
#import "PinYinO.h"

#import <Foundation/Foundation.h>

@interface PINYINSort : NSObject



/**
 *	@brief	拼音首字母 排序得到所有的首字母排序数组
 
 给一个数组 allagentarr  即可返回该数组所有的首字母排序数组
 */
-(NSArray *)sortByIndexPinyin:(NSArray *)tmparr;



/**
 *	@brief 数组更具拼音排序  更具索引返回数组中相应地数据数组
 *
 *	@param 	tmparr
 *	@param 	key
 *
 *	@return
 */
- (NSArray *)sortBypinyinInSection:(NSArray *)tmparr sectionKey:(NSString *)key
;
@end
