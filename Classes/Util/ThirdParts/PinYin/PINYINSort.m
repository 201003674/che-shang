//
//  PINYINSort.m
//  转发人
//
//  Created by 上海耸智 on 14-9-9.
//  Copyright (c) 2014年 tianping. All rights reserved.
//

#import "PINYINSort.h"

@implementation PINYINSort




/**
 *	@brief	拼音首字母 排序得到所有的首字母排序数组
 
 给一个数组 allagentarr  即可返回该数组所有的首字母排序数组
 */
-(NSArray *)sortByIndexPinyin:(NSArray *)tmparr
{
    if(!tmparr)
    {
        return nil;
    }
    char indexname = '?';
    
    NSMutableArray *allKeysArr = [NSMutableArray array];
    
    for(id name in tmparr)
    {
        indexname = pinyinFirstLetter([name characterAtIndex:0]);
        
        if(indexname >= 'a'&& indexname <= 'z')
        {
            indexname = indexname - 32;
        }
        NSString *key = [NSString stringWithFormat:@"%c",indexname];
        
        int flag=0;
        
        for(NSUInteger ii=0;ii<allKeysArr.count;ii++)
        {
            if([key isEqualToString:[allKeysArr objectAtIndex:ii]])
            {
                flag=1;
                break;
                
            }
            
        }
        if(flag==0)
        {
            [allKeysArr addObject:key];
        }
        
        
    }
    
    //排序
    NSComparator cmptr = ^(NSString *obj1,NSString *obj2)
    {
        if([obj1 characterAtIndex:0]> [obj2 characterAtIndex:0])
        {
            return (NSComparisonResult)NSOrderedDescending;
        }
        if([obj1 characterAtIndex:0]< [obj2 characterAtIndex:0])
        {
            return (NSComparisonResult)NSOrderedAscending;
        }
        
        return (NSComparisonResult)NSOrderedSame;
    };
    
    [allKeysArr sortUsingComparator:cmptr];
    
    
    
    return allKeysArr;
    
}

/**
 *	@brief 数组更具拼音排序  更具索引返回数组中相应地数据数组
 *
 *	@param 	tmparr
 *	@param 	key
 *
 *	@return
 */
- (NSArray *)sortBypinyinInSection:(NSArray *)tmparr sectionKey:(NSString *)key

{
    char indexname;
    
    NSLog(@"%@",key);
    
    NSMutableArray *secArr = [NSMutableArray array];
    for(id name in tmparr)
    {
        indexname = pinyinFirstLetter([name characterAtIndex:0]);
        if(indexname>='a'&& indexname<='z')
        {
            indexname = indexname-32;
        }
        else
        {
            indexname = '#';
        }
        
        
        if(indexname ==[key characterAtIndex:0])
        {
            PinYinO *py = [[PinYinO alloc]init];
            py.hz = name;
            py.pinyin = [POAPinyin convert:name];
            [secArr addObject:py];
        }
    }
    //排序
    //拼音排序结果
    
    
    NSLog(@"%@",secArr);
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"pinyin" ascending:YES]];
    [secArr sortUsingDescriptors:sortDescriptors];
    
    NSMutableArray *resultarr = [NSMutableArray array];
    for(PinYinO *name in secArr)
    {
        NSLog(@"%@",name.hz);
        NSString *namestr = name.hz;
        [resultarr addObject:namestr];
    }
    
    NSLog(@"%@",resultarr);
    
    
    return resultarr;
    
}


@end
