//
//  MJPhotoToolbar.h
//  FingerNews
//
//  Created by mj on 13-9-24.
//  Copyright (c) 2013年 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MJPhotoToolbarDelegate <NSObject>

-(void)rotationImageWithIndex:(NSUInteger )index;

@end
@interface MJPhotoToolbar : UIView
// 所有的图片对象
@property (nonatomic, strong) NSArray *photos;
// 当前展示的图片索引
@property (nonatomic, assign) NSUInteger currentPhotoIndex;
//代理对象
@property (nonatomic, weak) id<MJPhotoToolbarDelegate> toolbarDelegate;

@end
