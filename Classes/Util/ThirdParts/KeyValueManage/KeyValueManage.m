//
//  KeyValueManage.m
//  KeyValueCache
//
//  Created by Allianture on 14-4-28.
//  Copyright (c) 2014年 Allianture. All rights reserved.
//

#import "KeyValueManage.h"

@implementation KeyValueManage

static KeyValueManage *instance = nil;

+(KeyValueManage *)sharedInstance
{
    if (instance == nil)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            instance = [[KeyValueManage alloc] init];
        });
    }
    return instance;
}


- (id)init
{
    if (self = [super init])
    {
        
    }
    return self;
}

/* 创建键值缓存 */
-(void)KeyValueCreat:(id)value Key:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
}

/* 删除键值缓存 */
-(void)KeyValueremove:(NSString *)key
{
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults removeObjectForKey:key];
}

/* 获取键值缓存 */
-(id)KeyValueGet:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    return [defaults objectForKey:key];
}
@end
