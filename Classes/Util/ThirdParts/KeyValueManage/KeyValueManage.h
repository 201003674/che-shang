//
//  KeyValueManage.h
//  KeyValueCache
//
//  Created by Allianture on 14-4-28.
//  Copyright (c) 2014年 Allianture. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeyValueManage : NSObject

+(KeyValueManage *)sharedInstance;

/* 创建键值缓存 */
-(void)KeyValueCreat:(id)value Key:(NSString *)key;

/* 删除键值缓存 */
-(void)KeyValueremove:(NSString *)key;

/* 获取键值缓存 */
-(id)KeyValueGet:(NSString *)key;

@end
