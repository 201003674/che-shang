//扩展上传图片插件
;(function($) {
	$.fn.scratcher = function(list,defaultValue, Ycallback, Ncallback) {
		    	//插件默认选项
				var that = $(this);
				var datetime = false;
				var indexY = 1;
				var initY = list.indexOf(defaultValue);//初始化显示数据位置
				var contentScroll = null;
				$.fn.scratcher.defaultOptions = {
						//theme: "iScroll", //控件样式
						mode: null, //操作模式（滑动模式）
						event: "click", //打开插件默认方式为点击后后弹出
						array: list,
						show: true
					}
					//用户选项覆盖插件默认选项   
				var opts = $.extend(true, {}, $.fn.scratcher.defaultOptions, list);
				if (!opts.show) {
					that.unbind('click');
				} else {
					//绑定事件（默认事件为获取焦点）
						createUL(); //动态生成控件显示的内容
						init_iScrll(); //初始化iscrll
						extendOptions(); //显示控件
						that.blur();
						refreshData();
						bindButton();
				};
				function refreshData() {
					contentScroll.refresh();
					contentScroll.scrollTo(0, initY * 40, 100, true);
				}

				function bindButton() {
					$("#dateconfirm").unbind('click').click(function() {
						var datestr = $("#contentwrappers ul li:eq(" + indexY + ")").text().substr(0, $("#contentwrappers ul li:eq(" + indexY + ")").text().length);
						if (Ycallback === undefined) {
							return datestr;
						} else {
							$('#datePages').remove();
							$('#dateshadow').remove();
							//Ycallback(datestr
							Ycallback(datestr,indexY-1);
						}
					});
					$("#datecancle").click(function() {
						$('#datePages').remove();
						$('#dateshadow').remove();
//						Ncallback(false);
					});
				}

				function extendOptions() {
					$("#datePages").show();
					$("#dateshadow").show();
				}

				function init_iScrll() {
					var strY = $("#contentwrappers ul li:eq(" + indexY + ")").text().substr(0, $("#contentwrappers ul li:eq(" + indexY + ")").text().length);
					contentScroll = new iScroll("contentwrappers", {
						snap: "li",
						vScrollbar: false,
						onScrollEnd: function() {
							indexY = Math.ceil((this.y / 40) * (-1) + 1);
							//console.log(Math.ceil(indexY))
							$("#contentwrappers ul li:eq(" + indexY + ")").css({
								'color': 'black',
								'font-size': '.9em'
							});
							$("#contentwrappers ul li:eq(" + indexY + ")").siblings().css({
								'color': '#898989',
								'font-size': '.6em'
							});
						}
					});
				}

				function createUL() {
					CreateDateUI();
					$("#contentwrappers ul").html(createCONTENT_UL());
				}

				function CreateDateUI() {
					var str = '' +
						'<div id="dateshadow"></div>' +
						'<div id="datePages" class="pages">' +
							'<footer id="dateFooters">' +
								'<div id="setcancles">' +
									'<ul>' +
										'<li id="datecancle">取消</li>' +
										'<li id="dateconfirm">确认</li>' +
									'</ul>' +
								'</div>' +
							'</footer>' +
							'<section>' +
								'<div id="datemarks"></div>' +
								'<div id="datescrolls">' +
									'<div id="contentwrappers">' +
										'<ul></ul>' +
									'</div>' +
									'<div class="lineScratcher"></div>'+
								'</div>' +
							'</section>' +
						'</div>'
					$("body").append(str);
				}

				function createCONTENT_UL() {
					var str = "<li>&nbsp;</li>";
					for (var i = 0; i < opts.array.length; i++) {
						str += '<li>' + opts.array[i] + '</li>'
					}
					return str + "<li>&nbsp;</li>";
				}
			}
})($)
