$(function(){
	vm.init();
});
//报价清单分页参数
var pagePrams = {
	page: 0,
	pageSize: 10,
	totalPage:1
};
//离线清单数据
var surveyList=[];
//报价清单数据
var quotationDataList = [];
var vm={
		//返回crm我页面
		goCrmMyInfo:function(){
			//window.location.href = './crm/pages/mine/myInfo.html';
		},
		//返回crm车险我的任务页面
		goCrmMyTask:function(){
			//window.location.href = './crm/pages/mine/myTask.html';
		},
		//刷新页面
		doRefresh:function(){
			pagePrams.page=0;
			vm.searchIndexDB();
		},
		//选择状态
		selectStatus:function(){
			var params=[];
			var defaultVal = $("#statusCode").html();
			var quotationStatusData = [{name:"全部"},{code: "21", name: "等待采集"},{code: "22", name: "采集审核"}];
			$.each(quotationStatusData,function(index,element){
				params.push(element.name);
			});
			$("#datePages").scratcher(params,defaultVal,function(result,targetIndex){
				$("#statusCode").html(result);
				$('#getMore').css("display","none");
				$('#getNoMore').css("display","none");
				pagePrams.page=0;
				vm.searchIndexDB();
			});
		},
		init:function(){
			//从indexDB取数据
			vm.openANDgetIndexDB(vm.searchIndexDB);
		},
		clickToFn:function(status,surveyId) {
			//等待采集
			if(status=='21'){
				vm.changeIndexDB(surveyId);
				window.location.href = './paulBeforeView_indexDB.html';
			}
			//采集审核
			else if(status=='22'){
			    window.location.href = './paulBeforeView_indexDB.html';
			}
		},
		//从indexDB中取得数据
		openANDgetIndexDB:function(successfn){
			window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
			//浏览器是否支持IndexedDB
			if (window.indexedDB) {
				   //打开数据库，如果没有，则创建
				   var openRequest = window.indexedDB.open("DB90", 1);
				   openRequest.onupgradeneeded = function(e) {
					   console.log("onupgradeneeded");
					   var thisDB = e.target.result;
				       if(!thisDB.objectStoreNames.contains("surveyListTable")) {
				           //创建存储对象， 还创建索引
				    	   var objectStore = thisDB.createObjectStore("surveyListTable",{keyPath:"surveyId"});
				    	   objectStore.createIndex("surveyId","surveyId", {unique:true});
				       }
				   };
				   //DB成功打开回调
				   openRequest.onsuccess = function(e) {
					 //保存全局的数据库对象，后面会用到
					    db = e.target.result;
					    //var data=[];
					    db.transaction(["surveyListTable"], "readonly").objectStore("surveyListTable").openCursor().onsuccess = function(e) {
					    	var cursor = e.target.result;
					        if(cursor) {
					        	surveyList.push(cursor.value);
					            cursor.continue();
					         }else{
					        	 if(surveyList.length!=0){
					        		 successfn();
					        	 }else{
					        		 $('#getMore').css("display","none");
									 $('#getNoMore').css("display","block");
					        	 }
					         }
					    };
				   };
				   //DB打开失败回调
				   openRequest.onerror = function(e) {
				      console.log("Error");
				    };
				}else{
				    alert('Sorry! Your browser doesn\'t support the IndexedDB.');
				}
		},
		//修改indexDB
		changeIndexDB:function(surveyId){
			transaction = db.transaction(["surveyListTable"],"readwrite");
			store = transaction.objectStore("surveyListTable");
			var index = store.index("surveyId");
			var request = index.get(surveyId);
		    request.onsuccess = function(e) {
		       var result = e.target.result;
		       result.status="22";
		       result.statusName="采集审核";
		       store.put(result);
		       surveyList=[];
		       vm.openANDgetIndexDB(vm.searchIndexDB);
		    }
		},
		//查询list列表
		searchIndexDB:function(){
	    	quotationDataList=[];
	    	var quoSearchword = $("#searchWord").val();
			var statusCode = $('#statusCode').text();
			if(statusCode=="全部"){
				statusCode="";
			}else if(statusCode=="等待采集"){
				statusCode="21";
			}else if(statusCode=="采集审核"){
				statusCode="22";
			}
			for(var i=0;i<surveyList.length;i++){
    			if((statusCode==undefined || statusCode=="") && (quoSearchword==undefined || quoSearchword=="")){
    				quotationDataList.push(surveyList[i]);
    			}else if((statusCode!=undefined && statusCode!="") && (quoSearchword==undefined || quoSearchword=="")){
    				if(surveyList[i].status.indexOf(statusCode)>-1){
    					quotationDataList.push(surveyList[i]);
    				}
    			}else if((statusCode==undefined || statusCode=="") && (quoSearchword!=undefined && quoSearchword!="")){
    				if(surveyList[i].projectName.indexOf(quoSearchword)>-1){
    					quotationDataList.push(surveyList[i]);
    				}
    			}else if((statusCode!=undefined && statusCode!="") && (quoSearchword!=undefined && quoSearchword!="")){
    				if((surveyList[i].status.indexOf(statusCode)>-1) && (surveyList[i].projectName.indexOf(quoSearchword)>-1)){
    					quotationDataList.push(surveyList[i]);
    				}
    			}
			}
			totalPage=Math.ceil((quotationDataList.length)/10);
			var len=0;
			if(quotationDataList.length>(pagePrams.page+1)*10){
					len=(pagePrams.page+1)*10;
				}else{
					len=quotationDataList.length;
				}
				if(quotationDataList.length<(pagePrams.page+1)*10){
	    			$('#getMore').css("display","none");
					  $('#getNoMore').css("display","block");
	    		}else{
	    			$('#getMore').css("display","block");
					  $('#getNoMore').css("display","none");
	    		}
			quotationDataList=quotationDataList.slice(0,len);
			var listHtml="";
			$('#quotationDataList').html("");
			for(var i=0;i<quotationDataList.length;i++){
				listHtml+='<div class="item-list item">'
					+' <div class="row">'
					+' 		<div class="col-5 textAlignCenter">'
					+' 			<div class="panel-heading">';
				if((quotationDataList[i].status + quotationDataList[i].policyFlag)=="210"){
					listHtml+='<i class="ion-wait-square"></i>';
				}
				listHtml+=
					' 			</div>'
					+' 		</div>'
					+' 		<div class="col-65 textAlignLeft">'
					+' 			<ul>'
					+' 				<li class="headingText cutLength ng-binding">'+quotationDataList[i].projectName+'</li>'
					+' 				<li class="list-group-item ng-binding">更新时间:'+formatDate(quotationDataList[i].updateDate)+'</li>'
					+' 			</ul>'
					+' 		</div>'
					+' 		<div class="col-30 textAlignCenter" onclick="vm.clickToFn(\''+quotationDataList[i].status+'\',\''+quotationDataList[i].surveyId+'\')">'
					+' 			<div class="panel-point">'
					+' 					<div class="quotationStatus ng-binding">'+quotationDataList[i].statusName+'</div>'
					+' 					<i class="icon ion-ios-arrow-right color9d"></i>'
					+' 			</div>'
					+' 		</div>'
					+' </div>'
					+' </div>'
			}
			$('#quotationDataList').append(listHtml);
    		if(pagePrams.totalPage !== totalPage){
				pagePrams.totalPage = totalPage;
			}
			if(pagePrams.page < pagePrams.totalPage){
				pagePrams.page++;
			}
		}	
	};
function formatDate(updateTime){
	var date=new Date(updateTime);
	var year=date.getFullYear();
	var month=formatNum(date.getMonth()+1);
	var day=formatNum(date.getDate());
	var hour=formatNum(date.getHours());
	var minute=formatNum(date.getMinutes());
	return year+"-"+month+"-"+day+" "+hour+":"+minute;
}
function formatNum(num){
	if(num<10){
		return num="0"+num;
	}else{
		return num;
	}
}
(function($){
	$.fn.search = function(){
		$(this).on('blur', function() {
			$('#getMore').css("display","none");
			$('#getNoMore').css("display","none");
			pagePrams.page=0;
			vm.searchIndexDB();
		});
	}
})($)
$('#searchWord').search();