//
//  MainCollectionViewCell.h
//  DemoIPA
//
//  Created by FBI01 on 15/8/20.
//  Copyright (c) 2015年 FBI01. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end
