//
//  ShareWX.h
//  CRM
//
//  Created by 林科 on 16/1/15.
//  Copyright © 2016年 FBI01. All rights reserved.
//

#import <Cordova/CDV.h>
#import <Cordova/CDVPlugin.h>
#import <Foundation/Foundation.h>

#import "WXApi.h"
#import "UIImage+Resize.h"

@interface ShareWX : CDVPlugin<WXApiManagerDelegate>
{
    
}

@property (nonatomic, strong) CDVInvokedUrlCommand *command;

#pragma mark -
#pragma mark - 分享
-(void)shareWX:(CDVInvokedUrlCommand*)command;

#pragma mark -
#pragma mark - 登录
-(void)loginWX:(CDVInvokedUrlCommand*)command;


@end
