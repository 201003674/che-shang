//
//  ShareWX.m
//  CRM
//
//  Created by 林科 on 16/1/15.
//  Copyright © 2016年 FBI01. All rights reserved.
//

#import "ShareWX.h"
#import "AppDelegate.h"

@implementation ShareWX

#pragma mark -
#pragma mark - 分享
-(void)shareWX:(CDVInvokedUrlCommand*)command
{
    if (![WXApi isWXAppInstalled])
    {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                          messageAsString:@"微信未安装!"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
        return;
    }
    
    NSString *destType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(destType);
    
    BOOL webviewResult = YES;

    if([destType rangeOfString:@"getPicFromServer.do"].location != NSNotFound)
    {
        webviewResult = [self WXApiImageShare:destType];
    }
    else if([destType isEqualToString:@"HTML"])
    {
        //H5分享
        webviewResult = [self WXApiHtmlShare:destType WithCDVInvokedUrlCommand:command];
    }
    else if([destType rangeOfString:@"data:image/png"].location != NSNotFound)
    {
        //图片分享
        webviewResult = [self WXApiBaseImageShare:destType];
    }
    else
    {
        //文字分享
        webviewResult = [self WXApiTextShare:destType];
    }
    
    if(webviewResult)
    {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                          messageAsString:@"分享成功"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
    else
    {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                          messageAsString:@"分享失败"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

#pragma mark -
#pragma mark - 微信图片分享
- (BOOL)WXApiImageShare:(NSString *)imageDataString
{
    WXImageObject *req = [[WXImageObject alloc] init];
    if (!req) {
        req = [[WXImageObject alloc] init];
    }
    
    NSString *finialUrlString = [NSString stringWithFormat:@"%@/%@",[[[ClassFactory getInstance] getLocalCfg] getHttpServerAddr],imageDataString];
    STRING_NIL_TO_NONE(finialUrlString);
    
    NSData *imageFinialData = [NSData dataWithContentsOfURL:[NSURL URLWithString:finialUrlString]];
    
    req.imageData = imageFinialData;
    
    WXMediaMessage *message = [WXMediaMessage message];
    
    if (!message) {
        message = [WXMediaMessage message];
    }
    
    UIImage *tempThumbImageData = [UIImage imageWithData:imageFinialData];
    
    NSData  *thumbImageData = UIImageJPEGRepresentation([UIImage compressImage:tempThumbImageData toTargetWidth:100.0],0.00001);
    
    UIImage *thumbImage = [UIImage imageWithData:thumbImageData];
    [message setThumbImage:thumbImage];
    
    message.mediaObject = req;
    
    SendMessageToWXReq *wxReq = [[SendMessageToWXReq alloc]init];
    if (!wxReq) {
        wxReq = [[SendMessageToWXReq alloc]init];
    }
    
    wxReq.bText = NO;
    wxReq.message = message;
    wxReq.scene = WXSceneSession;
    
    return [WXApi sendReq:wxReq];
}

#pragma mark -
#pragma mark - 微信文字分享
- (BOOL)WXApiTextShare:(NSString *)textDataString
{
    if (STRING_ISNIL(textDataString))
    {
        [[[ClassFactory getInstance] getInfoHUD] showHud:@"分享内容不能为空！"];
        
        return NO;
    }
    
    SendMessageToWXReq *wxReq = [[SendMessageToWXReq alloc]init];
    if (!wxReq) {
        wxReq = [[SendMessageToWXReq alloc]init];
    }
    
    wxReq.text = textDataString;
    wxReq.bText = YES;
    wxReq.scene = WXSceneSession;
    
    return [WXApi sendReq:wxReq];
}

#pragma mark -
#pragma mark - 微信base64图片分享
- (BOOL)WXApiBaseImageShare:(NSString *)imageDataString
{
    NSRange range = [imageDataString rangeOfString:@","];
    
    NSString *dataStr=[NSString stringWithFormat:@"%@",[imageDataString substringFromIndex:range.location+1]];
    
    NSData *imageFinialData = [NSData cdv_dataFromBase64String:dataStr];
    
//    BOOL IsValidPNG = [Check dataIsValidPNG:imageFinialData];
//    
//    if (IsValidPNG)
//    {
//        NSLog(@"图片有效!");
//    }
//    else
//    {
//        NSLog(@"图片失效!");
//    }
    
    WXImageObject *req = [[WXImageObject alloc] init];
    if (!req) {
        req = [[WXImageObject alloc] init];
    }
    req.imageData = imageFinialData;
    
    WXMediaMessage *message = [WXMediaMessage message];
    
    if (!message) {
        message = [WXMediaMessage message];
    }
    UIImage *tempThumbImageData = [UIImage imageWithData:imageFinialData];
    
    NSData  *thumbImageData = UIImageJPEGRepresentation([UIImage compressImage:tempThumbImageData toTargetWidth:80.0],0.00001);
    
    UIImage *thumbImage = [UIImage imageWithData:thumbImageData];
    
    [message setThumbImage:thumbImage];
    
    [message setMediaObject:req];
    
    SendMessageToWXReq *wxReq = [[SendMessageToWXReq alloc]init];
    if (!wxReq) {
        wxReq = [[SendMessageToWXReq alloc] init];
    }
    
    wxReq.bText = NO;
    wxReq.message = message;
    wxReq.scene = WXSceneSession;
    
    return [WXApi sendReq:wxReq];
}

#pragma mark -
#pragma mark - 微信Html分享
- (BOOL)WXApiHtmlShare:(NSString *)imageDataString WithCDVInvokedUrlCommand:(CDVInvokedUrlCommand*)command
{
    // 0 type 1 title 2 description 3 webpageUrl
    //创建发送对象实例
    SendMessageToWXReq *sendReq = [[SendMessageToWXReq alloc] init];
    sendReq.bText = NO;//不使用文本信息
    sendReq.scene = 0;//0 = 好友列表 1 = 朋友圈 2 = 收藏
    
    //创建分享内容对象
    WXMediaMessage *urlMessage = [WXMediaMessage message];
    
    NSString *title = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(title);
    urlMessage.title = title;//分享标题
    
    NSString *description = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
    STRING_NIL_TO_NONE(description);
    urlMessage.description = description;//分享描述
    
    [urlMessage setThumbImage:[UIImage imageNamed:@"waterMask.png"]];//分享图片,使用SDK的setThumbImage方法可压缩图片大小
    
    //创建多媒体对象
    WXWebpageObject *webObj = [WXWebpageObject object];
    
    NSString *webpageUrl = [NSString stringWithFormat:@"%@",[command argumentAtIndex:3]];
    STRING_NIL_TO_NONE(webpageUrl);
    webObj.webpageUrl = webpageUrl;//分享链接
    
    //完成发送对象实例
    urlMessage.mediaObject = webObj;
    sendReq.message = urlMessage;
    
    //发送分享信息
    return [WXApi sendReq:sendReq];
}


#pragma mark -
#pragma mark - 登录
-(void)loginWX:(CDVInvokedUrlCommand*)command
{
    if (![WXApi isWXAppInstalled])
    {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                          messageAsString:@"微信未安装!"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
        return;
    }
    
    //command 初始化
    self.command = command;
    
    //构造SendAuthReq结构体
    SendAuthReq* req = [[SendAuthReq alloc] init];
    req.scope = @"snsapi_userinfo";
    req.state = @"123";
    
    [[WXApiManager sharedManager] setDelegate:self];
    
    //第三方向微信终端发送一个SendAuthReq消息结构
    [WXApi sendReq:req];
}

#pragma mark -
#pragma mark - WXApiManagerDelegate
/*! @brief 收到一个来自微信的请求，第三方应用程序处理完后调用sendResp向微信发送结果
 *
 * 收到一个来自微信的请求，异步处理完成后必须调用sendResp发送处理结果给微信。
 * 可能收到的请求有GetMessageFromWXReq、ShowMessageFromWXReq等。
 * @param req 具体请求内容，是自动释放的
 */
-(void)onReq:(BaseReq*)req;
{
    
}

/*! @brief 发送一个sendReq后，收到微信的回应
 *
 * 收到一个来自微信的处理结果。调用一次sendReq后会收到onResp。
 * 可能收到的处理结果有SendMessageToWXResp、SendAuthResp等。
 * @param resp具体的回应内容，是自动释放的
 */
-(void)onResp:(BaseResp*)resp
{
    SendAuthResp *sendAuthResp = (SendAuthResp *)resp;
    
    //ERR_OK = 0(用户同意) ERR_AUTH_DENIED = -4（用户拒绝授权） ERR_USER_CANCEL = -2（用户取消）
    if (sendAuthResp.errCode != 0)
    {
        NSString *errStr = [NSString stringWithFormat:@"%@",resp.errStr];
        STRING_NIL_TO_NONE(errStr);
        
        [[[ClassFactory getInstance] getInfoHUD] showHud:errStr];
        
        return;
    }
    
    //用户换取access_token的code，仅在ErrCode为0时有效
    NSString *code = [NSString stringWithFormat:@"%@",sendAuthResp.code];
    STRING_NIL_TO_NONE(code);
    
    NSString *appid = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getWeixinAppId]];
    STRING_NIL_TO_NONE(appid);
    
    NSString *secret = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getWexinAppSecret]];
    STRING_NIL_TO_NONE(secret);
    
    /*
     通过code获取access_token
     https://api.weixin.qq.com/sns/oauth2/access_token?
     appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code
     */
    
    AFSessionManager *manager = [AFSessionManager manager];
    //发送格式
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    //接受格式
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //进程标签
    manager.targetOnlyStr = @"";
    
    NSMutableString *strUrl = [NSMutableString stringWithFormat:@"%@",[self.command argumentAtIndex:0]];
    [strUrl appendFormat:@"/sns/oauth2/access_token?"];
    
    NSString *urlstring = [NSString stringWithFormat:@"%@appid=%@&secret=%@&code=%@&grant_type=%@",
                           strUrl,
                           appid,
                           secret,
                           code,
                           @"authorization_code"];
    STRING_NIL_TO_NONE(urlstring);
    
    [[[ClassFactory getInstance] getNetComm] showHud:nil];
    [manager postRequestWithUrl:urlstring path:urlstring parameters:nil success:^(id JSON) {
        
        NSDictionary *requestResultDic = [[NSDictionary alloc] initWithDictionary:JSON];
        NSString *errcode = [NSString stringWithFormat:@"%@",[requestResultDic objectForKey:@"errcode"]];
        STRING_NIL_TO_NONE(errcode);
        
        if (STRING_ISNIL(errcode))
        {
            NSMutableDictionary *resultDic = [NSMutableDictionary dictionaryWithDictionary:requestResultDic];
            [resultDic setObject:@"1" forKey:@"resultCode"];
            
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsDictionary:resultDic];
            //send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:self.command.callbackId];
        }
        else
        {
            NSMutableDictionary *resultDic = [NSMutableDictionary dictionaryWithDictionary:requestResultDic];
            [resultDic setObject:@"0" forKey:@"resultCode"];
            
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:resultDic];
            //send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:self.command.callbackId];
        }
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
        
    } failure:^(NSError *error, id JSON) {
        
        NSString *errorInfo = [NSString stringWithFormat:@"%@",error.localizedDescription];
        STRING_NIL_TO_NONE(errorInfo);
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:
                                    @"0",@"resultCode",
                                    errorInfo,@"message",
                                    nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:self.command.callbackId];
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
    }];
}

@end
