//
//  CDVTHC.m
//  IOSFramework
//
//  Created by 林科 on 2018/9/11.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import "CDVTHC.h"

#import "AppDelegate.h"
@implementation CDVTHC

- (void)thcLoginSuccess:(CDVInvokedUrlCommand*)command
{
    //是否登录成功
    NSString *isLoginSuccess = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(isLoginSuccess)
    
    //提示信息
    NSString *message = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(message)
    
    //跳转URL
    NSString *requestString = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
    STRING_NIL_TO_NONE(requestString)
    
    //登录TYPE值
    //0 太好创 1 客户经理
    NSString *isLoginType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:3]];
    STRING_NIL_TO_NONE(isLoginType)
    
    if ([isLoginSuccess isEqualToString:@"1"])
    {
        
    }
    else
    {
        
    }
}

@end
