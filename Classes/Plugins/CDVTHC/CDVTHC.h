//
//  CDVTHC.h
//  IOSFramework
//
//  Created by 林科 on 2018/9/11.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import <Cordova/CDV.h>
#import <Cordova/CDVPlugin.h>

@interface CDVTHC : CDVPlugin

- (void)thcLoginSuccess:(CDVInvokedUrlCommand*)command;

@end
