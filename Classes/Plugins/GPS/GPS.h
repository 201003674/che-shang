//
//  GPS.h
//  CRM
//
//  Created by 林科 on 16/1/25.
//  Copyright © 2016年 FBI01. All rights reserved.
//

#import <Cordova/CDV.h>
#import <Cordova/CDVPlugin.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKReverseGeocoder.h>
#import <MapKit/MKPlacemark.h>


@interface GPS : CDVPlugin<CLLocationManagerDelegate, MKReverseGeocoderDelegate>
{
    CLLocationManager *gps;
    CDVInvokedUrlCommand* _command;

}

@end
