//
//  GPS.m
//  CRM
//
//  Created by 林科 on 16/1/25.
//  Copyright © 2016年 FBI01. All rights reserved.
//

#import "GPS.h"
@implementation GPS

- (void)getLocation:(CDVInvokedUrlCommand*)command
{
    _command = command;
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"ok" message:@"ok" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
    [alert show];
    gps = [[CLLocationManager alloc] init];
    gps.delegate = self;
    gps.desiredAccuracy = kCLLocationAccuracyBest;
    gps.distanceFilter = kCLDistanceFilterNone;
    [gps startUpdatingLocation];
    
    
}

- (void)locationManager:(CLLocationManager *)locationManager didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *) oldLocation;
{
    NSString *location = [NSString stringWithFormat:@"%f,%f",newLocation.coordinate.latitude, newLocation.coordinate.longitude];
    [self startedReverseGeoderWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if ( [error code] == kCLErrorDenied ) {
        [manager stopUpdatingHeading];
    } else if ([error code] == kCLErrorHeadingFailure) {
        
    }
}

-(void) startedReverseGeoderWithLatitude:(double)latitude longitude:(double)longitude{
    CLLocationCoordinate2D coordinate2D;
    coordinate2D.longitude = longitude;
    coordinate2D.latitude = latitude;
    MKReverseGeocoder *geoCoder = [[MKReverseGeocoder alloc] initWithCoordinate:coordinate2D];
    geoCoder.delegate = self;
    [geoCoder start];
}

-(void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark
{
    
    NSString *subthroung=placemark.thoroughfare;
    NSString *local=placemark.locality;
    NSString *street = placemark.subThoroughfare;
    NSString *name = placemark.name;
    NSString *address = [NSString stringWithFormat:@"您当前所在位置:%@,%@,%@,%@",local, subthroung,street,name];
    CDVPluginResult *result = [CDVPluginResult new];
    result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:address];
    [self.commandDelegate sendPluginResult:result callbackId:_command.callbackId];
}
-(void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFailWithError:(NSError *)error
{
}

@end
