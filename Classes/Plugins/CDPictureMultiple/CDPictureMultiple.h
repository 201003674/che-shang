//
//  CDPictureMultiple.h
//  IOSFramework
//
//  Created by 林科 on 2018/7/11.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import "CDVBasePlugin.h"

#import "UIImage+Extension.h"
#import <CoreLocation/CLLocationManager.h>
@interface CDPictureMultiple : CDVBasePlugin<UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,TZImagePickerControllerDelegate>
{
    //是否验车照片 需要强制允许定位功能 验车照片
    NSString *isAllowLocation;
    
    //是否需要保存相册
    NSString *isAllowSaveToAlbum;
    
    //是否支持多选
    NSString *MultipleFalg;
    
    //多选限制数量
    NSString *MultipleLimitNum;
    
    //提交清单
    NSMutableArray *mutableArray;
}
//初始化
- (void)init:(CDVInvokedUrlCommand*)command;

//图片选择
- (void)addImage:(CDVInvokedUrlCommand*)command;

//图片删除
- (void)deleteImage:(CDVInvokedUrlCommand*)command;

//图片上传中台
- (void)uploadImage:(CDVInvokedUrlCommand*)command;
@end
