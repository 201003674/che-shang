//
//  CDPictureMultiple.m
//  IOSFramework
//
//  Created by 林科 on 2018/7/11.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import "CDPictureMultiple.h"

@implementation CDPictureMultiple

//初始化清空提交数组
- (void)init:(CDVInvokedUrlCommand*)command
{
    if (!mutableArray) {
        mutableArray = [NSMutableArray array];
    }
    
    [mutableArray removeAllObjects];
    
    if (mutableArray.count == 0)
    {
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsString:@"初始化成功!"];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
    else
    {
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsString:@"初始化失败!"];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
}

//图片选择
- (void)addImage:(CDVInvokedUrlCommand*)command
{
    [self setCommand:command];
    
    /**
     0 相机模式:0 相册 1 相机 其他 相机相册都支持
     1 是否添加水印
     2 限制图片数量
    */
    
    //多选限制数量
    MultipleLimitNum = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
    STRING_NIL_TO_NONE(MultipleLimitNum)
    
    //是否强制需要使用定位功能
    isAllowLocation = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(isAllowLocation)
    if ([isAllowLocation isEqualToString:@"1"])
    {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
        {
            [[[ClassFactory getInstance] getInfoHUD] showHud:@"请允许应用打开定位功能，否则无法添加地理位置信息。"];
            
            return;
        }
        else
        {
            UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                                                initWithTitle:nil
                                                delegate:self
                                                cancelButtonTitle:@"取消"
                                                destructiveButtonTitle:nil
                                                otherButtonTitles: @"打开照相机",nil];
            actionSheet.command = command;
            
            [actionSheet showInView:self.viewController.view];
            
            return;
        }
    }
    
    NSString *pickerCameraType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(pickerCameraType)
    
    if ([pickerCameraType isEqualToString:@"0"])
    {
        UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                                            initWithTitle:nil
                                            delegate:self
                                            cancelButtonTitle:@"取消"
                                            destructiveButtonTitle:nil
                                            otherButtonTitles:@"从手机相册获取",nil];
        actionSheet.command = command;
        
        [actionSheet showInView:self.viewController.view];
    }
    else if ([pickerCameraType isEqualToString:@"1"])
    {
        UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                                            initWithTitle:nil
                                            delegate:self
                                            cancelButtonTitle:@"取消"
                                            destructiveButtonTitle:nil
                                            otherButtonTitles: @"打开照相机",nil];
        actionSheet.command = command;
        
        [actionSheet showInView:self.viewController.view];
    }
    else
    {
        UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                                            initWithTitle:nil
                                            delegate:self
                                            cancelButtonTitle:@"取消"
                                            destructiveButtonTitle:nil
                                            otherButtonTitles: @"打开照相机", @"从手机相册获取",nil];
        actionSheet.command = command;
        
        [actionSheet showInView:self.viewController.view];
    }
}

//图片删除
- (void)deleteImage:(CDVInvokedUrlCommand*)command
{
    /**
     0 图片名
     */
    NSString *imageName = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(imageName);
    
    BOOL success = NO;
    
    for (imageUploadModel *model in mutableArray)
    {
        if ([model.imageName isEqualToString:imageName])
        {
            [mutableArray removeObject:model];
            
            success = YES;
            
            break;
        }
    }
    
    if (success)
    {
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"resultCode",imageName,@"imageName",@"删除成功！",@"message", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
    else
    {
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",imageName,@"imageName",@"删除失败！",@"message", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
}

//图片上传中台
- (void)uploadImage:(CDVInvokedUrlCommand*)command
{
    /**
     0 上传接口名
     */
    [[[ClassFactory getInstance] getNetComm] showHud:nil];
    [self unlpoadImageWithCommand:command withFileMutArr:mutableArray withSuccess:^(id JSON) {
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
    } withFail:^(NSError *error) {
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
    }];
}

#pragma mark -
#pragma mark - UICustomActionSheetDelegate
-(void)actionSheet:(UICustomActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex)
    {
        NSString *index = [NSString stringWithFormat:@"%@",[actionSheet.command argumentAtIndex:4]];
        STRING_NIL_TO_NONE(index);
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"用户取消操作",@"message",index,@"index", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
        
        return;
    }
    
    UICustomImagePickerController *imagePicker=[[UICustomImagePickerController alloc]init];
    imagePicker.view.backgroundColor=[UIColor whiteColor];
    
    BOOL isCameraSupport = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    BOOL isPhotoLibrarySupport= [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
    
    NSString *pickerCameraType = [NSString stringWithFormat:@"%@",[actionSheet.command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(pickerCameraType)
    
    if ([pickerCameraType isEqualToString:@"0"])
    {
        //相册
        if(!isPhotoLibrarySupport)
        {
            [[[ClassFactory getInstance] getInfoHUD] showHud:@"您的设备不支持此功能！"];
            
            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"您的设备不支持此功能！",@"message", nil];
            
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:theMessage];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
            
            return;
        }
        else
        {
            switch (buttonIndex)
            {
                //打开相册
                case 0:
                {
                    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    
                    break;
                }
                default:
                    
                    break;
            }
        }
    }
    else if ([pickerCameraType isEqualToString:@"1"])
    {
        //相机
        if (!isCameraSupport)
        {
            [[[ClassFactory getInstance]getInfoHUD]showHud:@"您的设备不支持此功能！"];
            
            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"您的设备不支持此功能！",@"message", nil];
            
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:theMessage];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
            
            return;
        }
        else
        {
            switch (buttonIndex)
            {
                //打开照相机拍照
                case 0:
                {
                    if ([actionSheet.isAllowLocation isEqualToString:@"1"])
                    {
                        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
                        {
                            [[[ClassFactory getInstance] getInfoHUD] showHud:@"请允许应用打开定位功能，否则无法添加地理位置信息。"];
                            
                            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"请允许应用打开定位功能，否则无法添加地理位置信息。",@"message", nil];
                            
                            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                                    messageAsDictionary:theMessage];
                            // send cordova result
                            [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
                            
                            return;
                        }
                        else
                        {
                            MultipleFalg = @"";
                            
                            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                            
                            break;
                        }
                    }
                    else
                    {
                        MultipleFalg = @"";
                        
                        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                        
                        break;
                    }
                }
                default:
                    
                    break;
            }
            
        }
    }
    else
    {
        //两者
        switch (buttonIndex)
        {
            case 0:  //打开照相机拍照
            {
                //相机
                if (!isCameraSupport)
                {
                    [[[ClassFactory getInstance] getInfoHUD] showHud:@"您的设备不支持此功能！"];
                    
                    NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"您的设备不支持此功能！",@"message", nil];
                    
                    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                            messageAsDictionary:theMessage];
                    // send cordova result
                    [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
                    
                    return;
                }
                else
                {
                    if ([actionSheet.isAllowLocation isEqualToString:@"1"])
                    {
                        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
                        {
                            [[[ClassFactory getInstance] getInfoHUD] showHud:@"请允许应用打开定位功能，否则无法添加地理位置信息。"];
                            
                            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"请允许应用打开定位功能，否则无法添加地理位置信息。",@"message", nil];
                            
                            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                                    messageAsDictionary:theMessage];
                            // send cordova result
                            [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
                            
                            return;
                        }
                        else
                        {
                            MultipleFalg = @"";
                            
                            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                            
                            break;
                        }
                    }
                    else
                    {
                        MultipleFalg = @"";
                        
                        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                        
                        break;
                    }
                }
            }
                
            case 1:  //打开本地相册
            {
                //相册
                if(!isPhotoLibrarySupport)
                {
                    [[[ClassFactory getInstance]getInfoHUD]showHud:@"您的设备不支持此功能！"];
                    
                    NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"您的设备不支持此功能！",@"message", nil];
                    
                    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                            messageAsDictionary:theMessage];
                    // send cordova result
                    [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
                    
                    return;
                }
                else
                {
                    isAllowSaveToAlbum = nil;
                    
                    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    
                    break;
                }
            }
                
            default:
                
                break;
        }
    }
    
    if ((imagePicker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary))
    {
        // 跳转到相册页面
        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:[MultipleLimitNum integerValue] delegate:self];
        [imagePickerVc setAllowTakePicture:NO];
        [imagePickerVc setMaxImagesCount:MultipleLimitNum.integerValue];
        
        [self.viewController presentViewController:imagePickerVc animated:YES completion:nil];
    }
    else
    {
        NSString *requiredMediaType = ( NSString *)kUTTypeImage;
        NSArray *arrMediaTypes = [NSArray arrayWithObjects:requiredMediaType,nil];
        [imagePicker setMediaTypes:arrMediaTypes];
        
        //设置是否可以管理已经存在的图片或者视频
        [imagePicker setAllowsEditing:NO];
        
        [imagePicker setDelegate:self];
        
        [imagePicker setCommand:actionSheet.command];
        [self.viewController presentViewController:imagePicker animated:YES completion:nil];
    }
}

#pragma mark -
#pragma mark - TZImagePickerControllerDelegate
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto infos:(NSArray<NSDictionary *> *)infos
{
    //图片处理
    for (UIImage *theImage in photos)
    {
        NSString *userNameStr = [NSString stringWithFormat:@"%@.png", [[[ClassFactory getInstance] getLogic] getImageName]];
        STRING_NIL_TO_NONE(userNameStr);
        
        //写入本地
        [[SDImageCache sharedImageCache] storeImage:theImage forKey:userNameStr toDisk:YES completion:^{
            
        }];
        
        imageUploadModel *imageModel = [[imageUploadModel alloc] init];
        
        [imageModel setImage:theImage];
        [imageModel setImageName:userNameStr];
        
        NSData *data = UIImageJPEGRepresentation(imageModel.image, 0.0000001);
        NSString *imageDataString = [NSString stringWithFormat:@"%@",[data cdv_base64EncodedString]];
        STRING_NIL_TO_NONE(imageDataString);
        [imageModel setImageBase64String:imageDataString];
        
        [mutableArray addObject:imageModel];
    }
    
    if (mutableArray.count != 0)
    {
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsArray:[self convertTheEntityArrayToJsonArray:mutableArray]];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:self.command.callbackId];
    }
    else
    {
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsArray:[self convertTheEntityArrayToJsonArray:mutableArray]];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:self.command.callbackId];
    }
}

- (void)tz_imagePickerControllerDidCancel:(TZImagePickerController *)picker
{
    NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"用户取消操作",@"message", nil];
    
    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                            messageAsDictionary:theMessage];
    // send cordova result
    [self.commandDelegate sendPluginResult:result callbackId:self.command.callbackId];
}

#pragma mark -
#pragma mark - UIImagePickerControllerDelegate
-(void)imagePickerController:(UICustomImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        //通过UIImagePickerControllerMediaType判断返回的是照片还是视频
        NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        //如果返回的type等于kUTTypeImage，代表返回的是照片,并且需要判断当前相机使用的sourcetype是拍照还是相册
        if ([mediaType isEqualToString:( NSString *)kUTTypeImage])
        {
            UIImage *theImage = nil;
            // 判断，图片是否允许修改
            if ([picker allowsEditing])
            {
                //获取用户编辑之后的图像
                theImage = [UIImage fixOrientation:[info objectForKey:UIImagePickerControllerEditedImage]];
            }
            else
            {
                // 照片的元数据参数
                theImage = [UIImage fixOrientation:[info objectForKey:UIImagePickerControllerOriginalImage]];
            }
            
            NSString *userNameStr = [NSString stringWithFormat:@"%@.png", [[[ClassFactory getInstance] getLogic] getImageName]];
            STRING_NIL_TO_NONE(userNameStr);
            
            //写入本地
            [[SDImageCache sharedImageCache] storeImage:theImage forKey:userNameStr toDisk:YES completion:^{
                
            }];
            
            imageUploadModel *imageModel = [[imageUploadModel alloc] init];
            
            [imageModel setImage:theImage];
            [imageModel setImageName:userNameStr];
            
            NSData *data = UIImageJPEGRepresentation(imageModel.image, 0.0000001);
            NSString *imageDataString = [NSString stringWithFormat:@"%@",[data cdv_base64EncodedString]];
            STRING_NIL_TO_NONE(imageDataString);
            [imageModel setImageBase64String:imageDataString];
            
            [mutableArray addObject:imageModel];
        }
        
        if (mutableArray.count != 0)
        {
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                         messageAsArray:[self convertTheEntityArrayToJsonArray:mutableArray]];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:picker.command.callbackId];
        }
        else
        {
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                         messageAsArray:[self convertTheEntityArrayToJsonArray:mutableArray]];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:picker.command.callbackId];
        }
    }];
}

-(void)imagePickerControllerDidCancel:(UICustomImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
        NSString *index = [NSString stringWithFormat:@"%@",[picker.command argumentAtIndex:4]];
        STRING_NIL_TO_NONE(index);
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"用户取消操作",@"message",index,@"index", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:picker.command.callbackId];
        
    }];
}

#pragma mark -
#pragma mark - unlpoadImageHelper
- (void)unlpoadImageWithCommand:(CDVInvokedUrlCommand*)command withFileMutArr:(NSArray *)tempFileMutArr withSuccess:(void (^)(id JSON))success withFail:(void (^)(NSError *error))fail
{
    //上传服务器url
    NSString *url = [NSString stringWithFormat:@"%@/%@",[[[ClassFactory getInstance] getLocalCfg] getHttpServerAddr],[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(url);
    
    //车E保大保单
    NSString *policyID = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(policyID);
    
    //图片批次
    NSString *imgIndex = [NSString stringWithFormat:@"%u",(arc4random() % 501) + 500];
    STRING_NIL_TO_NONE(imgIndex);
    
    NSDictionary *uploadRequest = [NSDictionary dictionaryWithObjectsAndKeys:url,@"url",policyID,@"policyID",imgIndex,@"imgIndex",nil];
    
    [self unlpoadImageWithFileMutArr:tempFileMutArr parameters:uploadRequest withSuccess:^(id JSON) {
        
        NSDictionary *requestResultDic = [[NSDictionary alloc] initWithDictionary:JSON];
        NSString *resultCode = [NSString stringWithFormat:@"%@",[requestResultDic objectForKey:@"success"]];
        STRING_NIL_TO_NONE(resultCode);
        
        if ([resultCode isEqualToString:@"1"])
        {
            //上传成功回调
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsDictionary:requestResultDic];
            //send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        else
        {
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:requestResultDic];
            //send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        
        if (success)
        {
            success(JSON);
        }
        
    } withFail:^(NSError *error) {
        
        NSString *errorInfo = [NSString stringWithFormat:@"%@",error.localizedDescription];
        STRING_NIL_TO_NONE(errorInfo);
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",errorInfo,@"message", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        
        if (fail)
        {
            fail(error);
        }
        
    }];
}

#pragma mark -
#pragma mark - unlpoadImageWithFileMutArr上传
- (void)unlpoadImageWithFileMutArr:(NSArray *)tempFileMutArr parameters:(NSDictionary *)requestobjDic withSuccess:(void (^)(id JSON))success withFail:(void (^)(NSError *error))fail
{
    [[[ClassFactory getInstance] getFileUpLoadAFRequestQueue] addFileUpOperationWithFileMutArr:tempFileMutArr parameters:requestobjDic withOnlyTarget:@"" withSuccess:^(id JSON) {
        
        if (success)
        {
            success(JSON);
        }
        
    } withFail:^(NSError *error) {
        
        if (fail)
        {
            fail(error);
        }
        
    } progessDataBlock:^(float percentDone) {
        
    }];
}

#pragma mark -
#pragma mark - 数据处理
- (NSArray *)convertTheEntityArrayToJsonArray:(NSArray *)mutArr
{
    NSMutableArray *mutableArray = [NSMutableArray array];
    
    for (imageUploadModel *model in mutArr)
    {
        NSString *imageName = [NSString stringWithFormat:@"%@",model.imageName];
        STRING_NIL_TO_NONE(imageName);
        
        NSString *imageData = [NSString stringWithFormat:@"%@",model.imageBase64String];
        STRING_NIL_TO_NONE(imageData);
        
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:imageName,@"imageName",imageData,@"imageData", nil];
        
        [mutableArray addObject:dic];
    }
    
    return mutableArray;
}

#pragma mark -
#pragma mark -

@end
