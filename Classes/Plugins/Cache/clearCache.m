/*
 Copyright 2014 Modern Alchemists OG

 Licensed under MIT.

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
 to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/


#import "clearCache.h"

@implementation clearCache

@synthesize clearCommand;

- (void)clearCache:(CDVInvokedUrlCommand*)command
{
    self.clearCommand = command;

	// Arguments arenot used at the moment.
    // NSArray* arguments = command.arguments;

    [self.commandDelegate runInBackground:^{
    
        //WEB缓存
        [[[ClassFactory getInstance] getLogic] clearTheWebCache];
        
        //应用缓存
        [[[ClassFactory getInstance] getNetComm] clearTheCache];
        
        //清除js 缓存
        
        //清除数据库缓存
        FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
        
        [fmdbSql clearTable:UPLOAD_TABLE_NAME complete:^(BOOL isSuccess) {
            
            if (isSuccess){
                NSLog(@"数据库清除成功!");
            }
            else
            {
                NSLog(@"数据库清除失败!");
            }
        }];
    }];

    [self success];
    
    //退出应用
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [UIView beginAnimations:@"exit" context:nil];
    [UIView setAnimationDuration:2.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:app.window cache:NO];
    [UIView setAnimationDidStopSelector:@selector(animationFinished:finished:context:)];
    app.window.bounds = CGRectMake(0, 0, 0, 0);
    app.window.alpha = 0.0f;
    [UIView commitAnimations];
}

//退出APP代码
- (void)animationFinished:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    if ([animationID compare:@"exit"] == 0)
    {
        /* 退出 */
        exit(0);
    }
}

- (void)success
{
    NSString* resultMsg = @"Cordova iOS webview cache cleared.";
    NSLog(@"%@",resultMsg);

    // create acordova result
    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                messageAsString:[resultMsg stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];

    // send cordova result
    [self.commandDelegate sendPluginResult:result callbackId:clearCommand.callbackId];
    
    // 清理缓存成功
    [[[ClassFactory getInstance] getInfoHUD] showHud:@"缓存清理成功!"];
}

- (void)error:(NSString*)message
{
    NSString* resultMsg = [NSString stringWithFormat:@"Error while clearing webview cache (%@).", message];
    NSLog(@"%@",resultMsg);

    // create cordova result
    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsString:[resultMsg stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];

    // send cordova result
    [self.commandDelegate sendPluginResult:result callbackId:clearCommand.callbackId];

}

@end
