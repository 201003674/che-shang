//
//  CDBioAssay.h
//  IOSFramework
//
//  Created by 林科 on 2017/10/10.
//  Copyright © 2017年 allianture. All rights reserved.
//
#import <Cordova/CDVPlugin.h>

#import "STLivenessController.h"
#import "STLivenessDetector.h"
#import "STLivenessCommon.h"

@interface CDBioAssay : CDVPlugin<STLivenessDetectorDelegate, STLivenessControllerDelegate>
{  
    //是否打开声音
    BOOL openVoice;
    
    //是否随机
    BOOL openRandom;
    
    //
    NSString *outType;
    
    //活体检测复杂度
    LivefaceComplexity complexity;
    
    //随机序列
    NSArray *sequence;
    
    //活体检测 视图
    STLivenessController *multipleLiveVC;
    
    CDVInvokedUrlCommand *urlCommand;
}
//活体检测
- (void)bioAssay:(CDVInvokedUrlCommand *)command;

@end
