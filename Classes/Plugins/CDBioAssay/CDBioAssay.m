//
//  CDBioAssay.m
//  IOSFramework
//
//  Created by 林科 on 2017/10/10.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import "CDBioAssay.h"

@implementation CDBioAssay

#pragma mark -
#pragma mark - 活体检测
- (void)bioAssay:(CDVInvokedUrlCommand *)command
{
    //初始化 默认值
    openVoice = YES;
    outType = @"video";
    complexity = LIVE_COMPLEXITY_NORMAL;
    openRandom = NO;
    sequence = [NSArray arrayWithObjects:@"BLINK", @"MOUTH", @"NOD", @"YAW", nil];
    
    //urlCommand 赋值
    urlCommand = command;
    
    /*
     0 是否打开声音提示 1 outType 2 活体检测复杂度 3 是否随机 4 活体检测动作序列（BLINK,MOUTH,NOD,YAW）
     */
    
    
    //是否打开声音
    NSString *openVoiceString = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(openVoiceString);
    if ([openVoiceString isEqualToString:@"1"])
    {
        openVoice = YES;
    }
    else
    {
        openVoice = NO;
    }

    //outType
    NSString *outTypeString = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(outTypeString);
    if (STRING_ISNOTNIL(outTypeString))
    {
        outType = outTypeString;
    }

    //complexity 活体检测复杂度
    NSString *complexityString = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
    STRING_NIL_TO_NONE(complexityString);
    if (STRING_ISNOTNIL(complexityString))
    {
        complexity = complexityString.integerValue;
    }

    //随机序列
    NSString *sequenceString = [NSString stringWithFormat:@"%@",[command argumentAtIndex:4]];
    STRING_NIL_TO_NONE(sequenceString);
      
    NSRange range = [sequenceString rangeOfString:@","];
    
    NSMutableArray *tempSequenceArr = [NSMutableArray array];
    if (range.location == NSNotFound)
    {
        [tempSequenceArr addObject:sequenceString];
    }
    else
    {
        [tempSequenceArr addObjectsFromArray:[sequenceString componentsSeparatedByString:@","]];
    }
    
    //映射为TYPE值
    for (NSInteger i = 0 ; i < tempSequenceArr.count ; i ++ )
    {
        NSString *string  = [NSString stringWithFormat:@"%@",[tempSequenceArr objectAtIndex:i]];
        STRING_NIL_TO_NONE(string);
        
        if ([string isEqualToString:@"BLINK"])
        {
            [tempSequenceArr replaceObjectAtIndex:i withObject:@(LIVE_BLINK)];
        }
        else if ([string isEqualToString:@"MOUTH"])
        {
            [tempSequenceArr replaceObjectAtIndex:i withObject:@(LIVE_MOUTH)];
        }
        else if ([string isEqualToString:@"NOD"])
        {
            [tempSequenceArr replaceObjectAtIndex:i withObject:@(LIVE_NOD)];
        }
        else if ([string isEqualToString:@"YAW"])
        {
            [tempSequenceArr replaceObjectAtIndex:i withObject:@(LIVE_YAW)];
        }
        else
        {
            [tempSequenceArr removeObjectAtIndex:i];
        }
    }

    //是否随机动作
    NSString *openRandom = [NSString stringWithFormat:@"%@",[command argumentAtIndex:3]];
    STRING_NIL_TO_NONE(openRandom);

    // 动作个数
    NSInteger actionCount = tempSequenceArr.count;
    
    if ([openRandom isEqualToString:@"1"] && actionCount > 1)
    {
        // 随机动作 1:BLINK 2:MOUTH 3:NOD 4:YAW
        NSArray *sequenceArray = [tempSequenceArr sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            if (arc4random_uniform(2) == 0)
            {
                return [obj2 compare:obj1]; //降序
            }
            else
            {
                return [obj1 compare:obj2]; //升序
            }
        }];
        
        sequence = [sequenceArray copy];
    }
    else
    {
        if (tempSequenceArr.count != 0)
        {
            sequence = [tempSequenceArr copy];
        }
    }
    
    //开始活体检测
    multipleLiveVC = [[STLivenessController alloc] initWithApiKey:ACCOUNT_API_KEY apiSecret:ACCOUNT_API_SECRET setDelegate:self detectionSequence:sequence];
    
    //设置每个模块的超时时间
    [multipleLiveVC.detector setTimeOutDuration:kDefaultTimeOutDuration];
    
    // 设置活体检测复杂度
    [multipleLiveVC.detector setComplexity:complexity];
    
    // 设置活体检测的阈值
    [multipleLiveVC.detector setHacknessThresholdScore:kDefaultHacknessThresholdScore];
    
    // 设置是否进行眉毛遮挡的检测，如不设置默认为不检测
    multipleLiveVC.detector.isBrowOcclusion = NO;
    
    // 设置默认语音提示状态,如不设置默认为开启
    multipleLiveVC.isVoicePrompt = openVoice;
    
    [self.viewController presentViewController:multipleLiveVC animated:YES completion:nil];
    
}

#pragma mark -
#pragma mark - STLivenessDetectorDelegate
/**
 *  活体检测成功回调
 *
 *  @param protobufId         回传加密后的二进制数据在公有云的id
 *  @param protobufData       回传加密后的二进制数据
 *  @param requestId          网络请求的id
 *  @param imageArr           根据指定输出方案回传 STImage 数组 , STImage属性见 STImage.h
 */
- (void)livenessDidSuccessfulGetProtobufId:(NSString *)protobufId //! OCLINT
                              protobufData:(NSData *)protobufData
                                 requestId:(NSString *)requestId //! OCLINT
                                    images:(NSArray *)imageArr
{
    NSString *userNameStr = [NSString stringWithFormat:@"%@.png", [[[ClassFactory getInstance] getLogic] getImageName]];
    STRING_NIL_TO_NONE(userNameStr);
    
    //缓存加密数据
    [[[ClassFactory getInstance] getLogic] setEncryTarData:protobufData];
    
    //缓存活体图片
    [[[[ClassFactory getInstance] getLogic] encryArrLFImage] removeAllObjects];
    [[[[ClassFactory getInstance] getLogic] encryArrLFImage] addObjectsFromArray:imageArr];
    
    NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"resultCode",@"活体检测成功!",@"message",nil];
    
    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                messageAsString:[theMessage JSONString]];
    // send cordova result
    [self.commandDelegate sendPluginResult:result callbackId:urlCommand.callbackId];
    
    [multipleLiveVC dismissViewControllerAnimated:YES completion:nil];
}

/**
 *  活体检测失败回调
 *
 *  @param errorType         失败的类型
 *  @param protobufData      回传加密后的二进制数据
 *  @param requestId         网络请求的id
 *  @param imageArr          根据指定输出方案回传 STImage 数组 , STImage属性见 STImage.h
 */
- (void)livenessDidFailWithErrorType:(LivefaceErrorType)errorType
                        protobufData:(NSData *)protobufData
                           requestId:(NSString *)requestId //! OCLINT
                              images:(NSArray *)imageArr
{
    NSString *errorMessage = nil;
    switch (errorType) {
        case STIDLiveness_E_LICENSE_INVALID: {
            errorMessage = @"未通过授权验证";
            break;
        }
        case STIDLiveness_E_LICENSE_FILE_NOT_FOUND: {
            errorMessage = @"授权文件不存在";
            break;
        }
        case STIDLiveness_E_LICENSE_BUNDLE_ID_INVALID: {
            errorMessage = @"绑定包名错误";
            break;
        }
        case STIDLiveness_E_LICENSE_EXPIRE: {
            errorMessage = @"授权文件过期";
            break;
        }
        case STIDLiveness_E_LICENSE_VERSION_MISMATCH: {
            errorMessage = @"License与SDK版本不匹";
            break;
        }
        case STIDLiveness_E_LICENSE_PLATFORM_NOT_SUPPORTED: {
            errorMessage = @"License不支持当前平台";
            break;
        }
        case STIDLiveness_E_MODEL_INVALID: {
            errorMessage = @"模型文件错误";
            break;
        }
        case STIDLiveness_E_MODEL_FILE_NOT_FOUND: {
            errorMessage = @"模型文件不存在";
            break;
        }
        case STIDLiveness_E_MODEL_EXPIRE: {
            errorMessage = @"模型文件过期";
            break;
        }
        case STIDLiveness_E_NOFACE_DETECTED: {
            errorMessage = @"动作幅度过⼤,请保持人脸在屏幕中央,重试⼀次";
            
            break;
        }
        case STIDLiveness_E_FACE_OCCLUSION: {
            errorMessage = @"请调整人脸姿态，去除面部遮挡，正对屏幕重试一次";
            
            break;
        }
        case STIDLiveness_E_TIMEOUT: {
            errorMessage = @"检测超时,请重试一次";
            break;
        }
        case STIDLiveness_E_INVALID_ARGUMENT: {
            errorMessage = @"参数设置不合法";
            break;
        }
        case STIDLiveness_E_CALL_API_IN_WRONG_STATE: {
            errorMessage = @"错误的方法状态调用";
            break;
        }
        case STIDLiveness_E_API_KEY_INVALID: {
            errorMessage = @"API_KEY或API_SECRET错误";
            break;
        }
        case STIDLiveness_E_SERVER_ACCESS: {
            errorMessage = @"服务器访问错误";
            break;
        }
        case STIDLiveness_E_SERVER_TIMEOUT: {
            errorMessage = @"网络连接超时，请查看网络设置，重试一次";
            break;
        }
        case STIDLiveness_E_HACK: {
            errorMessage = @"未通过活体检测";
            break;
        }
    }
    
    //缓存加密数据
    [[[ClassFactory getInstance] getLogic] setEncryTarData:protobufData];
    
    //缓存活体图片
    [[[[ClassFactory getInstance] getLogic] encryArrLFImage] removeAllObjects];
    [[[[ClassFactory getInstance] getLogic] encryArrLFImage] addObjectsFromArray:imageArr];
    
    NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",errorMessage,@"message",nil];
    
    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsString:[theMessage JSONString]];
    
    // send cordova result
    [self.commandDelegate sendPluginResult:result callbackId:urlCommand.callbackId];
    
    [multipleLiveVC dismissViewControllerAnimated:YES completion:nil];
}

/**
 *  活体检测被取消的回调
 */
- (void)livenessDidCancel
{
    NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",@"活体检测被取消!",@"message",nil];
    
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsString:[theMessage JSONString]];
    
    // send cordova result
    [self.commandDelegate sendPluginResult:result callbackId:urlCommand.callbackId];
    
    [multipleLiveVC dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark - STLivenessControllerDelegate
- (void)livenessControllerDeveiceError:(STIdDeveiceError)deveiceError
{
    NSString *errorMessage = nil;
    switch (deveiceError)
    {
        case STIDLiveness_E_CAMERA:
            errorMessage = @"相机权限获取失败:请在设置-隐私-相机中开启后重试";
            break;
            
        case STIDLiveness_WILL_RESIGN_ACTIVE:
            errorMessage = @"活体检测已经取消";
            break;
    }
    
    NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",errorMessage,@"message",nil];
    
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsString:[theMessage JSONString]];
    
    // send cordova result
    [self.commandDelegate sendPluginResult:result callbackId:urlCommand.callbackId];
    
    [multipleLiveVC dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark -

@end
