//
//  imageUploadHelper.m
//  IOSFramework
//
//  Created by 林科 on 2017/3/29.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import "imageUploadHelper.h"

#import "UIImage+Extension.h"
@implementation imageUploadHelper

#pragma mark -
#pragma mark - deviceready CRM
//初始化
- (void)deviceready:(CDVInvokedUrlCommand*)command
{
    NSString *requestNo = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(requestNo);
    
    //初始化 */
    [[[[ClassFactory getInstance] getLogic] mutArr] removeAllObjects];
    
    //添加缓存图片到提交确认清单中
    FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
    
    NSArray *keys = [NSArray array];
    NSArray *where = [NSArray arrayWithObjects:@"requestNo",@"=",requestNo, nil];
    
    [fmdbSql queryWithTableName:UPLOAD_TABLE_NAME keys:keys where:where complete:^(NSArray *array) {
        
        //添加数据
        [[[[ClassFactory getInstance] getLogic] mutArr] addObjectsFromArray:array];
        
        //是否图片保存到相册 初始化
        isAllowSaveToAlbum = nil;
        
        //是否强制需要使用定位功能 初始化
        isAllowLocation = nil;
        
        if ([[[[ClassFactory getInstance] getLogic] mutArr] count] == 0)
        {
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                        messageAsString:@"初始化成功!"];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        else
        {
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                        messageAsString:@"初始化失败!"];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        
    }];
}

#pragma mark -
#pragma mark - 初始化太享贷
- (void)devicereadyTXD:(CDVInvokedUrlCommand*)command
{
    /* command 参数 */
    [self setCommand:command];
    
    NSString *requestNo = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(requestNo);
    
    //初始化 */
    [[[[ClassFactory getInstance] getLogic] mutArr] removeAllObjects];
    
    //添加缓存图片到提交确认清单中
    FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
    
    NSArray *keys = [NSArray array];
    NSArray *where = [NSArray arrayWithObjects:@"requestNo",@"=",requestNo, nil];
    
    [fmdbSql queryWithTableName:UPLOAD_TABLE_NAME keys:keys where:where complete:^(NSArray *array) {
        
        NSArray *sortedArray = [array sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *p1, NSDictionary *p2){
            
            NSString *firstImageIndex = [NSString stringWithFormat:@"%@",[p1 objectForKey:@"imageIndex"]];
            STRING_NIL_TO_NONE(firstImageIndex);
            
            NSString *secondImageIndex = [NSString stringWithFormat:@"%@",[p2 objectForKey:@"imageIndex"]];
            STRING_NIL_TO_NONE(secondImageIndex);
            
            NSComparisonResult result = [firstImageIndex compare:secondImageIndex];
            
            return result == NSOrderedDescending; // 升序
        }];
        
        //添加数据
        [[[[ClassFactory getInstance] getLogic] mutArr] addObjectsFromArray:sortedArray];
        
        if ([[[[ClassFactory getInstance] getLogic] mutArr] count] == 0)
        {
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                        messageAsString:@"初始化成功!"];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        else
        {
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                        messageAsString:@"初始化失败!"];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
    }];
}

#pragma mark -
#pragma mark - 校验是否需要关联OCR
- (BOOL)checkIsNeedUpLoadOCRImage:(NSArray *)array withRequestNo:(NSString *)requestNo
{
    BOOL result = YES;
    
    if ([requestNo hasPrefix:@"08"])
    {
        NSLog(@"CRM流程=================不需要添加OCR识别照片!=================");
        
        result = NO;
    }
    
    NSString *deviceId = [NSString stringWithFormat:@"%@.png",[[[UIDevice currentDevice] identifierForVendor] UUIDString]];
    STRING_NIL_TO_NONE(deviceId);
    
    for (NSDictionary *dic in array)
    {
        NSString *imageIndex = [NSString stringWithFormat:@"%@",[dic objectForKey:@"imageIndex"]];
        STRING_NIL_TO_NONE(imageIndex);
        
        NSString *imageType = [NSString stringWithFormat:@"%@",[dic objectForKey:@"imageType"]];
        STRING_NIL_TO_NONE(imageType);
        
        NSString *imageName = [NSString stringWithFormat:@"%@",[dic objectForKey:@"imageName"]];
        STRING_NIL_TO_NONE(imageName);
        
        if ([imageType isEqualToString:@"B0101"] && [imageIndex isEqualToString:@"1"])
        {
            result = NO;
            
            NSLog(@"=================不需要添加OCR识别照片!=================");
        }
    }
    
    return result;
}

#pragma mark -
#pragma mark - getImageCache
//获取缓存
- (void)getImageCache:(CDVInvokedUrlCommand*)command
{
    /* command 参数 */
    [self setCommand:command];
    
    //获取任务申请号
    NSString *requestNo = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(requestNo);
    
    NSMutableArray *temp = [NSMutableArray array];
    
    NSArray *tempArr = [NSArray arrayWithArray:[[[ClassFactory getInstance] getLogic] mutArr]];
    
    for (NSDictionary *dic in tempArr)
    {
        //图片名称
        NSString *imageName = [NSString stringWithFormat:@"%@",[dic objectForKey:@"imageName"]];
        STRING_NIL_TO_NONE(imageName);
        
        //图片类型（对应影像系统ID）
        NSString *imageType = [NSString stringWithFormat:@"%@",[dic objectForKey:@"imageType"]];
        STRING_NIL_TO_NONE(imageType);
        
        //任务申请号
        NSString *requestNo = [NSString stringWithFormat:@"%@",[dic objectForKey:@"requestNo"]];
        STRING_NIL_TO_NONE(requestNo);
        
        //图片序号
        NSString *imageIndex = [NSString stringWithFormat:@"%@",[dic objectForKey:@"imageIndex"]];
        STRING_NIL_TO_NONE(imageIndex);
        
        //图片流
        UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:imageName];
        NSData *data = UIImageJPEGRepresentation(image, 0.000001);
        
        NSString *imageDataString = [NSString stringWithFormat:@"%@",[data cdv_base64EncodedString]];
        STRING_NIL_TO_NONE(imageDataString);
        
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:imageName,@"imageName",imageType,@"imageType",requestNo,@"requestNo",imageDataString,@"imageData",imageIndex,@"Index", nil];
        
        //添加到缓存临时返回
        [temp addObject:dic];
    }
    
    if (temp.count != 0)
    {
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsArray:temp];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
    else
    {
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsArray:temp];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
}

#pragma mark -
#pragma mark - insertIntoSubmitList
//单独添加到提交清单
- (void)insertIntoSubmitList:(CDVInvokedUrlCommand *)command
{
    /* command 参数 */
    [self setCommand:command];
    
    //获取任务申请号
    NSString *requestNo = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(requestNo);
    
    //图片名称
    NSString *imageName = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(imageName);
    
    //图片顺序
    NSString *imageIndex = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
    STRING_NIL_TO_NONE(imageIndex);
    
    //图片类型
    NSString *imageType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:3]];
    STRING_NIL_TO_NONE(imageType);
    
	NSDictionary *requestResultDic = [NSDictionary dictionaryWithObjectsAndKeys:imageName,@"imageName",imageType,@"imageType",requestNo,@"requestNo",imageIndex,@"imageIndex", nil];
    
    NSArray *mutArr = [NSArray arrayWithArray:[[[ClassFactory getInstance] getLogic] mutArr]];
    
    BOOL success = [self checkFileisNeedUpload:mutArr andUrlCommand:command andRequestNo:requestNo andImageName:imageName WithRequestResultDic:requestResultDic];
    
    if (success)
    {
        //缓存文件更新
        FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
        
        BOOL isSuccess = [[[ClassFactory getInstance] getLogic] addImageToMutArr:requestResultDic WithIndex:imageIndex];
        
        if (isSuccess)
        {
            NSLog(@"文件%@添加临时提交数组成功!",imageName);
        }
        else
        {
            NSLog(@"文件%@添加临时提交数组失败!",imageName);
        }
        
        [fmdbSql insertIntoTableName:UPLOAD_TABLE_NAME Dict:requestResultDic complete:^(BOOL isSuccess) {
            
            if (isSuccess)
            {
                NSLog(@"文件%@写入数据库成功!",imageName);
                
                //上传成功回调
                CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                        messageAsDictionary:requestResultDic];
                //send cordova result
                [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            }
            else
            {
                NSLog(@"文件%@写入数据库失败!",imageName);
                
                CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                        messageAsDictionary:requestResultDic];
                //send cordova result
                [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            }
            
        }];
    }
    else
    {
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"resultCode",@"图片已经添加，无需重复添加!",@"message",nil];
        
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                messageAsDictionary:theMessage];
        //send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
}

- (BOOL)checkFileisNeedUpload:(NSArray *)fileUploadArr andUrlCommand:(CDVInvokedUrlCommand *)command andRequestNo:(NSString *)requestNo andImageName:(NSString *)imageName WithRequestResultDic:(NSDictionary *)requestResultDic
{
    for (NSInteger i = 0 ; i < fileUploadArr.count ; i ++ )
    {
        NSDictionary *tempDic = [NSDictionary dictionaryWithDictionary:[fileUploadArr objectAtIndex:i]];
        
        //任务申请号
        NSString *tempRequestNo = [NSString stringWithFormat:@"%@",[tempDic objectForKey:@"requestNo"]];
        STRING_NIL_TO_NONE(tempRequestNo);
        
        //图片名称
        NSString *tempImageName = [NSString stringWithFormat:@"%@",[tempDic objectForKey:@"imageName"]];
        STRING_NIL_TO_NONE(tempImageName);
        
        if ([tempImageName.lastPathComponent containsString:@"pdf"])
        {
            NSArray *where = [NSArray arrayWithObjects:@"requestNo",@"=",requestNo,@"imageName",@"=",@"%pdf", nil];
            
            FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
            //更新清单
            [fmdbSql updateWithTableName:UPLOAD_TABLE_NAME valueDict:requestResultDic where:where complete:^(BOOL isSuccess) {
                if (isSuccess)
                {
                    NSLog(@"文件%@更新数据库成功!",imageName);
                }
                else
                {
                    NSLog(@"文件%@更新数据库失败!",imageName);
                }
                
            }];
            
            //更新清单
            if (fileUploadArr.count > i)
            {
                [[[[ClassFactory getInstance] getLogic] mutArr] replaceObjectAtIndex:i withObject:requestResultDic];
            }
            
            return NO;
        }
    }
    
    return YES;
}

#pragma mark -
#pragma mark - DeleteDetailedlist
- (void)DeleteDetailedlist:(CDVInvokedUrlCommand *)command
{
    NSString *requestNo = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(requestNo);
    
    //初始化 */
    [[[[ClassFactory getInstance] getLogic] mutArr] removeAllObjects];
    
    //添加缓存图片到提交确认清单中
    FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
    
    NSArray *where = [NSArray arrayWithObjects:@"requestNo",@"=",requestNo, nil];
    
    [fmdbSql deleteWithTableName:UPLOAD_TABLE_NAME where:where complete:^(BOOL isSuccess) {
        
        if (isSuccess)
        {
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                        messageAsString:@"数据清除成功!"];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        else
        {
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                        messageAsString:@"数据清除失败!"];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
    }];
    
}

#pragma mark -
#pragma mark - addImage
//添加图片
- (void)addImage:(CDVInvokedUrlCommand*)command
{
    /* command 参数 */
    [self setCommand:command];
    
    //是否图片保存到相册
    isAllowSaveToAlbum = [NSString stringWithFormat:@"%@",[command argumentAtIndex:5]];
    STRING_NIL_TO_NONE(isAllowSaveToAlbum)
    
    //是否强制需要使用定位功能
    isAllowLocation = [NSString stringWithFormat:@"%@",[command argumentAtIndex:6]];
    STRING_NIL_TO_NONE(isAllowLocation)
    
    //是否支持多选
    MultipleFalg = [NSString stringWithFormat:@"%@",[command argumentAtIndex:7]];
    STRING_NIL_TO_NONE(MultipleFalg)
    
    //多选限制数量
    MultipleLimitNum = [NSString stringWithFormat:@"%@",[command argumentAtIndex:8]];
    STRING_NIL_TO_NONE(MultipleLimitNum)
    
    if ([isAllowLocation isEqualToString:@"1"])
    {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
        {
            [[[ClassFactory getInstance] getInfoHUD] showHud:@"请允许应用打开定位功能，否则无法添加地理位置信息。"];
            
            return;
        }
        else
        {
            UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                                                initWithTitle:nil
                                                delegate:self
                                                cancelButtonTitle:@"取消"
                                                destructiveButtonTitle:nil
                                                otherButtonTitles: @"打开照相机",nil];
            actionSheet.command = command;
            
            [actionSheet showInView:self.viewController.view];
            
            return;
        }
    }
    
    NSString *pickerCameraType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(pickerCameraType)
    
    if ([pickerCameraType isEqualToString:@"0"])
    {
        UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                         initWithTitle:nil
                         delegate:self
                         cancelButtonTitle:@"取消"
                         destructiveButtonTitle:nil
                         otherButtonTitles:@"从手机相册获取",nil];
        actionSheet.command = command;
        
        [actionSheet showInView:self.viewController.view];
    }
    else if ([pickerCameraType isEqualToString:@"1"])
    {
        UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                         initWithTitle:nil
                         delegate:self
                         cancelButtonTitle:@"取消"
                         destructiveButtonTitle:nil
                         otherButtonTitles: @"打开照相机",nil];
        actionSheet.command = command;
        
        [actionSheet showInView:self.viewController.view];
    }
    else
    {
        UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                         initWithTitle:nil
                         delegate:self
                         cancelButtonTitle:@"取消"
                         destructiveButtonTitle:nil
                         otherButtonTitles: @"打开照相机", @"从手机相册获取",nil];
        actionSheet.command = command;
        
        [actionSheet showInView:self.viewController.view];
    }
}

#pragma mark -
#pragma mark - 添加图片太享贷
- (void)addImageTXD:(CDVInvokedUrlCommand*)command
{
    /* command 参数 */
    [self setCommand:command];
    
    /*
        0 是否打开相册 1 上传url日志 2 任务申请号 3 上传图片类型 4 图片序号 5 图片标记 6 是否放开图片多选 7 限制多选数量 8 是否图片保存到相册
     9 是否强制需要使用定位，添加水印功能 10 不缓存数据 11 不上传中台影像 12 拍照还是录像，默认拍照
     */
    
    //0 相册; 1 相机; 2 相册/相机
    NSString *pickerCameraType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(pickerCameraType)
    
    //是否支持多选
    MultipleFalg = [NSString stringWithFormat:@"%@",[command argumentAtIndex:6]];
    STRING_NIL_TO_NONE(MultipleFalg)
    
    //多选限制数量
    MultipleLimitNum = [NSString stringWithFormat:@"%@",[command argumentAtIndex:7]];
    STRING_NIL_TO_NONE(MultipleLimitNum)
    
    //是否图片保存到相册
    isAllowSaveToAlbum = [NSString stringWithFormat:@"%@",[command argumentAtIndex:8]];
    STRING_NIL_TO_NONE(isAllowSaveToAlbum)
    
    //是否添加水印功能
    isAllowLocation = [NSString stringWithFormat:@"%@",[command argumentAtIndex:9]];
    STRING_NIL_TO_NONE(isAllowLocation)
    
    if ([pickerCameraType isEqualToString:@"0"])
    {
        UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                                            initWithTitle:nil
                                            delegate:self
                                            cancelButtonTitle:@"取消"
                                            destructiveButtonTitle:nil
                                            otherButtonTitles:@"从手机相册获取",nil];
        actionSheet.command = command;
        actionSheet.isAllowLocation = isAllowLocation;
        
        [actionSheet showInView:self.viewController.view];
    }
    else if ([pickerCameraType isEqualToString:@"1"])
    {
        UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                                            initWithTitle:nil
                                            delegate:self
                                            cancelButtonTitle:@"取消"
                                            destructiveButtonTitle:nil
                                            otherButtonTitles: @"打开照相机",nil];
        actionSheet.command = command;
        actionSheet.isAllowLocation = isAllowLocation;
        
        [actionSheet showInView:self.viewController.view];
    }
    else
    {
        UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                                            initWithTitle:nil
                                            delegate:self
                                            cancelButtonTitle:@"取消"
                                            destructiveButtonTitle:nil
                                            otherButtonTitles: @"打开照相机", @"从手机相册获取",nil];
        actionSheet.command = command;
        actionSheet.isAllowLocation = isAllowLocation;
        
        [actionSheet showInView:self.viewController.view];
    }
}

#pragma mark -
#pragma mark - deleteImage
//删除图片
- (void)deleteImage:(CDVInvokedUrlCommand*)command
{
    /* command 参数 */
    [self setCommand:command];
    
    //获取任务申请号
    NSString *requestNo = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(requestNo);
    
    //图片名称
    NSString *imageName = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(imageName);
    
    BOOL success = [self removeImageMutArr:imageName WithFolder:requestNo];
    
    if (success)
    {
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsString:@"删除成功!"];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
    else
    {
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsString:@"删除失败!"];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
}

- (BOOL)removeImageMutArr:(NSString *)imageName WithFolder:(NSString *)requestNo
{
    NSMutableArray *mutArr = [NSMutableArray arrayWithArray:[[[ClassFactory getInstance] getLogic] mutArr]];
    
    for (NSDictionary *dic in mutArr)
    {
        NSString *imageNameStr = [NSString stringWithFormat:@"%@",[dic objectForKey:@"imageName"]];
        STRING_NIL_TO_NONE(imageNameStr);
        
        if ([imageNameStr isEqualToString:imageName])
        {
            [[[[ClassFactory getInstance] getLogic] mutArr] removeObject:dic];
            
            //缓存文件更新
            FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
            NSArray *where = [NSArray arrayWithObjects:@"requestNo",@"=",requestNo,@"imageName",@"=",imageName, nil];
            
            [fmdbSql deleteWithTableName:UPLOAD_TABLE_NAME where:where complete:^(BOOL isSuccess) {
                
                if (isSuccess){
                    NSLog(@"文件%@写入成功!",imageName);
                }
                else
                {
                    NSLog(@"文件%@写入失败!",imageName);
                }
            }];
            
            return YES;
        }
    }
    
    return NO;
}

//确认上传
- (void)ensureUpload:(CDVInvokedUrlCommand*)command
{
    NSInteger operationCount = [[[[ClassFactory getInstance] getFileUpLoadAFRequestQueue] operationQueue] operationCount];
    
    if (operationCount != 0)
    {
        [[[ClassFactory getInstance] getNetComm] showHud:@"任务正在提交，请稍后再重试。"];
        
        return;
    }
    
    [self ensureContinueUpload:command];
}

- (void)ensureContinueUpload:(CDVInvokedUrlCommand*)command
{
//    0 上传url ；1 任务申请号；2 是否上传pdf；3 pdf 名称；4 分公司代码；5 分公司名称；6 经办人代码 7 经办人名称；
    
    //上传url
    NSString *url = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(url);
    
    //任务申请号
    NSString *requestNo = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(requestNo);
    
    NSMutableArray *dataList = [NSMutableArray arrayWithCapacity:10];
    
    if ([[[[ClassFactory getInstance] getLogic] mutArr] count] == 0)
    {
        [[[ClassFactory getInstance] getInfoHUD] showHud:@"提交清单为空!"];
        
        return;
    }
    
    for (NSDictionary *dic in [[[ClassFactory getInstance] getLogic] mutArr])
    {
        NSString *imageName = [NSString stringWithFormat:@"%@",[dic objectForKey:@"imageName"]];
        STRING_NIL_TO_NONE(imageName);
        
        NSString *imageType = [NSString stringWithFormat:@"%@",[dic objectForKey:@"imageType"]];
        STRING_NIL_TO_NONE(imageType);
        
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:imageName,@"data",imageType,@"type", nil];
        
        [dataList addObject:dic];
    }
    
    //是否上传pdf
    NSString *uploadPdf = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
    STRING_NIL_TO_NONE(uploadPdf);
    
    //pdf 名称
    NSString *pdfName = [NSString stringWithFormat:@"%@",[command argumentAtIndex:3]];
    STRING_NIL_TO_NONE(pdfName);
    
    //分公司代码
    NSString *unitCode = [NSString stringWithFormat:@"%@",[command argumentAtIndex:4]];
    STRING_NIL_TO_NONE(unitCode);
    
    //分公司名称
    NSString *unitName = [NSString stringWithFormat:@"%@",[command argumentAtIndex:5]];
    STRING_NIL_TO_NONE(unitName);
    
    //员工代码
    NSString *agentCode = [NSString stringWithFormat:@"%@",[command argumentAtIndex:6]];
    STRING_NIL_TO_NONE(agentCode);
    
    //员工名称
    NSString *agentName = [NSString stringWithFormat:@"%@",[command argumentAtIndex:7]];
    STRING_NIL_TO_NONE(agentName);
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
                         url,@"url",
                         requestNo,@"requestNo",
                         uploadPdf,@"uploadPdf",
                         pdfName,@"pdfName",
                         unitCode,@"unitCode",
                         unitName,@"unitName",
                         agentCode,@"agentCode",
                         agentName,@"agentName",
                         dataList,@"dataList", nil];
    
    [[[ClassFactory getInstance] getNetComm] showHud:nil];
    [[[ClassFactory getInstance] getNetComm] ensureUploadImageList:dic success:^(id JSON) {
        
        NSDictionary *requestResultDic = [[NSDictionary alloc] initWithDictionary:JSON];
        NSString *resultCode = [requestResultDic objectForKey:@"resultCode"];
        
        NSString *message = [NSString stringWithFormat:@"%@",[requestResultDic objectForKey:@"message"]];
        STRING_NIL_TO_NONE(message);
        
        if ([resultCode isEqualToString:@"1"])
        {
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                        messageAsDictionary:requestResultDic];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            
            //清空缓存数据
            FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
            NSArray *where = [NSArray arrayWithObjects:@"requestNo",@"=",requestNo, nil];
            
            [fmdbSql deleteWithTableName:UPLOAD_TABLE_NAME where:where complete:^(BOOL isSuccess) {
                
                if (isSuccess){
                    NSLog(@"文件夹%@ 删除成功!",requestNo);
                }
                else
                {
                    NSLog(@"文件夹%@ 删除失败!",requestNo);
                }
            }];
        }
        else
        {
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                        messageAsDictionary:requestResultDic];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
        
    } failure:^(NSError *error) {
        
        NSString *errorCode = [NSString stringWithFormat:@"%ld",(long)error.code];
        STRING_NIL_TO_NONE(errorCode);

        if([errorCode isEqualToString:@"-1001"])
        {
            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",error.localizedDescription,@"message",nil];

            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:theMessage];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        else
        {
            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",error.localizedDescription,@"message",nil];
            
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:theMessage];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
    }];
}

#pragma mark -
#pragma mark - UICustomActionSheetDelegate
-(void)actionSheet:(UICustomActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex)
    {
        NSString *index = [NSString stringWithFormat:@"%@",[actionSheet.command argumentAtIndex:4]];
        STRING_NIL_TO_NONE(index);
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"用户取消操作",@"message",index,@"index", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
        
        return;
    }
    
    UICustomImagePickerController *imagePicker=[[UICustomImagePickerController alloc]init];
    imagePicker.view.backgroundColor = [UIColor whiteColor];
    
    BOOL isCameraSupport = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    BOOL isPhotoLibrarySupport= [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
    
    NSString *pickerCameraType = [NSString stringWithFormat:@"%@",[actionSheet.command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(pickerCameraType)
    
    if ([pickerCameraType isEqualToString:@"0"])
    {
        //相册
        if(!isPhotoLibrarySupport)
        {
            [[[ClassFactory getInstance] getInfoHUD] showHud:@"您的设备不支持此功能！"];
            
            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"您的设备不支持此功能！",@"message", nil];
            
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:theMessage];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
            
            return;
        }
        else
        {
            switch (buttonIndex)
            {
                //打开相册
                case 0:
                {
                    isAllowSaveToAlbum = nil;
                    
                    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    
                    break;
                }
                default:
                    
                    break;
            }
        }
    }
    else if ([pickerCameraType isEqualToString:@"1"])
    {
        //相机
        if (!isCameraSupport)
        {
            [[[ClassFactory getInstance]getInfoHUD]showHud:@"您的设备不支持此功能！"];
            
            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"您的设备不支持此功能！",@"message", nil];
            
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:theMessage];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
            
            return;
        }
        else
        {
            switch (buttonIndex)
            {
                //打开照相机拍照
                case 0:
                {
                    if ([actionSheet.isAllowLocation isEqualToString:@"1"])
                    {
                        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
                        {
                            [[[ClassFactory getInstance] getInfoHUD] showHud:@"请允许应用打开定位功能，否则无法添加地理位置信息。"];
                            
                            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"请允许应用打开定位功能，否则无法添加地理位置信息。",@"message", nil];
                            
                            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                                    messageAsDictionary:theMessage];
                            // send cordova result
                            [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
                            
                            return;
                        }
                        else
                        {
                            MultipleFalg = @"";
                            
                            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                            
                            break;
                        }
                    }
                    else
                    {
                        MultipleFalg = @"";
                        
                        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                        
                        break;
                    }
                }
                default:
                   
                    break;
            }
            
        }
    }
    else
    {
        //两者
        switch (buttonIndex)
        {
            case 0:  //打开照相机拍照
            {
                //相机
                if (!isCameraSupport)
                {
                    [[[ClassFactory getInstance] getInfoHUD] showHud:@"您的设备不支持此功能！"];
                    
                    NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"您的设备不支持此功能！",@"message", nil];
                    
                    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                            messageAsDictionary:theMessage];
                    // send cordova result
                    [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
                    
                    return;
                }
                else
                {
                    if ([actionSheet.isAllowLocation isEqualToString:@"1"])
                    {
                        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
                        {
                            [[[ClassFactory getInstance] getInfoHUD] showHud:@"请允许应用打开定位功能，否则无法添加地理位置信息。"];
                            
                            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"请允许应用打开定位功能，否则无法添加地理位置信息。",@"message", nil];
                            
                            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                                    messageAsDictionary:theMessage];
                            // send cordova result
                            [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
                            
                            return;
                        }
                        else
                        {
                            MultipleFalg = @"";
                            
                            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                            
                            break;
                        }
                    }
                    else
                    {
                        MultipleFalg = @"";
                        
                        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                        
                        break;
                    }
                }
            }
        
            case 1:  //打开本地相册
            {
                //相册
                if(!isPhotoLibrarySupport)
                {
                    [[[ClassFactory getInstance]getInfoHUD]showHud:@"您的设备不支持此功能！"];
                    
                    NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"您的设备不支持此功能！",@"message", nil];
                    
                    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                            messageAsDictionary:theMessage];
                    // send cordova result
                    [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
                    
                    return;
                }
                else
                {
                    isAllowSaveToAlbum = nil;
                    
                    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    
                    break;
                }
            }
                
            default:
                
                break;
        }
    }
    
    //拍照还是录像:0拍照1录像
    NSString *cameraOrVideoType = [NSString stringWithFormat:@"%@",[actionSheet.command argumentAtIndex:12]];
    STRING_NIL_TO_NONE(cameraOrVideoType)
    
    if ([MultipleFalg isEqualToString:@"1"] && (imagePicker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary))
    {
        // 跳转到相册页面
        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:[MultipleLimitNum integerValue] delegate:self];
        [imagePickerVc setAllowTakePicture:NO];
        [imagePickerVc setMaxImagesCount:MultipleLimitNum.integerValue];
        
        [self.viewController presentViewController:imagePickerVc animated:YES completion:nil];
    }
    else
    {        
        if ([cameraOrVideoType isEqualToString:@"1"])
        {
            //媒体类型多媒体格式（声音和视频）/照片
            NSString *typeMovie = ( NSString *)kUTTypeMovie;
            NSArray *arrMediaTypes = [NSArray arrayWithObjects:typeMovie,nil];
            [imagePicker setMediaTypes:arrMediaTypes];
            //设置是否可以管理已经存在的图片或者视频
            [imagePicker setAllowsEditing:YES];
            //设置录制的最大时长，默认是10分钟
            [imagePicker setVideoMaximumDuration:60*5];
        
            //设置录像的质量
            [imagePicker setVideoQuality:UIImagePickerControllerQualityTypeMedium];
        }
        else
        {
            NSString *requiredMediaType = ( NSString *)kUTTypeImage;
            NSArray *arrMediaTypes = [NSArray arrayWithObjects:requiredMediaType,nil];
            [imagePicker setMediaTypes:arrMediaTypes];
            
            //设置是否可以管理已经存在的图片或者视频
            [imagePicker setAllowsEditing:NO];
            
            //连续拍摄标记
            isContinuousShoot = @"1";
        }
        
        [imagePicker setDelegate:self];
        
        [imagePicker setCommand:actionSheet.command];
        
        [self.viewController presentViewController:imagePicker animated:YES completion:nil];
    }
}

#pragma mark -
#pragma mark - TZImagePickerControllerDelegate
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto infos:(NSArray<NSDictionary *> *)infos
{
    //不上传中台
    NSString *noUploadingImage = [NSString stringWithFormat:@"%@",[self.command argumentAtIndex:11]];
    STRING_NIL_TO_NONE(noUploadingImage)
    
    if ([noUploadingImage isEqualToString:@"1"])
    {
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"resultCode",@"不上传中台系统",@"message", nil];
        
        //上传成功回调
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                messageAsDictionary:theMessage];
        //send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:self.command.callbackId];
        
        return;
    }
    
    NSMutableArray *mutArr = [NSMutableArray array];
    
    //图片处理
    for (UIImage *theImage in photos)
    {
        NSDateFormatter *creatDateFormatter = [[NSDateFormatter alloc] init];
        [creatDateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        
        NSString *dateFormatterStr = [NSString stringWithFormat:@"%@",[creatDateFormatter stringFromDate:[NSDate date]]];
        
        NSString *userNameStr = [NSString stringWithFormat:@"%@_%u.png", dateFormatterStr,(arc4random() % 501) + 500];
        STRING_NIL_TO_NONE(userNameStr);
        
        //不缓存数据
        NSString *uncachedData = [NSString stringWithFormat:@"%@",[self.command argumentAtIndex:10]];
        STRING_NIL_TO_NONE(uncachedData)
    
        if (![uncachedData isEqualToString:@"1"])
        {
            //写入本地
            [[SDImageCache sharedImageCache] storeImage:theImage forKey:userNameStr toDisk:YES completion:^{
                
            }];
        }
        
        imageUploadModel *imageModel = [[imageUploadModel alloc] init];
        
        [imageModel setImage:theImage];
        [imageModel setImageName:userNameStr];
        
        [mutArr addObject:imageModel];
    }
    
    if (mutArr.count == 0)
    {
        [[[ClassFactory getInstance] getInfoHUD] showHud:@"提交清单为空!"];
        
        return;
    }
    //不进行数据提交
    [[[ClassFactory getInstance] getNetComm] showHud:nil];
    [self unlpoadImageWithCommand:self.command withFileMutArr:mutArr withSuccess:^(id JSON) {
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
    } withFail:^(NSError *error) {
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
    }];
}

- (void)tz_imagePickerControllerDidCancel:(TZImagePickerController *)picker
{
    NSString *index = [NSString stringWithFormat:@"%@",[self.command argumentAtIndex:4]];
    STRING_NIL_TO_NONE(index);
    
    NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"用户取消操作",@"message",index,@"index", nil];
    
    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                            messageAsDictionary:theMessage];
    // send cordova result
    [self.commandDelegate sendPluginResult:result callbackId:self.command.callbackId];
}

#pragma mark -
#pragma mark - UIImagePickerControllerDelegate
-(void)imagePickerController:(UICustomImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
	[[[ClassFactory getInstance] getNetComm] showHud:nil];
    
    NSDateFormatter *creatDateFormatter = [[NSDateFormatter alloc] init];
    [creatDateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString *dateFormatterStr = [NSString stringWithFormat:@"%@",[creatDateFormatter stringFromDate:[NSDate date]]];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        //通过UIImagePickerControllerMediaType判断返回的是照片还是视频
        NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        //如果返回的type等于kUTTypeImage，代表返回的是照片,并且需要判断当前相机使用的sourcetype是拍照还是相册
        if ([mediaType isEqualToString:( NSString *)kUTTypeImage])
        {
            UIImage *theImage = nil;
            // 判断，图片是否允许修改
            if ([picker allowsEditing])
            {
                //获取用户编辑之后的图像
                theImage = [UIImage fixOrientation:[info objectForKey:UIImagePickerControllerEditedImage]];
            }
            else
            {
                // 照片的元数据参数
                theImage = [UIImage fixOrientation:[info objectForKey:UIImagePickerControllerOriginalImage]];
            }
            
            NSString *userNameStr = [NSString stringWithFormat:@"%@_%u.png", dateFormatterStr,(arc4random() % 501) + 500];
            STRING_NIL_TO_NONE(userNameStr);
            
            //图片处理
            [self unlpoadImageWithCommand:picker.command withImage:theImage WithNameStr:userNameStr withSuccess:^(id JSON) {
                
                [[[ClassFactory getInstance] getNetComm] hiddenHud];
            } withFail:^(NSError *error) {
                
                [[[ClassFactory getInstance] getNetComm] hiddenHud];
            }];
        }
        else if ([mediaType isEqualToString:( NSString *)kUTTypeMovie])
        {
            NSURL *mediaURL = [info objectForKey:UIImagePickerControllerMediaURL];
            
            NSString *userNameStr = [NSString stringWithFormat:@"%@_%u.mp4", dateFormatterStr,(arc4random() % 501) + 500];
            STRING_NIL_TO_NONE(userNameStr);
            
            //视频压缩
            VideoServerHelper *videoServer = [[VideoServerHelper alloc] init];
    
            [videoServer compressVideo:mediaURL andVideoName:userNameStr andSave:NO successCompress:^(NSData *resultData) {

                if (resultData == nil || resultData.length == 0 ) {
                    //如果压缩失败，把未压缩的视频流直接上传给服务端
                    NSData *beforeVideoData = [NSData dataWithContentsOfURL:mediaURL];
                    [self unlpoadImageWithCommand:picker.command withFileData:beforeVideoData WithNameStr:userNameStr WithInfo:info withSuccess:^(id JSON) {

                        [[[ClassFactory getInstance] getNetComm] hiddenHud];
                    } withFail:^(NSError *error) {

                        [[[ClassFactory getInstance] getNetComm] hiddenHud];
                    }];
                }else{
                    //压缩成功的视频流上传给服务端
                    [self unlpoadImageWithCommand:picker.command withFileData:resultData WithNameStr:userNameStr WithInfo:info withSuccess:^(id JSON) {

                        [[[ClassFactory getInstance] getNetComm] hiddenHud];
                    } withFail:^(NSError *error) {

                        [[[ClassFactory getInstance] getNetComm] hiddenHud];
                    }];
                }
            }];
        }
        else
        {
            //
        }
        
        //图片上传
    }];
}

-(void)imagePickerControllerDidCancel:(UICustomImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
       
        NSString *index = [NSString stringWithFormat:@"%@",[picker.command argumentAtIndex:4]];
        STRING_NIL_TO_NONE(index);
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"用户取消操作",@"message",index,@"index", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:picker.command.callbackId];
        
    }];
}

#pragma mark -
#pragma mark - unlpoadImageHelper
- (void)unlpoadImageWithCommand:(CDVInvokedUrlCommand*)command withImage:(UIImage *)theImage WithNameStr:(NSString *)userNameStr withSuccess:(void (^)(id JSON))success withFail:(void (^)(NSError *error))fail
{
    UIImage *finialTempImage = nil;

    NSString *createDate = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLogic] getNowTimeString:@"YYYY-MM-dd HH:mm:ss"]];
    
    if ([isAllowLocation isEqualToString:@"1"])
    {
        //验车照片 添加水印图片 以及 时间戳
        UIImage *waterMask = [UIImage imageNamed:@"waterMask.png"];
        
        CGFloat width = (theImage.size.height - 20)/20.0;
        CGFloat height = (theImage.size.height - 20)/20.0;
        
        CGFloat x = 10;
        CGFloat y = theImage.size.height - 10 - height;
        
        NSString *location = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getAddress]];
        STRING_ADDRESS_UNABLE_NONE(location);
        
        NSString *locationString = [NSString stringWithFormat:@"地点：%@",location];
        STRING_NIL_TO_NONE(locationString);
        
        NSString *timeString = [NSString stringWithFormat:@"时间：%@",createDate];
        STRING_NIL_TO_NONE(timeString)
        
        //时间
        CGFloat time_x = x + width + 10;
        CGFloat time_y = y;
        CGFloat time_width = theImage.size.width - width - 20.0;
        CGFloat time_height = height/2.0;
        
        //地点
        CGFloat location_x = x + width + 10;
        CGFloat location_y = y + time_height;
        CGFloat location_width = theImage.size.width - width - 20.0;
        CGFloat location_height = height/2.0;
        
        CGFloat fontSize = width/2.0;
        
        finialTempImage = [theImage imageWithWaterMask:waterMask inRect:CGRectMake(x, y, width, height) WithStringWaterTimeMarkString:timeString atTimeRect:CGRectMake(time_x, time_y, time_width, time_height) WithStringWaterLocationString:locationString atLocationRect:CGRectMake(location_x, location_y, location_width, location_height) color:[UIColor redColor] font:[UIFont systemFontOfSize:fontSize]];
    }
    else
    {
        finialTempImage = [[UIImage alloc] initWithCGImage:theImage.CGImage];
    }
    
    NSData *imageData = UIImageJPEGRepresentation(finialTempImage, 1.0);
    
    //进行图片像素限制压缩 320万
    if (finialTempImage.size.width * finialTempImage.size.height > MAX_PIXEL)
    {
        UIImage *finialImage = [UIImage compressImage:finialTempImage toPixel:MAX_PIXEL];
        
        imageData = UIImageJPEGRepresentation(finialImage, 1.0);
    }
    else
    {
        imageData = UIImageJPEGRepresentation(finialTempImage, 1.0);
    }

    //不缓存数据
    NSString *uncachedData = [NSString stringWithFormat:@"%@",[command argumentAtIndex:10]];
    STRING_NIL_TO_NONE(uncachedData)
    
    if (![uncachedData isEqualToString:@"1"])
    {
        //写入本地
        [[SDImageCache sharedImageCache] storeImage:finialTempImage forKey:userNameStr toDisk:YES completion:^{
            
        }];
    }
    
    NSString *imageSize = [NSString stringWithFormat:@"%ld",(long)imageData.length];
    STRING_NIL_TO_NONE(imageSize)
    
    //文件上传批次 任务申请号
    NSString *requestNo = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
    STRING_NIL_TO_NONE(requestNo);
    
    //上传服务器url
    NSString *url = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(url);
    
    //图片类型（对应影像系统ID）
    NSString *imageType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:3]];
    STRING_NIL_TO_NONE(imageType);
    
    //图片标记
    NSString *imageIndex = [NSString stringWithFormat:@"%@",[command argumentAtIndex:4]];
    STRING_NIL_TO_NONE(imageIndex);
    
    NSDictionary *uploadRequest = [NSDictionary dictionaryWithObjectsAndKeys:imageSize,@"imageData",createDate,@"createDate",requestNo,@"requestNo",url,@"url",nil];
    
    [self unlpoadImageWithFileName:userNameStr withFileData:imageData withCommand:command parameters:uploadRequest withSuccess:^(id JSON) {
        NSDictionary *requestResultDic = [[NSDictionary alloc] initWithDictionary:JSON];
        NSString *resultCode = [requestResultDic objectForKey:@"resultCode"];
        
        if ([resultCode isEqualToString:@"1"])
        {
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:userNameStr,@"imageName",imageType,@"imageType",requestNo,@"requestNo",imageIndex,@"imageIndex", nil];
            
            BOOL isSuccess = [[[ClassFactory getInstance] getLogic] addImageToMutArr:dic WithIndex:imageIndex];
            
            if (isSuccess)
            {
                NSLog(@"文件%@添加临时提交数组成功!",userNameStr);
            }
            else
            {
                NSLog(@"文件%@添加临时提交数组失败!",userNameStr);
            }
            
            if (![uncachedData isEqualToString:@"1"])
            {
                //写入数据库
                FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
                
                [fmdbSql insertIntoTableName:UPLOAD_TABLE_NAME Dict:dic complete:^(BOOL isSuccess) {
                    
                    if (isSuccess)
                    {
                        NSLog(@"文件%@写入数据库成功!",userNameStr);
                    }
                    else
                    {
                        NSLog(@"文件%@写入数据库失败!",userNameStr);
                    }
                    
                }];
            }
            
            //图片上传成功之后，保存相册
            if ([isAllowSaveToAlbum isEqualToString:@"1"])
            {
                UIImageWriteToSavedPhotosAlbum(finialTempImage,self,@selector(imageSavedToPhotosAlbum:didFinishSavingWithError:contextInfo:),nil);
            }
            
            NSMutableDictionary *returnMutDic = [NSMutableDictionary dictionaryWithDictionary:requestResultDic];
            
            //图片名称
            [returnMutDic setObject:userNameStr forKey:@"imageName"];
            
            //图片流
            NSString *imageDataString = [NSString stringWithFormat:@"%@",[imageData cdv_base64EncodedString]];
            STRING_NIL_TO_NONE(imageDataString);

            [returnMutDic setObject:imageDataString forKey:@"imageData"];
            
            //图片标记
            [returnMutDic setObject:imageIndex forKey:@"index"];
            
            //上传成功回调
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsDictionary:returnMutDic];
            //send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        else
        {
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:requestResultDic];
            //send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        
        if (success)
        {
            success(JSON);
        }
        
    } withFail:^(NSError *error) {
        
        NSString *errorInfo = [NSString stringWithFormat:@"%@",error.localizedDescription];
        STRING_NIL_TO_NONE(errorInfo);
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",errorInfo,@"message", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        
        if (fail)
        {
            fail(error);
        }
        
    }];
}

#pragma mark -
#pragma mark - unlpoadImage 单张图片上传
- (void)unlpoadImageWithFileName:(NSString *)tempFileName withFileData:(NSData *)fileData withCommand:(CDVInvokedUrlCommand*)command parameters:(NSDictionary *)requestobjDic withSuccess:(void (^)(id JSON))success withFail:(void (^)(NSError *error))fail
{
    //不上传中台
    NSString *noUploadingImage = [NSString stringWithFormat:@"%@",[command argumentAtIndex:11]];
    STRING_NIL_TO_NONE(noUploadingImage)
    
    if ([noUploadingImage isEqualToString:@"1"])
    {
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"resultCode",@"不上传中台系统",@"message", nil];
        
        success(theMessage);
        
        return;
    }
    
    [[[ClassFactory getInstance] getFileUpLoadAFRequestQueue] addFileUpOperationWithFileName:tempFileName parameters:requestobjDic withFileData:fileData withOnlyTarget:tempFileName withSuccess:^(id JSON) {
        
        if (success)
        {
            success(JSON);
        }
        
    } withFail:^(NSError *error) {
        
        if (fail)
        {
            fail(error);
        }
        
    } progessDataBlock:^(float percentDone) {

        
    }];
}
#pragma mark -
#pragma mark - 保存图片后的回调
- (void)imageSavedToPhotosAlbum:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(id)contextInfo
{
    if(!error)
    {
        [[[ClassFactory getInstance] getInfoHUD] showHud:@"图片保存相册成功！"];
    }
}

#pragma mark -
#pragma mark - 保存视频后的回调
- (void)videoSavedToPhotosAlbum:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (!error)
    {
        [[[ClassFactory getInstance] getInfoHUD] showHud:@"视频保存相册成功！"];
    }
}

#pragma mark -
#pragma mark - unlpoadImage 单张视频上传
- (void)unlpoadImageWithCommand:(CDVInvokedUrlCommand*)command withFileData:(NSData *)fileData WithNameStr:(NSString *)userNameStr WithInfo:(NSDictionary *)info withSuccess:(void (^)(id JSON))success withFail:(void (^)(NSError *error))fail
{
    //不缓存数据
    NSString *uncachedData = [NSString stringWithFormat:@"%@",[command argumentAtIndex:10]];
    STRING_NIL_TO_NONE(uncachedData)
    
    NSString *imageSize = [NSString stringWithFormat:@"%ld",(long)fileData.length];
    STRING_NIL_TO_NONE(imageSize)
    
    //文件上传批次 任务申请号
    NSString *requestNo = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
    STRING_NIL_TO_NONE(requestNo);
    
    //上传服务器url
    NSString *url = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(url);
    
    //图片类型（对应影像系统ID）
    NSString *imageType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:3]];
    STRING_NIL_TO_NONE(imageType);
    
    //图片标记
    NSString *imageIndex = [NSString stringWithFormat:@"%@",[command argumentAtIndex:4]];
    STRING_NIL_TO_NONE(imageIndex);
    
    //当前日期
    NSString *createDate = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLogic] getNowTimeString:@"YYYY-MM-dd HH:mm:ss"]];
    
    NSDictionary *uploadRequest = [NSDictionary dictionaryWithObjectsAndKeys:imageSize,@"imageData",createDate,@"createDate",requestNo,@"requestNo",url,@"url",nil];
    
    [self unlpoadImageWithFileName:userNameStr withFileData:fileData withCommand:command parameters:uploadRequest withSuccess:^(id JSON) {
        NSDictionary *requestResultDic = [[NSDictionary alloc] initWithDictionary:JSON];
        NSString *resultCode = [requestResultDic objectForKey:@"resultCode"];
        
        if ([resultCode isEqualToString:@"1"])
        {
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:userNameStr,@"imageName",imageType,@"imageType",requestNo,@"requestNo",imageIndex,@"imageIndex", nil];
            
            BOOL isSuccess = [[[ClassFactory getInstance] getLogic] addImageToMutArr:dic WithIndex:imageIndex];
            
            if (isSuccess)
            {
                NSLog(@"文件%@添加临时提交数组成功!",userNameStr);
            }
            else
            {
                NSLog(@"文件%@添加临时提交数组失败!",userNameStr);
            }
            
            //视频上传成功之后，保存相册
            if ([isAllowSaveToAlbum isEqualToString:@"1"])
            {
                NSString *videoPath = [NSString stringWithFormat:@"%@",[[info objectForKey:UIImagePickerControllerMediaURL] path]];
                STRING_NIL_TO_NONE(videoPath)
                
                if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(videoPath))
                {
                    UISaveVideoAtPathToSavedPhotosAlbum(videoPath, self, @selector(videoSavedToPhotosAlbum:didFinishSavingWithError:contextInfo:), nil);
                }
                else
                {
                    [[[ClassFactory getInstance] getInfoHUD] showHud:@"视频保存错误!"];
                }
                
            }
            
            NSMutableDictionary *returnMutDic = [NSMutableDictionary dictionaryWithDictionary:requestResultDic];
            
            //图片名称
            [returnMutDic setObject:userNameStr forKey:@"imageName"];
            
            //图片标记
            [returnMutDic setObject:imageIndex forKey:@"index"];
            
            //上传成功回调
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsDictionary:returnMutDic];
            //send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        else
        {
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:requestResultDic];
            //send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            
            NSString *message = [NSString stringWithFormat:@"%@",[requestResultDic objectForKey:@"message"]];
            STRING_NIL_TO_NONE(message)
            [[[ClassFactory getInstance] getInfoHUD] showHud:message];
        }
        
        if (success)
        {
            success(JSON);
        }
        
    } withFail:^(NSError *error) {
        
        NSString *errorInfo = [NSString stringWithFormat:@"%@",error.localizedDescription];
        STRING_NIL_TO_NONE(errorInfo);
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",errorInfo,@"message", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        
        if (fail)
        {
            fail(error);
        }
        
    }];
}

#pragma mark -
#pragma mark - unlpoadImageHelper
- (void)unlpoadImageWithCommand:(CDVInvokedUrlCommand*)command withFileMutArr:(NSArray *)tempFileMutArr withSuccess:(void (^)(id JSON))success withFail:(void (^)(NSError *error))fail
{
    //文件上传批次 任务申请号
    NSString *requestNo = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
    STRING_NIL_TO_NONE(requestNo);
    
    //上传服务器url
    NSString *url = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(url);
    
    //图片类型（对应影像系统ID）
    NSString *imageType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:3]];
    STRING_NIL_TO_NONE(imageType);
    
    //图片标记
    NSString *imageIndex = [NSString stringWithFormat:@"%@",[command argumentAtIndex:4]];
    STRING_NIL_TO_NONE(imageIndex);
    
    NSDictionary *uploadRequest = [NSDictionary dictionaryWithObjectsAndKeys:requestNo,@"requestNo",url,@"url",nil];
    
    [self unlpoadImageWithFileMutArr:tempFileMutArr parameters:uploadRequest withSuccess:^(id JSON) {
        
        NSDictionary *requestResultDic = [[NSDictionary alloc] initWithDictionary:JSON];
        NSString *resultCode = [requestResultDic objectForKey:@"resultCode"];
        
        if ([resultCode isEqualToString:@"1"])
        {
            NSMutableArray *mutArr = [NSMutableArray array];
            
            for (imageUploadModel *imageModel in tempFileMutArr)
            {
                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:imageModel.imageName,@"imageName",imageType,@"imageType",requestNo,@"requestNo",imageIndex,@"imageIndex", nil];
                
                BOOL isSuccess = [[[ClassFactory getInstance] getLogic] addImageToMutArr:dic WithIndex:imageIndex];
                
                if (isSuccess)
                {
                    NSLog(@"文件%@添加临时提交数组成功!",imageModel.imageName);
                }
                else
                {
                    NSLog(@"文件%@添加临时提交数组失败!",imageModel.imageName);
                }
                
                //不缓存数据
                NSString *uncachedData = [NSString stringWithFormat:@"%@",[command argumentAtIndex:10]];
                STRING_NIL_TO_NONE(uncachedData)
                
                if (![uncachedData isEqualToString:@"1"])
                {
                    //写入数据库
                    FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
                    
                    [fmdbSql insertIntoTableName:UPLOAD_TABLE_NAME Dict:dic complete:^(BOOL isSuccess) {
                        
                        if (isSuccess)
                        {
                            NSLog(@"文件%@写入数据库成功!",imageModel.imageName);
                        }
                        else
                        {
                            NSLog(@"文件%@写入数据库失败!",imageModel.imageName);
                        }
                        
                    }];
                }
                
                NSMutableDictionary *mutDic = [NSMutableDictionary dictionary];
                
                //图片名称
                [mutDic setObject:imageModel.imageName forKey:@"imageName"];
                
                //图片流
                NSData *data = UIImageJPEGRepresentation(imageModel.image, 0.0000001);
                NSString *imageDataString = [NSString stringWithFormat:@"%@",[data cdv_base64EncodedString]];
                STRING_NIL_TO_NONE(imageDataString);
                
                [mutDic setObject:imageDataString forKey:@"imageData"];
                
                //图片标记
                [mutDic setObject:imageIndex forKey:@"index"];
                [mutDic setObject:imageType forKey:@"imageType"];
                
                [mutArr addObject:mutDic];
            }
            
            NSMutableDictionary *returnMutDic = [NSMutableDictionary dictionaryWithDictionary:requestResultDic];
            
            [returnMutDic setObject:mutArr forKey:@"files"];
            [returnMutDic setObject:@"2" forKey:@"resultCode"];

            //上传成功回调
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsDictionary:returnMutDic];
            //send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        else
        {
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:requestResultDic];
            //send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        
        if (success)
        {
            success(JSON);
        }
        
    } withFail:^(NSError *error) {
        
        NSString *errorInfo = [NSString stringWithFormat:@"%@",error.localizedDescription];
        STRING_NIL_TO_NONE(errorInfo);
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",errorInfo,@"message", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        
        if (fail)
        {
            fail(error);
        }
        
    }];
}

#pragma mark -
#pragma mark - unlpoadImage 批量
- (void)unlpoadImageWithFileMutArr:(NSArray *)tempFileMutArr parameters:(NSDictionary *)requestobjDic withSuccess:(void (^)(id JSON))success withFail:(void (^)(NSError *error))fail
{
    [[[ClassFactory getInstance] getFileUpLoadAFRequestQueue] addFileUpOperationWithFileMutArr:tempFileMutArr parameters:requestobjDic withOnlyTarget:@"" withSuccess:^(id JSON) {
        
        if (success)
        {
            success(JSON);
        }
        
    } withFail:^(NSError *error) {
        
        if (fail)
        {
            fail(error);
        }
        
    } progessDataBlock:^(float percentDone) {
        
    }];
}

#pragma mark -
#pragma mark -

@end
