//
//  imageUploadModel.h
//  IOSFramework
//
//  Created by 林科 on 2017/11/6.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface imageUploadModel : NSObject
{
    
}
@property(nonatomic,strong) UIImage *image;

@property(nonatomic,strong) NSString *imageName;

@property(nonatomic,strong) NSString *imageBase64String;

@end
