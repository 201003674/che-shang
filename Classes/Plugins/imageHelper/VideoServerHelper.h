//
//  VideoServerHelper.h
//  IOSFramework
//
//  Created by 林科 on 2018/6/14.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Photos/Photos.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface VideoServerHelper : NSObject
{
    
}

///视频名字
@property (nonatomic,strong) NSString *videoName;

/// 压缩视频
-(void)compressVideo:(NSURL *)path andVideoName:(NSString *)name andSave:(BOOL)saveState
     successCompress:(void(^)(NSData *resultData))successCompress;

/// 获取视频的首帧缩略图
- (UIImage *)imageWithVideoURL:(NSURL *)url;


@end
