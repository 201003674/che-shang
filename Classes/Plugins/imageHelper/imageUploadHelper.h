//
//  imageUploadHelper.h
//  IOSFramework
//
//  Created by 林科 on 2017/3/29.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import "CDVBasePlugin.h"

#import "UIImage+Utility.h"
#import "imageUploadModel.h"
#import <CoreLocation/CLLocationManager.h>

#import "VideoServerHelper.h"
@interface imageUploadHelper : CDVBasePlugin<UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,TZImagePickerControllerDelegate>
{
    //是否验车照片 需要强制允许定位功能 验车照片
    NSString *isAllowLocation;
    
    //是否需要保存相册
    NSString *isAllowSaveToAlbum;
    
    //是否支持多选
    NSString *MultipleFalg;
    
    //多选限制数量
    NSString *MultipleLimitNum;
    
    //是否支持连续拍摄
    NSString *isContinuousShoot;
    
}
//初始化
- (void)deviceready:(CDVInvokedUrlCommand *)command;

//初始化太享贷
- (void)devicereadyTXD:(CDVInvokedUrlCommand*)command;

//获取缓存
- (void)getImageCache:(CDVInvokedUrlCommand *)command;

//添加图片
- (void)addImage:(CDVInvokedUrlCommand *)command;

// 添加图片太享贷
- (void)addImageTXD:(CDVInvokedUrlCommand*)command;

//删除图片
- (void)deleteImage:(CDVInvokedUrlCommand *)command;

//确认上传
- (void)ensureUpload:(CDVInvokedUrlCommand *)command;

//单独添加到提交清单
- (void)insertIntoSubmitList:(CDVInvokedUrlCommand *)command;

//清空下提交清单
- (void)DeleteDetailedlist:(CDVInvokedUrlCommand *)command;

@end
