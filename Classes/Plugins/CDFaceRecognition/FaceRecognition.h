//
//  FaceRecognition.h
//  IOSFramework
//
//  Created by 林科 on 2017/6/22.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import <Cordova/CDVPlugin.h>

#import "STImage.h"
#import "NSString+Additions.h"
@interface FaceRecognition : CDVPlugin
{
    NSString *lfImageName;
}

//人脸识别
- (void)linkeface:(CDVInvokedUrlCommand *)command;

//人脸识别 活体检测
- (void)linkefaceWithBioAssay:(CDVInvokedUrlCommand *)command;

@end
