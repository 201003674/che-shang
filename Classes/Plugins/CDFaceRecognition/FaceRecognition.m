//
//  FaceRecognition.m
//  IOSFramework
//
//  Created by 林科 on 2017/6/22.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import "FaceRecognition.h"

#import "NSObject+MJKeyValue.h"

@implementation FaceRecognition

#pragma mark -
#pragma mark - 客户核身
- (void)linkeface:(CDVInvokedUrlCommand *)command
{
    //OCR信息识别
    [self ocrRecognition:command success:^(id JSON) {
        
        //身份证号
        NSString *idNumber = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
        STRING_NIL_TO_NONE(idNumber);
        
        NSDictionary *dic = [[NSDictionary alloc] initWithDictionary:[[JSON mj_JSONString] objectFromJSONString]];
        
        NSString *type = [NSString stringWithFormat:@"%@",[dic objectForKey:@"type"]];
        STRING_NIL_TO_NONE(type);
        
        if ([type isEqualToString:@"未知类型"])
        {
            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",@"无法识别图片,请重新拍照",@"message",nil];
            
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                        messageAsString:[theMessage JSONString]];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            
            
            [[[ClassFactory getInstance] getNetComm] hiddenHud];
        }
        else
        {
            //识别身份证号
            NSString *id_number = [NSString stringWithFormat:@"%@",[dic objectForKey:@"id_number"]];
            STRING_NIL_TO_NONE(id_number);
            
            if ([id_number isEqualToString:idNumber])
            {
                //进行人脸比对
                [self startFaceRecognitionAction:command];
            }
            else
            {
                NSString *error = [NSString stringWithFormat:@"证件照片中身份证号（%@）与录入身份证号（%@）不一致!",id_number,idNumber];
                STRING_NIL_TO_NONE(error)
                
                NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",error,@"message",nil];
                
                CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                            messageAsString:[theMessage JSONString]];
                // send cordova result
                [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
                
                
                [[[ClassFactory getInstance] getNetComm] hiddenHud];
            }
        }
        
    } failure:^(NSError *error) {
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",error.localizedDescription,@"message",nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsString:[theMessage JSONString]];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
    }];
}

#pragma mark -
#pragma mark - OCR识别
- (void)ocrRecognition:(CDVInvokedUrlCommand *)command success:(void (^)(id JSON))success
               failure:(void (^)(NSError *error))failure
{
    //ocr请求url
    NSString *ocrUrl = [NSString stringWithFormat:@"%@",[command argumentAtIndex:4]];
    STRING_NIL_TO_NONE(ocrUrl);
    
    //身份证号
    NSString *idNumber = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(idNumber);
    
    AFSessionManager *manager = [AFSessionManager manager];
    
    //发送格式
     manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    //接受格式
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    //进程标签
    manager.targetOnlyStr = idNumber;
    
    [manager POST:ocrUrl parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        /*
         此方法参数
         1. 要上传的[二进制数据]
         2. 对应网站上[upload.php中]处理文件的[字段"file"]
         3. 要保存在服务器上的[文件名]
         4. 上传文件的[mimeType]
         
         */
        //身份证图片
        NSString *idNumberImg = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
        STRING_NIL_TO_NONE(idNumberImg);
        
        if([Check checkUrl:idNumberImg])
        {
            //URL 获取服务器图片
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:idNumberImg]];
            
            [formData appendPartWithFileData:imageData name:@"file" fileName:idNumberImg mimeType:@"image/jpeg"];
        }
        else
        {
            //本地 获取本地缓存图片
            UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:idNumberImg];
            
            NSData *imageData = UIImageJPEGRepresentation(image, 0.00001);
            
            [formData appendPartWithFileData:imageData name:@"file" fileName:idNumberImg mimeType:@"image/jpeg"];
        }
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        // 配置上传 百分比
        float percentDone = uploadProgress.completedUnitCount/uploadProgress.totalUnitCount;
        
        NSLog(@"OCR百分比 ====================%f",percentDone);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success)         {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure)
        {
            failure(error);
        }
        
        NSString *errorInfo = [NSString stringWithFormat:@"%@",error.localizedDescription];
        STRING_NIL_TO_NONE(errorInfo);
        
        [[[ClassFactory getInstance] getInfoHUD] showHud:errorInfo];
    }];
}

#pragma mark -
#pragma mark - 人脸比对
- (void)startFaceRecognitionAction:(CDVInvokedUrlCommand*)command
{
    //请求url
    NSString *url = [NSString stringWithFormat:@"%@",[command argumentAtIndex:5]];
    STRING_NIL_TO_NONE(url);
    
    //任务申请号
    NSString *requestNo = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(requestNo);
    
    //身份证号
    NSString *idNumber = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(idNumber);
    
    //身份证图片
    NSString *idNumberImg = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
    STRING_NIL_TO_NONE(idNumberImg);
    
    //手持投保单
    NSString *insuranceImg = [NSString stringWithFormat:@"%@",[command argumentAtIndex:3]];
    STRING_NIL_TO_NONE(insuranceImg);
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:url,@"url",requestNo,@"requestNo",idNumber,@"idNumber",idNumberImg,@"idNumberImg",insuranceImg,@"insuranceImg", nil];
    
    [[[ClassFactory getInstance] getNetComm] startFaceRecognitionAction:dic success:^(id JSON) {
            
        NSDictionary *requestResultDic = [[NSDictionary alloc] initWithDictionary:JSON];
        NSString *resultCode = [requestResultDic objectForKey:@"resultCode"];
        
        NSString *message = [NSString stringWithFormat:@"%@",[requestResultDic objectForKey:@"message"]];
        STRING_NIL_TO_NONE(message);
        
        if ([resultCode isEqualToString:@"1"])
        {
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsString:[requestResultDic JSONString]];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            
        }
        else
        {
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsString:[requestResultDic JSONString]];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
        
    } failure:^(NSError *error) {
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",error.localizedDescription,@"message",nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsString:[theMessage JSONString]];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
    }];
}

#pragma mark -
#pragma mark - 人脸识别 活体检测
- (void)linkefaceWithBioAssay:(CDVInvokedUrlCommand *)command
{
    /*
     0 任务申请号 1 身份证号 2 身份证图片 正面图片 3 身份证图片 反面图片 4 ocr 识别url地址 5 活体图片提交的单证顺序 6 活体图片提交的单证类型 7 客户姓名 8 分公司代码 9 经办人代码 10 type
     */
    
    [[[ClassFactory getInstance] getNetComm] showHud:nil];
    
    //开始人脸比对
    [self startFaceRecognitionActionWithBioAssay:command success:^(id JSON) {
        
        NSDictionary *requestResultDic = [[NSDictionary alloc] initWithDictionary:[[JSON mj_JSONString] objectFromJSONString]];
        
        NSString *resultCode = [requestResultDic objectForKey:@"resultCode"];
        
        NSString *message = [NSString stringWithFormat:@"%@",[requestResultDic objectForKey:@"message"]];
        STRING_NIL_TO_NONE(message);
        
        if ([resultCode isEqualToString:@"1"])
        {
            NSMutableDictionary *mutDic = [NSMutableDictionary dictionaryWithDictionary:requestResultDic];
            
            STRING_NIL_TO_NONE(lfImageName);
            [mutDic setObject:lfImageName forKey:@"imageName"];
            
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                        messageAsString:[mutDic JSONString]];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        else
        {
            NSMutableDictionary *mutDic = [NSMutableDictionary dictionaryWithDictionary:requestResultDic];
            
            STRING_NIL_TO_NONE(lfImageName);
            [mutDic setObject:lfImageName forKey:@"imageName"];
            
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                        messageAsString:[mutDic JSONString]];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
        
    } failure:^(NSError *error) {
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",error.localizedDescription,@"message",nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsString:[theMessage JSONString]];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
    }];
}

#pragma mark -
#pragma mark - 人脸比对 活体检测
- (void)startFaceRecognitionActionWithBioAssay:(CDVInvokedUrlCommand*)command success:(void (^)(id JSON))success failure:(void (^)(NSError *error))failure
{
    /*
     0 任务申请号 1 身份证号 2 身份证图片 正面图片 3 身份证图片 反面图片 4 ocr 识别url地址 5 活体图片提交的单证顺序 6 活体图片提交的单证类型 7 客户姓名 8 分公司代码 9 经办人代码 10 type
     */
    
    //任务申请号
    NSString *requestNo = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(requestNo);
    
    //身份证号
    NSString *idNumber = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(idNumber);
    
    //身份证图片 正面名称
    NSString *idNumberImg = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
    STRING_NIL_TO_NONE(idNumberImg);
    
    //客户姓名
    NSString *userName = [NSString stringWithFormat:@"%@",[command argumentAtIndex:7]];
    STRING_NIL_TO_NONE(userName);
    
    //分公司代码
    NSString *unitCode = [NSString stringWithFormat:@"%@",[command argumentAtIndex:8]];
    STRING_NIL_TO_NONE(unitCode);
    
    //经办人代码
    NSString *agentCode = [NSString stringWithFormat:@"%@",[command argumentAtIndex:9]];
    STRING_NIL_TO_NONE(agentCode);
    
    //type
    NSString *type = [NSString stringWithFormat:@"%@",[command argumentAtIndex:10]];
    STRING_NIL_TO_NONE(type);
    
    NSDictionary *requestObject = [NSDictionary dictionaryWithObjectsAndKeys:
                                   requestNo,@"requestNo",
                                   idNumber,@"idNumber",
                                   idNumberImg,@"idNumberImg",
                                   userName,@"userName",
                                   unitCode,@"unitCode",
                                   agentCode,@"agentCode",
                                   type,@"type",
                                   nil];
    
    AFSessionManager *manager = [AFSessionManager manager];
    
    //发送格式
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    //接受格式
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //进程标签
    manager.targetOnlyStr = idNumber;
    
    NSMutableString *strUrl = [NSMutableString stringWithFormat:@"%@/",
                               [[[ClassFactory getInstance] getLocalCfg] getHttpServerBookAddr]];
    [strUrl appendFormat:@"cpic-cxcim-web/crm/linkfaceliveness.do?"];
    
    NSString *platType = [[[ClassFactory getInstance] getLocalCfg] getPlatType];
    STRING_NIL_TO_NONE(platType);
    NSString *version = [[[ClassFactory getInstance] getLocalCfg] getVersionCode];
    STRING_NIL_TO_NONE(version);
    NSString *projectType = [[[ClassFactory getInstance] getLocalCfg] getProjectType];
    STRING_NIL_TO_NONE(projectType);
    NSString *userToken = [[[ClassFactory getInstance] getLocalCfg] getUserToken];
    STRING_NIL_TO_NONE(userToken);
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                projectType,@"projectType",
                                platType,@"platType",
                                version,@"version",
                                userToken,@"userToken",
                                requestObject,@"requestObject",
                                nil];
    
    NSString *urlstring = [NSString stringWithFormat:@"%@requestMessage=%@",strUrl,[parameters JSONString]];
    STRING_NIL_TO_NONE(urlstring);
    
    urlstring = [urlstring stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [manager POST:urlstring parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        /*
         此方法参数
         1. 要上传的[二进制数据]
         2. 对应网站上[upload.php中]处理文件的[字段"file"]
         3. 要保存在服务器上的[文件名]
         4. 上传文件的[mimeType]
         */
        
        NSString *imageDataName = [NSString stringWithFormat:@"%@.protobuf",[[[ClassFactory getInstance] getLogic] getImageName]];
        STRING_NIL_TO_NONE(imageDataName);
        
        NSData *imageData = [NSData dataWithData:[[[ClassFactory getInstance] getLogic] encryTarData]];
        
        [formData appendPartWithFileData:imageData name:imageDataName fileName:imageDataName mimeType:@"image/jpeg"];
        
        /* 提交活体检测图片 */
        if ([[[[ClassFactory getInstance] getLogic] encryArrLFImage] count] != 0)
        {
            STImage *lfImage = [[[[ClassFactory getInstance] getLogic] encryArrLFImage] firstObject];
            
            lfImageName = [NSString stringWithFormat:@"%@.png",[[[ClassFactory getInstance] getLogic] getImageName]];
            STRING_NIL_TO_NONE(lfImageName);
            
            //公安征信限制图片大小到32K以下
            NSData *lfImageData =  [NSData compressImage:lfImage.image toByte:32*1024];
            NSLog(@"公安征信限制图片%lu",lfImageData.length);
            
            [formData appendPartWithFileData:lfImageData name:lfImageName fileName:lfImageName mimeType:@"image/jpeg"];
            
            //图片顺序 取值任务申请号 作为更新操作
            NSString *imageIndex = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
            STRING_NIL_TO_NONE(imageIndex);

            //图片类型
            NSString *imageType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:6]];
            STRING_NIL_TO_NONE(imageType);
            
            NSArray *mutArr = [NSArray arrayWithArray:[[[ClassFactory getInstance] getLogic] mutArr]];
            
            //更新活体检测图片到提交清单 1正向流程 2 外访
            if([type isEqualToString:@"1"])
            {
                [self uploadLinkefaceWithBioAssay:mutArr WithRequestNo:requestNo WithImageName:lfImageName WithImageType:imageType WithImageIndex:imageIndex];
            }
            
            //清空活体图片
            [[[[ClassFactory getInstance] getLogic] encryArrLFImage] removeAllObjects];
        }
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        // 配置上传 百分比
        float percentDone = uploadProgress.completedUnitCount/uploadProgress.totalUnitCount;
        
        NSLog(@"人脸检测（活体）百分比 ====================%f",percentDone);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success){
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure)
        {
            failure(error);
        }
        
        NSString *errorInfo = [NSString stringWithFormat:@"%@",error.localizedDescription];
        STRING_NIL_TO_NONE(errorInfo);
        
        [[[ClassFactory getInstance] getInfoHUD] showHud:errorInfo];
    }];
}


#pragma mark -
#pragma mark - uploadLinkefaceWithBioAssay
- (void)uploadLinkefaceWithBioAssay:(NSArray *)mutArr WithRequestNo:(NSString *)requestNo WithImageName:(NSString *)imageName WithImageType:(NSString *)imageType WithImageIndex:(NSString *)imageIndex
{
    FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
    
    NSArray *keys = [NSArray array];
    NSArray *where = [NSArray arrayWithObjects:@"requestNo",@"=",requestNo,@"imageIndex",@"=",imageIndex, nil];
    
    NSDictionary *requestResultDic = [NSDictionary dictionaryWithObjectsAndKeys:imageName,@"imageName",imageType,@"imageType",requestNo,@"requestNo",imageIndex,@"imageIndex", nil];
    
    [fmdbSql queryWithTableName:UPLOAD_TABLE_NAME keys:keys where:where complete:^(NSArray *array) {
        
        if (array.count == 0)
        {
            //需要清单
            [fmdbSql insertIntoTableName:UPLOAD_TABLE_NAME Dict:requestResultDic complete:^(BOOL isSuccess) {
                
                if (isSuccess)
                {
                    NSLog(@"文件%@写入数据库成功!",imageName);
                }
                else
                {
                    NSLog(@"文件%@写入数据库失败!",imageName);
                }
            }];
            
            [[[[ClassFactory getInstance] getLogic] mutArr] addObject:requestResultDic];
        }
        else
        {
            //更新清单
            [fmdbSql updateWithTableName:UPLOAD_TABLE_NAME valueDict:requestResultDic where:where complete:^(BOOL isSuccess) {
                
                if (isSuccess)
                {
                    NSLog(@"文件%@更新数据库成功!",imageName);
                }
                else
                {
                    NSLog(@"文件%@更新数据库失败!",imageName);
                }
                
            }];
            
            NSArray *fileUploadArr = [NSArray arrayWithArray:[[[ClassFactory getInstance] getLogic] mutArr]];
            
            NSLog(@"*****************%@*****************",[fileUploadArr JSONString]);
            
            for (NSInteger i = 0 ; i < fileUploadArr.count ; i ++ )
            {
                NSDictionary *tempDic = [NSDictionary dictionaryWithDictionary:[fileUploadArr objectAtIndex:i]];
                
                //任务申请号
                NSString *tempRequestNo = [NSString stringWithFormat:@"%@",[tempDic objectForKey:@"requestNo"]];
                STRING_NIL_TO_NONE(tempRequestNo);
                
                //图片序号
                NSString *tempImageIndex = [NSString stringWithFormat:@"%@",[tempDic objectForKey:@"imageIndex"]];
                STRING_NIL_TO_NONE(tempImageIndex);
                
                if ([tempRequestNo isEqualToString:requestNo] && [tempImageIndex isEqualToString:imageIndex])
                {
                    //更新清单
                    if (fileUploadArr.count > i)
                    {
                        [[[[ClassFactory getInstance] getLogic] mutArr] replaceObjectAtIndex:i withObject:requestResultDic];
                    }
                }
                
            }
        }
    }];
    
    NSLog(@"*****************%@*****************",[[[[ClassFactory getInstance] getLogic] mutArr] JSONString]);
}

@end
