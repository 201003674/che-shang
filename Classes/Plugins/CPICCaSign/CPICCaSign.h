//
//  MBjca.h
//  DemoIPA
//
//  Created by 新致软件 on 15/8/28.
//  Copyright (c) 2015年 FBI01. All rights reserved.
//

#import <Cordova/CDV.h>
#import <Cordova/CDVPlugin.h>

#import "SignAPI.h"

@interface CPICCaSign : CDVPlugin
{
    
}
@property (nonatomic, strong) NSString *tid;
@property (nonatomic, strong) NSString *singleType;
@property (nonatomic, strong) NSString *mutiType;
@property (nonatomic, strong) NSString *singleTitle;
@property (nonatomic, strong) NSString *mutiTitle;

@property (nonatomic, strong) SignAPI *signAPI;

-(void)getImageCallback:(NSString*)encodedImageStr;

-(void)getEncrypt:(CDVInvokedUrlCommand *)command;

@end
