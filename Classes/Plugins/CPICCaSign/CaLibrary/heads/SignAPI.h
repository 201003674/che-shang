//
//  SignAPI.h
//  bjcaHandwriteLib
//
//  Created by pingwanhui on 15/10/26.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SignAPI : NSObject
//签名
- (void)initSignView;//初始化签名页面
//message:获得签名图片的通知;flag:设置签名cid#signTID
- (void)setSignMessage:(NSString *)message flag:(NSString *)value;
- (void)setSignHandWriteColor:(UIColor *)color;
- (void)setSignScale:(CGFloat) value;
//设置单签笔迹粗细
- (void)setStrokeWidth:(float)value;
- (void)setShowSignScreen :(CGRect)rect;
//设置新签名页面每行显示的字数(大于1)
- (void)setWordNumberForline:(int)number;
//设置签名图片大小
- (void)setSignImageSize:(CGSize)imageSize;
/** 删除签名图片 */
- (void)deleteSignImageForFlag:(NSString *)flag;

//171208添加智多星单板签名识别设置接口
- (void)setPhoneNameCheckWithAppkey:(NSString *)appkey withName:(NSString *)name withModifyCount:(int)count andCallBackMessage:(NSString *)message;

//设置签名提醒文字title：提醒文字；fromOffset：从第几个字开始凸显；toOffset：到第几个字凸显结束
- (void)setTopTitle:(NSString *)title titleSpanFromOffset:(int)fromOffset titleSpanToOffset:(int)toOffset;
- (UIView *)getSignView;//获得签名view
- (void)cancel;
//批注
- (void)initMutiSignView;//初始化批注页面

//？？？可能是指定批注的归属签名index
- (void)setMyId:(NSString *) idTmp;

- (void)setMutiSignHandWriteColor:(UIColor *)color;
//message:获得批注图片的通知;flag:设置批注cid
- (void)setMutiSignMessage:(NSString *) message flag:(NSString *) keyValue;
- (void)setMutiSignScale:(CGFloat) value;
- (void)setImageSize:(CGSize) mysize;
//设置批注笔迹粗细
- (void)setMutiStrokeWidth:(float)value;
- (void)initWithCommitment:(NSString*) value;

//171208添加智多星平板批注识别设置接口,count为0时，识别不做次数限制，大于0时限制识别次数，count为整数
- (void)setPadCharacterCheckWithAppkey:(NSString *)appkey withModifyCount:(NSUInteger)count andCallBackMessage:(NSString *)message;


//仅手机批注中调用，效果不详
- (void)setSignFrame:(CGRect)rect;
- (UIView *)getMutiSignView;//获得批注view
//拍照
- (void)initCameraView;
- (void)setCameraCallbackMessage:(NSString *)message;
- (UIView *)getCameraView;
//录音
- (void)initRecordView;
- (void)mystop;
- (void)setRecordCallbackMessage:(NSString *)message;
- (UIView *)getRecordView;
//打包
@property (nonatomic, assign) BOOL isNecessary;//是否校验签名
@property (nonatomic, strong) NSString * signTID;
//生成加密报文
- (NSString *)enclosureForServer:(NSMutableDictionary *)ServerData  mybase64:(NSMutableArray *)base64Arry errorinfo:(NSMutableString *)error;
//清除API
- (void)reset;
//缓存
//设置签名缓存 SessionId:工单号；symmetryKey:对称密钥；
- (void)setBusinessSessionId:(NSString *)sessionId symmetryKey:(NSString *)key;
//恢复签名缓存 SessionId:工单号；symmetryKey:对称密钥；
- (BOOL)restoreBusinessSessionId:(NSString *)sessionId symmetryKey:(NSString *)key;
//获得缓存图片（restoreBusinessSessionId恢复签名以后调用）dataType：签名cid#signTID;批注cid；
- (NSData *)getSignatureImageData:(NSString *)dataType;
//删除缓存内容 SessionId:工单号；
- (BOOL)deleteBusinessSessionId:(NSString *)sessionId;
- (BOOL)deleteBusinessContextId:(NSString *)contextId;
//是否存在缓存
- (BOOL)hasBufferedRecord:(NSString *)contextId;
//日志开关
+ (void)turnLogAPISwitch:(BOOL)on;
//HW识别统计开关
+ (void)turnLogHWWordSwitch:(BOOL)on;
//是否显示新的签名页面：YES为显示新的签名页面，NO为显示旧的页面，该方法需要在initSignView方法之前调用
-(void)isShowNewSignViewController:(BOOL)isShow;
//签名用户是否为法定监护人，YES为法定监护人
-(void)isLegalGuardian:(BOOL)isGuardian;
//设置签名人姓名，可用于不开启百度识别
-(void)setSignName:(NSString *)signName;
@end
