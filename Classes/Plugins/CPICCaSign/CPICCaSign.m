//
//  MBjca.m
//  DemoIPA
//
//  Created by 新致软件 on 15/8/28.
//  Copyright (c) 2015年 FBI01. All rights reserved.
//

#import "CPICCaSign.h"

@implementation CPICCaSign

CDVInvokedUrlCommand* myCommand;

#pragma mark -
#pragma mark -初始化
-(void)initCA:(CDVInvokedUrlCommand*)command
{
    myCommand = command;

    NSString* title = [myCommand argumentAtIndex:0];

    NSArray *titleAry = [title componentsSeparatedByString:@","];

    if (titleAry.count < 2)
    {
        _singleTitle = [titleAry objectAtIndex:0];
        NSLog(@"###单字Title:%@",_singleTitle);
    }
    else
    {
        _singleTitle = [titleAry objectAtIndex:0];
        NSLog(@"###单字Title:%@",_singleTitle);
        _mutiTitle = [titleAry objectAtIndex:1];
        NSLog(@"###多字Title:%@",_mutiTitle);
    }

    #ifdef DEBUG
    
    #else
    
    self.signAPI = [[SignAPI alloc]init];
    [self.signAPI setBusinessSessionId:@"12345678" symmetryKey:@"abcde"];
    
    #endif
    
    NSString *type = [NSString stringWithFormat:@"%@",[myCommand argumentAtIndex:3]];
    STRING_NIL_TO_NONE(type);

    NSArray *typeAry = [NSArray arrayWithArray:[type componentsSeparatedByString:@","]];

    if (typeAry.count < 2)
    {
        self.singleType = [NSString stringWithFormat:@"%@",[typeAry objectAtIndex:0]];
        STRING_NIL_TO_NONE(self.singleType);
    }
    else
    {
        self.singleType = [NSString stringWithFormat:@"%@",[typeAry objectAtIndex:0]];
        STRING_NIL_TO_NONE(self.singleType);


        self.mutiType = [NSString stringWithFormat:@"%@",[typeAry objectAtIndex:1]];
        STRING_NIL_TO_NONE(self.mutiType);
    }


    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getimage:) name:@"mypic1" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getimage:) name:@"MUTIIMAGE" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signCallBack:) name:@"signZDXCallBack" object:nil];
}

#pragma mark -
#pragma mark -调用数字签名界面
-(void)sign:(CDVInvokedUrlCommand *)command
{
    myCommand = command;

    NSString *type = [NSString stringWithFormat:@"%@",[myCommand argumentAtIndex:3]];
    STRING_NIL_TO_NONE(type);

    if ([[myCommand arguments] count]>=5)
    {
        self.tid = [NSString stringWithFormat:@"%@",[myCommand argumentAtIndex:4]];
        STRING_NIL_TO_NONE(self.tid);
    }

    int typInt = [type intValue];

    if ((typInt >=20) &&(typInt <30))
    {
        [self singleSign:command];
    }
    else if ((typInt >=30) &&(typInt <40))
    {
        [self mutiSign:command];
    }
}

#pragma mark -
#pragma mark -获取加密数据
-(void)getEncrypt:(CDVInvokedUrlCommand *)command
{
    myCommand = command;
    
    #ifdef DEBUG

    #else
    
    NSString *formData = [NSString stringWithFormat:@"%@",[myCommand argumentAtIndex:0]];
    STRING_ISNOTNIL(formData);
    
    NSString *signer = [NSString stringWithFormat:@"%@",[myCommand argumentAtIndex:1]];
    STRING_ISNOTNIL(signer);
    
    NSString *channelid = [NSString stringWithFormat:@"%@",[myCommand argumentAtIndex:2]];
    STRING_ISNOTNIL(channelid);
    
    NSString *tid = [NSString stringWithFormat:@"%@",[myCommand argumentAtIndex:3 withDefault:@"3040"]];
    STRING_ISNOTNIL(tid);
    
    NSLog(@"formData:%@ signer:%@,channelid:%@,tid:%@",formData,signer,channelid,tid);
    
    self.signAPI.signTID = tid;
    
    NSMutableString *myerror = [[NSMutableString alloc] init] ;
    NSMutableDictionary *serverData = [[NSMutableDictionary alloc] init];
    
    NSMutableArray *baseFlag = [[NSMutableArray alloc] init];
    
//    [baseFlag addObject:@"13"];
//    [baseFlag addObject:@"14"];
//    [baseFlag addObject:@"16"];
    
    [serverData setObject:formData forKey:@"13"];
    [serverData setObject:signer forKey:@"14"];
    [serverData setObject:channelid forKey:@"16"];
    
    NSLog(@"##############%@##############",serverData.description);
    
    NSString *enclosureForServerString = [NSString stringWithFormat:@"%@",[self.signAPI enclosureForServer:serverData mybase64:baseFlag errorinfo:myerror]];
    STRING_NIL_TO_NONE(enclosureForServerString);
    
    NSLog(@"##############获取数字签名加密数据:%@,异常数据:%@ ##############",enclosureForServerString,myerror);
    
    [self sendResultToJs:enclosureForServerString key:@"data"];
    
    #endif
}


//显示ca签名框
-(void)singleSign:(CDVInvokedUrlCommand*)command
{
    #ifdef DEBUG

    #else
    NSString *isShowNewSignView = [NSString stringWithFormat:@"%@",[myCommand argumentAtIndex:5]];
    STRING_ISNOTNIL(isShowNewSignView);
    
    if ([isShowNewSignView isEqualToString:@"1"])
    {
        //是否显示新的签名页面：YES为显示新的签名页面，NO为显示旧的页面，该方法需要在initSignView方法之前调用
        [self.signAPI isShowNewSignViewController:YES];
    }
    
    //签名调用修改
    [self.signAPI initSignView];
    
    if ([isShowNewSignView isEqualToString:@"1"])
    {
        [self.signAPI setSignImageSize:CGSizeMake(160, 60)];
        [self.signAPI setSignScale:1.0];
    }
    else
    {
        float iscale = (1.0/4.0);
        [self.signAPI setSignScale:iscale];
    }
    
    NSString *titleString = [NSString stringWithFormat:@"请按本文字方向签上您的姓名。"];
    STRING_NIL_TO_NONE(titleString);
    [self.signAPI setTopTitle:titleString titleSpanFromOffset:0 titleSpanToOffset:(int)titleString.length -1];
    
    [self.signAPI setSignMessage:@"mypic1" flag:[NSString stringWithFormat:@"%@#%@",_singleType,_tid]];
    [self.signAPI setSignHandWriteColor:[UIColor blackColor]];
    
    [self.signAPI setStrokeWidth:6.0];
    [self.signAPI setWordNumberForline:6];
    
    NSString *signName = [NSString stringWithFormat:@"%@",[myCommand argumentAtIndex:6]];
    STRING_NIL_TO_NONE(signName);

    //1207新增开启签名识别接口，不开启则注释此行代码。
    //tip:确保项目中导入了hwsdk.bundle并确认项目bundleID已向百度申请了权限
    NSString *baiDuCaAppKey = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getBaiDuCaAppKey]];
    STRING_NIL_TO_NONE(baiDuCaAppKey)
    
    //签名姓名不为空，切开启新签名画板
    if (STRING_ISNOTNIL(signName) && [isShowNewSignView isEqualToString:@"1"])
    {
        NSString *modifyCount = [NSString stringWithFormat:@"%@",[myCommand argumentAtIndex:7 withDefault:@"10"]];
        STRING_ISNOTNIL(modifyCount);
        
        [self.signAPI setPhoneNameCheckWithAppkey:baiDuCaAppKey withName:signName withModifyCount:modifyCount.intValue andCallBackMessage:@"signZDXCallBack"];
    }
    
    UIViewController *rootViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    [rootViewController.view addSubview:[self.signAPI getSignView]];
    
    #endif
}

- (void)mutiSign:(CDVInvokedUrlCommand*)command
{
    #ifdef DEBUG

    #else
    
    [self.signAPI initMutiSignView];
    [self.signAPI setMutiSignMessage:@"MUTIIMAGE" flag:[NSString stringWithFormat:@"%@#%@",_mutiType,_tid]];
    [self.signAPI initWithCommitment:self.mutiTitle];
    [self.signAPI setImageSize:CGSizeMake(300, 136)];
    float x=(1.0/4.0);
    [self.signAPI setMutiSignScale:x];
    
    [[[[UIApplication sharedApplication] keyWindow] rootViewController].view addSubview:[self.signAPI getSignView]];
    
    #endif
}

#pragma mark -
#pragma mark -接收签名后的数据回传
-(void)getImageCallback:(NSString*)encodedImageStr
{
    [self sendResultToJs:encodedImageStr key:@"base64Img"];
}

#pragma mark -
#pragma mark -CA签名后返回的图片文件，通知回调方法
- (void)getimage:(NSNotification *)noti
{
    NSDictionary *info = [NSDictionary dictionaryWithDictionary:[noti userInfo]];

    UIImage *SignPicture = [info objectForKey:@"myimage"];

    NSData *data = UIImagePNGRepresentation(SignPicture);

    NSString *encodedImageStr = [NSString stringWithFormat:@"%@",[data base64EncodedStringWithOptions:0]];
    STRING_ISNOTNIL(encodedImageStr);

    [self sendResultToJs:encodedImageStr key:@"base64Img"];
}

#pragma mark -
#pragma mark - 智多星识别回调
- (void)signCallBack:(NSNotification *)noti
{
    NSDictionary *info = [NSDictionary dictionaryWithDictionary:[noti object]];
    
    NSString *result = [NSString stringWithFormat:@"%@",[info objectForKey:@"result"]];
    STRING_NIL_TO_NONE(result);
    
    if ([result isEqualToString:@"1"])
    {
        NSLog(@"智多星识别成功!");
    }
    else
    {
        NSLog(@"智多星识别失败!");
    }
}

#pragma mark -
#pragma mark - 将数据返回给JS端
-(void)sendResultToJs:(NSString*)retval key:(NSString*) key
{
    NSDictionary *dic =[NSDictionary dictionaryWithObjectsAndKeys:retval,key, nil];

    CDVPluginResult *result = [CDVPluginResult new];

    if (ISNOTNILDIC(dic))
    {
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dic];
    }
    else
    {
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }

    if (result)
    {
        [self.commandDelegate sendPluginResult:result callbackId:myCommand.callbackId];
    }
}

#pragma mark -
#pragma mark -


@end
