//
//  CDOcrRecognition.h
//  IOSFramework
//
//  Created by 林科 on 2017/10/13.
//  Copyright © 2017年 allianture. All rights reserved.
//

//身份证
#import <ISIDReaderSDK/ISIDReaderSDK.h>
#import <ISIDReaderPreviewSDK/ISIDReaderPreviewSDK.h>
#import <ISOpenSDKFoundation/ISOpenSDKFoundation.h>
//增强识别
#import <ISImageProcessSDK/ISImageProcessSDK.h>

#import <Cordova/CDVPlugin.h>

@interface CDOcrRecognition : CDVPlugin<UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,ISOpenSDKCameraViewControllerDelegate,ISCropImageViewControllerDelegate>
{
    NSString *ocrFlag;
    
    NSInteger totalMount;
    
    NSInteger ocrTotalMount;
}
@property(nonatomic, strong) CDVInvokedUrlCommand *urlCommand;

//OCR 识别
- (void)ocrRecognition:(CDVInvokedUrlCommand *)command;

- (void)ocrNewRecognition:(CDVInvokedUrlCommand *)command;

@end
