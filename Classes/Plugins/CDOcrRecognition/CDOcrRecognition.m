//
//  CDOcrRecognition.m
//  IOSFramework
//
//  Created by 林科 on 2017/10/13.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import "CDOcrRecognition.h"

#define APP_KEY @"3rUCQ58dBd5BgrDaTR6E7Qe8"

@implementation CDOcrRecognition

- (void)ocrRecognition:(CDVInvokedUrlCommand *)command
{
    totalMount = 0;
    
    ocrTotalMount = 0;
    
    //OCR识别信息 1.png,2.png,3.png
    NSString *str = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(str);
    
    NSArray *imageArr = [NSArray arrayWithArray:[str componentsSeparatedByString:@","]];
    
    //初始化返回数据
    NSMutableArray *mutableArray = [NSMutableArray array];
    
    ocrFlag = @"YES";
    
    ocrTotalMount = imageArr.count;
    
    [[[ClassFactory getInstance] getNetComm] showHud:nil];
    
    if (ocrTotalMount == 0)
    {
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                     messageAsArray:mutableArray];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
        
        return;
    }
    
    for (NSInteger index = 0 ; index < imageArr.count ; index ++ )
    {
        [mutableArray addObject:[NSDictionary dictionary]];
        
        NSString *imageName = [NSString stringWithFormat:@"%@",[imageArr objectAtIndex:index]];
        STRING_NIL_TO_NONE(imageName);
        
        [self ocrRecognition:command WithImageName:imageName WithIndex:index success:^(id JSON,NSInteger i) {
            
            NSDictionary *dic = [[NSDictionary alloc] initWithDictionary:[[JSON mj_JSONString] objectFromJSONString]];
            
            [mutableArray replaceObjectAtIndex:i withObject:dic];
            
            ocrFlag = @"YES";
            
            totalMount ++ ;
            
            [self returnOcrRecognitionMessage:command withReturnArr:mutableArray];
            
        } failure:^(NSError *error) {
            
            ocrFlag = @"NO";
            
            totalMount ++ ;
            
            [self returnOcrRecognitionMessage:command withReturnArr:mutableArray];
            
        }];
    }
}

#pragma mark -
#pragma mark - OCR识别
- (void)ocrRecognition:(CDVInvokedUrlCommand *)command WithImageName:(NSString *)imageName WithIndex:(NSInteger)index success:(void (^)(id JSON,NSInteger i))success
               failure:(void (^)(NSError *error))failure
{
    //ocr请求url
    NSString *ocrUrl = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(ocrUrl);
    
    //识别图片名
    NSString *idNumber = [NSString stringWithFormat:@"%@",imageName];
    STRING_NIL_TO_NONE(idNumber);
    
    AFSessionManager *manager = [AFSessionManager manager];
    
    //发送格式
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    //接受格式
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    //进程标签
    manager.targetOnlyStr = idNumber;
    
    [manager POST:ocrUrl parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        /*
         此方法参数
         1. 要上传的[二进制数据]
         2. 对应网站上[upload.php中]处理文件的[字段"file"]
         3. 要保存在服务器上的[文件名]
         4. 上传文件的[mimeType]
         
         */
        if([Check checkUrl:idNumber])
        {
            //URL 获取服务器图片
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:idNumber]];
            
            [formData appendPartWithFileData:imageData name:@"file" fileName:idNumber mimeType:@"image/jpeg"];
        }
        else
        {
            //本地 获取本地缓存图片
            UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:idNumber];
            
            NSData *imageData = UIImageJPEGRepresentation(image, 1);
            
            [formData appendPartWithFileData:imageData name:@"file" fileName:idNumber mimeType:@"image/jpeg"];
        }
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        // 配置上传 百分比
        float percentDone = uploadProgress.completedUnitCount/uploadProgress.totalUnitCount;
        
        NSLog(@"OCR百分比 ====================%f",percentDone);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success)         {
            success(responseObject,index);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure)
        {
            failure(error);
        }
        
        NSString *errorInfo = [NSString stringWithFormat:@"%@",error.localizedDescription];
        STRING_NIL_TO_NONE(errorInfo);
        
        [[[ClassFactory getInstance] getInfoHUD] showHud:errorInfo];
    }];
}

#pragma mark -
#pragma mark - returnOcrRecognitionMessage
- (void)returnOcrRecognitionMessage:(CDVInvokedUrlCommand *)command withReturnArr:(NSArray *)mutArray
{
    if (totalMount != ocrTotalMount)
    {
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
        
        return;
    }
    
    //识别回调信息
    STRING_NIL_TO_NONE(ocrFlag);
    if ([ocrFlag isEqualToString:@"YES"])
    {
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                     messageAsArray:mutArray];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
    else
    {
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                     messageAsArray:mutArray];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
    
    [[[ClassFactory getInstance] getNetComm] hiddenHud];
}

#pragma mark -
#pragma mark -
- (void)ocrNewRecognition:(CDVInvokedUrlCommand *)command
{
    UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                                        initWithTitle:nil
                                        delegate:self
                                        cancelButtonTitle:@"取消"
                                        destructiveButtonTitle:nil
                                        otherButtonTitles:@"预览识别",@"拍照识别",nil];
    actionSheet.command = command;
    self.urlCommand = command;
    
    [actionSheet showInView:self.viewController.view];
}

#pragma mark -
#pragma mark - UICustomActionSheetDelegate
-(void)actionSheet:(UICustomActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex)
    {
        //图片标记
        NSString *imageIndex = [NSString stringWithFormat:@"%@",[actionSheet.command argumentAtIndex:3]];
        STRING_NIL_TO_NONE(imageIndex);
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"用户取消操作",@"message",imageIndex,@"imageIndex", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
        
        return;
    }
    
    UICustomImagePickerController *imagePicker=[[UICustomImagePickerController alloc]init];
    imagePicker.view.backgroundColor = [UIColor whiteColor];
    
    BOOL isCameraSupport = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    
    isCameraSupport = YES;
    
    switch (buttonIndex)
    {
        case 0:
        {
            //预览识别
            if (!isCameraSupport)
            {
                [[[ClassFactory getInstance]getInfoHUD]showHud:@"您的设备不支持此功能！"];
                
                NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"您的设备不支持此功能！",@"message", nil];
                
                CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                        messageAsDictionary:theMessage];
                // send cordova result
                [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
                
                return;
            }
            else
            {
                ISOpenSDKCameraViewController *sdkCameraView = [[ISOpenSDKCameraViewController alloc] init];
                //身份证
                sdkCameraView = [[ISIDCardReaderController sharedISOpenSDKController] cameraViewControllerWithAppkey:APP_KEY subAppkey:nil needCompleteness:YES];
                [sdkCameraView setCustomInfo:@"请将身份证放在框内识别"];
                
                [sdkCameraView setDelegate:self];
                [sdkCameraView setDebugMode:YES];
                [sdkCameraView setShouldHightlightCorners:YES];
                [self.viewController presentViewController:sdkCameraView animated:YES completion:nil];
                
                break;
            }
        }
        case 1:
        {
            //拍照识别
            if(!isCameraSupport)
            {
                [[[ClassFactory getInstance] getInfoHUD] showHud:@"您的设备不支持此功能！"];
                
                NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"您的设备不支持此功能！",@"message", nil];
                
                CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                        messageAsDictionary:theMessage];
                // send cordova result
                [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
                
                return;
            }
            else
            {
                imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                
                break;
            }
        }
        default:
            
            break;
    }
    
    //旧模式 OCR
    NSString *requiredMediaType = ( NSString *)kUTTypeImage;
    NSArray *arrMediaTypes = [NSArray arrayWithObjects:requiredMediaType,nil];
    [imagePicker setMediaTypes:arrMediaTypes];
    [imagePicker setDelegate:self];
    [imagePicker setCommand:actionSheet.command];
    [self.viewController presentViewController:imagePicker animated:YES completion:nil];
}

#pragma mark -
#pragma mark - ISOpenSDKCameraViewControllerDelegate
//相机模块初始化SDK回调
- (void)constructResourcesDidFinishedWithStatusCode:(ISOpenSDKStatus)status
{
    if (status != ISOpenSDKStatusSuccess)
    {
        NSString *message = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLogic] getOpenSDKStatusMessage:status]];
        STRING_NIL_TO_NONE(message)
        
        NSLog(@"相机模块初始化 Auth failed:%ld  %@",(long)status,message);
    }
}
//相机模块授权失败SDK回调
- (void)accessCameraDidFailed
{
    NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",@"相机模块授权失败!",@"message", nil];
    
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                            messageAsDictionary:theMessage];
    
    // send cordova result
    [self.commandDelegate sendPluginResult:result callbackId:self.urlCommand.callbackId];
}
//相机模块边缘检测回调
- (void)cameraViewController:(UIViewController *)viewController didFinishDetectCardWithResult:(int)result borderPoints:(NSArray *)borderPoints
{
    //
}
//相机模块识别结果回调
- (void)cameraViewController:(UIViewController *)viewController didFinishRecognizeCard:(NSDictionary *)resultInfo cardSDKType:(ISOpenPreviewSDKType)sdkType
{
    if(sdkType == ISOpenPreviewSDKTypeIDReader)
    {
        //身份证 原图
        UIImage *originImage = [resultInfo objectForKey:kOpenSDKCardResultTypeOriginImage];
        
        NSString *imageName = [NSString stringWithFormat:@"%@.png",[[[ClassFactory getInstance] getLogic] getImageName]];
        STRING_NIL_TO_NONE(imageName);
        
        //任务申请号
        NSString *requestNo = [NSString stringWithFormat:@"%@",[self.urlCommand argumentAtIndex:1]];
        STRING_NIL_TO_NONE(requestNo);
        
        if (STRING_ISNIL(requestNo))
        {
            NSDictionary *dic = [NSDictionary dictionaryWithDictionary:[self showResultWithAttributes:resultInfo CardImage:originImage WithImageName:imageName WithEntrance:0]];
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsDictionary:dic];
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:self.urlCommand.callbackId];
        }
        else
        {
            //图片上传
            [self unlpoadImageWithCommand:self.urlCommand withImage:originImage WithNameStr:imageName withSuccess:^(id JSON) {
                
                NSDictionary *requestResultDic = [[NSDictionary alloc] initWithDictionary:JSON];
                NSString *resultCode = [requestResultDic objectForKey:@"resultCode"];
                
                if ([resultCode isEqualToString:@"1"])
                {
                    NSDictionary *dic = [NSDictionary dictionaryWithDictionary:[self showResultWithAttributes:resultInfo CardImage:originImage WithImageName:imageName WithEntrance:0]];
                    
                    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                            messageAsDictionary:dic];
                    
                    // send cordova result
                    [self.commandDelegate sendPluginResult:result callbackId:self.urlCommand.callbackId];
                }
                else
                {
                    //图片标记
                    NSString *imageIndex = [NSString stringWithFormat:@"%@",[self.urlCommand argumentAtIndex:3]];
                    STRING_NIL_TO_NONE(imageIndex);
                    
                    NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",@"识别失败!",@"message",imageIndex,@"imageIndex", nil];
                    
                    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                            messageAsDictionary:theMessage];
                    
                    // send cordova result
                    [self.commandDelegate sendPluginResult:result callbackId:self.urlCommand.callbackId];                }
                
                
            } withFail:^(NSError *error) {
                
                //图片标记
                NSString *imageIndex = [NSString stringWithFormat:@"%@",[self.urlCommand argumentAtIndex:3]];
                STRING_NIL_TO_NONE(imageIndex);
                
                NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",@"识别失败!",@"message",imageIndex,@"imageIndex", nil];
                
                CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                        messageAsDictionary:theMessage];
                
                // send cordova result
                [self.commandDelegate sendPluginResult:result callbackId:self.urlCommand.callbackId];
                
            }];
        }
    }
    else
    {
        //图片标记
        NSString *imageIndex = [NSString stringWithFormat:@"%@",[self.urlCommand argumentAtIndex:3]];
        STRING_NIL_TO_NONE(imageIndex);
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",@"识别失败!",@"message",imageIndex,@"imageIndex", nil];
        
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:self.urlCommand.callbackId];
    }
    
    [viewController dismissViewControllerAnimated:YES completion:nil];
}
//相机模块返回按钮点击回调
- (void)cameraViewController:(UIViewController *)viewController didClickCancelButton:(id)sender
{
    //图片标记
    NSString *imageIndex = [NSString stringWithFormat:@"%@",[self.urlCommand argumentAtIndex:3]];
    STRING_NIL_TO_NONE(imageIndex);
    
    NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"用户取消!",@"message",imageIndex,@"imageIndex", nil];
    
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                            messageAsDictionary:theMessage];
    
    // send cordova result
    [self.commandDelegate sendPluginResult:result callbackId:self.urlCommand.callbackId];
    
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark - unlpoadImage
- (void)unlpoadImageWithCommand:(CDVInvokedUrlCommand*)command withImage:(UIImage *)originImage WithNameStr:(NSString *)userNameStr withSuccess:(void (^)(id JSON))success withFail:(void (^)(NSError *error))fail
{
    NSData *imageData = UIImageJPEGRepresentation(originImage, 1);
    
    //进行图片像素限制压缩 320万
    if (originImage.size.width * originImage.size.height > MAX_PIXEL)
    {
        UIImage *finialImage = [UIImage compressImage:originImage toPixel:MAX_PIXEL];
        
        imageData = UIImageJPEGRepresentation(finialImage, 1.0);
    }
    else
    {
        imageData = UIImageJPEGRepresentation(originImage, 1.0);
    }
    
    NSString *imageSize = [NSString stringWithFormat:@"%ld",(long)imageData.length];
    STRING_NIL_TO_NONE(imageSize)

    //任务申请号
    NSString *requestNo = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(requestNo);
    
    //上传服务器url
    NSString *url = [NSString stringWithFormat:@"%@",[command argumentAtIndex:4]];
    STRING_NIL_TO_NONE(url);
    
    //图片类型（对应影像系统ID）
    NSString *imageType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
    STRING_NIL_TO_NONE(imageType);
    
    //图片标记
    NSString *imageIndex = [NSString stringWithFormat:@"%@",[command argumentAtIndex:3]];
    STRING_NIL_TO_NONE(imageIndex);
    
    //创建时间
    NSString *createDate = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLogic] getNowTimeString:@"YYYY-MM-dd HH:mm:ss"]];
    STRING_NIL_TO_NONE(createDate)
    
    NSDictionary *uploadRequest = [NSDictionary dictionaryWithObjectsAndKeys:imageSize,@"imageData",createDate,@"createDate",requestNo,@"requestNo",url,@"url",nil];
    
    [self unlpoadImageWithFileName:userNameStr withFileData:imageData withCommand:command parameters:uploadRequest withSuccess:^(id JSON) {
        
        NSDictionary *requestResultDic = [[NSDictionary alloc] initWithDictionary:JSON];
        NSString *resultCode = [requestResultDic objectForKey:@"resultCode"];
        
        if ([resultCode isEqualToString:@"1"])
        {
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:userNameStr,@"imageName",imageType,@"imageType",requestNo,@"requestNo",imageIndex,@"imageIndex", nil];
            
            BOOL isSuccess = [[[ClassFactory getInstance] getLogic] addImageToMutArr:dic WithIndex:imageIndex];
            
            if (isSuccess)
            {
                NSLog(@"文件%@添加临时提交数组成功!",userNameStr);
            }
            else
            {
                NSLog(@"文件%@添加临时提交数组失败!",userNameStr);
            }
            
            //写入数据库
            FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
            
            [fmdbSql insertIntoTableName:UPLOAD_TABLE_NAME Dict:dic complete:^(BOOL isSuccess) {
                
                if (isSuccess)
                {
                    NSLog(@"文件%@写入数据库成功!",userNameStr);
                }
                else
                {
                    NSLog(@"文件%@写入数据库失败!",userNameStr);
                }
                
            }];
            
            [[SDImageCache sharedImageCache] storeImage:originImage forKey:userNameStr toDisk:YES completion:^{
                
            }];
            
        }
        else
        {
            
        }
        
        if (success)
        {
            success(JSON);
        }
        
    } withFail:^(NSError *error) {
        
        if (fail)
        {
            fail(error);
        }
    }];
}

- (void)unlpoadImageWithFileName:(NSString *)tempFileName withFileData:(NSData *)fileData withCommand:(CDVInvokedUrlCommand*)command parameters:(NSDictionary *)requestobjDic withSuccess:(void (^)(id JSON))success withFail:(void (^)(NSError *error))fail
{
    [[[ClassFactory getInstance] getFileUpLoadAFRequestQueue] addFileUpOperationWithFileName:tempFileName parameters:requestobjDic withFileData:fileData withOnlyTarget:tempFileName withSuccess:^(id JSON) {
        
        if (success)
        {
            success(JSON);
        }
        
    } withFail:^(NSError *error) {
        
        if (fail)
        {
            fail(error);
        }
        
    } progessDataBlock:^(float percentDone) {
        
        
    }];
}

#pragma mark -
#pragma mark - UIImagePickerControllerDelegate
-(void)imagePickerController:(UICustomImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [[[ClassFactory getInstance] getNetComm] showHud:nil];
    [picker dismissViewControllerAnimated:YES completion:^{
        
        //通过UIImagePickerControllerMediaType判断返回的是照片还是视频
        NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        //如果返回的type等于kUTTypeImage，代表返回的是照片,并且需要判断当前相机使用的sourcetype是拍照还是相册
        if ([mediaType isEqualToString:( NSString *)kUTTypeImage])
        {
            UIImage *theImage = nil;
            // 判断，图片是否允许修改
            if ([picker allowsEditing])
            {
                //获取用户编辑之后的图像
                theImage = [info objectForKey:UIImagePickerControllerEditedImage];
            }
            else
            {
                // 照片的元数据参数
                theImage = [info objectForKey:UIImagePickerControllerOriginalImage];
            }
            NSString *imageName = [NSString stringWithFormat:@"%@.png",[[[ClassFactory getInstance] getLogic] getImageName]];
            STRING_NIL_TO_NONE(imageName);
            
            //识别OCR信息
            [[[ClassFactory getInstance] getNetComm] showHud:nil];
            [self ocrRecognition:picker.command WithImage:theImage WithImageName:imageName success:^(id JSON) {
                
                NSDictionary *dic = [[NSDictionary alloc] initWithDictionary:[[JSON mj_JSONString] objectFromJSONString]];
                
                NSString *resultCode = [NSString stringWithFormat:@"%@",[dic objectForKey:@"resultCode"]];
                STRING_NIL_TO_NONE(resultCode);
                
                if ([resultCode isEqualToString:@"1"])
                {
                    NSString *certResponse = [NSString stringWithFormat:@"%@",[[dic objectForKey:@"certResponse"] mj_JSONString]];
                    STRING_NIL_TO_NONE(certResponse);
                    
                    NSDictionary *finialDic = [NSDictionary dictionaryWithDictionary:[certResponse objectFromJSONString]];
                    
                    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                            messageAsDictionary:[self showResultWithAttributes:finialDic CardImage:theImage WithImageName:imageName WithEntrance:1]];
                    // send cordova result
                    [self.commandDelegate sendPluginResult:result callbackId:picker.command.callbackId];
                    
                    //任务申请号
                    NSString *requestNo = [NSString stringWithFormat:@"%@",[self.urlCommand argumentAtIndex:1]];
                    STRING_NIL_TO_NONE(requestNo);
                    
                    if (STRING_ISNOTNIL(requestNo))
                    {
                        //图片类型
                        NSString *imageType = [NSString stringWithFormat:@"%@",[self.urlCommand argumentAtIndex:2]];
                        STRING_NIL_TO_NONE(imageType);
                        //图片类型
                        NSString *imageIndex = [NSString stringWithFormat:@"%@",[self.urlCommand argumentAtIndex:3]];
                        STRING_NIL_TO_NONE(imageIndex);
                        
                        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:imageName,@"imageName",imageType,@"imageType",requestNo,@"requestNo",imageIndex,@"imageIndex", nil];
                        
                        BOOL isSuccess = [[[ClassFactory getInstance] getLogic] addImageToMutArr:dic WithIndex:imageIndex];
                        
                        if (isSuccess)
                        {
                            NSLog(@"文件%@添加临时提交数组成功!",imageName);
                        }
                        else
                        {
                            NSLog(@"文件%@添加临时提交数组失败!",imageName);
                        }
                        
                        [[SDImageCache sharedImageCache] storeImage:theImage forKey:imageName toDisk:YES completion:^{
                            
                        }];
                        
                        //写入数据库
                        FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
                        
                        [fmdbSql insertIntoTableName:UPLOAD_TABLE_NAME Dict:dic complete:^(BOOL isSuccess) {
                            
                            if (isSuccess)
                            {
                                NSLog(@"文件%@写入数据库成功!",imageName);
                            }
                            else
                            {
                                NSLog(@"文件%@写入数据库失败!",imageName);
                            }
                            
                        }];
                    }
                }
                else
                {
                    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                            messageAsDictionary:dic];
                    // send cordova result
                    [self.commandDelegate sendPluginResult:result callbackId:picker.command.callbackId];
                }
                
                [[[ClassFactory getInstance] getNetComm] hiddenHud];
    
            } failure:^(NSError *error) {
                
                NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",error.localizedDescription,@"message", nil];
                
                CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                        messageAsDictionary:theMessage];
                
                // send cordova result
                [self.commandDelegate sendPluginResult:result callbackId:picker.command.callbackId];
                
                [[[ClassFactory getInstance] getNetComm] hiddenHud];
            }];
        }
        else
        {
            
        }
    }];
}

-(void)imagePickerControllerDidCancel:(UICustomImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
        //图片标记
        NSString *imageIndex = [NSString stringWithFormat:@"%@",[picker.command argumentAtIndex:3]];
        STRING_NIL_TO_NONE(imageIndex);
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"用户取消操作",@"message",imageIndex,@"imageIndex", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:picker.command.callbackId];
        
    }];
}

#pragma mark -
#pragma mark - OCR识别
- (void)ocrRecognition:(CDVInvokedUrlCommand *)command WithImage:(UIImage *)image WithImageName:(NSString *)imageName success:(void (^)(id JSON))success failure:(void (^)(NSError *error))failure
{
    //ocr请求url
    NSString *ocrUrl = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(ocrUrl);
    
    //任务申请号
    NSString *requestNo = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(requestNo);
    
    AFSessionManager *manager = [AFSessionManager manager];
    
    //发送格式
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    //接受格式
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", @"text/plain",nil];
    
    //进程标签
    manager.targetOnlyStr = requestNo;
    
    NSDictionary *requestObject = [NSDictionary dictionaryWithObjectsAndKeys:
                                   requestNo,@"requestNo",
                                   imageName,@"imageName",
                                   nil];
    
    NSString *platType = [[[ClassFactory getInstance] getLocalCfg] getPlatType];
    STRING_NIL_TO_NONE(platType);
    NSString *version = [[[ClassFactory getInstance] getLocalCfg] getVersionCode];
    STRING_NIL_TO_NONE(version);
    NSString *projectType = [[[ClassFactory getInstance] getLocalCfg] getProjectType];
    STRING_NIL_TO_NONE(projectType);
    NSString *userToken = [[[ClassFactory getInstance] getLocalCfg] getUserToken];
    STRING_NIL_TO_NONE(userToken);
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                projectType,@"projectType",
                                platType,@"platType",
                                version,@"version",
                                userToken,@"userToken",
                                requestObject,@"requestObject",
                                nil];
    
    NSString *urlstring = [NSString stringWithFormat:@"%@?requestMessage=%@",ocrUrl,[parameters JSONString]];
    STRING_NIL_TO_NONE(urlstring);

    urlstring = [urlstring stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [manager POST:urlstring parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        /*
         此方法参数
         1. 要上传的[二进制数据]
         2. 对应网站上[upload.php中]处理文件的[字段"file"]
         3. 要保存在服务器上的[文件名]
         4. 上传文件的[mimeType]
         
         */
        NSData *imageData = UIImageJPEGRepresentation(image, 1);
        
        //进行图片像素限制压缩 320万
        if (image.size.width * image.size.height > MAX_PIXEL)
        {
            UIImage *finialImage = [UIImage compressImage:image toPixel:MAX_PIXEL];
            
            imageData = UIImageJPEGRepresentation(finialImage, 1.0);
        }
        else
        {
            imageData = UIImageJPEGRepresentation(image, 1.0);
        }
        
        [formData appendPartWithFileData:imageData name:@"file" fileName:imageName mimeType:@"image/jpeg"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        // 配置上传 百分比
        float percentDone = uploadProgress.completedUnitCount/uploadProgress.totalUnitCount;
        
        NSLog(@"OCR百分比 ====================%f",percentDone);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success){
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure)
        {
            failure(error);
        }
        
        NSString *errorInfo = [NSString stringWithFormat:@"%@",error.localizedDescription];
        STRING_NIL_TO_NONE(errorInfo);
        
        [[[ClassFactory getInstance] getInfoHUD] showHud:errorInfo];
    }];
}

#pragma mark -
#pragma mark - 解析OCR SDK识别结果信息
- (NSDictionary *)showResultWithAttributes:(NSDictionary *)attributes CardImage:(UIImage *)image WithImageName:(NSString *)imageName WithEntrance:(NSInteger)type
{
    //服务器识别
    NSMutableDictionary *resultMutDic = [NSMutableDictionary dictionary];
    
    if (type == 1)
    {
        //服务器识别
        NSArray *cardArrString = [NSArray arrayWithObjects:@"name",
                                  @"sex",
                                  @"id_number",
                                  @"address",
                                  @"people",
                                  @"birthday",
                                  @"issue_authority",
                                  @"validity",
                                  nil];
        
        for (NSInteger i = 0 ; i < cardArrString.count ; i ++ )
        {
            NSString *key = [NSString stringWithFormat:@"%@",[cardArrString objectAtIndex:i]];
            STRING_NIL_TO_NONE(key);
            
            NSString *keyValue = [NSString stringWithFormat:@"%@",[attributes objectForKey:key]];
            STRING_NIL_TO_NONE(keyValue);
            
            [resultMutDic setObject:keyValue forKey:key];
        }
    }
    else
    {
        NSMutableDictionary *mutableAttributes = [NSMutableDictionary dictionaryWithDictionary:attributes];
        
        NSDictionary *itemDict = [mutableAttributes objectForKey:kOpenSDKCardResultTypeCardItemInfo];
        /******************************** 身份证 ********************************/
        NSArray *cardArr = [NSArray arrayWithObjects:kCardItemName,
                            kCardItemGender,
                            kCardItemIDNumber,
                            kCardItemAddress,
                            kCardItemNation,
                            kCardItemBirthday,
                            kCardItemIssueAuthority,
                            kCardItemValidity, nil];
        
        
        
        NSArray *cardArrString = [NSArray arrayWithObjects:@"name",
                                  @"sex",
                                  @"id_number",
                                  @"address",
                                  @"people",
                                  @"birthday",
                                  @"issue_authority",
                                  @"validity",
                                  nil];
        
        for (NSInteger i = 0 ; i < cardArr.count ; i ++ )
        {
            NSString *key = [NSString stringWithFormat:@"%@",[cardArr objectAtIndex:i]];
            STRING_NIL_TO_NONE(key);
            
            ISCardReaderResultItem *resultItem = itemDict[key];
            
            if (resultItem != nil)
            {
                NSString *keyString = [NSString stringWithFormat:@"%@",[cardArrString objectAtIndex:i]];
                STRING_NIL_TO_NONE(keyString);
                
                if([resultItem isKindOfClass:[NSString class]])
                {
                    [resultMutDic setObject:resultItem forKey:keyString];
                }
                else
                {
                    [resultMutDic setObject:resultItem.itemValue forKey:keyString];
                }
            }
        }
    }
    
    /******************************** ***** ********************************/
    NSData *data = UIImageJPEGRepresentation(image, 0.0000001);
    NSString *imageDataString = [NSString stringWithFormat:@"%@",[data base64EncodedString]];
    STRING_NIL_TO_NONE(imageDataString);

    [resultMutDic setObject:imageDataString forKey:@"imageData"];
    [resultMutDic setObject:imageName forKey:@"imageName"];
    /******************************** ***** ********************************/
    //图片类型
    NSString *imageIndex = [NSString stringWithFormat:@"%@",[self.urlCommand argumentAtIndex:3]];
    STRING_NIL_TO_NONE(imageIndex);
    [resultMutDic setObject:imageIndex forKey:@"imageIndex"];
    
    [resultMutDic setObject:@"1" forKey:@"resultCode"];
    /******************************** ***** ********************************/
    
    return resultMutDic;
}

@end

