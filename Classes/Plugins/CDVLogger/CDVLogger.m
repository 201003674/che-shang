//
//  CDVLogger.m
//  IOSFramework
//
//  Created by 林科 on 2017/7/10.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import "CDVLogger.h"

@implementation CDVLogger

#pragma mark -
#pragma mark - 日志记录
- (void)logRecordPlugin:(CDVInvokedUrlCommand *)command
{
    //姓名
    NSString *name = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(name);
    //登录用户名
    NSString *userName = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(userName);
    //分公司编码
    NSString *branchCompanyCode = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
    STRING_NIL_TO_NONE(branchCompanyCode);
    //分公司名称
    NSString *branchCompanyName = [NSString stringWithFormat:@"%@",[command argumentAtIndex:3]];
    STRING_NIL_TO_NONE(branchCompanyName);
    //部门组编码
    NSString *departmentGroupCode = [NSString stringWithFormat:@"%@",[command argumentAtIndex:4]];
    STRING_NIL_TO_NONE(departmentGroupCode);
    //部门组名称
    NSString *departmentGroupName = [NSString stringWithFormat:@"%@",[command argumentAtIndex:5]];
    STRING_NIL_TO_NONE(departmentGroupName);
    //部门编码
    NSString *departmentCode = [NSString stringWithFormat:@"%@",[command argumentAtIndex:6]];
    STRING_NIL_TO_NONE(departmentCode);
    //部门名称
    NSString *departmentName = [NSString stringWithFormat:@"%@",[command argumentAtIndex:7]];
    STRING_NIL_TO_NONE(departmentName);
    //科室编码
    NSString *sectionOfficeCode = [NSString stringWithFormat:@"%@",[command argumentAtIndex:8]];
    STRING_NIL_TO_NONE(sectionOfficeCode);
    //科室名称
    NSString *sectionOfficeName = [NSString stringWithFormat:@"%@",[command argumentAtIndex:9]];
    STRING_NIL_TO_NONE(sectionOfficeName);
    //操作类型:登录、消息、业务办理、产品介绍、代办业务、贷款计算器、进件资格查询、预约、草稿件、回退件、门店复核中、审批中 、待签约、待放款、拒绝取消件
    NSString *operationType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:10]];
    STRING_NIL_TO_NONE(operationType);
    
    //获取设备UUID
    NSString *deviceId = [NSString stringWithFormat:@"%@",[[[UIDevice currentDevice] identifierForVendor] UUIDString]];
    STRING_NIL_TO_NONE(deviceId);
    //系统版本 例如：IOS8.2 、Android4.3
    NSString *systemVersion = [NSString stringWithFormat:@"IOS %@",[UIDevice currentDevice].systemVersion];
    STRING_NIL_TO_NONE(systemVersion)
    //当前app操作版本号
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    STRING_NIL_TO_NONE(appVersion);
    
    //创建时间(yyyy-MM-dd HH:mm:ss)
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //hh与HH的区别:分别表示12小时制,24小时制
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSString *createTime = [NSString stringWithFormat:@"%@",[formatter stringFromDate:[NSDate date]]];
    STRING_NIL_TO_NONE(createTime);
    //IP地址
    NSString *ipAddress = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getIPAddress]];
    STRING_NIL_TO_NONE(ipAddress);
    //地址信息
    NSString *address = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getAddress]];
    STRING_ADDRESS_NIL_TO_NONE(address);
    //MAC地址
    NSString *macAddress = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getMacAddress]];
    STRING_NIL_TO_NONE(macAddress);
    
    
    NSDictionary *finialDic = [NSDictionary dictionaryWithObjectsAndKeys:
                               name,@"name",
                               userName,@"userName",
                               branchCompanyCode,@"branchCompanyCode",
                               branchCompanyName,@"branchCompanyName",
                               departmentGroupCode,@"departmentGroupCode",
                               departmentGroupName,@"departmentGroupName",
                               departmentCode,@"departmentCode",
                               departmentName,@"departmentName",
                               sectionOfficeCode,@"sectionOfficeCode",
                               sectionOfficeName,@"sectionOfficeName",
                               operationType,@"operationType",
                               deviceId,@"deviceId",
                               systemVersion,@"systemVersion",
                               appVersion,@"appVersion",
                               createTime,@"createTime",
                               ipAddress,@"ipAddress",
                               address,@"address",
                               macAddress,@"macAddress",nil];
    
    /* 数据缓存 更新 */
    FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
    
    [fmdbSql insertIntoTableName:LOG_TABLE_NAME Dict:finialDic complete:^(BOOL isSuccess) {
        
        if (isSuccess)
        {
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                        messageAsString:@"插入成功"];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            
        }
        else
        {
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                        messageAsString:@"插入失败"];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
    }];
}

#pragma mark -
#pragma mark -

@end
