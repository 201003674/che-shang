//
//  CDVLogger.h
//  IOSFramework
//
//  Created by 林科 on 2017/7/10.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import <Cordova/CDVPlugin.h>

@interface CDVLogger : CDVPlugin

- (void)logRecordPlugin:(CDVInvokedUrlCommand *)command;

@end
