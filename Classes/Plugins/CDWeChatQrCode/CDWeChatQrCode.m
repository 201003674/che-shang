//
//  CDWeChatQrCode.m
//  IOSFramework
//
//  Created by 林科 on 2018/5/14.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import "CDWeChatQrCode.h"
#import "NSString+JSONCategories.h"
#import "NSString+Additions.h"

@implementation CDWeChatQrCode 

#pragma mark -
#pragma mark - 获取微信二维码
-(void)getWeChatQrCode:(CDVInvokedUrlCommand*)command
{
    //0 请求url 1 参数传值
    NSString *strUrl = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(strUrl);
    
    NSString *parmarsString = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(parmarsString);
    
    NSString *parmars = [NSString stringWithFormat:@"%@",[parmarsString UrlDecodedString]];
    STRING_NIL_TO_NONE(parmars);
    
    [[[ClassFactory getInstance] getNetComm] showHud:@""];
    [[[ClassFactory getInstance] getNetComm] getWeChatQrCode:strUrl withParmars:[parmars JSONValue] success:^(id JSON) {
        
        NSDictionary *requestResultDic = [[NSDictionary alloc] initWithDictionary:JSON];
        NSString *rtnCode = [NSString stringWithFormat:@"%@",[requestResultDic objectForKey:@"rtnCode"]];
        STRING_NIL_TO_NONE(rtnCode)
        
        NSString *rtnMsg = [NSString stringWithFormat:@"%@",[requestResultDic objectForKey:@"rtnMsg"]];
        STRING_NIL_TO_NONE(rtnMsg)
        
        if ([rtnCode isEqualToString:@"10000"])
        {
            // create acordova result
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                        messageAsString:rtnMsg];
            
            NSLog(@"======= %@ =======",rtnMsg);
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        else
        {
            // create acordova result
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                         messageAsString:rtnMsg];
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
        
    } failure:^(NSError *error) {
        
        NSString *errorInfo = [NSString stringWithFormat:@"%@",[error localizedDescription]];
        STRING_NIL_TO_NONE(errorInfo);
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsString:errorInfo];
        
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
        
    }];
}

#pragma mark -
#pragma mark -

@end
