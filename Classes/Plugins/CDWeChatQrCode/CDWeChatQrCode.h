//
//  CDWeChatQrCode.h
//  IOSFramework
//
//  Created by 林科 on 2018/5/14.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import <Cordova/CDVPlugin.h>

@interface CDWeChatQrCode : CDVPlugin

-(void)getWeChatQrCode:(CDVInvokedUrlCommand*)command;

@end
