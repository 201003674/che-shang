//
//  UmengAnalyticsPlugin.m
//  CRM
//
//  Created by 林科 on 16/7/12.
//  Copyright © 2016年 FBI01. All rights reserved.
//

#import "UmengAnalyticsPlugin.h"

#import <Cordova/CDVPluginResult.h>

@implementation UmengAnalyticsPlugin

- (void)init:(CDVInvokedUrlCommand*)command
{
    NSString *callbackId = command.callbackId;
    
//    [MobClick startWithAppkey:@"YOU_APP_KEY" reportPolicy:BATCH channelId:@"Web"];
    
    CDVPluginResult* pluginResult = nil;
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
}

- (void)setDebugMode:(CDVInvokedUrlCommand*)command
{
    NSString* debug = [command.arguments objectAtIndex:0];
    NSLog(@"友盟调试模式：%@", debug);
}

- (void)onPause:(CDVInvokedUrlCommand*)command;
{
    NSLog(@"App Pause!");
}

- (void)onResume:(CDVInvokedUrlCommand*)command;
{
    NSLog(@"App Resume!");
}

- (void)pageStart:(CDVInvokedUrlCommand*)command
{
    [self init:command];
    NSString* pageName = [command.arguments objectAtIndex:0];
    NSLog(@"%@ - 页面开始", pageName);
//    [MobClick beginLogPageView:pageName];
}

- (void)pageEnd:(CDVInvokedUrlCommand*)command
{
    [self init:command];
    
    NSString* pageName = [command.arguments objectAtIndex:0];
    NSLog(@"%@ - 页面结束", pageName);
//    [MobClick endLogPageView:pageName];
}

- (void)pageEvent:(CDVInvokedUrlCommand*)command
{
    [self init:command];
    
    NSString* eventId = [command.arguments objectAtIndex:0];
//    [MobClick event:eventId];
}

@end
