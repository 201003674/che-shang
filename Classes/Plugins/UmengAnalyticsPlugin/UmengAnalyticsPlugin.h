//
//  UmengAnalyticsPlugin.h
//  CRM
//  友盟统计
//  Created by 林科 on 16/7/12.
//  Copyright © 2016年 FBI01. All rights reserved.
//


#import <Cordova/CDVPlugin.h>

@interface UmengAnalyticsPlugin : CDVPlugin

- (void)init:(CDVInvokedUrlCommand*)command;

- (void)setDebugMode:(CDVInvokedUrlCommand*)command;

- (void)onPause:(CDVInvokedUrlCommand*)command;

- (void)onResume:(CDVInvokedUrlCommand*)command;

- (void)pageStart:(CDVInvokedUrlCommand*)command;

- (void)pageEnd:(CDVInvokedUrlCommand*)command;

- (void)pageEvent:(CDVInvokedUrlCommand*)command;

@end