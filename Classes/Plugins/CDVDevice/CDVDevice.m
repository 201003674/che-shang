/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

#include <sys/types.h>
#include <sys/sysctl.h>

#import <Cordova/CDV.h>
#import "CDVDevice.h"

@implementation UIDevice (ModelVersion)

- (NSString*)modelVersion
{
    size_t size;

    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char* machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString* platform = [NSString stringWithUTF8String:machine];
    free(machine);

    return platform;
}

@end

@interface CDVDevice () {}
@end

@implementation CDVDevice

- (NSString*)uniqueAppInstanceIdentifier:(UIDevice*)device
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    static NSString* UUID_KEY = @"CDVUUID";

    NSString* app_uuid = [userDefaults stringForKey:UUID_KEY];

    if (app_uuid == nil) {
        CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
        CFStringRef uuidString = CFUUIDCreateString(kCFAllocatorDefault, uuidRef);

        app_uuid = [NSString stringWithString:(__bridge NSString*)uuidString];
        [userDefaults setObject:app_uuid forKey:UUID_KEY];
        [userDefaults synchronize];

        CFRelease(uuidString);
        CFRelease(uuidRef);
    }

    return app_uuid;
}

- (void)CDVDevice:(CDVInvokedUrlCommand*)command
{
    NSDictionary* deviceProperties = [NSDictionary dictionaryWithDictionary:[self deviceProperties]];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:deviceProperties];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getDeviceInfo:(CDVInvokedUrlCommand*)command
{
    NSDictionary* deviceProperties = [self deviceProperties];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:deviceProperties];

    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (NSDictionary*)deviceProperties
{
    NSMutableDictionary* devProps = [NSMutableDictionary dictionaryWithCapacity:10];

    [devProps setObject:@"Apple" forKey:@"manufacturer"];
    [devProps setObject:[[UIDevice currentDevice] modelVersion] forKey:@"model"];
    [devProps setObject:@"iOS" forKey:@"platform"];
    [devProps setObject:[[UIDevice currentDevice] systemVersion] forKey:@"systemVersion"];
    
    NSString *version = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getVersionCode]];
    STRING_NIL_TO_NONE(version);
    [devProps setObject:version forKey:@"version"];
    
    [devProps setObject:[self uniqueAppInstanceIdentifier:[UIDevice currentDevice]] forKey:@"uuid"];
    [devProps setObject:[[self class] cordovaVersion] forKey:@"cordova"];
    
    //设备类型
    NSString *deviceType = [NSString stringWithFormat:@"ios"];
    STRING_NIL_TO_NONE(deviceType)
    
    [devProps setObject:deviceType forKey:@"deviceType"];
    
    //设备令牌
    NSString *deviceToken = [NSString stringWithFormat:@"%@",[[UIDevice currentDevice].identifierForVendor UUIDString]];
    STRING_NIL_TO_NONE(deviceToken)
    
    [devProps setObject:deviceToken forKey:@"deviceToken"];
    
    //手机型号
    NSString* phoneModel = [NSString stringWithFormat:@"%@",[[UIDevice currentDevice] model]];
    STRING_NIL_TO_NONE(phoneModel)
    
    [devProps setObject:phoneModel forKey:@"phoneModel"];
    
    //系统版本
    NSString* systemType = [NSString stringWithFormat:@"%@",[[UIDevice currentDevice] systemVersion]];
    STRING_NIL_TO_NONE(systemType);
    
    [devProps setObject:systemType forKey:@"systemType"];
    
    //系统版本号
    NSString* systemNumber = [NSString stringWithFormat:@"%@",[[UIDevice currentDevice] systemVersion]];
    STRING_NIL_TO_NONE(systemNumber);
    
    [devProps setObject:systemNumber forKey:@"systemNumber"];
    
    //网络运营商
    NSString *networkOperator = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getNetworkName]];
    STRING_NIL_TO_NONE(networkOperator);
    
    [devProps setObject:networkOperator forKey:@"networkOperator"];
    
    //移动网络类型
    NSString *networkType = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getNetworkType]];
    STRING_NIL_TO_NONE(networkType);
    
    [devProps setObject:networkType forKey:@"networkType"];
    
    //IP地址
    NSString *ipAddress = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getIPAddress]];
    STRING_NIL_TO_NONE(ipAddress);
    
    [devProps setObject:ipAddress forKey:@"ipAddress"];
    
    //appType
    [devProps setObject:version forKey:@"appType"];
    
    //平台类型
    [devProps setObject:@"1" forKey:@"plateType"];
    
    //项目类型
    [devProps setObject:@"CRM" forKey:@"projectType"];
    

    NSDictionary* devReturn = [NSDictionary dictionaryWithDictionary:devProps];
    return devReturn;
}

+ (NSString*)cordovaVersion
{
    return CDV_VERSION;
}

@end
