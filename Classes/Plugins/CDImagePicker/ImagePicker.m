//
//  ImagePicker.m
//  IOSFramework
//
//  Created by 林科 on 2017/6/21.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import "ImagePicker.h"

#import "MJPhoto.h"
#import "MJPhotoBrowser.h"

#import "NSString+JSONCategories.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

#import <MediaPlayer/MediaPlayer.h>

#import "NSString+XHMD5.h"
@implementation ImagePicker

- (void)ShowPictures:(CDVInvokedUrlCommand *)command
{
    //获取默认图片位置
    NSString *index = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(index);
    
    //媒体数据
    NSString *string = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(string);
    
    //获取图片数组
    NSMutableArray *imageArr = [NSMutableArray arrayWithArray:[string JSONValue]];
    
    //获取默认图片
    NSDictionary *defaultDic = [NSDictionary dictionaryWithDictionary:[imageArr objectAtIndex:index.integerValue]];

    NSString *defaultUrl = [NSString stringWithFormat:@"%@",[defaultDic objectForKey:@"url"]];
    STRING_NIL_TO_NONE(defaultUrl);
    
    //是否在线预览
    NSString *whetherOnline = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
    STRING_NIL_TO_NONE(whetherOnline);
    
    if([whetherOnline isEqualToString:@"1"])
    {
        NSCharacterSet *encodeUrlSet = [NSCharacterSet URLQueryAllowedCharacterSet];
        NSString *encodeUrl = [defaultUrl stringByAddingPercentEncodingWithAllowedCharacters:encodeUrlSet];
        
        NSURL* url = [[NSURL alloc] initWithString:encodeUrl];
        
        if (url)
        {
            NSURL *documentsUrl = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask].firstObject;
            
            NSString *pdfNameString = [NSString stringWithFormat:@"%@.pdf",[[[ClassFactory getInstance] getLogic] getImageName]];
            STRING_NIL_TO_NONE(pdfNameString)
            
            NSURL *destinationUrl = [documentsUrl URLByAppendingPathComponent:pdfNameString];
            
            // Actually download
            NSError* err = nil;
            NSData* fileData = [[NSData alloc] initWithContentsOfURL:url options:NSDataReadingUncached error:&err];
            if (!err && fileData && fileData.length && [fileData writeToURL:destinationUrl atomically:true])
            {
                //PDF 直接预览
                UIDocumentInteractionController *document = [UIDocumentInteractionController interactionControllerWithURL:destinationUrl];
                document.delegate = self;
                
                //不展示可选操作
                [document presentPreviewAnimated:YES];
            }
        }
        return;
    }
    
    if ([defaultUrl rangeOfString:@".pdf"].location != NSNotFound)
    {
        NSString *fileName = [NSString stringWithFormat:@"%@.pdf",[defaultUrl shortUrl]];
        STRING_NIL_TO_NONE(fileName);
        
        [self openFilesWithDownLoadFoldName:@"2010036742" WithFileName:fileName WithUrl:defaultUrl];
    
        return;
    }
    else if ([defaultUrl rangeOfString:@".mp4"].location != NSNotFound)
    {
        //MP4 短连接
        NSString *fileName = [NSString stringWithFormat:@"%@.mp4",[defaultUrl shortUrl]];
        STRING_NIL_TO_NONE(fileName);
        
        [self openFilesWithDownLoadFoldName:@"2010036742" WithFileName:fileName WithUrl:defaultUrl];
        
        return;
    }
    else
    {
        //
    }
    
    
    MJPhotoBrowser *brower = [[MJPhotoBrowser alloc] init];
    
	NSMutableArray *finialMutArr = [NSMutableArray array];
    
    for (NSDictionary *tempMutDic in imageArr)
    {
        NSString *tempImageString = [NSString stringWithFormat:@"%@",[tempMutDic objectForKey:@"url"]];
        STRING_NIL_TO_NONE(tempImageString);
        
        if ([tempImageString rangeOfString:@".pdf"].location != NSNotFound)
        {
            
        }
        else if([tempImageString rangeOfString:@".mp4"].location != NSNotFound)
        {
            
        }
        else
        {
            [finialMutArr addObject:tempMutDic];
        }
    }
    
    CGFloat width = VIEWSCREEN_WIDTH/finialMutArr.count;
    
	NSMutableArray *photos = [NSMutableArray array];
    
    //2.告诉图片浏览器显示所有的图片
    for (int i = 0 ; i < finialMutArr.count; i++)
    {
        //传递数据给浏览器
        MJPhoto *photo = [[MJPhoto alloc] init];
        
        NSDictionary *dic = [NSDictionary dictionaryWithDictionary:[finialMutArr objectAtIndex:i]];
        
        NSString *imageString = [NSString stringWithFormat:@"%@",[dic objectForKey:@"url"]];
        STRING_NIL_TO_NONE(imageString);
        
        CGRect rect = CGRectMake(0 + width * i, VIEWSCREEN_IOS7_Y + (VIEWSCREEN_HEIGHT - width)/2.0, width, 20);
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:rect];
        
        if ([Check checkUrl:imageString])
        {
            //url
            [photo setUrl:[NSURL URLWithString:imageString]];
            
            [imageView setImageWithURL:[NSURL URLWithString:imageString]];
        }
        else
        {
            //本地imagePath 图片流
            UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:imageString];
            [photo setImage:image];
            
            [imageView setImage:image];
        }
        [photo setSrcImageView:imageView];
        
        [photos addObject:photo];
    }
    [brower setPhotos:photos];
    
    //3.设置默认显示的图片索引
    brower.currentPhotoIndex = index.integerValue;
    
    //4.显示浏览器
    [brower show];
}
#pragma mark -
#pragma mark - 文件打开
- (void)openFileWithFileName:(NSString *)fileName WithFilePath:(NSString *)filePath
{
    if([fileName rangeOfString:@".mp4"].location != NSNotFound)
    {
        [self playMP4WithFilePath:filePath];
    }
    else if ([fileName rangeOfString:@".pdf"].location != NSNotFound)
    {
        [self playPDFWithFilePath:filePath];
    }
    else
    {
        [[[ClassFactory getInstance] getNetComm] showHud:@"文件类型暂不支持!"];
    }
}

#pragma mark -
#pragma mark - 下载预览
- (void)openFilesWithDownLoadFoldName:(NSString *)downLoadFoldName WithFileName:(NSString *)downLoadFileName WithUrl:(NSString *)url
{
    NSString *filePath = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLogic] creatFileDownLoadDocuments:downLoadFoldName WithFileName:downLoadFileName]];
    STRING_NIL_TO_NONE(filePath);
    
    //判断文件是否存在
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        [self openFileWithFileName:downLoadFileName WithFilePath:filePath];
    }
    else
    {
        [self downLoadFileWithDownLoadFoldName:downLoadFoldName WithFileName:downLoadFileName WithUrl:url withSuccess:^(id JSON) {
            
            [self openFileWithFileName:downLoadFileName WithFilePath:filePath];
            
        } withFail:^(NSError *error) {
            
        }];
    }
}

- (void)downLoadFileWithDownLoadFoldName:(NSString *)downLoadFoldName WithFileName:(NSString *)downLoadFileName WithUrl:(NSString *)url withSuccess:(void (^)(id JSON))success withFail:(void (^)(NSError *error))fail
{
    [[[ClassFactory getInstance] getNetComm] showHud:nil];
    [[[ClassFactory getInstance] getFileDownLoadAFRequestQueue] addFileDownOperationWithFileName:downLoadFileName WithFolder:downLoadFoldName withURL:url withOnlyTarget:url withSuccess:^(id JSON) {
        
        if (success) {
            success(JSON);
        }
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
    } withFail:^(NSError *error) {
        
        if (fail) {
            fail(error);
        }
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
    } progessDataBlock:^(float percentDone) {
        
    }];
}

#pragma mark -
#pragma mark - MP4播放
- (void)playMP4WithFilePath:(NSString *)filePath
{
    MPMoviePlayerViewController *moviePlayerView = [[MPMoviePlayerViewController alloc]initWithContentURL:[NSURL fileURLWithPath:filePath]];
    
    [self.viewController presentViewController:moviePlayerView animated:YES completion:nil];
    
    //注销通知
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                           selector:@selector(movieFinishedCallback:)
//                                                    name:MPMoviePlayerPlaybackDidFinishNotification
//                                                  object:moviePlayerView.moviePlayer];
    
}

-(void)movieFinishedCallback:(NSNotification*)notification
{
    MPMoviePlayerController *moviePlayer = [notification object];
    
    [moviePlayer stop];
    [moviePlayer.view removeFromSuperview];
    
    [self.viewController dismissMoviePlayerViewControllerAnimated];
}

#pragma mark -
#pragma mark - MP4播放
- (void)playPDFWithFilePath:(NSString *)filePath
{
    NSURL *destinationUrl = [NSURL fileURLWithPath:filePath];

    if(!self.document)
    {
        self.document = [UIDocumentInteractionController interactionControllerWithURL:destinationUrl];
    }
    
    self.document.delegate = self;
    //不展示可选操作
    //[document presentPreviewAnimated:YES];
    [self.document presentOpenInMenuFromRect:CGRectZero inView:self.viewController.view animated:YES];
}

#pragma mark -
#pragma mark - UIDocumentInteractionControllerDelegate
- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller
{
    return self.viewController;
}
/**
 *  文件分享面板退出时调用
 */
- (void)documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController *)controller
{
    NSLog(@"dismiss");
}

/**
 *  文件分享面板弹出的时候调用
 */
- (void)documentInteractionControllerWillPresentOpenInMenu:(UIDocumentInteractionController *)controller
{
    NSLog(@"WillPresentOpenInMenu");
}

/**
 *  当选择一个文件分享App的时候调用
 */
- (void)documentInteractionController:(UIDocumentInteractionController *)controller willBeginSendingToApplication:(nullable NSString *)application
{
    NSLog(@"begin send : %@", application);
}

- (void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(NSString *)application
{
    NSLog(@"begin send : %@", application);
}

@end
