//
//  ImagePicker.h
//  IOSFramework
//
//  Created by 林科 on 2017/6/21.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import <Cordova/CDVPlugin.h>

@interface ImagePicker : CDVPlugin<UIDocumentInteractionControllerDelegate>
{
    
}
//PDF 阅读器
@property (nonatomic, strong) UIDocumentInteractionController *document;


- (void)ShowPictures:(CDVInvokedUrlCommand *)command;

@end
