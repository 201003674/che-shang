
#import <UIKit/UIKit.h>
#import "UIView+Extension.h"
#import "XJKeyboardAccessoryBtn.h"

@class XJKeyboardView;

@protocol XJKeyboardDelegate <NSObject>

@optional
/** 点击了数字按钮 */
- (void)keyboard:(XJKeyboardView *)keyboard didClickButton:(UIButton *)button;
/** 点击删除按钮 */
- (void)keyboard:(XJKeyboardView *)keyboard didClickDeleteBtn:(UIButton *)deleteBtn;
/** 点击return按钮 */
- (void)keyboard:(XJKeyboardView *)keyboard didClickReturnBtn:(UIButton *)returnBtn;

@end

@interface XJKeyboardView : UIView
{
    
}
/** 删除按钮 */
@property (nonatomic, weak) UIButton *deleteBtn;

/** 切换大小写按钮 */
@property (nonatomic, weak) UIButton *shiftButton;

//c_symbol_keyboardSwitchButton

/** 切换数字 英文 */
@property (nonatomic, weak) UIButton *changeButton;

/** 符号按钮 */
@property (nonatomic, weak) UIButton *symbolBtn;

/** ABC 文字按钮 */
@property (nonatomic, weak) UIButton *textBtn;

/** 空格按钮 */
@property (nonatomic, weak) UIButton *blankBtn;

/** return按钮 */
@property (nonatomic, weak) UIButton *returnBtn;
/** 切换按钮状态*/
@property (nonatomic, assign, getter = isChange) BOOL shiftChange;

@property (assign, nonatomic) BOOL isSymbolSelect;

//是否随机
@property (assign, nonatomic) BOOL isRandom;

@property (nonatomic, assign) id<XJKeyboardDelegate> delegate;

@end
