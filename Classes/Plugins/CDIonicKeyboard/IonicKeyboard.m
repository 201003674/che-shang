#import "IonicKeyboard.h"

@implementation IonicKeyboard

#pragma mark -
#pragma mark -
-(void)show:(CDVInvokedUrlCommand*)command
{ 
    self.command = command;
    
    NSString *text = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(text)
    
    textField = [[XJSafeTextField alloc] initWithFrame:CGRectZero];
    textField.safeDelegate = self;
    textField.text = text;
    [self.webView addSubview:textField];
    
    if ([textField canBecomeFirstResponder])
    {
        [textField becomeFirstResponder];
    }
}

-(void)close:(CDVInvokedUrlCommand*)command
{
    if ([textField canResignFirstResponder]) {
        
        [textField resignFirstResponder];
        
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                          messageAsString:textField.text];
        //CDVPluginResult.keepCallback 设置为 true ,则不会销毁callback
        [pluginResult setKeepCallbackAsBool:YES];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:self.command.callbackId];
        
    }
}

#pragma mark -
#pragma mark - XJSafeTextFieldDelegate
- (void)textFiledClickActon:(XJSafeTextField*)textField
{
    NSLog(@"textFiledClickActon:%@", textField.text);
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                      messageAsString:textField.text];
    //CDVPluginResult.keepCallback 设置为 true ,则不会销毁callback
    [pluginResult setKeepCallbackAsBool:YES];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:self.command.callbackId];
    
}
- (void)textFiledDoneActon:(XJSafeTextField*)textField
{
    NSLog(@"textFiledDoneActon:%@", textField.text);
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                      messageAsString:textField.text];
    //CDVPluginResult.keepCallback 设置为 true ,则不会销毁callback
    [pluginResult setKeepCallbackAsBool:YES];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:self.command.callbackId];
    
}
- (void)textFiledDeleteActon:(XJSafeTextField*)textField
{
    NSLog(@"textFiledDeleteActon:%@", textField.text);
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                      messageAsString:textField.text];
    //CDVPluginResult.keepCallback 设置为 true ,则不会销毁callback
    [pluginResult setKeepCallbackAsBool:YES];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:self.command.callbackId];
    
}

@end

