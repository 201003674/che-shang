#import "CDVBasePlugin.h"

#import "XJSafeTextField.h"
@interface IonicKeyboard : CDVBasePlugin<XJSafeTextFieldDelegate>
{
    XJSafeTextField *textField;
}

-(void)show:(CDVInvokedUrlCommand*)command;
-(void)close:(CDVInvokedUrlCommand*)command;

@end
