//
//  CDVGcapp.m
//  CRM
//
//  Created by 林科 on 16/7/12.
//  Copyright © 2016年 FBI01. All rights reserved.
//

#import "CDVGcapp.h"
#import <Cordova/CDVViewController.h>
#import <Cordova/CDVScreenOrientationDelegate.h>

@implementation CDVGcapp

- (void)version:(CDVInvokedUrlCommand*)command
{
    NSString* value0 = [NSString stringWithFormat:@"%@(%@)", [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleShortVersionString"] ,[[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey]];
    
    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:value0];
    
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

@end
