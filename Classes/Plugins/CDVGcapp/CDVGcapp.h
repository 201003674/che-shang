//
//  CDVGcapp.h
//  CRM
//  获取APP版本信息
//  Created by 林科 on 16/7/12.
//  Copyright © 2016年 FBI01. All rights reserved.
//

#import <Cordova/CDVPlugin.h>
#import <Foundation/Foundation.h>

@interface CDVGcapp : CDVPlugin

- (void)version:(CDVInvokedUrlCommand*)command;

@end
