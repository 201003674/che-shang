//
//  CDVPUSH.m
//  IOSFramework
//
//  Created by 林科 on 2018/9/20.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import "CDVPUSH.h"

@implementation CDVPUSH

- (void)pushBind:(CDVInvokedUrlCommand*)command
{
    NSString *aUniqueID = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(aUniqueID);

    /* 推送设备绑定 */
    NSString *device = @"nomal";
    STRING_NIL_TO_NONE(device)

    #ifdef DEBUG

    NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"resultCode",@"绑定成功!",@"message", nil];

    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                            messageAsDictionary:theMessage];
    // send cordova result
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];

    #else

    [MPPPushServer bindPushWithTags:@[device] userName:aUniqueID];

    if (MPPPushServer.isAllowedNotification)
    {
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"resultCode",@"绑定成功!",@"message", nil];

        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
    else
    {
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",@"绑定失败,请确认APP是否允许推送!",@"message", nil];

        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }

    #endif
}

@end
