#import "Sms.h"
//#import <Cordova/NSArray+Comparisons.h>

@implementation Sms
@synthesize callbackID;

//- (CDVPlugin *)initWithWebView:(UIWebView *)theWebView {
//    self = (Sms *)[super initWithWebView:theWebView];
//    return self;
//}

#pragma mark -
#pragma mark -
- (void)Sms:(CDVInvokedUrlCommand*)command
{
    NSArray *arr = [NSArray arrayWithArray:[command.arguments objectAtIndex:0]];
 
    NSLog(@"=======%@  %@",[arr objectAtIndex:0],[arr objectAtIndex:1]);
    
    self.callbackID = command.callbackId;
    
    if(![MFMessageComposeViewController canSendText])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notice"
                                                        message:@"SMS Text not available."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil
                              ];
        [alert show];
        return;
    }
    
    MFMessageComposeViewController *composeViewController = [[MFMessageComposeViewController alloc] init];
    composeViewController.messageComposeDelegate = self;
    
    if (arr.count > 1)
    {
        NSString *body = [NSString stringWithFormat:@"%@",[arr objectAtIndex:1]];
        
        if (body != nil) {
            [composeViewController setBody:body];
        }
    }
    
    if (arr.count > 0)
    {
        NSMutableArray* recipients = [NSMutableArray arrayWithObjects:[arr objectAtIndex:0], nil];
//        if (recipients != nil){
//            if ([recipients.firstObject isEqual: @""]) {
//                [recipients replaceObjectAtIndex:0 withObject:@"?"];
//            }
        
            [composeViewController setRecipients:recipients];
//        }
    }
    
    [self.viewController presentViewController:composeViewController animated:YES completion:nil];
}

#pragma mark -
#pragma mark - MFMessageComposeViewControllerDelegate Implementation
// Dismisses the composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    // Notifies users about errors associated with the interface
    int webviewResult = 0;
    NSString* message = @"";
    
    switch(result) {
        case MessageComposeResultCancelled:
            webviewResult = 0;
            message = @"Message cancelled.";
            break;
        case MessageComposeResultSent:
            webviewResult = 1;
            message = @"Message sent.";
            break;
        case MessageComposeResultFailed:
            webviewResult = 2;
            message = @"Message failed.";
            break;
        default:
            webviewResult = 3;
            message = @"Unknown error.";
            break;
    }
    
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
    
    if(webviewResult == 1)
    {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                          messageAsString:message];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:self.callbackID];
    }
    else
    {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                          messageAsString:message];

        [self.commandDelegate sendPluginResult:pluginResult callbackId:self.callbackID];
    }
}

@end
