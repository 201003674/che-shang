#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>

@interface Sms : CDVPlugin <MFMessageComposeViewControllerDelegate>
{
    CDVInvokedUrlCommand *commandsasa;
}

@property(strong) NSString* callbackID;

- (void)Sms:(CDVInvokedUrlCommand*)command;

@end