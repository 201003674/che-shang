//
//  SGQRCodeScanningViewController.m
//  IOSFramework
//
//  Created by 林科 on 2017/7/17.
//  Copyright © 2017年 allianture. All rights reserved.
//

#import "SGQRCodeScanningViewController.h"

@implementation SGQRCodeScanningViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    SGQRCodeScanManager *manager = [SGQRCodeScanManager sharedManager];
    
    NSArray *arr = @[AVMetadataObjectTypeQRCode, AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode128Code];
    // AVCaptureSessionPreset1920x1080 推荐使用，对于小型的二维码读取率较高
    [manager SG_setupSessionPreset:AVCaptureSessionPreset1920x1080 metadataObjectTypes:arr currentController:self];
    
    [manager setDelegate:self];
}

#pragma mark -
#pragma mark - SGQRCodeScanManagerDelegate
- (void)QRCodeScanManager:(SGQRCodeScanManager *)scanManager didOutputMetadataObjects:(NSArray *)metadataObjects
{
    if (metadataObjects != nil && metadataObjects.count > 0)
    {
        [scanManager SG_palySoundName:@"SGQRCode.bundle/sound.caf"];
        [scanManager SG_stopRunning];
        [scanManager SG_videoPreviewLayerRemoveFromSuperlayer];
        
        [self.ViewControllerBDelegate sendMetadataObjects:metadataObjects];
    }
    else
    {
        [self.ViewControllerBDelegate sendMetadataObjects:metadataObjects];
    }
}

@end
