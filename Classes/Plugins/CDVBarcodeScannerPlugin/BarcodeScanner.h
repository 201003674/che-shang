//
//  CDVBarcodeScannerPlugin.h
//  CRM
//
//  Created by 林科 on 16/7/27.
//  Copyright © 2016年 FBI01. All rights reserved.
//


#import <Cordova/CDV.h>
#import <Cordova/CDVPlugin.h>

#import "SGQRCodeScanningViewController.h"
@interface BarcodeScanner : CDVPlugin<ViewControllerBDelegate>
{
    CDVInvokedUrlCommand *barcodeScannerCommand;
}
@end
