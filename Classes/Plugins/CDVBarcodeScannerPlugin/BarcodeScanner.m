//
//  CDVBarcodeScannerPlugin.m
//  CRM
//
//  Created by 林科 on 16/7/27.
//  Copyright © 2016年 FBI01. All rights reserved.
//

#import "BarcodeScanner.h"

@implementation BarcodeScanner
-(void)BarcodeScanner:(CDVInvokedUrlCommand*)command
{
    barcodeScannerCommand = command;
    
    SGQRCodeScanningViewController *SGQRCodeScanning = [[SGQRCodeScanningViewController alloc] init];
    [SGQRCodeScanning setViewControllerBDelegate:self];
    
    [self.viewController.navigationController presentViewController:SGQRCodeScanning animated:YES completion:^{
        
    }];
}
#pragma mark -
#pragma mark - ViewControllerBDelegate
- (void)sendMetadataObjects:(NSArray *)metadataObjects
{
    if (metadataObjects != nil && metadataObjects.count > 0)
    {
        [self.viewController.navigationController dismissViewControllerAnimated:YES completion:nil];
        
        for (AVMetadataMachineReadableCodeObject *metadataObject in metadataObjects)
        {
            NSString *stringValue = [NSString stringWithFormat:@"%@",metadataObject.stringValue];
            STRING_NIL_TO_NONE(stringValue);
            
            //发送到页面上
            CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:stringValue];
            
            [self.commandDelegate sendPluginResult:pluginResult callbackId:barcodeScannerCommand.callbackId];
        }
    }
    else
    {
        [self.viewController.navigationController dismissViewControllerAnimated:YES completion:nil];
        
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        
        [self.commandDelegate sendPluginResult:result callbackId:barcodeScannerCommand.callbackId];
        
    }
}

#pragma mark -
#pragma mark -

@end
