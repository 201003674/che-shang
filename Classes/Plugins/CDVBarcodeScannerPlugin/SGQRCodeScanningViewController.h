//
//  SGQRCodeScanningViewController.h
//  IOSFramework
//
//  Created by 林科 on 2017/7/17.
//  Copyright © 2017年 allianture. All rights reserved.
//

@protocol ViewControllerBDelegate // 代理传值方法

- (void)sendMetadataObjects:(NSArray *)metadataObjects;

@end

#import <UIKit/UIKit.h>

#import "SGQRCodeScanManager.h"
@interface SGQRCodeScanningViewController : UIStandardViewController<SGQRCodeScanManagerDelegate>
{
    
}
// 委托代理人，代理一般需使用弱引用(weak)
@property (weak, nonatomic) id ViewControllerBDelegate;

@end
