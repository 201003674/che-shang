#import <Cordova/CDV.h>
#import <Cordova/CDVPlugin.h>

@interface StartApp : CDVPlugin

- (void)check:(CDVInvokedUrlCommand*)command;
- (void)start:(CDVInvokedUrlCommand*)command;
- (void)getVersionNumber:(CDVInvokedUrlCommand*)command;
- (void)queryOtherApp:(CDVInvokedUrlCommand*)command;
@end