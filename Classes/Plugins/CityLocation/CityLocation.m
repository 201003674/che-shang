//
//  CityLocation.m
//  DemoIPA
//
//  Created by 林科 on 15/8/24.
//  Copyright (c) 2015年 FBI01. All rights reserved.
//

#import "CityLocation.h"

#import <CoreLocation/CLLocationManager.h>
@implementation CityLocation

- (void)getLocation:(CDVInvokedUrlCommand*)command
{
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        NSDictionary *returnMutDic = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",@"请允许应用打开定位功能，否则无法添加地理位置信息。",@"message", nil];
        
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsString:[returnMutDic JSONString]];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
    else
    {
        NSString *locationString = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getAddress]];
        STRING_NIL_TO_NONE(locationString);
          
        if (STRING_ISNIL(locationString))
        {
            NSDictionary *returnMutDic = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",@"无法定位到当前位置信息，请稍后再试!",@"message", nil];
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsString:[returnMutDic JSONString]];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        else
        {
            CLLocationCoordinate2D coord = [[[ClassFactory getInstance] getLogic] coord];
            
            NSString *latitude = [NSString stringWithFormat:@"%f",coord.latitude];
            STRING_NIL_TO_NONE(latitude);
            
            NSString *longitude = [NSString stringWithFormat:@"%f",coord.longitude];
            STRING_NIL_TO_NONE(longitude);
            
            NSDictionary *returnMutDic = [NSDictionary dictionaryWithObjectsAndKeys:
                                          @"1",@"resultCode",
                                          locationString,@"message",
                                          latitude,@"latitude",
                                          longitude,@"longitude",
                                          nil];
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsString:[returnMutDic JSONString]];
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
    }
}

#pragma mark -
#pragma mark - getLocationList
- (void)getLocationList:(CDVInvokedUrlCommand*)command
{
    //urlCommand
    self.urlCommand = command;
    
    //初始化检索对象
    BMKPoiSearch *searcher =[[BMKPoiSearch alloc]init];
    searcher.delegate = self;
    //发起检索
    BMKPOINearbySearchOption *option = [[BMKPOINearbySearchOption alloc]init];
    
    //页码
    NSString *pageIndex = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(pageIndex);
    if (STRING_ISNIL(pageIndex)) {
        pageIndex = @"0";
    }
    option.pageIndex = pageIndex.integerValue;
    //页数
    NSString *pageSize = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(pageSize);
    if (STRING_ISNIL(pageSize)) {
        pageSize = @"20";
    }
    option.pageSize = pageSize.integerValue;
    //定位中心
    option.location = [[[ClassFactory getInstance] getLogic] coord];
    //检索关键字
    option.keywords = [NSArray arrayWithObjects:@"小吃",@"建筑",@"银行",@"小区",@"医院",@"保险", nil];
//    option.isRadiusLimit = YES;
    option.radius = 10000;
    BOOL flag = [searcher poiSearchNearBy:option];

    if(flag)
    {
        [[[ClassFactory getInstance] getNetComm] showHud:nil];
        NSLog(@"周边检索发送成功");
    }
    else
    {
        NSLog(@"周边检索发送失败");
        NSDictionary *returnMutDic = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",@"检索发起失败!",@"message", nil];
        
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:returnMutDic];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:self.urlCommand.callbackId];
    }
}

#pragma mark -
#pragma mark - BMKPoiSearchDelegate
//实现PoiSearchDeleage处理回调结果
/**
 *返回POI搜索结果
 *@param searcher 搜索对象
 *@param poiResult 搜索结果列表
 *@param errorCode 错误号，@see BMKSearchErrorCode
 */
- (void)onGetPoiResult:(BMKPoiSearch*)searcher result:(BMKPOISearchResult*)poiResult errorCode:(BMKSearchErrorCode)errorCode
{
    if (errorCode == BMK_SEARCH_NO_ERROR)
    {
        NSMutableArray *locationsMutArr = [NSMutableArray array];
        for (BMKPoiInfo *poiInfo in poiResult.poiInfoList)
        {
            /// POI名称
            NSString *name = [NSString stringWithFormat:@"%@",poiInfo.name];
            STRING_NIL_TO_NONE(name)
            /// POI坐标
            NSString *coordinate2D = [NSString stringWithFormat:@"%f %f",poiInfo.pt.latitude,poiInfo.pt.longitude];
            STRING_NIL_TO_NONE(coordinate2D)
            /// POI地址信息
            NSString *address = [NSString stringWithFormat:@"%@",poiInfo.address];
            STRING_NIL_TO_NONE(address)
            /// POI电话号码
            NSString *phone = [NSString stringWithFormat:@"%@",poiInfo.phone];
            STRING_NIL_TO_NONE(phone)
            /// POI唯一标识符uid
            NSString *UID = [NSString stringWithFormat:@"%@",poiInfo.UID];
            STRING_NIL_TO_NONE(UID)
            /// POI所在省份
           NSString *province = [NSString stringWithFormat:@"%@",poiInfo.province];
            STRING_NIL_TO_NONE(province)
            /// POI所在城市
            NSString *city = [NSString stringWithFormat:@"%@",poiInfo.city];
            STRING_NIL_TO_NONE(city)
            /// POI所在行政区域
            NSString *area = [NSString stringWithFormat:@"%@",poiInfo.area];
            STRING_NIL_TO_NONE(area)
            
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:name,@"name",coordinate2D,@"coordinate2D",address,@"address",phone,@"phone",UID,@"UID",province,@"province",city,@"city",area,@"area", nil];
            
            [locationsMutArr addObject:dic];
        }
        
        NSDictionary *returnMutDic = [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"resultCode",locationsMutArr,@"data",@"获取数据成功!",@"message", nil];
        
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:returnMutDic];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:self.urlCommand.callbackId];

    }
    else
    {
        NSDictionary *returnMutDic = [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"resultCode",@"获取数据失败!",@"message", nil];
        
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                messageAsDictionary:returnMutDic];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:self.urlCommand.callbackId];

    }
    [[[ClassFactory getInstance] getNetComm] hiddenHud];
}

/**
 *返回POI详情搜索结果
 *@param searcher 搜索对象
 *@param poiDetailResult 详情搜索结果
 *@param errorCode 错误号，@see BMKSearchErrorCode
 */
- (void)onGetPoiDetailResult:(BMKPoiSearch*)searcher result:(BMKPOIDetailSearchResult*)poiDetailResult errorCode:(BMKSearchErrorCode)errorCode
{
    
}


@end
