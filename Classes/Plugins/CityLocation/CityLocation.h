//
//  CityLocation.h
//  CRM
//
//  Created by 林科 on 16/1/28.
//  Copyright © 2016年 FBI01. All rights reserved.
//

#import <Cordova/CDV.h>
#import <Cordova/CDVPlugin.h>

#import <BaiduMapAPI_Base/BMKBaseComponent.h>
#import <BaiduMapAPI_Search/BMKSearchComponent.h>

@interface CityLocation : CDVPlugin<BMKPoiSearchDelegate>
{
    
}

@property(nonatomic, strong) CDVInvokedUrlCommand *urlCommand;

- (void)getLocation:(CDVInvokedUrlCommand*)command;

- (void)getLocationList:(CDVInvokedUrlCommand*)command;

@end
