//
//  CDVBasePlugin.h
//  IOSFramework
//
//  Created by 林科 on 2018/9/22.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import <Cordova/CDV.h>
#import <Cordova/CDVPlugin.h>

NS_ASSUME_NONNULL_BEGIN

@interface CDVBasePlugin : CDVPlugin
{
    
}

//Cordova 参数
@property(nonatomic, strong) CDVInvokedUrlCommand *command;

@end

NS_ASSUME_NONNULL_END
