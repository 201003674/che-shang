//
//  QuickPickImage.h
//  IOSFramework
//
//  Created by 林科 on 2018/8/21.
//  Copyright © 2018年 allianture. All rights reserved.
//
#import <Cordova/CDV.h>
#import <Cordova/CDVPlugin.h>

#import "UIImage+Utility.h"
#import "UIImage+Extension.h"

#import "QuickImagePickerController.h"
@interface QuickPickImage : CDVPlugin<UIActionSheetDelegate,QuickImagePickerControllerDelegate>
{
    //是否添加水印
    NSString *isAddWatermark;
    
    //是否需要保存相册
    NSString *isAllowSaveToAlbum;
}
//快捷拍照
- (void)addQuickImageTXD:(CDVInvokedUrlCommand*)command;

@end
