//
//  QuickPickImage.m
//  IOSFramework
//
//  Created by 林科 on 2018/8/21.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import "QuickPickImage.h"

@implementation QuickPickImage

//快捷拍照
- (void)addQuickImageTXD:(CDVInvokedUrlCommand*)command
{
    /*
     0 是否打开相册 1 保存相册 2 添加水印
     */
    
    //0 相册; 1 相机; 2 相册/相机
    NSString *pickerCameraType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(pickerCameraType)
    
    //是否图片保存到相册
    isAllowSaveToAlbum = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(isAllowSaveToAlbum)
    
    //是否添加水印功能
    isAddWatermark = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
    STRING_NIL_TO_NONE(isAddWatermark)
    
    if ([pickerCameraType isEqualToString:@"0"])
    {
        UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                                            initWithTitle:nil
                                            delegate:self
                                            cancelButtonTitle:@"取消"
                                            destructiveButtonTitle:nil
                                            otherButtonTitles:@"从手机相册获取",nil];
        actionSheet.command = command;
        actionSheet.isAllowLocation = isAddWatermark;
        
        [actionSheet showInView:self.viewController.view];
    }
    else if ([pickerCameraType isEqualToString:@"1"])
    {
        UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                                            initWithTitle:nil
                                            delegate:self
                                            cancelButtonTitle:@"取消"
                                            destructiveButtonTitle:nil
                                            otherButtonTitles: @"打开照相机",nil];
        actionSheet.command = command;
        actionSheet.isAllowLocation = isAddWatermark;
        
        [actionSheet showInView:self.viewController.view];
    }
    else
    {
        UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                                            initWithTitle:nil
                                            delegate:self
                                            cancelButtonTitle:@"取消"
                                            destructiveButtonTitle:nil
                                            otherButtonTitles: @"打开照相机", @"从手机相册获取",nil];
        actionSheet.command = command;
        actionSheet.isAllowLocation = isAddWatermark;
        
        [actionSheet showInView:self.viewController.view];
    }
}

#pragma mark -
#pragma mark - UICustomActionSheetDelegate
-(void)actionSheet:(UICustomActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex)
    {
        NSString *index = [NSString stringWithFormat:@"%@",[actionSheet.command argumentAtIndex:4]];
        STRING_NIL_TO_NONE(index);
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"用户取消操作",@"message",index,@"index", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
        
        return;
    }
    
    if ([actionSheet.isAllowLocation isEqualToString:@"1"] && [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        [[[ClassFactory getInstance] getInfoHUD] showHud:@"请允许应用打开定位功能，否则无法添加地理位置信息。"];
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"请允许应用打开定位功能，否则无法添加地理位置信息。",@"message", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
        
        return;
    }
    
    QuickImagePickerController *imagePicker = [[QuickImagePickerController alloc]init];
    imagePicker.view.backgroundColor = [UIColor whiteColor];
    imagePicker.delegate = self;
    imagePicker.command = actionSheet.command;
    
    [self.viewController presentViewController:imagePicker animated:YES completion:nil];
}

#pragma mark -
#pragma mark - QuickImagePickerControllerDelegate
- (void)getImageData:(NSData *)data
{
    NSDateFormatter *creatDateFormatter = [[NSDateFormatter alloc] init];
    [creatDateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString *dateFormatterStr = [NSString stringWithFormat:@"%@",[creatDateFormatter stringFromDate:[NSDate date]]];
    
    NSString *userNameStr = [NSString stringWithFormat:@"%@_%u.png", dateFormatterStr,(arc4random() % 501) + 500];
    STRING_NIL_TO_NONE(userNameStr);
    
    UIImage *theImage = [UIImage imageWithData:data];
    
    //图片处理
    [self saveImage:theImage WithNameStr:userNameStr];
}

-(void)imagePickerDidCancel:(QuickImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
        NSString *index = [NSString stringWithFormat:@"%@",[picker.command argumentAtIndex:4]];
        STRING_NIL_TO_NONE(index);
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"用户取消操作",@"message",index,@"index", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:picker.command.callbackId];
    }];
}

#pragma mark -
#pragma mark - saveImage
- (void)saveImage:(UIImage *)theImage WithNameStr:(NSString *)userNameStr
{
    UIImage *finialTempImage = nil;
    
    NSString *createDate = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLogic] getNowTimeString:@"YYYY-MM-dd HH:mm:ss"]];
    
    if ([isAddWatermark isEqualToString:@"1"])
    {
        //验车照片 添加水印图片 以及 时间戳
        UIImage *waterMask = [UIImage imageNamed:@"waterMask.png"];
        
        CGFloat width = (theImage.size.height - 20)/20.0;
        CGFloat height = (theImage.size.height - 20)/20.0;
        
        CGFloat x = 10;
        CGFloat y = theImage.size.height - 10 - height;
        
        NSString *location = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getAddress]];
        STRING_ADDRESS_UNABLE_NONE(location);
        
        NSString *locationString = [NSString stringWithFormat:@"地点：%@",location];
        STRING_NIL_TO_NONE(locationString);
        
        NSString *timeString = [NSString stringWithFormat:@"时间：%@",createDate];
        STRING_NIL_TO_NONE(timeString)
        
        //时间
        CGFloat time_x = x + width + 10;
        CGFloat time_y = y;
        CGFloat time_width = theImage.size.width - width - 20.0;
        CGFloat time_height = height/2.0;
        
        //地点
        CGFloat location_x = x + width + 10;
        CGFloat location_y = y + time_height;
        CGFloat location_width = theImage.size.width - width - 20.0;
        CGFloat location_height = height/2.0;
        
        CGFloat fontSize = width/2.0;
        
        finialTempImage = [theImage imageWithWaterMask:waterMask inRect:CGRectMake(x, y, width, height) WithStringWaterTimeMarkString:timeString atTimeRect:CGRectMake(time_x, time_y, time_width, time_height) WithStringWaterLocationString:locationString atLocationRect:CGRectMake(location_x, location_y, location_width, location_height) color:[UIColor redColor] font:[UIFont systemFontOfSize:fontSize]];
    }
    else
    {
        finialTempImage = [[UIImage alloc] initWithCGImage:theImage.CGImage];
    }
    
    //保存相册
    if ([isAllowSaveToAlbum isEqualToString:@"1"])
    {
        UIImageWriteToSavedPhotosAlbum(finialTempImage,self,@selector(imageSavedToPhotosAlbum:didFinishSavingWithError:contextInfo:),nil);
    }
}

#pragma mark -
#pragma mark - 保存图片后的回调
- (void)imageSavedToPhotosAlbum:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(id)contextInfo
{
    if(!error)
    {
        [[[ClassFactory getInstance] getInfoHUD] showHud:@"图片保存相册成功！"];
    }
}

@end
