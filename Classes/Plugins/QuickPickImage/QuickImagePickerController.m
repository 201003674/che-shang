//
//  QuickImagePickerViewtrollerViewController.m
//  Cmpi
//
//  Created by hdl on 14-12-18.
//
//
#define Color [UIColor colorWithRed:76/255.0 green:213/255.0 blue:68/255.0 alpha:1]
#define FalseColor [UIColor colorWithRed:255/255.0 green:0/255.0 blue:0/255.0 alpha:1]
#import "QuickImagePickerController.h"
//@interface QuickImagePickerController
//{
//    BOOL isSelected;
//    UIButton* flashLight;
//    CAShapeLayer *pathLayer;
//}
//@property(nonatomic,strong)UIImageView* positionView;
//@property(nonatomic,strong)UILabel* label;
//@property(nonatomic,assign)CATransform3D labelTransform;
//
//@end
@implementation QuickImagePickerController
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.session) {
        [self.session startRunning];
    }
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initialSession];
    
    [self setUpCameraLayer];
    
    //取消
    UIButton* retakePic=[UIButton buttonWithType:UIButtonTypeCustom];
    [retakePic setTitle:@"取消" forState:UIControlStateNormal];
    [retakePic setFrame:CGRectMake(20,SCREEN_HEIGHT - 50, 50, 50)];
    [retakePic setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [retakePic addTarget:self action:@selector(closeView) forControlEvents:UIControlEventTouchUpInside];
    //拍照
    UIImage *camerImage = [UIImage imageNamed:@"camera_shoot.png"];
    UIButton *cameraBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cameraBtn setFrame:CGRectMake(0, 0, camerImage.size.width, camerImage.size.height)];
    [cameraBtn setCenter:CGPointMake(SCREEN_WIDTH/2.0, SCREEN_HEIGHT - camerImage.size.height/2 - 10)];
    [cameraBtn setImage:camerImage forState:UIControlStateNormal];
    [cameraBtn addTarget:self action:@selector(shutterCamera) forControlEvents:UIControlEventTouchUpInside];
    //聚光灯
    flashLight = [UIButton buttonWithType:UIButtonTypeCustom];
    [flashLight setImage:[UIImage imageNamed:@"lampClose"] forState:UIControlStateNormal];
    [flashLight setImage:[UIImage imageNamed:@"lampOpen"] forState:UIControlStateSelected];
    [flashLight setContentMode:UIViewContentModeScaleAspectFit];
    [flashLight setFrame:CGRectMake(VIEWSCREEN_WIDTH-flashLight.imageView.image.size.height*0.4-5,10,flashLight.imageView.image.size.width*0.45,flashLight.imageView.image.size.height*0.45)];
    flashLight.layer.transform=CATransform3DMakeRotation(M_PI_2, 0, 0, 1);
    [flashLight setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [flashLight addTarget:self action:@selector(toggleFlashlight) forControlEvents:UIControlEventTouchUpInside];
    [self createPositionView];
    [self.view addSubview:flashLight];
    [self.view addSubview:retakePic];
    [self.view addSubview:cameraBtn];
    self.view.backgroundColor = [UIColor clearColor];
}

- (void)toggleFlashlight
{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    if (![device hasTorch]) {
        
        //[LogHelper cpicLog:@"no torch"];
       
    }else
    {
        [device lockForConfiguration:nil];
        
        if (device.torchMode==AVCaptureTorchModeOff) {
            [device setTorchMode: AVCaptureTorchModeOn];
            //[device setFlashMode:AVCaptureFlashModeOn];
            flashLight.selected=YES;
            
        }
        else
        {
            [device setTorchMode: AVCaptureTorchModeOff];
            //[device setFlashMode:AVCaptureFlashModeOff];
            flashLight.selected=NO;
        }
        
        [device unlockForConfiguration];
    }
}

-(void)createPositionView
{
    //***************框
    // 中间空心洞的区域
   
    NSUInteger frameWid =self.view.frame.size.width-40;
    float frameHei = SCREEN_HEIGHT-72-50;
    CGRect cutRect = CGRectMake(20,
                              72,
                              frameWid,
                              frameHei);
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:self.view.frame];
    // 挖空心洞 显示区域
    UIBezierPath *cutRectPath = [UIBezierPath bezierPathWithRect:cutRect];
    //        将circlePath添加到path上
    [path appendPath:cutRectPath];
    path.usesEvenOddFillRule = YES;
    
    CAShapeLayer *fillLayer = [CAShapeLayer layer];
    fillLayer.path = path.CGPath;
    fillLayer.fillRule = kCAFillRuleEvenOdd;
    fillLayer.opacity = 0.5;//透明度
    fillLayer.backgroundColor = [UIColor lightGrayColor].CGColor;
    [self.view.layer addSublayer:fillLayer];
    
    // 边界校准线
    const CGFloat lineWidth = 2;
    UIBezierPath *linePath = [UIBezierPath bezierPathWithRect:CGRectMake(cutRect.origin.x - lineWidth,
                                                                         cutRect.origin.y - lineWidth,
                                                                         cutRect.size.width / 5.0,
                                                                         lineWidth)];
    //        追加路径
    [linePath appendPath:[UIBezierPath bezierPathWithRect:CGRectMake(cutRect.origin.x - lineWidth,
                                                                     cutRect.origin.y - lineWidth,
                                                                     lineWidth,
                                                                     cutRect.size.height / 5.0)]];
    
    [linePath appendPath:[UIBezierPath bezierPathWithRect:CGRectMake(cutRect.origin.x + frameWid - cutRect.size.width / 5.0 + lineWidth,
                                                                     cutRect.origin.y - lineWidth,
                                                                     cutRect.size.width / 5.0,
                                                                     lineWidth)]];
    [linePath appendPath:[UIBezierPath bezierPathWithRect:CGRectMake(cutRect.origin.x + frameWid ,
                                                                     cutRect.origin.y - lineWidth,
                                                                     lineWidth,
                                                                     cutRect.size.height / 5.0)]];
    
    [linePath appendPath:[UIBezierPath bezierPathWithRect:CGRectMake(cutRect.origin.x - lineWidth,
                                                                     cutRect.origin.y + frameHei - cutRect.size.height / 5.0 + lineWidth,
                                                                     lineWidth,
                                                                     cutRect.size.height / 5.0)]];
    [linePath appendPath:[UIBezierPath bezierPathWithRect:CGRectMake(cutRect.origin.x - lineWidth,
                                                                     cutRect.origin.y + frameHei,
                                                                     cutRect.size.width / 5.0,
                                                                     lineWidth)]];
    
    [linePath appendPath:[UIBezierPath bezierPathWithRect:CGRectMake(cutRect.origin.x + frameWid,
                                                                     cutRect.origin.y + frameHei - cutRect.size.height / 5.0 + lineWidth,
                                                                     lineWidth,
                                                                     cutRect.size.height / 5.0)]];
    [linePath appendPath:[UIBezierPath bezierPathWithRect:CGRectMake(cutRect.origin.x + frameWid - cutRect.size.width / 5.0 + lineWidth,
                                                                     cutRect.origin.y + frameHei,
                                                                     cutRect.size.width / 5.0,
                                                                     lineWidth)]];
    
    pathLayer = [CAShapeLayer layer];
    pathLayer.path = linePath.CGPath;// 从贝塞尔曲线获取到形状
    pathLayer.fillColor = Color.CGColor; // 闭环填充的颜色
    //        pathLayer.lineCap       = kCALineCapSquare;               // 边缘线的类型
    //        pathLayer.strokeColor = [UIColor blueColor].CGColor; // 边缘线的颜色
    //        pathLayer.lineWidth     = 4.0f;                           // 线条宽度
    [self.view.layer addSublayer:pathLayer];
}

-(void)closeView
{
    if ([self.delegate respondsToSelector:@selector(imagePickerDidCancel:)])
    {
        [self.delegate imagePickerDidCancel:self];
    }
    else
    {
         [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark -拍照获取照片
- (void)shutterCamera
{
    AVCaptureConnection * videoConnection = [self.stillImageOutput connectionWithMediaType:AVMediaTypeVideo];
    if (!videoConnection)
    {
        return;
    }
    
    [self.stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
        if (imageDataSampleBuffer == NULL) {
            return;
        }
        
        NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
        
        [self.delegate getImageData:imageData];
    }];
}

- (void) setUpCameraLayer
{
    if (self.previewLayer == nil) {
        self.previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.session];
        if(self.cameraShowView==nil){
            self.cameraShowView = self.view;
        }
        UIView * view = self.cameraShowView;
        
        CALayer * viewLayer = [view layer];
        [viewLayer setMasksToBounds:YES];
        
        CGRect bounds = [view bounds];
        [self.previewLayer setFrame:bounds];
        [self.previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        
        [viewLayer insertSublayer:self.previewLayer below:[[viewLayer sublayers] objectAtIndex:0]];
    }
}

- (void) initialSession
{
    //这个方法的执行我放在init方法里了
    self.session = [[AVCaptureSession alloc] init];
    self.videoInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self backCamera] error:nil];
    //[self fronCamera]方法会返回一个AVCaptureDevice对象，因为我初始化时是采用前摄像头，所以这么写，具体的实现方法后面会介绍
    self.stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary * outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys:AVVideoCodecJPEG,AVVideoCodecKey, nil];
    //这是输出流的设置参数AVVideoCodecJPEG参数表示以JPEG的图片格式输出图片
    
    
    [self.stillImageOutput setOutputSettings:outputSettings];
    
    //设置拍照质量

    self.session.sessionPreset = AVCaptureSessionPresetHigh;

    [self.videoInput.device addObserver:self forKeyPath:@"adjustingFocus" options:NSKeyValueObservingOptionNew context:nil];
    
    if ([self.session canAddInput:self.videoInput]) {
        [self.session addInput:self.videoInput];
        
    }
    if ([self.session canAddOutput:self.stillImageOutput]) {
        [self.session addOutput:self.stillImageOutput];
    }
    
}

- (AVCaptureDevice *)cameraWithPosition:(AVCaptureDevicePosition) position {
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices) {
        if ([device position] == position) {
            return device;
        }
    }
    return nil;
}


- (AVCaptureDevice *)frontCamera {
    return [self cameraWithPosition:AVCaptureDevicePositionFront];
}

- (AVCaptureDevice *)backCamera {
    return [self cameraWithPosition:AVCaptureDevicePositionBack];
}



#pragma mark - 对焦
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isKindOfClass:[AVCaptureDevice class]])
    {
        if ([keyPath isEqualToString:@"adjustingFocus"])
        {
            BOOL adjustingFocus=[[change objectForKey:NSKeyValueChangeNewKey] isEqualToNumber:[NSNumber numberWithInt:1]];
            
            if (adjustingFocus) {
                
                pathLayer.fillColor = FalseColor.CGColor;
                
               
               
            }
            else
            {
                pathLayer.fillColor = Color.CGColor;
                
               
               
            }
        }
        
    }
}

#pragma mark - 手动设置对焦
-(void)tapClick:(UITapGestureRecognizer*)tapGestureRecognizer
{
    CGPoint touchPoint=[tapGestureRecognizer locationInView:self.view];
    //先锁定
    [self.videoInput.device lockForConfiguration:NULL];
    //设置对焦模式
    //[self.videoInput.device setFocusMode:AVCaptureFocusModeContinuousAutoFocus];
    //设置对焦点
    [self.videoInput.device setFocusPointOfInterest:touchPoint];
    //解锁
    [self.videoInput.device unlockForConfiguration];
    
    //[LogHelper cpicLog:@"手动对焦-----------"];

}
#pragma mark - 获取当前设备大小
-(NSString*)whichIphoneSize
{
    CGSize size=[UIScreen mainScreen].bounds.size;
    if (size.width==320 && size.height==480) {
        return @"iphone4And4s";
    }
    else if(size.width==320 && size.height==568)
    {
        return @"iphone5And5s";
    }
    else if(size.width==375 && size.height==667)
    {
        return @"iphone6";
    }else if(size.width==414 && size.height==736)
    {
        return @"iphone6Plus";
    }
    else if(size.width==375 && size.height==812)
    {
        return @"iphoneX";
    }
    return nil;
    
}
- (void)dealloc
{
    [self.videoInput.device removeObserver:self forKeyPath:@"adjustingFocus" context:nil];
    
}
@end
