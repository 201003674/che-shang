//
//  CDQueriesSchemes.h
//  IOSFramework
//
//  Created by 林科 on 2018/12/3.
//  Copyright © 2018 allianture. All rights reserved.
//

#import "CDVBasePlugin.h"
#import <MapKit/MKMapItem.h>
NS_ASSUME_NONNULL_BEGIN

@interface CDQueriesSchemes : CDVBasePlugin<UIActionSheetDelegate>
{
    
}

- (void)openSchemes:(CDVInvokedUrlCommand*)command;

@end

NS_ASSUME_NONNULL_END
