//
//  CDQueriesSchemes.m
//  IOSFramework
//
//  Created by 林科 on 2018/12/3.
//  Copyright © 2018 allianture. All rights reserved.
//

#import "CDQueriesSchemes.h"

@implementation CDQueriesSchemes

- (void)openSchemes:(CDVInvokedUrlCommand*)command
{
    // 0 起点经度 1 起点维度 2 终点经度 3 终点维度
    UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                                        initWithTitle:nil
                                        delegate:self
                                        cancelButtonTitle:@"取消"
                                        destructiveButtonTitle:nil
                                        otherButtonTitles: @"百度地图",@"高德地图",@"苹果地图",nil];
    actionSheet.command = command;
    
    [actionSheet showInView:self.viewController.view];
}

#pragma mark -
#pragma mark - UICustomActionSheetDelegate
-(void)actionSheet:(UICustomActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex)
    {
        NSString *index = [NSString stringWithFormat:@"%@",[actionSheet.command argumentAtIndex:4]];
        STRING_NIL_TO_NONE(index);
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"用户取消操作",@"message",index,@"index", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
        
        return;
    }
    
    //打开三方地图
    BOOL success = [self openSchemesAction:actionSheet.command Index:buttonIndex];
    
    if (success)
    {
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsString:@"打开成功!"];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
    }
    else
    {
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsString:@"打开失败!"];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
    }
}

#pragma mark -
#pragma mark - openSchemesAction
- (BOOL )openSchemesAction:(CDVInvokedUrlCommand *)command Index:(NSInteger)buttonIndex
{
    //起点
    NSString *fromLatitude = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(fromLatitude);
    NSString *fromLongitude = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(fromLongitude);
    //终点
    NSString *toLatitude = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
    STRING_NIL_TO_NONE(toLatitude);
    NSString *toLongitude = [NSString stringWithFormat:@"%@",[command argumentAtIndex:3]];
    STRING_NIL_TO_NONE(toLongitude);
    
    // 打开地图的优先级顺序：百度地图->高德地图->苹果地图
    if (buttonIndex == 0)
    {
        // 百度地图
        NSString *urlString = [[NSString stringWithFormat:@"baidumap://map/direction?origin=%@,%@&destination=%@,%@&mode=riding&src=",fromLatitude,fromLongitude, toLatitude, toLongitude] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:urlString]])
        {
            return [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        }
        else
        {
            // 没有安装上面三种地图APP，弹窗提示安装地图APP
            [[[ClassFactory getInstance] getInfoHUD] showHud:@"未找到指定应用程序，请到AppStore市场进行下载安装！"];
            
            return false;
        }
    }
    else if (buttonIndex == 1)
    {
        // 高德地图
        NSString *urlString = [[NSString stringWithFormat:@"iosamap://path?sourceApplication=移动CRM&sid=BGVIS1&slat=%@&slon=%@&sname=起点&did=BGVIS2&dlat=%@&dlon=%@&dname=终点&dev=0&t=0",fromLatitude,fromLongitude, toLatitude, toLongitude] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:urlString]])
        {
            return [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        }
        else
        {
            // 没有安装上面三种地图APP，弹窗提示安装地图APP
            [[[ClassFactory getInstance] getInfoHUD] showHud:@"未找到指定应用程序，请到AppStore市场进行下载安装！"];
            
            return false;
        }
    }
    else if (buttonIndex == 2)
    {
        // 苹果地图
        MKMapItem *currentLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(fromLatitude.floatValue, fromLongitude.floatValue)] ];

        CLLocationCoordinate2D desCorrdinate = CLLocationCoordinate2DMake(toLatitude.floatValue, toLongitude.floatValue);
        MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:desCorrdinate addressDictionary:nil]];
        
        //默认驾车
        BOOL canOpenSuccess = [MKMapItem openMapsWithItems:@[currentLocation, toLocation]
                       launchOptions:@{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving,
                                       MKLaunchOptionsMapTypeKey:[NSNumber numberWithInteger:0],
                                       MKLaunchOptionsShowsTrafficKey:[NSNumber numberWithBool:YES]}];
        
        if (canOpenSuccess)
        {
            return canOpenSuccess;
        }
        else
        {
            // 没有安装上面三种地图APP，弹窗提示安装地图APP
            [[[ClassFactory getInstance] getInfoHUD] showHud:@"未找到指定应用程序，请到AppStore市场进行下载安装！"];
            
            return false;
        }
    }
    else
    {
        // 没有安装上面三种地图APP，弹窗提示安装地图APP
        [[[ClassFactory getInstance] getInfoHUD] showHud:@"未找到指定应用程序，请到AppStore市场进行下载安装！"];
        
        return false;
    }
}

@end
