//
//  CDVWeiXinPlugin.h
//  IOSFramework
//
//  Created by 林科 on 2019/5/31.
//  Copyright © 2019 allianture. All rights reserved.
//

#import "CDVBasePlugin.h"

#import "WXApi.h"
@interface CDVWeiXinPlugin : CDVBasePlugin<WXApiManagerDelegate>
{
    
}

-(void)openWeiXinSmallProgram:(CDVInvokedUrlCommand*)command;


@end
