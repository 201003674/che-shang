//
//  CDVWeiXinPlugin.m
//  IOSFramework
//
//  Created by 林科 on 2019/5/31.
//  Copyright © 2019 allianture. All rights reserved.
//

#import "CDVWeiXinPlugin.h"

@implementation CDVWeiXinPlugin

-(void)openWeiXinSmallProgram:(CDVInvokedUrlCommand*)command
{
    if (![WXApi isWXAppInstalled])
    {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                          messageAsString:@"微信未安装!"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
        return;
    }
    
    [[WXApiManager sharedManager] setDelegate:self];
    
    WXLaunchMiniProgramReq *launchMiniProgramReq = [WXLaunchMiniProgramReq object];
    
    //拉起的小程序的username
    NSString *userName = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(userName);
    
    launchMiniProgramReq.userName = userName;
    
    //拉起小程序页面的可带参路径，不填默认拉起小程序首页
    NSString *path = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(path);
    launchMiniProgramReq.path = path;
    
    //拉起小程序的类型 0 正式版 1 开发版 2 验版
    NSString *miniProgramType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
    STRING_NIL_TO_NONE(miniProgramType);
    launchMiniProgramReq.miniProgramType = miniProgramType.integerValue;
    
    if([WXApi sendReq:launchMiniProgramReq])
    {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                          messageAsString:@"调用成功"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
    else
    {
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                          messageAsString:@"调用失败"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

#pragma mark -
#pragma mark - WXApiManagerDelegate
/*! @brief 收到一个来自微信的请求，第三方应用程序处理完后调用sendResp向微信发送结果
 *
 * 收到一个来自微信的请求，异步处理完成后必须调用sendResp发送处理结果给微信。
 * 可能收到的请求有GetMessageFromWXReq、ShowMessageFromWXReq等。
 * @param req 具体请求内容，是自动释放的
 */
-(void)onReq:(BaseReq*)req;
{
    
}

/*! @brief 发送一个sendReq后，收到微信的回应
 *
 * 收到一个来自微信的处理结果。调用一次sendReq后会收到onResp。
 * 可能收到的处理结果有SendMessageToWXResp、SendAuthResp等。
 * @param resp具体的回应内容，是自动释放的
 */
-(void)onResp:(BaseResp*)resp
{
    
}

@end
