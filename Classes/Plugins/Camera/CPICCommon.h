//
//  CPICCommon.h
//  CordovaLib
//
//  Created by IBH1 on 14-10-13.
//
//

#import <Foundation/Foundation.h>

#define ISFIRST @"cncpic.plist"
#define LOACTION @"location.plist"
#define DEVICE @"CPICDevice.plist"
#define SSOPARAMS @"SSOParams.plist"
#define CNCPICFLODER @"cncpic"
@interface CPICCommon : NSObject
#pragma 获取指定key值的对应的URL
+(NSString*)getURL: (NSString*) key;
+(NSString*)getAPPID:(NSString*) key;

@end
