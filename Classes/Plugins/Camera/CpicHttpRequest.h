//
//  HttpRequest.h
//  CordovaLib
//
//  Created by hdl on 14-10-20.
//
//

#import <Foundation/Foundation.h>
@protocol NetUploadDelegate <NSObject>
/*通知页面加载*/
- (void)getDataSuccess:(id)responses;
- (void)getDataError:(id)responses;
@end
@interface CpicHttpRequest : NSObject<NSURLConnectionDelegate>
-(id)upload:(NSString *)url widthParams:(NSDictionary *)params andFileName:(NSString *)fileName;
@property id<NetUploadDelegate> delegate;
@end
