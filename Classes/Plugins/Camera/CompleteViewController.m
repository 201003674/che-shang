//
//  CompleteViewController.m
//  Cmpi
//
//  Created by IBH-02 on 14-12-19.
//
//

#import "CompleteViewController.h"
#import "DLImagePickerController.h"

@interface CompleteViewController ()

@end

@implementation CompleteViewController
- (instancetype)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor grayColor];

    UIButton* reTake=[UIButton buttonWithType:UIButtonTypeCustom];
    [reTake setTitle:@"重拍" forState:UIControlStateNormal];
    
    reTake.frame= CGRectMake(20,self.view.frame.size.height-70 , 60, 60);
    
    [reTake setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [reTake addTarget:self action:@selector(ComReTakePicture) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton* usePic=[UIButton buttonWithType:UIButtonTypeCustom];
    [usePic setTitle:@"使用图片" forState:UIControlStateNormal];
    
    usePic.frame= CGRectMake(self.view.frame.size.width-130,self.view.frame.size.height-70 , 110, 60);
    
    [usePic setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [usePic addTarget:self action:@selector(usePicture) forControlEvents:UIControlEventTouchUpInside];
    
    _imageView=[[UIImageView alloc]init];
    
    _imageView.frame=self.view.frame;
    
    _imageView.contentMode=UIViewContentModeScaleAspectFit;
    
    _imageView.center=CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0);
    
    _imageView.image=_image;
    
    
    [self.view addSubview:_imageView];
    
    [self.view addSubview:reTake];
    
    [self.view addSubview:usePic];
    
    
    
}


-(void)ComReTakePicture
{
    [self.delegate SubComReTakePicture];
}

-(void)usePicture
{
     [self.delegate SubusePicture:_image];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 获取当前设备大小
-(NSString*)whichIphoneSize
{
    CGSize size=[UIScreen mainScreen].bounds.size;
    if (size.width==320 && size.height==480) {
        return @"iphone4And4s";
    }
    else if(size.width==320 && size.height==568)
    {
        return @"iphone5And5s";
    }
    else if(size.width==375 && size.height==667)
    {
        return @"iphone6";
    }else if(size.width==414 && size.height==736)
    {
        return @"iphone6Plus";
    }
    else if(size.width==414 && size.height==736)
    {
        return @"iphone6Plus";
    }
    else if(size.width==375 && size.height==812)
    {
        return @"iphoneX";
    }
    
    return nil;
    
}

-(void)setImage:(UIImage *)image
{
    CGSize size = image.size;
    //    float x = [UIScreen mainScreen].bounds.size.width/320;
    //    float y = [UIScreen mainScreen].bounds.size.height/568;
    //  rect = CGRectMake( (size.height - size.height*5/7)*y/2,(size.width - size.width*9/10)*x/2,size.height*5/7*y,size.width*9/10*x);
    CGRect rect;
    
//    if ([[self whichIphoneSize] isEqualToString:@"iphone4And4s"]) {
//        rect = CGRectMake( (size.height - size.height*4.5/7)/2,(size.width - size.width*8/10)/2,size.height*4.5/7,size.width*8/10);
//    }
//    else if ([[self whichIphoneSize] isEqualToString:@"iphone5And5s"])
//    {
//        rect = CGRectMake( (size.height - size.height*4.5/7)/2,(size.width - size.width*8/10)/2,size.height*4.5/7,size.width*8/10);
//    }
//    else
//    {
//        rect = CGRectMake( (size.height - size.height*5/7)/2,(size.width - size.width*9/10)/2,size.height*5/7,size.width*9/10);
//    }
    
    NSUInteger sscale= [UIScreen mainScreen].scale;
   rect = CGRectMake(72*sscale,20*sscale,size.height-122*sscale,size.width-40*sscale);
    image = [self imageFromImage:image inRect:rect];
    ////[LogHelper cpicLog:[NSString stringWithFormat:@"rect = %@,mainScreen = %@",NSStringFromCGRect(rect),NSStringFromCGRect([UIScreen mainScreen].bounds)]];
    if(_imageView!=nil){
        //[LogHelper cpicLog:@"come in the _imageView"];
        _imageView.image = image;
    }
    //[LogHelper cpicLog:[NSString stringWithFormat:@"the image is %@",image==nil?@"nil":@"not nil"]];
    _image = image;
}

/**
 *从图片中按指定的位置大小截取图片的一部分
 * UIImage image 原始的图片
 * CGRect rect 要截取的区域
 */
- (UIImage *)imageFromImage:(UIImage *)image inRect:(CGRect)rect {
    CGImageRef sourceImageRef = [image CGImage];
    CGImageRef newImageRef = CGImageCreateWithImageInRect(sourceImageRef, rect);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    return newImage;
}
@end
