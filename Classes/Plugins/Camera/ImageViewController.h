//
//  ImageViewController.h
//  sxtbpzsc
//
//  Created by IBH1 on 14-7-25.
//  Copyright (c) 2014年 IBH1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCamera.h"

@interface ImageViewController : UIViewController

@property NSString *imageName;
@property NSString *appId;
@property MCamera *viewController;

@property (nonatomic,retain) UIImageView *imageView;
@property (nonatomic,retain) UIButton *btn_delete,*btn_left,*btn_right,*btn_sure,*btn_return;

@end
