//
//  ImageViewController.m
//  sxtbpzsc
//  显示图片并对图片进行旋转操作
//  Created by 阳葵 on 14-7-25.
//  Copyright (c) 2014年 IBH1. All rights reserved.
//

#import "ImageViewController.h"
#import "ImageUtil.h"
#import "MCamera.h"

@interface ImageViewController ()


@property UIImage *image;
@property BOOL hasModify ;//是否已经修改过了

@end

@implementation ImageViewController

#define WIDTH 300
#define HEIGHT 300
#define BTNWIDTH 70
#define BTNHEIGHT 40
CGRect img_frame;
float scale;
bool isProtrait = YES;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    isProtrait = YES;
    NSString *filePath = [ImageUtil getImagePath:_imageName appId:_appId];
    NSLog(@"####filePath%@",filePath);
    _image = [UIImage imageWithContentsOfFile:filePath];
    NSLog(@"--------imageSize = %f*%f",_image.size.width,_image.size.height);
    [self imageFrameInit:_image.size.width andHeight:_image.size.height];
    CGRect rx = [[ UIScreen mainScreen ] bounds];
    CGSize size = rx.size;
    
    CGFloat viewWidth =   size.width;
    CGFloat viewHeight = size.height;
    
    CGFloat left =(viewWidth-viewWidth*0.98);

    _imageView = [[UIImageView alloc] initWithFrame:img_frame];
    [_imageView setImage:_image];
    _btn_delete = [[UIButton alloc] initWithFrame:CGRectMake(left, (viewHeight*0.9), BTNWIDTH, BTNHEIGHT)];
    _btn_delete.backgroundColor = [UIColor blueColor];
    [_btn_delete setTitle:@"删除" forState:UIControlStateNormal];
    [_btn_delete addTarget:self action:@selector(deleteClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _btn_left = [[UIButton alloc] initWithFrame:CGRectMake(left+(viewWidth/4)*1, (viewHeight*0.9), BTNWIDTH, BTNHEIGHT)];
    _btn_left.backgroundColor = [UIColor blueColor];
    [_btn_left setTitle:@"左旋" forState:UIControlStateNormal];
    [_btn_left addTarget:self action:@selector(leftTurn:) forControlEvents:UIControlEventTouchUpInside];
    
    _btn_right = [[UIButton alloc] initWithFrame:CGRectMake(left+(viewWidth/4)*2, (viewHeight*0.9), BTNWIDTH, BTNHEIGHT)];
    _btn_right.backgroundColor = [UIColor blueColor];
    [_btn_right setTitle:@"右旋" forState:UIControlStateNormal];
    [_btn_right addTarget:self action:@selector(rightTurn:) forControlEvents:UIControlEventTouchUpInside];
    
    _btn_sure = [[UIButton alloc] initWithFrame:CGRectMake(left+(viewWidth/4)*3, (viewHeight*0.9), BTNWIDTH, BTNHEIGHT)];
    _btn_sure.backgroundColor = [UIColor blueColor];
    [_btn_sure setTitle:@"确定" forState:UIControlStateNormal];
    [_btn_sure addTarget:self action:@selector(okClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *label;
    //用于覆盖背景色
    UIView *view;
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>=7.0) {
        //导航栏以下开始绘制，IOS7新功能
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.edgesForExtendedLayout = UIRectEdgeBottom | UIRectEdgeLeft | UIRectEdgeRight;
        self.navigationController.navigationBar.translucent = NO;
        self.tabBarController.tabBar.translucent = NO;
        //添加返回按钮
        _btn_return = [[UIButton alloc] initWithFrame:CGRectMake(10, 20, BTNWIDTH, BTNHEIGHT)];
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, size.width, BTNHEIGHT)];
        view =[[UIView alloc] initWithFrame:CGRectMake(0, 20, size.width, size.height)];
    }else{
        //添加返回按钮
        _btn_return = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, BTNWIDTH, BTNHEIGHT)];
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, size.width, BTNHEIGHT)];
        view =[[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
        
    }
    label.text = @"图片编辑";
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor blueColor];
    label.textColor = [UIColor whiteColor];
    _btn_return.backgroundColor = [UIColor clearColor];
    [_btn_return setTitle:@"返回" forState:UIControlStateNormal];
    [_btn_return addTarget:self action:@selector(backTurn:) forControlEvents:UIControlEventTouchUpInside];
    view.backgroundColor = [UIColor blackColor];
    [self.view addSubview:view];
    [self.view addSubview:_imageView];
    [self.view addSubview:_imageView];
    [self.view addSubview:label];
    [self.view addSubview:_btn_delete];
    [self.view addSubview:_btn_left];
    [self.view addSubview:_btn_right];
    [self.view addSubview:_btn_sure];
    [self.view addSubview:_btn_return];
    self.view.backgroundColor = [UIColor whiteColor];
    
//    [self.view addSubview:_imageView];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)deleteClick:(id)sender {
    [_viewController editorImageCallback:@"delete"];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)leftTurn:(id)sender {
    [self changeFrame];
    _image = [ImageUtil image:_image rotation:UIImageOrientationLeft];
    _imageView.image=_image;
    _hasModify = YES;
}

- (void)rightTurn:(id)sender {
    [self changeFrame];
    _image = [ImageUtil image:_image rotation:UIImageOrientationRight];
    _imageView.image=_image;
    _hasModify = YES;
    
}
//设置图片的frame
-(void)changeFrame{
    if(isProtrait){
        isProtrait = NO;
        float y = ([UIScreen mainScreen].bounds.size.height-img_frame.size.width*scale)/2;
        _imageView.frame = CGRectMake(img_frame.origin.x, y, img_frame.size.height*scale, img_frame.size.width*scale);
    }else{
        _imageView.frame = img_frame;
        isProtrait = YES;
    }
}
//根据图片文件大小设置imageViewframe
-(void)imageFrameInit:(float)width andHeight:(float)height{
    scale = width/height;
    float s = 0.8;
    CGRect rx = [[ UIScreen mainScreen ] bounds];
    CGSize size = rx.size;
    float scale_main = size.width/size.height;
    if(scale>scale_main){
        img_frame = CGRectMake(size.width*(1-s)/2, (size.height-size.width*s/scale)/2, size.width*s, size.width*s/scale);
    }else if(scale<scale_main){
        img_frame = CGRectMake(size.width*(size.width-size.height*s*scale)/2, size.height*(1-s)/2, size.height*s*scale, size.height*s);
    }
}
- (void)okClick:(id)sender {
    if (_hasModify) {
        NSData *imageData = UIImageJPEGRepresentation(_image, 1.0);
        [ImageUtil saveImage:_imageName image:imageData appId:_appId];
        
        NSData *data ;
        if(UIImagePNGRepresentation(_image)==nil){
            data = UIImageJPEGRepresentation(_image, 1.0);
        }else{
            data = UIImagePNGRepresentation(_image);
        }
        NSString *imageStr  =[data cdv_base64EncodedString];
        [_viewController editorImageCallback:imageStr];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)backTurn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
@end
