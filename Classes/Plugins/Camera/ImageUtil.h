//
//  ImageUtil.h
//  sxtbpzsc
//
//  Created by IBH1 on 14-7-25.
//  Copyright (c) 2014年 IBH1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageUtil : NSObject

+ (UIImage *)image:(UIImage *)image rotation:(UIImageOrientation)orientation;
+(void)saveImage:(NSString*) imageName image:(NSData*) image appId:(NSString*)appId;
+(void)deleteImage:(NSString*) imageName appId:(NSString*)appId;
+(void)deleteAllImage:(NSString*) appId;
+(NSString *)getImagePath:(NSString*)imageName appId:(NSString*)appId;
+(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
+ (UIImage *) addTextFont:(UIImage *)img text:(NSString *)mark WithHeadString:(NSString *)string;
@end
