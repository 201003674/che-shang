//
//  ImageUtil.m
//  sxtbpzsc
//
// 图片操作工具
//  Created by 阳葵 on 14-7-25.
//  Copyright (c) 2014年 IBH1. All rights reserved.
//

#import "ImageUtil.h"

@implementation ImageUtil

//对图片进行旋转
+ (UIImage *)image:(UIImage *)image rotation:(UIImageOrientation)orientation
{
    long double rotate = 0.0;
    CGRect rect;
    float translateX = 0;
    float translateY = 0;
    float scaleX = 1.0;
    float scaleY = 1.0;
    
    switch (orientation) {
        case UIImageOrientationLeft:
            rotate = M_PI_2;
            rect = CGRectMake(0, 0, image.size.height, image.size.width);
            translateX = 0;
            translateY = -rect.size.width;
            scaleY = rect.size.width/rect.size.height;
            scaleX = rect.size.height/rect.size.width;
            break;
        case UIImageOrientationRight:
            rotate = 3 * M_PI_2;
            rect = CGRectMake(0, 0, image.size.height, image.size.width);
            translateX = -rect.size.height;
            translateY = 0;
            scaleY = rect.size.width/rect.size.height;
            scaleX = rect.size.height/rect.size.width;
            break;
        case UIImageOrientationDown:
            rotate = M_PI;
            rect = CGRectMake(0, 0, image.size.width, image.size.height);
            translateX = -rect.size.width;
            translateY = -rect.size.height;
            break;
        default:
            rotate = 0.0;
            rect = CGRectMake(0, 0, image.size.width, image.size.height);
            translateX = 0;
            translateY = 0;
            break;
    }
    
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    //做CTM变换
    CGContextTranslateCTM(context, 0.0, rect.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextRotateCTM(context, rotate);
    CGContextTranslateCTM(context, translateX, translateY);
    
    CGContextScaleCTM(context, scaleX, scaleY);
    //绘制图片
    CGContextDrawImage(context, CGRectMake(0, 0, rect.size.width, rect.size.height), image.CGImage);
    
    UIImage *newPic = UIGraphicsGetImageFromCurrentImageContext();
    
    return newPic;
}

//保存指定图片文件到文档目录
+(void)saveImage:(NSString*) imageName image:(NSData*) image appId:(NSString *)appId
{
    NSString *tmpPath =[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
//    if (appId==NULL||appId==nil||[appId isKindOfClass:[NSNull class]]||appId.length==0) {
//        tmpPath =[NSString stringWithFormat:@"%@/newcarImage",tmpPath];
//        
//    } else {
         tmpPath =[NSString stringWithFormat:@"%@/%@",tmpPath,appId];
//    }
   
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
//    if (![tmpPath containsString:@"newcarImage"]) {
    [fileManager createDirectoryAtPath:tmpPath withIntermediateDirectories:YES
                                attributes:nil error:nil];
//    }
    NSString *filePath =[NSString stringWithFormat:@"%@/%@",tmpPath,imageName];
    
    NSLog(@"###filePath:%@",filePath);
    
    [fileManager createFileAtPath:filePath contents:image attributes:nil];
    
}

+(void)deleteImage:(NSString*) imageName appId:(NSString*)appId{
    NSString *tmpPath =[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    tmpPath =[NSString stringWithFormat:@"%@/%@",tmpPath,appId];
    NSString *filePath =[NSString stringWithFormat:@"%@/%@",tmpPath,imageName];
    NSLog(@"###删除文件:%@",filePath);
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtURL:[NSURL fileURLWithPath:filePath]  error:NULL];
}

+(void)deleteAllImage:(NSString*) appId{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [NSString stringWithFormat:@"%@/%@",[paths objectAtIndex:0],appId];
    NSLog(@"###需要删除的文件目录:%@",documentsDirectory);
    [fileManager removeItemAtURL:[NSURL fileURLWithPath:documentsDirectory]  error:NULL];
   
    /* NSArray *contents = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:NULL];
    NSEnumerator *e = [contents objectEnumerator];
    NSString *filename;
    while ((filename = [e nextObject])) {
        NSLog(@"删除文件:%@",filename);
        
        [fileManager removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:filename] error:NULL];
        
    }*/
}

//获取图片文件在documents中全路径
+(NSString *)getImagePath :(NSString*)imageName appId:(NSString *)appId{
    //图片保存到沙盒当中
    NSString *tmpPath =[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
//    
//    if (appId==NULL||appId==nil||[appId isKindOfClass:[NSNull class]]||appId.length==0) {
//        tmpPath =[NSString stringWithFormat:@"%@/newcarImage",tmpPath];
//        
//    } else {
//        tmpPath =[NSString stringWithFormat:@"%@/%@",tmpPath,appId];
//    }

    NSString *filePath =[NSString stringWithFormat:@"%@/%@/%@",tmpPath,appId,imageName];

    return filePath;
    
}

//对图片尺寸进行压缩--
+(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    // Create a graphics image context
    UIGraphicsBeginImageContext(newSize);
    
    // Tell the old image to draw in this new context, with the desired
    // new size
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Get the new image from the context
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // End the context
    UIGraphicsEndImageContext();
    
    // Return the new image.
    return newImage;
}

//添加文字水印
+ (UIImage *)addTextFont:(UIImage *)img text:(NSString *)mark WithHeadString:(NSString *)string
{
    NSDate *currentDate = [NSDate date];//获取当前时间，日期
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY/MM/dd HH:mm:ss"];
    
    mark = [NSString stringWithFormat:@"%@%@",string,[dateFormatter stringFromDate:currentDate]];
    STRING_NIL_TO_NONE(mark);
    
    int w = img.size.width;
    int h = img.size.height;
    UIGraphicsBeginImageContext(img.size);
    [[UIColor whiteColor] set];
    
    NSDictionary *attr = @{
                           NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0],  //设置字体
                           NSForegroundColorAttributeName : HEXCOLOR(0X188FC3)   //设置字体颜色
                           };
    
    [img drawInRect:CGRectMake(0, 0, w, h)];
    [mark drawInRect:CGRectMake(10, h-h*0.15, w-30, h-h*0.05) withAttributes:attr];
    UIImage *aimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return aimg;
}

@end
