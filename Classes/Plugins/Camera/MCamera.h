//
//  CPICCamera.h
//  CordovaLib
//
//  Created by IBH1 on 14-10-12.
//
//

#import <Cordova/CDV.h>
#import <Cordova/CDVPlugin.h>

#import "CpicHttpRequest.h"
#import "DLImagePickerController.h"
@interface MCamera : CDVPlugin<UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,NetUploadDelegate,DLImagePickerControllerDelegate>
{
    NSString *imageSourceType;
    
    //是否验车照片 需要强制允许定位功能 验车照片
    NSString *isAllowLocation;
}

-(void)editorImageCallback:(NSString*) editorRetval;

@end
