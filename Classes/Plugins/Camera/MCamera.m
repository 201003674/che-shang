//
//  CPICCamera.m
//  CordovaLib
//
//  Created by IBH1 on 14-10-12.
//
//

#import "MCamera.h"
#import "ImageUtil.h"
#import "ImageViewController.h"
#import "ZipArchive.h"
#import "CPICCommon.h"
#import "CpicHttpRequest.h"

#import <SDWebImage/SDImageCache.h>
#import <CoreLocation/CLLocationManager.h>
#import <CommonCrypto/CommonDigest.h>

@interface MCamera()
{
    UIActionSheet *myActionSheet;
    
    //拍照参数
    NSString *destType;
    NSString *srcType;
    NSInteger *targetWidth;
    NSInteger *targetHeight;
    NSInteger *encodingType;
    NSInteger *mediaType;
    BOOL correctOrientation;
    BOOL saveToPhotoAlbum;
    NSString *imageName;
    NSString *appId;
    NSString *markTxt;
    BOOL isOCR;
}


@end
CDVInvokedUrlCommand* _command;


@implementation MCamera

#pragma 打开OCR相机拍照
-(void)takeOCRPicture:(CDVInvokedUrlCommand *)command
{
    NSLog(@"ORC arguments:%@",command.arguments.description);
    
    _command = command;
    destType = [command argumentAtIndex:0];
    srcType = [command argumentAtIndex:1];
    //    targetWidth = [command argumentAtIndex:2];
    //    targetHeight = [command argumentAtIndex:0];
    //    encodingType = [command argumentAtIndex:1];
    //    mediaType = [command argumentAtIndex:2];
    
    appId = [command argumentAtIndex:12];
    NSLog(@"ORC  command appID:%@",[command argumentAtIndex:12]);
    
    imageName = [NSString stringWithFormat:@"%@.jpg",[command argumentAtIndex:13] ];
    NSLog(@"ORC  command imageName:%@",[command argumentAtIndex:13]);
    //    imageName = [NSString stringWithFormat:@"%@.jpg",returnImageName];
    isOCR=YES;
    
    markTxt = [command argumentAtIndex:2];
    
    [self doPickerCamera:command];
    
}
#pragma 打开普通相机拍照
-(void)takePicture:(CDVInvokedUrlCommand *)command
{
    NSLog(@" arguments:%@",command.arguments);
    
    _command = command;
    destType = [command argumentAtIndex:0];
    srcType = [command argumentAtIndex:1];
    //    targetWidth = [command argumentAtIndex:2];
    //    targetHeight = [command argumentAtIndex:0];
    //    encodingType = [command argumentAtIndex:1];
    //    mediaType = [command argumentAtIndex:2];
    
    appId = [command argumentAtIndex:12];
    NSLog(@"command appID:%@",[command argumentAtIndex:12]);
    
    imageName = [NSString stringWithFormat:@"%@.jpg",[command argumentAtIndex:13] ];
    
    NSLog(@"command imageName:%@",[command argumentAtIndex:13]);
    isOCR=NO;
    
    markTxt = [command argumentAtIndex:2];
    
    [self doPickerCamera:command];
}

#pragma 编辑图片
-(void)toEdit:(CDVInvokedUrlCommand *)command{
    _command = command;
    appId = [command argumentAtIndex:0];
    imageName = [NSString stringWithFormat:@"%@.jpg",[command argumentAtIndex:1] ];
    
    ImageViewController *imageViewController = [[ImageViewController alloc] init];
    imageViewController.imageName = imageName;
    imageViewController.appId = appId;
    
    imageViewController.viewController = self;
    [self.viewController presentViewController:imageViewController animated:YES completion:nil];
}

#pragma 根据文件名删除图片
-(void)deletePhotoByName:(CDVInvokedUrlCommand *)command
{
    _command = command;
    appId = [command argumentAtIndex:0];
    imageName = [NSString stringWithFormat:@"%@.jpg",[command argumentAtIndex:1] ];
    
    [ImageUtil deleteImage:imageName appId:appId];
}

#pragma 上传文件
-(void)toUpLoadFile:(CDVInvokedUrlCommand *)command
{
    _command = command;
    NSString *bussinessCode = [command argumentAtIndex:0];
    appId = [command argumentAtIndex:1];
    NSString *subDir = [command argumentAtIndex:2];
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;//防止屏幕自动锁屏
    NSString *zipFileName = [NSString stringWithFormat:@"zip%@",appId];
    
    NSString *zipFilePath = [self createZipArchiveWithFiles:zipFileName appId:appId];
    NSLog(@"----zipFilePath = %@",zipFilePath);
    NSString *encryptValue = [self md5:[NSString stringWithFormat:@"%@_!Abcwo29480CAei4859",bussinessCode]];
    NSString *FILEURL = [CPICCommon getURL:@"FILEURL"];
    NSLog(@"###开始上传:%@,encryptValue:%@",FILEURL,encryptValue);
    /*
     ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:FILEURL]];
     [request setRequestMethod:@"POST"];
     [request addPostValue:@"123" forKey:@"bussinessCode"];
     [request addPostValue:appId forKey:@"appId"];
     [request addPostValue:subDir forKey:@"subDir"];
     [request addPostValue:@"4io0ce24_90CiAe94wo!9b80co49w82!" forKey:@"encryptValue"];
     [request addFile:zipFilePath forKey:@"file"];
     [request setValidatesSecureCertificate:NO];
     
     request.timeOutSeconds=30;//超时时间
     [request startSynchronous];//等待文件上传
     
     NSString *result = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];
     
     NSLog(@"上传结果:%@",result);
     
     
     NSDictionary *retval = [NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
     
     NSString *status = [ NSString stringWithFormat:@"%@",[retval objectForKey:@"status"]] ;
     
     if ([status isEqualToString:@"0"]) { //false即为0
     [self sendResultToJs:[ NSString stringWithFormat:@"%@",[retval objectForKey:@"errorMsg"]] :@"errorMsg"];
     
     return  ;
     
     }
     
     NSLog(@"上传成功，准备删除临时文件");
     //删除本地临时创建的zip文件
     [ImageUtil deleteAllImage:appId];
     
     [UIApplication sharedApplication].idleTimerDisabled = NO;//允许屏幕自动锁屏
     //将图片文件传递到页面上
     CDVPluginResult *jsReslut = [CDVPluginResult new];
     jsReslut = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"success"];
     [self.commandDelegate sendPluginResult:jsReslut callbackId:_command.callbackId];
     */
    
    NSData *zipData = [NSData dataWithContentsOfFile:zipFilePath];
    NSLog(@"-----zipData length = %lu ,zipFileName = %@",(unsigned long)[zipData length],zipFileName);
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:zipData forKey:@"file"];
    //    [params setObject:@"upload" forKey:@"action"];
    [params setObject:@"123" forKey:@"bussinessCode"];
    [params setObject:appId forKey:@"appId"];
    [params setObject:subDir forKey:@"subDir"];
    [params setObject:@"4io0ce24_90CiAe94wo!9b80co49w82!" forKey:@"encryptValue"];
    
    CpicHttpRequest *request = [[CpicHttpRequest alloc] init];
    request.delegate = self;
    [request upload:FILEURL widthParams:params andFileName:[zipFileName stringByAppendingString:@".zip"]];
    [UIApplication sharedApplication].idleTimerDisabled = NO;//允许屏幕自动锁屏
}
#pragma mark ---调用上传接口回调
//成功回调
- (void)getDataSuccess:(id)responses
{
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responses options:kNilOptions error: nil];
    bool status = [json objectForKey:@"status"];
    if(status)
    {
        [ImageUtil deleteAllImage:appId];
        //成功回调js
        CDVPluginResult *jsReslut = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"success"];
        
        [self.commandDelegate sendPluginResult:jsReslut callbackId:_command.callbackId];
    }
    else
    {
        [self getDataError:responses];
    }
    
}
//失败回调
- (void)getDataError:(id)responses
{
    CDVPluginResult *jsReslut = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"error"];
    
    [self.commandDelegate sendPluginResult:jsReslut callbackId:_command.callbackId];
}
#pragma 删除所有图片
-(void)deleteAllPhotos:(CDVInvokedUrlCommand *)command
{
    _command = command;
    appId = [command argumentAtIndex:1];
    [ImageUtil deleteAllImage:appId];    
}


#pragma mark zip包创建
- (NSString*) createZipArchiveWithFiles:(NSString*)zipFileName appId:(NSString*)appIdString
{
    ZipArchive *zip = [[ZipArchive alloc] init];
    NSString *docPath =[NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@",appIdString]];
    NSString* zipPath = [NSString stringWithFormat:@"%@/%@.zip", docPath,zipFileName];
    NSLog(@"当前zip包文件名:%@",zipPath);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:docPath error:NULL];
    [zip CreateZipFile2:zipPath];
    NSEnumerator *e = [contents objectEnumerator];
    NSString *filename;
    while ((filename = [e nextObject])) {
        if([filename rangeOfString:@".zip"].length<=0){
            NSString *saveStatus = [zip addFileToZip:[NSString stringWithFormat:@"%@/%@",docPath,filename] newname:[filename lastPathComponent]]?@"sucess":@"false";
            NSLog(@"-------image = %@,status = %@",[NSString stringWithFormat:@"%@/%@",docPath,filename],
                  saveStatus
                  );
            ;
        }
    }
    
    [zip CloseZipFile2];
    
    return zipPath;
}


#pragma mark 打开相机
- (void)doPickerCamera:(CDVInvokedUrlCommand *)command
{    
    NSString *pickerCameraType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(pickerCameraType)
    
    //是否强制需要使用定位功能
    isAllowLocation = [NSString stringWithFormat:@"%@",[command argumentAtIndex:14]];
    STRING_NIL_TO_NONE(isAllowLocation)
    
    if ([isAllowLocation isEqualToString:@"1"])
    {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
        {
            [[[ClassFactory getInstance] getInfoHUD] showHud:@"请允许应用打开定位功能，否则无法添加地理位置信息。"];
            
            return;
        }
    }
      
    if ([pickerCameraType isEqualToString:@"0"])
    {
        myActionSheet = [[UIActionSheet alloc]
                         initWithTitle:nil
                         delegate:self
                         cancelButtonTitle:@"取消"
                         destructiveButtonTitle:nil
                         otherButtonTitles:@"从手机相册获取",nil];
    }
    else if ([pickerCameraType isEqualToString:@"1"])
    {
        myActionSheet = [[UIActionSheet alloc]
                         initWithTitle:nil
                         delegate:self
                         cancelButtonTitle:@"取消"
                         destructiveButtonTitle:nil
                         otherButtonTitles: @"打开照相机",nil];
    }
    else
    {
        myActionSheet = [[UIActionSheet alloc]
                         initWithTitle:nil
                         delegate:self
                         cancelButtonTitle:@"取消"
                         destructiveButtonTitle:nil
                         otherButtonTitles: @"打开照相机", @"从手机相册获取",nil];
    }
    
    [myActionSheet showInView:self.viewController.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == myActionSheet.cancelButtonIndex)
    {
        return;
    }
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    //初始化
    //先设定sourceType为相机，然后判断相机是否可用（ipod）没相机，不可用将sourceType设定为相片库
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    
    NSString *pickerCameraType = [NSString stringWithFormat:@"%@",[_command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(pickerCameraType)
    
    if ([pickerCameraType isEqualToString:@"0"])
    {
        //相册
        sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

    }
    else if ([pickerCameraType isEqualToString:@"1"])
    {
        //相机
        if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
    }
    else
    {
        //两者
        switch (buttonIndex)
        {
            case 0:  //打开照相机拍照
            {
                if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
                {
                    sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                }
            }
                break;
                
            case 1:  //打开本地相册
            {
                sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                
            }
                break;
        }
    }

    picker.sourceType = sourceType;
    picker.delegate = self;
    picker.allowsEditing = NO;//设置是否可编辑
    if (sourceType==UIImagePickerControllerSourceTypeCamera)
    {
        imageSourceType = @"P";
        
        if (isOCR)
        {
            DLImagePickerController *zdyCamera=[[DLImagePickerController alloc]init];
            zdyCamera.delegate=self;
            [self.viewController presentViewController:zdyCamera animated:YES completion:nil];
        }
        else
        {
            [self.viewController presentViewController:picker animated:YES completion:nil];
        }
    }
    else
    {
        imageSourceType = @"X";
        [self.viewController presentViewController:picker animated:YES completion:nil];
    }
}
-(void)DLGetImage:(UIImage *)image
{
    //图片保存到缓存
    NSString *deviceId = [NSString stringWithFormat:@"%@.png",[[[UIDevice currentDevice] identifierForVendor] UUIDString]];
    STRING_NIL_TO_NONE(deviceId);
    
    [[SDImageCache sharedImageCache] storeImage:image forKey:deviceId toDisk:YES completion:^{
        
    }];
    
    //图片质量压缩，减少体积
    NSData *data = UIImagePNGRepresentation(image);
    //对图片进行base64编码                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
    NSString *imageStr  = [data cdv_base64EncodedString];
    
    //图片保存到沙盒当中
    NSDate *date=[NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString *destDateString = [dateFormatter stringFromDate:date];
    NSString *returnImageName=[NSString stringWithFormat:@"%@.jpg",destDateString];
    NSLog(@"returnImageName:%@",returnImageName);
    [ImageUtil saveImage:returnImageName image:data appId:appId];
    
    //图片本地路径地址
    NSString *filePath=[ImageUtil getImagePath:returnImageName appId:appId];
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:imageStr,@"base64",filePath,@"fileName", nil];
    
    //发送到页面上
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dic];
    [self.commandDelegate sendPluginResult:result callbackId:_command.callbackId];
    
    //关闭相册选取界面
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
}
//选择了一个图片后处理的方法
#pragma mark 获取照片
-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [[[ClassFactory getInstance] getNetComm] showHud:nil];
    [picker dismissViewControllerAnimated:YES completion:^{
       
        if(info == nil)
        {
            return;
        }
        NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
        
        UIImage *finialImage = nil;
        
        if([type isEqualToString:@"public.image"])
        {
            UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
            
            int  perMBBytes = 1024*1024;
            
            CGImageRef cgimage = image.CGImage;
            size_t bpp = CGImageGetBitsPerPixel(cgimage);
            size_t bpc = CGImageGetBitsPerComponent(cgimage);
            size_t bytes_per_pixel = bpp / bpc;
            
            long lPixelsPerMB  = perMBBytes/bytes_per_pixel;
            
            long totalPixel = CGImageGetWidth(image.CGImage)*CGImageGetHeight(image.CGImage);
            
            long totalFileMB = totalPixel/lPixelsPerMB;
            NSLog(@"图片大小 = %ld",totalFileMB);
            CGSize imagesize = image.size;
            float h = imagesize.height;
            NSLog(@"原图片高度:%f 宽度:%f",imagesize.height,imagesize.width);
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            NSString *createDate = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]];
            
            STRING_NIL_TO_NONE(isAllowLocation);
            if ([isAllowLocation isEqualToString:@"1"])
            {
                //验车照片 添加水印图片 以及 时间戳
                UIImage *waterMask = [UIImage imageNamed:@"waterMask.png"];
                
                CGFloat width = (image.size.height - 20)/20.0;
                CGFloat height = (image.size.height - 20)/20.0;
                
                CGFloat x = 10;
                CGFloat y = image.size.height - 10 - height;
                
                NSString *location = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLocalCfg] getAddress]];
                STRING_ADDRESS_UNABLE_NONE(location);
                
                NSString *locationString = [NSString stringWithFormat:@"地点：%@",location];
                STRING_NIL_TO_NONE(locationString);
                
                NSString *timeString = [NSString stringWithFormat:@"时间：%@",createDate];
                STRING_NIL_TO_NONE(timeString)
                
                //时间
                CGFloat time_x = x + width + 10;
                CGFloat time_y = y;
                CGFloat time_width = image.size.width - width - 20.0;
                CGFloat time_height = height/2.0;
                
                //地点
                CGFloat location_x = x + width + 10;
                CGFloat location_y = y + time_height;
                CGFloat location_width = image.size.width - width - 20.0;
                CGFloat location_height = height/2.0;
                
                CGFloat fontSize = width/2.0;
                
                finialImage = [image imageWithWaterMask:waterMask inRect:CGRectMake(x, y, width, height) WithStringWaterTimeMarkString:timeString atTimeRect:CGRectMake(time_x, time_y, time_width, time_height) WithStringWaterLocationString:locationString atLocationRect:CGRectMake(location_x, location_y, location_width, location_height) color:[UIColor redColor] font:[UIFont systemFontOfSize:fontSize]];
            }
            else
            {
                finialImage = [UIImage imageWithData:UIImagePNGRepresentation(image)];
                
                //添加水印
                finialImage = [ImageUtil addTextFont:finialImage text:markTxt WithHeadString:imageSourceType];
            }
            
            if(h>1000)
            {
                imagesize.height = imagesize.height*0.5;
                imagesize.width = imagesize.width*0.5;
                
            }
            else if(h>500&&h<=1000)
            {
                imagesize.height = imagesize.height*0.8;
                imagesize.width = imagesize.width*0.8;
            }
            
            finialImage = [ImageUtil imageWithImage:finialImage scaledToSize:imagesize];
            
            //图片质量压缩，减少体积
            NSData *data = UIImageJPEGRepresentation(finialImage, 0.8);
            //对图片进行base64编码
            NSString *imageStr  = [NSString stringWithFormat:@"%@",[data cdv_base64EncodedString]];
            
            //根据时间图片命名
            NSString *returnImageName = [NSString stringWithFormat:@"%@.jpg",createDate];
            
            //图片保存到沙盒当中
            [ImageUtil saveImage:returnImageName image:data appId:appId];
            
            //图片本地路径地址
            NSString *filePath=[ImageUtil getImagePath:returnImageName appId:appId];
            
            NSString *newImageStr=[[NSString alloc]initWithFormat:@"{\"base64\":\"%@\",\"fileName\":\"%@\"}",imageStr,filePath];
            
            //发送到页面上
            CDVPluginResult *result = [CDVPluginResult new];
            result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:newImageStr];
            
            [self.commandDelegate sendPluginResult:result callbackId:_command.callbackId];
        }
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
        
    }];
}


#pragma 将数据返回给JS端
-(void)sendResultToJs:(NSString*)retval :(NSString*) key
{
    
    NSMutableDictionary*dic1=[NSMutableDictionary dictionary];
    [dic1 setObject:retval forKey:key];
    CDVPluginResult *result = [CDVPluginResult new];
    if (dic1)
    {
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dic1];
    }
    else
    {
        
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:result callbackId:_command.callbackId];
    
}

#pragma mark 图片编辑后的操作
-(void)editorImageCallback:(NSString*) editorRetval{
    NSLog(@"####editorImageCallback");
    //将图片文件传递到页面上
    CDVPluginResult *result = [CDVPluginResult new];
    result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:editorRetval];
    [self.commandDelegate sendPluginResult:result callbackId:_command.callbackId];
}

-(NSString*)md5:(NSString*)input{
    
    const char* str = [input UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, strlen(str), result);
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
    
    for(int i = 0; i<CC_MD5_DIGEST_LENGTH; i++) {
        [ret appendFormat:@"%02X",result[i]];
    }
    return ret;
}

#pragma mark - 保存图片后的回调
- (void)imageSavedToPhotosAlbum:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(id)contextInfo
{
    if(!error)
    {
        [[[ClassFactory getInstance] getInfoHUD] showHud:@"图片保存相册成功！"];
        
        return;
    }
}


@end
