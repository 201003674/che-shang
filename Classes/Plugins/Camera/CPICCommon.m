//
//  CPICCommon.m
//  CordovaLib
//
//  Created by IBH1 on 14-10-13.
//
//

#import "CPICCommon.h"


#ifndef CpicMobile_Header_h
#define CpicMobile_Header_h
/*
 
 ft,uat,sit版本：1.2.6 126    －－11-04
 ol 版本：1.0.2 302    －－11-04
 
 */
#define HJ 0
#if HJ==0 //ft
#define IP @"https://appft.ecpic.com.cn"
#define cmpIP [IP stringByAppendingString:@"/cmphtml"]
#elif HJ==1 //uat
#define IP @"https://appft.ecpic.com.cn"
#define cmpIP [IP stringByAppendingString:@"/cmphtml2"]
#elif HJ==2 //sit
#define IP @"https://cmpsit.ecpic.com.cn"
#define cmpIP [IP stringByAppendingString:@"/cmphtml"]
#elif HJ==3 //product
#define IP @"https://app.ecpic.com.cn"
#define cmpIP [IP stringByAppendingString:@"/cmphtml"]
#else
#define IP @""
#define cmpID @""
#endif


//#define MAINURL [IP stringByAppendingString:@"/public/index.html"]
//index.html
#define MAINURL [cmpIP stringByAppendingString:@"/pages/index.html"]
#define SERVICEURL [IP stringByAppendingString:@"/cmps/access/doSubmit.do"]
#define FILEURL [IP stringByAppendingString:@"/cmpfile/file/upload.do"]
#define FXURL [cmpIP stringByAppendingString:@"/pages/personal/download.html"]
#define WXAPPID @"wxdf7797cc383f6b9e";
#define BAIDU @"PLIsDHWGCO7lzHGwjGLuCAlU";


#endif

@implementation CPICCommon
#pragma 获取指定key值的对应的URL
+(NSString*)getURL: (NSString*) key{
    if([key isEqual:@"MAINURL"]){
        return MAINURL;
    }else if([key isEqual:@"FILEURL"]){
        return FILEURL;
    }else if([key isEqual:@"SERVICEURL"]){
        return SERVICEURL;
    }else if([key isEqual:@"FXURL"]){
        return FXURL;
    }
    else{
        return @"";
    }
}

+(NSString*)getAPPID:(NSString*) key{
    if([key isEqual:@"WX"]){
        return WXAPPID;
    }else if([key isEqual:@"BAIDU"]){
        return BAIDU;
    }
    
    return WXAPPID;
}
@end
