//
//  DLImagePickerViewtrollerViewController.h
//  Cmpi
//
//  Created by hdl on 14-12-18.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
@protocol DLImagePickerControllerDelegate

-(void)DLGetImage:(UIImage*)image;
@end
@interface DLImagePickerController : UIViewController
/*自定义相机*/
@property (nonatomic, strong)       AVCaptureSession            * session;
//AVCaptureSession对象来执行输入设备和输出设备之间的数据传递
@property (nonatomic, strong)       AVCaptureDeviceInput        * videoInput;
//AVCaptureDeviceInput对象是输入流
@property (nonatomic, strong)       AVCaptureStillImageOutput   * stillImageOutput;
//照片输出流对象，当然我的照相机只有拍照功能，所以只需要这个对象就够了
@property (nonatomic, strong)       AVCaptureVideoPreviewLayer  * previewLayer;
//预览图层，来显示照相机拍摄到的画面
@property (nonatomic, strong)       UIBarButtonItem             * toggleButton;
//切换前后镜头的按钮
@property (nonatomic, strong)       UIButton                    * shutterButton;
//拍照按钮
@property (nonatomic, strong)       UIView                      * cameraShowView;


@property(nonatomic,assign)id<DLImagePickerControllerDelegate>delegate;
//放置预览图层的View
-(void)closeView;
-(void)retakePicture;
@end
