//
//  CompleteViewController.h
//  Cmpi
//
//  Created by IBH-02 on 14-12-19.
//
//

#import <UIKit/UIKit.h>
@protocol CompleteViewControllerDelegate <NSObject>
-(void)SubComReTakePicture;
-(void)SubusePicture:(UIImage*)image;
@end
@interface CompleteViewController : UIViewController
@property(nonatomic,strong)UIImageView* imageView;
@property(nonatomic,strong)UIImage* image;
@property(nonatomic,assign)id<CompleteViewControllerDelegate
>delegate;
@end
