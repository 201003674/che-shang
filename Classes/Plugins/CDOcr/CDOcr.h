//
//  CDOcr.h
//  IOSFramework
//
//  Created by 林科 on 2018/6/4.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import <Cordova/CDVPlugin.h>

//身份证
#import <ISIDReaderSDK/ISIDReaderSDK.h>
#import <ISIDReaderPreviewSDK/ISIDReaderPreviewSDK.h>
//行驶证
#import <ISVehicleLicenseSDK/ISVehicleLicenseSDK.h>
#import <ISVehicleLicensePreviewSDK/ISVehicleLicensePreviewSDK.h>
//驾驶证
#import <ISDrivingLicenseSDK/ISDrivingLicenseSDK.h>
#import <ISDrivingLicensePreviewSDK/ISDrivingLicensePreviewSDK.h>
//合格证
#import <ISVehicleCertificateSDK/ISVehicleCertificateSDK.h>

#import <ISOpenSDKFoundation/ISOpenSDKFoundation.h>
//增强识别
#import <ISImageProcessSDK/ISImageProcessSDK.h>

@interface CDOcr:CDVPlugin<UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,ISOpenSDKCameraViewControllerDelegate,ISCropImageViewControllerDelegate>
{
    ISOpenPreviewSDKType previewSDKType;
}

@property(nonatomic, strong) CDVInvokedUrlCommand *urlCommand;

@property(nonatomic, strong) ISCropImageViewController *cropImageVC;

/* 标记用户选择项 */
@property(nonatomic) NSInteger buttonIndex;

//OCR 功能
-(void)CDOcr:(CDVInvokedUrlCommand*)command;

@end
