//
//  ISVehicleCertificateController.h
//  ISVehicleCertificateSDK
//
//  Created by Johnson Zhang on 16/7/19.
//  Copyright © 2016年 Johnson Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ISOpenSDKFoundation/ISOpenSDKFoundation.h>

extern NSString *const kVCItemModelNumber;//车辆型号
extern NSString *const kVCItemModelVIN;//车辆识别代号/车架号
extern NSString *const kVCItemEnginNumber;//发动机号
extern NSString *const kVCItemDisplacement;//排量
extern NSString *const kVCItemPower;//功率
extern NSString *const kVCItemMassRated;//额定载质量(kg)
extern NSString *const kVCItemSeatingCapacity;//额定载客(人)

typedef void(^RecognizeCardFinishHandler)(NSDictionary *cardInfo);
typedef void(^ConstructResourcesFinishHandler)(ISOpenSDKStatus status);

@interface ISVehicleCertificateController : NSObject

+ (ISVehicleCertificateController *)sharedISOpenSDKController;
/*
 You should call this method to construct resources before call other APIs in the SDK,calling this API will cause an online authorization for the first time this SDK is used.Once authorized successfully, this API will be returned immediately within expire time and try to update authorization status in background
 */
- (void)constructResourcesWithAppKey:(NSString *)appKey
                           subAppkey:(NSString *)subAppKey
                       finishHandler:(ConstructResourcesFinishHandler)handler;

- (ISOpenSDKStatus)processCardImage:(UIImage *)image
                  withFinishHandler:(RecognizeCardFinishHandler)handler;

/*
 Use this method to release SDK resources
 */
- (void)destructResources;

/*
 Use this method to get the current SDK version information
 */
+ (NSString *)versionBankCardSDK;

@end
