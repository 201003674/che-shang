//
//  CDOcr.m
//  IOSFramework
//
//  Created by 林科 on 2018/6/4.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import "CDOcr.h"

#define APP_KEY @"3rUCQ58dBd5BgrDaTR6E7Qe8"

@implementation CDOcr

#pragma mark -
#pragma mark - CDOcr
-(void)CDOcr:(CDVInvokedUrlCommand*)command
{
    self.urlCommand = command;
    
    //OCR SDK 初始化
    NSString *ocrType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(ocrType)

    if ([ocrType isEqualToString:@"0"])
    {
        //身份证
        previewSDKType = ISOpenPreviewSDKTypeIDReader;
    }
    else if ([ocrType isEqualToString:@"1"])
    {
        //行驶证
        previewSDKType = ISOpenPreviewSDKTypeVehicleLicense;
    }
    else if ([ocrType isEqualToString:@"2"])
    {
        //合格证
        previewSDKType = 999;
    }
    else if ([ocrType isEqualToString:@"3"])
    {
        //驾驶证
        previewSDKType = ISOpenPreviewSDKTypeDrivingLicense;
    }
    else
    {
        previewSDKType = 999;
    }
    
    [self ocrInitSuccess:command];
}

#pragma mark -
#pragma mark - ocrInit
-(void)ocrInitSuccess:(CDVInvokedUrlCommand*)command
{
    //0 1新模式 老模式
    NSString *pickerCameraType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(pickerCameraType)
    
    if ([pickerCameraType isEqualToString:@"1"])
    {
        if(previewSDKType == ISOpenPreviewSDKTypeIDReader || previewSDKType == ISOpenPreviewSDKTypeVehicleLicense)
        {
            //身份证 行驶证
            UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                                                initWithTitle:nil
                                                delegate:self
                                                cancelButtonTitle:@"取消"
                                                destructiveButtonTitle:nil
                                                otherButtonTitles:@"预览自动识别",@"拍摄照片识别",@"相册照片识别",nil];
            actionSheet.command = command;
            
            [actionSheet showInView:self.viewController.view];
        }
        else
        {
            UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                                                initWithTitle:nil
                                                delegate:self
                                                cancelButtonTitle:@"取消"
                                                destructiveButtonTitle:nil
                                                otherButtonTitles:@"拍摄照片识别",@"相册照片识别",nil];
            actionSheet.command = command;
            
            [actionSheet showInView:self.viewController.view];
        }
    }
    else
    {
        UICustomActionSheet *actionSheet = [[UICustomActionSheet alloc]
                                            initWithTitle:nil
                                            delegate:self
                                            cancelButtonTitle:@"取消"
                                            destructiveButtonTitle:nil
                                            otherButtonTitles:@"拍摄照片识别",@"相册照片识别",nil];
        actionSheet.command = command;
        
        [actionSheet showInView:self.viewController.view];
    }
}

#pragma mark -
#pragma mark - UICustomActionSheetDelegate
-(void)actionSheet:(UICustomActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex)
    {
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"用户取消操作",@"message", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
        
        return;
    }
    
    UICustomImagePickerController *imagePicker=[[UICustomImagePickerController alloc]init];
    imagePicker.view.backgroundColor = [UIColor whiteColor];
    self.buttonIndex = buttonIndex;
    
    BOOL isCameraSupport = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    BOOL isPhotoLibrarySupport= [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
    
    NSString *pickerCameraType = [NSString stringWithFormat:@"%@",[actionSheet.command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(pickerCameraType)
    
    if ([pickerCameraType isEqualToString:@"1"])
    {
        if(previewSDKType == ISOpenPreviewSDKTypeIDReader || previewSDKType == ISOpenPreviewSDKTypeVehicleLicense)
        {
            //身份证 行驶证
            switch (buttonIndex)
            {
                //预览自动识别 拍摄照片识别 相册照片识别
                case 0:
                {
                    if (!isCameraSupport)
                    {
                        [[[ClassFactory getInstance]getInfoHUD]showHud:@"您的设备不支持此功能！"];
                        
                        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"您的设备不支持此功能！",@"message", nil];
                        
                        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                                messageAsDictionary:theMessage];
                        // send cordova result
                        [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
                        
                        return;
                    }
                    else
                    {
                        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                        
                        break;
                    }
                }
                case 1:
                {
                    if(!isCameraSupport)
                    {
                        [[[ClassFactory getInstance] getInfoHUD] showHud:@"您的设备不支持此功能！"];
                        
                        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"您的设备不支持此功能！",@"message", nil];
                        
                        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                                messageAsDictionary:theMessage];
                        // send cordova result
                        [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
                        
                        return;
                    }
                    else
                    {
                        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                        
                        break;
                    }
                }
                case 2:
                {
                    if (!isPhotoLibrarySupport)
                    {
                        [[[ClassFactory getInstance]getInfoHUD]showHud:@"您的设备不支持此功能！"];
                        
                        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"您的设备不支持此功能！",@"message", nil];
                        
                        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                                messageAsDictionary:theMessage];
                        // send cordova result
                        [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
                        
                        return;
                    }
                    else
                    {
                        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                        
                        break;
                    }
                }
                default:
                    
                    break;
            }
        }
        else
        {
            switch (buttonIndex)
            {
                //拍摄照片识别 相册照片识别
                case 0:
                {
                    if (!isCameraSupport)
                    {
                        [[[ClassFactory getInstance]getInfoHUD]showHud:@"您的设备不支持此功能！"];
                        
                        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"您的设备不支持此功能！",@"message", nil];
                        
                        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                                messageAsDictionary:theMessage];
                        // send cordova result
                        [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
                        
                        return;
                    }
                    else
                    {
                        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                        
                        break;
                    }
                }
                case 1:
                {
                    if (!isPhotoLibrarySupport)
                    {
                        [[[ClassFactory getInstance]getInfoHUD]showHud:@"您的设备不支持此功能！"];
                        
                        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"您的设备不支持此功能！",@"message", nil];
                        
                        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                                messageAsDictionary:theMessage];
                        // send cordova result
                        [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
                        
                        return;
                    }
                    else
                    {
                        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                        
                        break;
                    }
                }
                default:
                    
                    break;
            }
        }
    }
    else
    {
        //旧模式
        switch (buttonIndex)
        {
            case 0:
            {
                //相机
                if (!isCameraSupport)
                {
                    [[[ClassFactory getInstance] getInfoHUD] showHud:@"您的设备不支持此功能！"];
                    
                    NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"您的设备不支持此功能！",@"message", nil];
                    
                    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                            messageAsDictionary:theMessage];
                    // send cordova result
                    [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
                    
                    return;
                }
                else
                {
                    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                    
                    break;
                }
            }
                
            case 1:
            {
                //相册
                if(!isPhotoLibrarySupport)
                {
                    [[[ClassFactory getInstance] getInfoHUD] showHud:@"您的设备不支持此功能！"];
                    
                    NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"您的设备不支持此功能！",@"message", nil];
                    
                    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                            messageAsDictionary:theMessage];
                    // send cordova result
                    [self.commandDelegate sendPluginResult:result callbackId:actionSheet.command.callbackId];
                    
                    return;
                }
                else
                {
                    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    
                    break;
                }
            }
                
            default:
                
                break;
        }
    }
    
    if ([pickerCameraType isEqualToString:@"1"])
    {
        //新模式OCR
        ISOpenSDKCameraViewController *sdkCameraView = [[ISOpenSDKCameraViewController alloc] init];

        if(previewSDKType == ISOpenPreviewSDKTypeIDReader && buttonIndex == 0)
        {
            //身份证
            sdkCameraView = [[ISIDCardReaderController sharedISOpenSDKController] cameraViewControllerWithAppkey:APP_KEY subAppkey:nil needCompleteness:YES];
            [sdkCameraView setCustomInfo:@"请将身份证放在框内识别"];
        }
        else if (previewSDKType == ISOpenPreviewSDKTypeDrivingLicense && buttonIndex == 0)
        {
            //驾驶证
            sdkCameraView = [[ISDrivingLicensePreviewController sharedISOpenSDKController] cameraViewControllerWithAppkey:APP_KEY subAppkey:nil];
            [sdkCameraView setCustomInfo:@"请将驾驶证放在框内识别"];
        }
        else if (previewSDKType == ISOpenPreviewSDKTypeVehicleLicense && buttonIndex == 0)
        {
            //行驶证
            sdkCameraView = [[ISVehicleLicensePreviewController sharedISOpenSDKController] cameraViewControllerWithAppkey:APP_KEY subAppkey:nil];
            [sdkCameraView setCustomInfo:@"请将行驶证放在框内识别"];
        }
        else
        {
            //其他
            NSString *requiredMediaType = ( NSString *)kUTTypeImage;
            NSArray *arrMediaTypes = [NSArray arrayWithObjects:requiredMediaType,nil];
            [imagePicker setMediaTypes:arrMediaTypes];
            [imagePicker setDelegate:self];
            [imagePicker setCommand:actionSheet.command];
            [self.viewController presentViewController:imagePicker animated:YES completion:nil];
            
            return;
        }
        
        [sdkCameraView setDelegate:self];
        [sdkCameraView setDebugMode:YES];
        [sdkCameraView setShouldHightlightCorners:YES];
        [self.viewController presentViewController:sdkCameraView animated:YES completion:nil];
    }
    else
    {
        //旧模式 OCR
        NSString *requiredMediaType = ( NSString *)kUTTypeImage;
        NSArray *arrMediaTypes = [NSArray arrayWithObjects:requiredMediaType,nil];
        [imagePicker setMediaTypes:arrMediaTypes];
        [imagePicker setDelegate:self];
        [imagePicker setCommand:actionSheet.command];
        [self.viewController presentViewController:imagePicker animated:YES completion:nil];
    }
}

#pragma mark -
#pragma mark - ISOpenSDKCameraViewControllerDelegate
//相机模块初始化SDK回调
- (void)constructResourcesDidFinishedWithStatusCode:(ISOpenSDKStatus)status
{
    if (status != ISOpenSDKStatusSuccess)
    {
        NSString *message = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLogic] getOpenSDKStatusMessage:status]];
        STRING_NIL_TO_NONE(message)
        
        NSLog(@"相机模块初始化 Auth failed:%ld  %@",(long)status,message);
    }
}
//相机模块授权失败SDK回调
- (void)accessCameraDidFailed
{
    NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",@"相机模块授权失败!",@"message", nil];
    
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                            messageAsDictionary:theMessage];
    
    // send cordova result
    [self.commandDelegate sendPluginResult:result callbackId:self.urlCommand.callbackId];
}
//相机模块边缘检测回调
- (void)cameraViewController:(UIViewController *)viewController didFinishDetectCardWithResult:(int)result borderPoints:(NSArray *)borderPoints
{
    //
}
//相机模块识别结果回调
- (void)cameraViewController:(UIViewController *)viewController didFinishRecognizeCard:(NSDictionary *)resultInfo cardSDKType:(ISOpenPreviewSDKType)sdkType
{
    if(previewSDKType == ISOpenPreviewSDKTypeIDReader || previewSDKType == ISOpenPreviewSDKTypeDrivingLicense || previewSDKType == ISOpenPreviewSDKTypeVehicleLicense)
    {
        //身份证 原图
        UIImage *originImage = [resultInfo objectForKey:kOpenSDKCardResultTypeOriginImage];
        
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                messageAsDictionary:[self showResultWithAttributes:resultInfo CardImage:originImage]];

        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:self.urlCommand.callbackId];
    }
    else
    {
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",@"用户取消!",@"message", nil];
        
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:self.urlCommand.callbackId];
    }
    
    [viewController dismissViewControllerAnimated:YES completion:nil];
}
//相机模块返回按钮点击回调
- (void)cameraViewController:(UIViewController *)viewController didClickCancelButton:(id)sender
{
    NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",@"用户取消!",@"message", nil];
    
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                            messageAsDictionary:theMessage];
    
    // send cordova result
    [self.commandDelegate sendPluginResult:result callbackId:self.urlCommand.callbackId];
    
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark - UIImagePickerControllerDelegate
-(void)imagePickerController:(UICustomImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [[[ClassFactory getInstance] getNetComm] showHud:nil];
    [picker dismissViewControllerAnimated:YES completion:^{
        
        //通过UIImagePickerControllerMediaType判断返回的是照片还是视频
        NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        //如果返回的type等于kUTTypeImage，代表返回的是照片,并且需要判断当前相机使用的sourcetype是拍照还是相册
        if ([mediaType isEqualToString:( NSString *)kUTTypeImage])
        {
            UIImage *theImage = nil;
            // 判断，图片是否允许修改
            if ([picker allowsEditing])
            {
                //获取用户编辑之后的图像
                theImage = [info objectForKey:UIImagePickerControllerEditedImage];
            }
            else
            {
                // 照片的元数据参数
                theImage = [info objectForKey:UIImagePickerControllerOriginalImage];
            }
            
            //0 1新模式 老模式
            NSString *pickerCameraType = [NSString stringWithFormat:@"%@",[picker.command argumentAtIndex:0]];
            STRING_NIL_TO_NONE(pickerCameraType)
            
            if ([pickerCameraType isEqualToString:@"1"])
            {
                //增强识别
                [self cropImageWithAttributes:picker.command WithCardImage:theImage];
            }
            else
            {
                //识别信息解析
                [self showResultWithAttributes:picker.command WithCardImage:theImage];
            }
        }
        else
        {
            
        }
        
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
    }];
}

-(void)imagePickerControllerDidCancel:(UICustomImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"-1",@"resultCode",@"用户取消操作",@"message", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:picker.command.callbackId];
        
    }];
}

#pragma mark -
#pragma mark - 增强识别SDK条用
- (void)cropImageWithAttributes:(CDVInvokedUrlCommand*)command WithCardImage:(UIImage *)image
{
    UIColor *color = [UIColor colorWithRed:0 green:91/255.0 blue:172/255.0 alpha:1.0];
    
    self.cropImageVC = [[ISCropImageViewController alloc] initWithNavigationBarTintCorlor:color bottomToolBarColor:color navigationTitle:@"识别处理" navigationTitleColor:[UIColor whiteColor]];
    
    self.cropImageVC = [[ISCropImageViewController alloc] init];
    self.cropImageVC.delegate = self;
    self.cropImageVC.debugMode = YES;
    [self.cropImageVC constructResourcesWithAppKey:APP_KEY subAppkey:@"" finishHandler:^(ISOpenSDKStatus status) {
        if (status == ISOpenSDKStatusSuccess)
        {
            [self.cropImageVC detectBorderWithCropImage:image enhanceMode:ISImageProcessEnhanceModeAllMode maxSize:2074];
           
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                // 进行UI控件的操作
                [self.viewController presentViewController:self.cropImageVC animated:YES completion:nil];
            }];
        }
        else
        {
             [self showResultWithAttributes:self.urlCommand WithCardImage:image];
        }
    }];
}
#pragma mark -
#pragma mark - ISCropImageViewControllerDelegate
- (void)ISCropImageViewControllerDidFinishDewarpImage:(UIImage *)resultImage
{
    [self.cropImageVC dismissViewControllerAnimated:YES completion:^{
       
        //增强识别 识别信息解析
        [self showResultWithAttributes:self.urlCommand WithCardImage:resultImage];
        
    }];
}

#pragma mark -
#pragma mark - OCR 识别信息解析
-(void)showResultWithAttributes:(CDVInvokedUrlCommand*)command WithCardImage:(UIImage *)image
{
    NSString *ocrType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(ocrType)
    
    [[[ClassFactory getInstance] getNetComm] showHud:nil];
    // 0 身份证 1 行驶证 2 合格证 3 驾驶证
    if ([ocrType isEqualToString:@"0"])
    {
        //身份证
        [self idReader:command WithCardImage:image];
    }
    else if ([ocrType isEqualToString:@"1"])
    {
        //行驶证
        [self vehicleLicense:command WithCardImage:image];
    }
    else if ([ocrType isEqualToString:@"2"])
    {
        //合格证
        [self vehicleCertificate:command WithCardImage:image];
    }
    else if ([ocrType isEqualToString:@"3"])
    {
        //驾驶证
        [self drivingLicense:command WithCardImage:image];
    }
    else
    {
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
        [[[ClassFactory getInstance] getInfoHUD] showHud:@"参数有误,请检验并核对参数!"];
    }
}

#pragma mark -
#pragma mark - 身份证
- (void)idReader:(CDVInvokedUrlCommand*)command WithCardImage:(UIImage *)image
{
    NSString *pickerCameraType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(pickerCameraType)
    
    if ([pickerCameraType isEqualToString:@"1"] && self.buttonIndex == 0)
    {
        //新OCR 模式 且 用户选择 SDK识别
        ISOpenSDKStatus status = [[ISCardReaderController sharedController] processCardImage:image returnCroppedImage:NO returnPortraitImage:NO withFinishHandler:^(NSDictionary *cardInfo) {
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsDictionary:[self showResultWithAttributes:cardInfo CardImage:image]];
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }];
        
        if (status != ISOpenSDKStatusSuccess)
        {
            NSString *message = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLogic] getOpenSDKStatusMessage:status]];
            STRING_NIL_TO_NONE(message)
            
            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",message,@"message", nil];
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:theMessage];
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        else
        {
            
        }
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
    }
    else
    {
        //服务器模式
        [self ocrRecognition:command WithImage:image success:^(id JSON) {
            
            NSDictionary *dic = [[NSDictionary alloc] initWithDictionary:[[JSON mj_JSONString] objectFromJSONString]];
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsDictionary:[self showServerResultWithAttributes:dic CardImage:image]];
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            
            [[[ClassFactory getInstance] getNetComm] hiddenHud];
        } failure:^(NSError *error) {
            
            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",error.localizedDescription,@"message", nil];
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:theMessage];
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            
            [[[ClassFactory getInstance] getNetComm] hiddenHud];
        }];
    }
}

#pragma mark -
#pragma mark - 行驶证
- (void)vehicleLicense:(CDVInvokedUrlCommand*)command WithCardImage:(UIImage *)image
{
    NSString *pickerCameraType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(pickerCameraType)
    
    if ([pickerCameraType isEqualToString:@"1"] && self.buttonIndex == 0)
    {
        //新OCR 模式
        ISOpenSDKStatus status = [[ISVehicleLicenseReaderController sharedController] processCardImage:image withFinishHandler:^(NSDictionary *cardInfo) {
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsDictionary:[self showResultWithAttributes:cardInfo CardImage:image]];
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            
        }];
        
        if (status != ISOpenSDKStatusSuccess)
        {
            NSString *message = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLogic] getOpenSDKStatusMessage:status]];
            STRING_NIL_TO_NONE(message)
            
            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",message,@"message", nil];
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:theMessage];
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        else
        {
            
        }
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
    }
    else
    {
        //服务器模式
        [self ocrRecognition:command WithImage:image success:^(id JSON) {
            
            NSDictionary *dic = [[NSDictionary alloc] initWithDictionary:[[JSON mj_JSONString] objectFromJSONString]];
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsDictionary:[self showServerResultWithAttributes:dic CardImage:image]];
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            
            [[[ClassFactory getInstance] getNetComm] hiddenHud];
        } failure:^(NSError *error) {
            
            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",error.localizedDescription,@"message", nil];
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:theMessage];
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            
            [[[ClassFactory getInstance] getNetComm] hiddenHud];
        }];
    }
}

#pragma mark -
#pragma mark - 驾驶证
- (void)drivingLicense:(CDVInvokedUrlCommand*)command WithCardImage:(UIImage *)image
{
    NSString *pickerCameraType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(pickerCameraType)
    
    if ([pickerCameraType isEqualToString:@"1"] && self.buttonIndex == 0)
    {
        //新OCR 模式
        ISOpenSDKStatus status = [[ISDrivingLicenseReaderController sharedController] processCardImage:image withFinishHandler:^(NSDictionary *cardInfo) {
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsDictionary:[self showResultWithAttributes:cardInfo CardImage:image]];
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }];
        
        if (status != ISOpenSDKStatusSuccess)
        {
            NSString *message = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLogic] getOpenSDKStatusMessage:status]];
            STRING_NIL_TO_NONE(message)
            
            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",message,@"message", nil];
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:theMessage];
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        else
        {
            
        }
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
    }
    else
    {
        //服务器模式
        [self ocrRecognition:command WithImage:image success:^(id JSON) {
            
            NSDictionary *dic = [[NSDictionary alloc] initWithDictionary:[[JSON mj_JSONString] objectFromJSONString]];
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsDictionary:[self showServerResultWithAttributes:dic CardImage:image]];
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            
            [[[ClassFactory getInstance] getNetComm] hiddenHud];
        } failure:^(NSError *error) {
            
            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",error.localizedDescription,@"message", nil];
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:theMessage];
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            
            [[[ClassFactory getInstance] getNetComm] hiddenHud];
        }];
    }
}

#pragma mark -
#pragma mark - 合格证
- (void)vehicleCertificate:(CDVInvokedUrlCommand*)command WithCardImage:(UIImage *)image
{
    NSString *pickerCameraType = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(pickerCameraType)
    
    if ([pickerCameraType isEqualToString:@"1"] && self.buttonIndex == 0)
    {
        //新OCR 模式
        ISOpenSDKStatus status = [[ISVehicleCertificateController sharedISOpenSDKController] processCardImage:image withFinishHandler:^(NSDictionary *cardInfo) {
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsDictionary:[self showResultWithAttributes:cardInfo CardImage:image]];
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }];
        
        if (status != ISOpenSDKStatusSuccess)
        {
            NSString *message = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLogic] getOpenSDKStatusMessage:status]];
            STRING_NIL_TO_NONE(message)
            
            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",message,@"message", nil];
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:theMessage];
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        else
        {
            
        }
        [[[ClassFactory getInstance] getNetComm] hiddenHud];
    }
    else
    {
        //服务器模式
        [self ocrRecognition:command WithImage:image success:^(id JSON) {
            
            NSDictionary *dic = [[NSDictionary alloc] initWithDictionary:[[JSON mj_JSONString] objectFromJSONString]];
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsDictionary:[self showServerResultWithAttributes:dic CardImage:image]];
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            
            [[[ClassFactory getInstance] getNetComm] hiddenHud];
        } failure:^(NSError *error) {
            
            NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",error.localizedDescription,@"message", nil];
            
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsDictionary:theMessage];
            
            // send cordova result
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
            
            [[[ClassFactory getInstance] getNetComm] hiddenHud];
        }];
    }
}
#pragma mark -
#pragma mark - 解析OCR 服务器识别结果信息
- (NSDictionary *)showServerResultWithAttributes:(NSDictionary *)attributes CardImage:(UIImage *)image
{
    NSMutableDictionary *resultMutDic = [NSMutableDictionary dictionary];
    
    NSMutableDictionary *mutableAttributes = [NSMutableDictionary dictionaryWithDictionary:attributes];
    
    /******************************** 身份证 ********************************/
    NSArray *cardArr = [NSArray arrayWithObjects:@"name",//姓名
                        @"sex",//性别
                        @"id_number",//证件号码
                        @"address",//地址
                        @"people",//名族
                        @"birthday",//出生日期
                        @"gender",//性别
                        @"nation",//名族
                        @"cardNo",//证件号码
                        @"validDateBegin",//有效起期
                        @"validDateEnd",//有效止期
                        @"issue",//签发机关
                        nil];
    
    
//    extern NSString *const kCardItemName;//姓名
//    extern NSString *const kCardItemGender;//性别
//    extern NSString *const kCardItemNation;//民族
//    extern NSString *const kCardItemBirthday;//出生日期
//    extern NSString *const kCardItemAddress;//住址
//    extern NSString *const kCardItemIDNumber;//号码
//    extern NSString *const kCardItemIssueAuthority;//签发机关
//    extern NSString *const kCardItemValidity;//有效期限
    
    NSArray *cardArrString = [NSArray arrayWithObjects:@"kCardItemName",
                              @"kCardItemGender",
                              @"kCardItemIDNumber",
                              @"kCardItemAddress",
                              @"kCardItemNation",
                              @"kCardItemBirthday",
                              @"kCardItemGender",
                              @"kCardItemNation",
                              @"kCardItemIDNumber",
                              @"kCardItemValidityBegin",
                              @"kCardItemValidityEnd",
                              @"kCardItemIssueAuthority",
                              nil];
    
    for (NSInteger i = 0 ; i < cardArr.count ; i ++ )
    {
        NSString *keyName = [NSString stringWithFormat:@"%@",[cardArr objectAtIndex:i]];
        STRING_NIL_TO_NONE(keyName);
        
        if (keyName != nil)
        {
            NSString *keyRealString = [NSString stringWithFormat:@"%@",[cardArrString objectAtIndex:i]];
            STRING_NIL_TO_NONE(keyRealString);
            
            NSString *keyValue = [NSString stringWithFormat:@"%@",[mutableAttributes objectForKey:keyName]];
            STRING_NIL_TO_NONE(keyValue);
            
            if (STRING_ISNOTNIL(keyValue)) {
                [resultMutDic setObject:keyValue forKey:keyRealString];
            }
        }
    }
    /******************************** 行驶证 ********************************/
    NSArray *vehicleCardArr = [NSArray arrayWithObjects:@"vehicle_license_main_owner",
                               @"vehicle_license_main_plate_num",
                               @"vehicle_license_main_engine_no",
                               @"vehicle_license_main_vin",
                               @"vehicle_license_main_model",
                               @"vehicle_license_main_register_date",
                               @"owner",//所有人
                               @"carNum",//车牌号
                               @"carType",//车辆类型
                               @"address",//住址
                               @"purpose",//使用性质
                               @"brand",//品牌型号
                               @"carCode",//车辆代号
                               @"engineNum",//发动机号码
                               @"registerDate",//注册日期
                               @"issueDate",//发证日期
                               @"type",//行驶证类型
                               nil];

//    extern NSString *const kVehicleCardItemPlateNumber;//号牌号码
//    extern NSString *const kVehicleCardItemVehicleType;//车辆类型
//    extern NSString *const kVehicleCardItemOwner;//所有人
//    extern NSString *const kVehicleCardItemAddress;//地址
//    extern NSString *const kVehicleCardItemUseProperty;//使用性质
//    extern NSString *const kVehicleCardItemBrandModel;//品牌型号
//    extern NSString *const kVehicleCardItemVIN;//车辆识别代号
//    extern NSString *const kVehicleCardItemEngineNumber;//发动机号码
//    extern NSString *const kVehicleCardItemRegisterDate;//注册日期
//    extern NSString *const kVehicleCardItemIssueDate;//发证日期
    
    NSArray *vehicleCardArrString = [NSArray arrayWithObjects:@"kVehicleCardItemOwner",
                                     @"kVehicleCardItemPlateNumber",
                                     @"kVehicleCardItemEngineNumber",
                                     @"kVehicleCardItemVIN",
                                     @"kVehicleCardItemBrandModel",
                                     @"kVehicleCardItemRegisterDate",
                                     @"kVehicleCardItemOwner",//所有人
                                     @"kVehicleCardItemPlateNumber",//车牌号
                                     @"kVehicleCardItemVehicleType",//车辆类型
                                     @"kVehicleCardItemAddress",//住址
                                     @"kVehicleCardItemUseProperty",//使用性质
                                     @"kVehicleCardItemBrandModel",//品牌型号
                                     @"kVehicleCardItemVIN",//车辆代号
                                     @"kVehicleCardItemEngineNumber",//发动机号码
                                     @"kVehicleCardItemRegisterDate",//注册日期
                                     @"kVehicleCardItemIssueDate",//发证日期
                                     @"kVehicleCardItemType",//行驶证类型
                                     nil];
    
    for (NSInteger i = 0 ; i < vehicleCardArr.count ; i ++ )
    {
        NSString *keyName = [NSString stringWithFormat:@"%@",[vehicleCardArr objectAtIndex:i]];
        STRING_NIL_TO_NONE(keyName);
        
        if (keyName != nil)
        {
            NSString *keyRealString = [NSString stringWithFormat:@"%@",[vehicleCardArrString objectAtIndex:i]];
            STRING_NIL_TO_NONE(keyRealString);
            
            NSString *keyValue = [NSString stringWithFormat:@"%@",[mutableAttributes objectForKey:keyName]];
            STRING_NIL_TO_NONE(keyValue);
            
            if (STRING_ISNOTNIL(keyValue)) {
                [resultMutDic setObject:keyValue forKey:keyRealString];
            }
        }
    }
    
    /******************************** 合格证 ********************************/
    NSArray *cardReaderArr = [NSArray arrayWithObjects:@"vehicle_certification_model_vin",
                              @"vehicle_certification_model_number",
                              @"vehicle_certification_engine_number",
                              @"vehicle_certification_displacement",
                              @"vehicle_certification_power",
                              @"vehicle_certification_mass_rated",
                              @"vehicle_certification_seating_Capacity",
                              @"modelNumber",//车辆型号
                              @"modelVin",//车辆识别代号/车架号
                              @"engineNumber",//发动机号
                              @"displacement",//排量
                              @"power",//功率
                              @"massRated",//额定载质量(kg)
                              @"capacity",//额定载客(人)
                              @"outlineOne",//外廓尺寸1
                              @"outlineTwo",//外廓尺寸2
                              @"outlineThree",//外廓尺寸3
                              @"tireParam",//轮胎规格
                              @"wheelTreadFront",//前轮距
                              @"wheelTreadRear",//后轮距
                              @"makeDate",//制造日期
                              nil];
    
//    extern NSString *const kVCItemModelNumber;//车辆型号
//    extern NSString *const kVCItemModelVIN;//车辆识别代号/车架号
//    extern NSString *const kVCItemEnginNumber;//发动机号
//    extern NSString *const kVCItemDisplacement;//排量
//    extern NSString *const kVCItemPower;//功率
//    extern NSString *const kVCItemMassRated;//额定载质量(kg)
//    extern NSString *const kVCItemSeatingCapacity;//额定载客(人)
    
    NSArray *cardReaderArrString = [NSArray arrayWithObjects:@"kVCItemModelVIN",
                                    @"kVCItemModelNumber",
                                    @"kVCItemEnginNumber",
                                    @"kVCItemDisplacement",
                                    @"kVCItemPower",
                                    @"kVCItemMassRated",
                                    @"kVCItemSeatingCapacity",
                                    @"kVCItemModelNumber",//车辆型号
                                    @"kVCItemModelVIN",//车辆识别代号/车架号
                                    @"kVCItemEnginNumber",//发动机号
                                    @"kVCItemDisplacement",//排量
                                    @"kVCItemPower",//功率
                                    @"kVCItemMassRated",//额定载质量(kg)
                                    @"kVCItemSeatingCapacity",//额定载客(人)
                                    @"kVCItemOutlineOne",//外廓尺寸1
                                    @"kVCItemOutlineTwo",//外廓尺寸2
                                    @"kVCItemOutlineThree",//外廓尺寸3
                                    @"kVCItemTireParam",//轮胎规格
                                    @"kVCItemWheelTreadFront",//前轮距
                                    @"kVCItemWheelTreadRear",//后轮距
                                    @"kVCItemMakeDate",//制造日期
                                    nil];
    
    for (NSInteger i = 0 ; i < cardReaderArr.count ; i ++ )
    {
        NSString *keyName = [NSString stringWithFormat:@"%@",[cardReaderArr objectAtIndex:i]];
        STRING_NIL_TO_NONE(keyName);
        
        if (keyName != nil)
        {
            NSString *keyRealString = [NSString stringWithFormat:@"%@",[cardReaderArrString objectAtIndex:i]];
            STRING_NIL_TO_NONE(keyRealString);
            
            NSString *keyValue = [NSString stringWithFormat:@"%@",[mutableAttributes objectForKey:keyName]];
            STRING_NIL_TO_NONE(keyValue);
            
            if (STRING_ISNOTNIL(keyValue)) {
                [resultMutDic setObject:keyValue forKey:keyRealString];
            }
        }
    }
    
    /******************************** ***** ********************************/
    NSData *data = UIImageJPEGRepresentation(image, 0.0000001);
    NSString *imageDataString = [NSString stringWithFormat:@"%@",[data cdv_base64EncodedString]];
    STRING_NIL_TO_NONE(imageDataString);
    
    [resultMutDic setObject:imageDataString forKey:@"imageData"];
    
    [resultMutDic setObject:@"1" forKey:@"resultCode"];
    /******************************** ***** ********************************/
    
    return resultMutDic;
}

#pragma mark -
#pragma mark - 解析OCR SDK识别结果信息
- (NSDictionary *)showResultWithAttributes:(NSDictionary *)attributes CardImage:(UIImage *)image
{
    NSMutableDictionary *resultMutDic = [NSMutableDictionary dictionary];
    
    NSMutableDictionary *mutableAttributes = [NSMutableDictionary dictionaryWithDictionary:attributes];
    NSDictionary *itemDict = [mutableAttributes objectForKey:kOpenSDKCardResultTypeCardItemInfo];
    if ([itemDict isKindOfClass:[NSDictionary class]])
    {
        /******************************** 身份证 ********************************/
        NSArray *cardArr = [NSArray arrayWithObjects:kCardItemName,
                            kCardItemGender,
                            kCardItemNation,
                            kCardItemBirthday,
                            kCardItemAddress,
                            kCardItemIDNumber,
                            kCardItemIssueAuthority,
                            kCardItemValidity, nil];
        
        NSArray *cardArrString = [NSArray arrayWithObjects:@"kCardItemName",
                            @"kCardItemGender",
                            @"kCardItemNation",
                            @"kCardItemBirthday",
                            @"kCardItemAddress",
                            @"kCardItemIDNumber",
                            @"kCardItemIssueAuthority",
                            @"kCardItemValidity", nil];
        
        for (NSInteger i = 0 ; i < cardArr.count ; i ++ )
        {
            NSString *key = [NSString stringWithFormat:@"%@",[cardArr objectAtIndex:i]];
            STRING_NIL_TO_NONE(key);
            
            ISCardReaderResultItem *resultItem = itemDict[key];
            
            if (resultItem != nil)
            {
                NSString *keyString = [NSString stringWithFormat:@"%@",[cardArrString objectAtIndex:i]];
                STRING_NIL_TO_NONE(keyString);
                
                if([resultItem isKindOfClass:[NSString class]])
                {
                    [resultMutDic setObject:resultItem forKey:keyString];
                }
                else
                {
                    [resultMutDic setObject:resultItem.itemValue forKey:keyString];
                }
            }
        }
        
        /******************************** 行驶证 ********************************/
        NSArray *vehicleCardArr = [NSArray arrayWithObjects:kVehicleCardItemPlateNumber,
                                   kVehicleCardItemVehicleType,
                                   kVehicleCardItemOwner,
                                   kVehicleCardItemAddress,
                                   kVehicleCardItemUseProperty,
                                   kVehicleCardItemBrandModel,
                                   kVehicleCardItemVIN,
                                   kVehicleCardItemEngineNumber,
                                   kVehicleCardItemRegisterDate,
                                   kVehicleCardItemIssueDate, nil];
        
        NSArray *vehicleCardArrString = [NSArray arrayWithObjects:@"kVehicleCardItemPlateNumber",
                                   @"kVehicleCardItemVehicleType",
                                   @"kVehicleCardItemOwner",
                                   @"kVehicleCardItemAddress",
                                   @"kVehicleCardItemUseProperty",
                                   @"kVehicleCardItemBrandModel",
                                   @"kVehicleCardItemVIN",
                                   @"kVehicleCardItemEngineNumber",
                                   @"kVehicleCardItemRegisterDate",
                                   @"kVehicleCardItemIssueDate", nil];
        
        for (NSInteger i = 0 ; i < vehicleCardArr.count ; i ++ )
        {
            NSString *key = [NSString stringWithFormat:@"%@",[vehicleCardArr objectAtIndex:i]];
            STRING_NIL_TO_NONE(key);
            
            ISCardReaderResultItem *resultItem = itemDict[key];
            
            if (resultItem != nil)
            {
                NSString *keyString = [NSString stringWithFormat:@"%@",[vehicleCardArrString objectAtIndex:i]];
                STRING_NIL_TO_NONE(keyString);
                
                if([resultItem isKindOfClass:[NSString class]])
                {
                    [resultMutDic setObject:resultItem forKey:keyString];
                }
                else
                {
                    [resultMutDic setObject:resultItem.itemValue forKey:keyString];
                }
            }
        }
        
        /******************************** 合格证 ********************************/
        NSArray *cardReaderArr = [NSArray arrayWithObjects:kVCItemModelNumber,
                                  kVCItemModelVIN,
                                  kVCItemEnginNumber,
                                  kVCItemDisplacement,
                                  kVCItemPower,
                                  kVCItemMassRated,
                                  kVCItemSeatingCapacity, nil];
        
        NSArray *cardReaderArrString = [NSArray arrayWithObjects:@"kVCItemModelNumber",
                                  @"kVCItemModelVIN",
                                  @"kVCItemEnginNumber",
                                  @"kVCItemDisplacement",
                                  @"kVCItemPower",
                                  @"kVCItemMassRated",
                                  @"kVCItemSeatingCapacity", nil];
        
        for (NSInteger i = 0 ; i < cardReaderArr.count ; i ++ )
        {
            NSString *key = [NSString stringWithFormat:@"%@",[cardReaderArr objectAtIndex:i]];
            STRING_NIL_TO_NONE(key);
            
            ISCardReaderResultItem *resultItem = itemDict[key];
            
            if (resultItem != nil)
            {
                NSString *keyString = [NSString stringWithFormat:@"%@",[cardReaderArrString objectAtIndex:i]];
                STRING_NIL_TO_NONE(keyString);
                
                if([resultItem isKindOfClass:[NSString class]])
                {
                    [resultMutDic setObject:resultItem forKey:keyString];
                }
                else
                {
                    [resultMutDic setObject:resultItem.itemValue forKey:keyString];
                }
            }
        }
        
        /******************************** 驾驶证 ********************************/
        NSArray *drivingLicenseArr = [NSArray arrayWithObjects:kDrivingCardItemIDNumber,
                                      kDrivingCardItemName,
                                      kDrivingCardItemSex,
                                      kDrivingCardItemNationality,
                                      kDrivingCardItemAddress,
                                      kDrivingCardItemBirthDay,
                                      kDrivingCardItemIssueDate,
                                      kDrivingCardItemDriveType,
                                      kDrivingCardItemValidFrom,
                                      kDrivingCardItemValidFor, nil];

        NSArray *drivingLicenseArrString = [NSArray arrayWithObjects:@"kDrivingCardItemIDNumber",
                                      @"kDrivingCardItemName",
                                      @"kDrivingCardItemSex",
                                      @"kDrivingCardItemNationality",
                                      @"kDrivingCardItemAddress",
                                      @"kDrivingCardItemBirthDay",
                                      @"kDrivingCardItemIssueDate",
                                      @"kDrivingCardItemDriveType",
                                      @"kDrivingCardItemValidFrom",
                                      @"kDrivingCardItemValidFor", nil];

        for (NSInteger i = 0 ; i < drivingLicenseArr.count ; i ++ )
        {
            NSString *key = [NSString stringWithFormat:@"%@",[drivingLicenseArr objectAtIndex:i]];
            STRING_NIL_TO_NONE(key);

            ISCardReaderResultItem *resultItem = itemDict[key];

            if (resultItem != nil)
            {
                NSString *keyString = [NSString stringWithFormat:@"%@",[drivingLicenseArrString objectAtIndex:i]];
                STRING_NIL_TO_NONE(keyString);

                if([resultItem isKindOfClass:[NSString class]])
                {
                    [resultMutDic setObject:resultItem forKey:keyString];
                }
                else
                {
                    [resultMutDic setObject:resultItem.itemValue forKey:keyString];
                }
            }
        }
    }
    
    /******************************** ***** ********************************/
    NSData *data = UIImageJPEGRepresentation(image, 0.0000001);
    NSString *imageDataString = [NSString stringWithFormat:@"%@",[data cdv_base64EncodedString]];
    STRING_NIL_TO_NONE(imageDataString);
    
    [resultMutDic setObject:imageDataString forKey:@"imageData"];
    
    [resultMutDic setObject:@"1" forKey:@"resultCode"];
    /******************************** ***** ********************************/
    
    return resultMutDic;
}

#pragma mark -
#pragma mark - OCR识别
- (void)ocrRecognition:(CDVInvokedUrlCommand *)command WithImage:(UIImage *)image success:(void (^)(id JSON))success
               failure:(void (^)(NSError *error))failure
{
    //ocr请求url
    NSString *ocrUrl = [NSString stringWithFormat:@"%@",[command argumentAtIndex:2]];
    STRING_NIL_TO_NONE(ocrUrl);
    
    AFSessionManager *manager = [AFSessionManager manager];
    
    //发送格式
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    //接受格式
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    //进程标签
    NSString *fileName = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLogic] getImageName]];
    STRING_NIL_TO_NONE(fileName);
    manager.targetOnlyStr = fileName;
    
    [manager POST:ocrUrl parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        NSData *imageData = UIImageJPEGRepresentation(image, 1);
        
        [formData appendPartWithFileData:imageData name:@"file" fileName:fileName mimeType:@"image/jpeg"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        // 配置上传 百分比
        float percentDone = uploadProgress.completedUnitCount/uploadProgress.totalUnitCount;
        
        NSLog(@"OCR百分比 ====================%f",percentDone);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success){
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure)
        {
            failure(error);
        }
        
        NSString *errorInfo = [NSString stringWithFormat:@"%@",error.localizedDescription];
        STRING_NIL_TO_NONE(errorInfo);
        
        [[[ClassFactory getInstance] getInfoHUD] showHud:errorInfo];
    }];
}


@end
