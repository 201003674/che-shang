//
//  ISVehicleLicenseReaderController.h
//  ISIDReaderSDK
//
//  Created by Felix on 15/11/11.
//  Copyright (c) 2015年 IntSig. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ISOpenSDKFoundation/ISOpenSDKFoundation.h>

extern NSString *const kDrivingCardItemIDNumber;//驾驶证号
extern NSString *const kDrivingCardItemName;//姓名
extern NSString *const kDrivingCardItemSex;//性别
extern NSString *const kDrivingCardItemNationality;//国籍
extern NSString *const kDrivingCardItemAddress;//地址
extern NSString *const kDrivingCardItemBirthDay;//出生日期
extern NSString *const kDrivingCardItemIssueDate;//初次申领日期
extern NSString *const kDrivingCardItemDriveType;//准驾车型
extern NSString *const kDrivingCardItemValidFrom;//有效起始日期
extern NSString *const kDrivingCardItemValidFor;//有效期限

typedef void(^RecognizeCardFinishHandler)(NSDictionary *cardInfo);
typedef void(^ConstructResourcesFinishHandler)(ISOpenSDKStatus status);

@interface ISDrivingLicenseReaderController : NSObject

+ (ISDrivingLicenseReaderController *)sharedController;

- (void)constructResourcesWithAppKey:(NSString *)appKey
                           subAppkey:(NSString *)subAppKey
                       finishHandler:(ConstructResourcesFinishHandler)handler;
- (ISOpenSDKStatus)processCardImage:(UIImage *)image
                withFinishHandler:(RecognizeCardFinishHandler)handler;
- (void)destructResources;
+ (NSString *)getSDKVersion;
@end
