//
//  ISVehicleLicenseReaderController.h
//  ISIDReaderSDK
//
//  Created by Simon liu on 17/2/6.
//  Copyright (c) 2016年 IntSig-xzliu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ISOpenSDKFoundation/ISOpenSDKFoundation.h>

extern NSString *const kVehicleCardItemPlateNumber;//号牌号码
extern NSString *const kVehicleCardItemVehicleType;//车辆类型
extern NSString *const kVehicleCardItemOwner;//所有人
extern NSString *const kVehicleCardItemAddress;//地址
extern NSString *const kVehicleCardItemUseProperty;//使用性质
extern NSString *const kVehicleCardItemBrandModel;//品牌型号
extern NSString *const kVehicleCardItemVIN;//车辆识别代号
extern NSString *const kVehicleCardItemEngineNumber;//发动机号码
extern NSString *const kVehicleCardItemRegisterDate;//注册日期
extern NSString *const kVehicleCardItemIssueDate;//发证日期

typedef void(^RecognizeCardFinishHandler)(NSDictionary *cardInfo);
typedef void(^ConstructResourcesFinishHandler)(ISOpenSDKStatus status);

@interface ISVehicleLicenseReaderController : NSObject

+ (ISVehicleLicenseReaderController *)sharedController;

/*
 You should call this method to construct resources before call other APIs in the SDK,calling this API will cause an online authorization for the first time this SDK is used.Once authorized successfully, this API will be returned immediately within expire time and try to update authorization status in background
 */
- (void)constructResourcesWithAppKey:(NSString *)appKey
                           subAppkey:(NSString *)subAppKey
                       finishHandler:(ConstructResourcesFinishHandler)handler;

- (ISOpenSDKStatus)processCardImage:(UIImage *)image
                withFinishHandler:(RecognizeCardFinishHandler)handler;

/*
 Use this method to release SDK resources
 */
- (void)destructResources;
/*
 Use this method to get the current SDK version information
 */
+ (NSString *)versionBankCardSDK;

@end
