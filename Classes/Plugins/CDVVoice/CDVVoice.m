/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

#import "CDVVoice.h"

#import "MCAudioInputQueue.h"
@implementation CDVVoice

//初始化录音
- (void)initRecording:(CDVInvokedUrlCommand*)command
{
    format.mFormatID = kAudioFormatLinearPCM;
    format.mFormatFlags = kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
    format.mBitsPerChannel = 16;
    format.mChannelsPerFrame = 1;
    format.mBytesPerPacket = format.mBytesPerFrame = (format.mBitsPerChannel / 8) * format.mChannelsPerFrame;
    format.mFramesPerPacket = 1;
    format.mSampleRate = 16000.0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_interrupted:) name:AVAudioSessionInterruptionNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_interrupted:) name:AVAudioSessionRouteChangeNotification object:nil];
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    
    //语音流
    mutableData = [[NSMutableData alloc] initWithCapacity:10];
}

#pragma mark -
#pragma mark - 开始录音
- (void)startRecording:(CDVInvokedUrlCommand*)command
{
    self.command = command;
    
    //初始化
    [self initRecording:command];
    
    recorder = [MCAudioInputQueue inputQueueWithFormat:format bufferDuration:1.0 delegate:self];
    recorder.meteringEnabled = YES;
    
    [recorder start];
}

#pragma mark -
#pragma mark - 结束录音
- (void)stopRecording:(CDVInvokedUrlCommand*)command
{  
    BOOL success = [recorder stop];
    
    if (success)
    {
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",@"结束录音成功!",@"message", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:self.command.callbackId];
    }
    else
    {
        NSDictionary *theMessage = [NSDictionary dictionaryWithObjectsAndKeys:@"0",@"resultCode",@"结束录音失败!",@"message", nil];
        
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                messageAsDictionary:theMessage];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:self.command.callbackId];
    }
}

#pragma mark -
#pragma mark - interrupt
- (void)_interrupted:(NSNotification *)notification
{
    
}

#pragma mark -
#pragma mark - MCAudioInputQueueDelegate
- (void)inputQueue:(MCAudioInputQueue *)inputQueue inputData:(NSData *)data numberOfPackets:(UInt32)numberOfPackets
{
    if (data)
    {
        [mutableData appendData:data];
    }

    //延迟0.2秒执行 发送数据给前端
    dispatch_time_t timer = dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC);
    dispatch_after(timer, dispatch_get_main_queue(), ^{

        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                messageAsArrayBuffer:mutableData];
        
        //CDVPluginResult.keepCallback 设置为 true ,则不会销毁callback
        [result setKeepCallback:[NSNumber numberWithInt:10000]];
        [result setKeepCallbackAsBool:YES];
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:self.command.callbackId];
        
        mutableData = [NSMutableData data];
        
    });

    [inputQueue updateMeters];
}
- (void)inputQueue:(MCAudioInputQueue *)inputQueue errorOccur:(NSError *)error
{
    [self stopRecording:self.command];
}

#pragma mark -
#pragma mark -

@end
