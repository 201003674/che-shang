#import "CDVBasePlugin.h"
#import "MCAudioInputQueue.h"

@interface CDVVoice : CDVBasePlugin<MCAudioInputQueueDelegate>
{
    AudioStreamBasicDescription format;
    
    MCAudioInputQueue *recorder;
    NSMutableData *mutableData;
}
//开始录音
- (void)startRecording:(CDVInvokedUrlCommand*)command;
//结束录音
- (void)stopRecording:(CDVInvokedUrlCommand*)command;

@end
