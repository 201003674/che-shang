//
//  CDVLocalStroage.m
//  IOSFramework
//
//  Created by 林科 on 2018/3/14.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import "CDVLocalStroage.h"

@implementation CDVLocalStroage

-(void) setLocalStroage: (CDVInvokedUrlCommand*)command
{
    // 0 key 1 Value
    NSString *key = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(key);
    
    // value
    NSString *value = [NSString stringWithFormat:@"%@",[command argumentAtIndex:1]];
    STRING_NIL_TO_NONE(value);
    
    [[[ClassFactory getInstance] getLogic] saveObject:value Path:key];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *isKeyExist = [NSString stringWithFormat:@"%@",[userDefaults stringForKey:key]];
    STRING_NIL_TO_NONE(isKeyExist)
    
    if (isKeyExist.length != 0)
    {
        // create acordova result
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsString:@"数据暂存成功!"];
        
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
    else
    {
        // create acordova result
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsString:@"数据暂存失败!"];
        
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
}

-(void) getLocalStroage: (CDVInvokedUrlCommand*)command
{
    NSString *key = [NSString stringWithFormat:@"%@",[command argumentAtIndex:0]];
    STRING_NIL_TO_NONE(key);
    
    NSString *value = [NSString stringWithFormat:@"%@",[[[ClassFactory getInstance] getLogic] getObject:key]];
    STRING_NIL_TO_NONE(value);
    
    if (value.length != 0)
    {
        // create acordova result
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK
                                                    messageAsString:value];
        
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
    else
    {
        // create acordova result
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR
                                                    messageAsString:value];
        
        // send cordova result
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
}

@end
