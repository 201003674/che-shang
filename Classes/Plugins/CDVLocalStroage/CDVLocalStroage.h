//
//  CDVLocalStroage.h
//  IOSFramework
//
//  Created by 林科 on 2018/3/14.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import <Cordova/CDVPlugin.h>

@interface CDVLocalStroage : CDVPlugin

-(void) setLocalStroage: (CDVInvokedUrlCommand*)command;

-(void) getLocalStroage: (CDVInvokedUrlCommand*)command;

@end
