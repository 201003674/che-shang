//
//  ExitCRM.m
//  CRM
//
//  Created by 林科 on 16/1/4.
//  Copyright © 2016年 FBI01. All rights reserved.
//

#import "ExitCRM.h"

#import "AppDelegate.h"

@implementation ExitCRM

-(void)exitCRM:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        
        for(NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
            
            if([[cookie domain] isEqualToString:@"userName"])
            {
                break;
            }
            else if ([[cookie domain] isEqualToString:@"unitCode"])
            {
                break;
            }
            else
            {
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
            }
        }
        
        #ifdef DEBUG
        
        #else
        
        [MPPPushServer delBindPushWithCallBack:^(BOOL isSucess) {
            
            if (!isSucess)
            {
                [[[ClassFactory getInstance] getInfoHUD] showHud:@"解绑失败!"];
            }
            
        }];
       
        #endif
        
        //WEB缓存
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        
        NSURLCache * cache = [NSURLCache sharedURLCache];
        [cache removeAllCachedResponses];
        [cache setDiskCapacity:0];
        [cache setMemoryCapacity:0];
        
        //应用缓存
        [[[ClassFactory getInstance] getNetComm] clearTheCache];
        
        //清除缓存
        [[[ClassFactory getInstance] getLogic] setLoginSuccess:NO];
        
        //清除移动CRM数据
        [[[ClassFactory getInstance] getNetComm] saveObject:nil Path:USER_PATH_INFO];
        
        //清除数据库缓存
        FMDBSqlite *fmdbSql = [[FMDBSqlite alloc] init];
        
        [fmdbSql clearTable:UPLOAD_TABLE_NAME complete:^(BOOL isSuccess) {
            
            if (isSuccess){
                NSLog(@"数据库清除成功!");
            }
            else
            {
                NSLog(@"数据库清除失败!");
            }
        }];
        
    }];
    
    //退出应用
    [(AppDelegate *)self.appDelegate loadLoginView];
}
@end
