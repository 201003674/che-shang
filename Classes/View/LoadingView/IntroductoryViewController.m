
#import "IntroductoryViewController.h"
#import <QuartzCore/QuartzCore.h>


/* 图片的坐标大小  */
#define IMAGEVIEW_WIDTH         320
#define IMAGEVIEW_HEIGHT        160

@interface IntroductoryViewController ()
{
    
}


@end


@implementation IntroductoryViewController

@synthesize myScrollView;
@synthesize pageControl;


#define MYTIMER             3.0f            /* 自动滑动间隔事件 */
#define PAGECONTROL_HIGHT   8               /* pagecontrol */



/* 初始化 */
- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        imageArray = [[NSMutableArray alloc] init];
    }
    return self;
}


/* 刷新数据 */
- (void)reloadPageControlDataArray:(NSArray *)imageUrlArr
{
    [self.pageControl removeFromSuperview];
    
    if (imageArray.count > 0)
    {
        [imageArray removeAllObjects];
    }
    
    /* 对数据的各种情况进行判断 */
    if (imageUrlArr != nil)
    {
        [imageArray addObjectsFromArray:imageUrlArr];
    }
    
    /* 创建scroller和pageControl */
    [self configScrollView];
}



/* 创建scroller和pageControl */
-(void)configScrollView
{
    //初始化UIScrollView，设置相关属性，均可在storyBoard中设置
    CGRect frame= self.frame;
    self.myScrollView = [[UIScrollView alloc]initWithFrame:frame];    //scrollView的大小
    self.myScrollView.backgroundColor=[UIColor clearColor];
    self.myScrollView.pagingEnabled=YES;//以页为单位滑动，即自动到下一页的开始边界
    self.myScrollView.showsVerticalScrollIndicator=NO;
    self.myScrollView.showsHorizontalScrollIndicator=NO;//隐藏垂直和水平显示条
    self.myScrollView.delegate=self;
    
    /* 拖动属性 */
    if (imageArray.count <= 1)
    {
        myScrollView.scrollEnabled = NO;
    }
    else
    {
        myScrollView.scrollEnabled = YES;
    }
    
    
    UIImage *defaultImage = [UIImage imageNamed:@"COMM_Page_DefaultImage.png"];
    
    CGFloat Width=self.myScrollView.frame.size.width;
    CGFloat Height=self.myScrollView.frame.size.height;
    
    
    for (int i=0; i<[imageArray count]; i++)
    {
        UIImageView *subViews=[[UIImageView alloc] initWithImage:defaultImage];
        subViews.userInteractionEnabled = YES;   /* 打开imageview的点击属性 */
        subViews.tag = 10001 + i;
        subViews.frame=CGRectMake(Width*i, 0, Width, Height);
        [self.myScrollView addSubview: subViews];
        
        NSString *imageName = [NSString stringWithFormat:@"%@",[imageArray objectAtIndex:i]];
        STRING_NIL_TO_NONE(imageName);
        
        subViews.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:imageName ofType:@"png"]];
        
        if (i == [imageArray count]-1)
        {
            /* 进入程序按钮 */
            UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];

            backButton.frame = CGRectMake(Width*i + (Width - 210)/2.0, self.myScrollView.center.y+90, 210, 90);
            
            [backButton setImage:[UIImage imageNamed:@"tutorial_enter"] forState:UIControlStateNormal];
            [backButton setImage:[UIImage imageNamed:@"tutorial_enter"] forState:UIControlStateHighlighted];
            
//            [backButton setTitle:@"进入程序" forState:UIControlStateNormal];
//            backButton.titleLabel.font = [UIFont systemFontOfSize:14];
            [backButton setTitleColor:HEXCOLOR(0XFF0000) forState:UIControlStateNormal];
            [backButton setTitleColor:HEXCOLOR(0XFF0000) forState:UIControlStateHighlighted];
            [backButton addTarget:self action:@selector(didClickOnButton:) forControlEvents:UIControlEventTouchUpInside];
            [self.myScrollView addSubview:backButton];
        }
    }
    
    [self.myScrollView setContentSize:CGSizeMake(Width*imageArray.count, Height)];
    [self addSubview:self.myScrollView];
    [self.myScrollView scrollRectToVisible:CGRectMake(0, 0, Width, Height) animated:NO];
    //show the real first image,not the first in the scrollView
    
    //设置pageControl的位置，及相关属性，可选
    CGRect pageControlFrame=CGRectMake(0, self.frame.size.height - PAGECONTROL_HIGHT - 2, self.frame.size.width, PAGECONTROL_HIGHT);
    self.pageControl=[[UIPageControl alloc]initWithFrame:pageControlFrame];
    [self.pageControl setBounds:CGRectMake(0, 0, 16*(self.pageControl.numberOfPages-1), 16)];//设置pageControl中点的间距为16
    [self.pageControl setBounds:CGRectMake(0, 0, self.frame.size.width, PAGECONTROL_HIGHT)];
    //[self.pageControl.layer setCornerRadius:8];//设置圆角
    self.pageControl.backgroundColor = [UIColor clearColor];
    
    self.pageControl.numberOfPages=imageArray.count;
    //    self.pageControl.backgroundColor=[UIColor blueColor];//背景
    self.pageControl.currentPage = 0;
    self.pageControl.enabled = YES;
    [self addSubview:self.pageControl];
}


/* 拖动图片变动 */
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (imageArray.count <= 1)
    {
        return;
    }
    
    CGFloat pageWidth=self.myScrollView.frame.size.width;
    int currentPage=floor((self.myScrollView.contentOffset.x-pageWidth/2)/pageWidth);
    
    self.pageControl.currentPage=currentPage+1;
}


- (IBAction)didClickOnButton:(id)sender
{
    if (_buttonBlock)
        _buttonBlock(sender);
}

- (void)setBackButtonBlock:(IntroButtonBlock)block
{
    _buttonBlock = block;
}


@end
