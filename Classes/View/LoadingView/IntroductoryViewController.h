//
//  UICustomPageController.h
//  GUOHUALIFE
//
//  Created by zte- s on 12-11-23.
//  Copyright (c) 2012年 zte. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICustomDefine.h"

typedef void (^IntroButtonBlock)(UIButton *button);

@interface IntroductoryViewController : UIView <UIScrollViewDelegate>
{
    NSMutableArray *imageArray;//存放图片
    NSTimer *myTimer;//定时器
    
    UIScrollView *myScrollView;
    UIPageControl *pageControl;
    
    NSInteger nowPageNumber;
    
    IntroButtonBlock _buttonBlock;
}
@property(nonatomic,retain)  UIScrollView *myScrollView;
@property(nonatomic,retain)  UIPageControl *pageControl;

@property(strong, nonatomic) id pSender;
@property(assign, nonatomic) SEL pSel;

- (void)setBackButtonBlock:(IntroButtonBlock)block;

/* 刷新数据 */
- (void)reloadPageControlDataArray:(NSArray *)imageUrlArr;


@end


