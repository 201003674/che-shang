/*********************************************************************
 * 版权所有 LK
 *
 * 文件名称：
 * 文件标识：
 * 内容摘要：
 * 其它说明：
 * 当前版本：
 * 作    者： 林科
 * 完成日期：
 **********************************************************************/

/***************************************************************************
 *                                文件引用
 ***************************************************************************/
#import "LoadingViewController.h"
#import "AppDelegate.h"
#import "APPUpDateFromServer.h"
#import "ICETutorialController.h"


/***************************************************************************
 *                                 宏定义
 ***************************************************************************/
/*动态加载图片计时器时间*/
#define LOADING_CARTON_TIME 0.3

/* 退出loading界面计时器时间 */
#define LOADING_LOADING_TIME 0.2

/* loading图标坐标和大小 */
#define ACTIVITYVIEW_RECT_X         (float)144.0f
#define ACTIVITYVIEW_RECT_Y         (float)275.0f
#define ACTIVITYVIEW_RECT_WIDTH     (float)32.0f
#define ACTIVITYVIEW_RECT_HIGHT     (float)32.0f


/* 图片 两种不同型号尺寸图片 */
#define LOADING_IMAGE_480           @"Default"
#define LOADING_IMAGE_568           @"Default-568h"


/***************************************************************************
 *                                 常量
 ***************************************************************************/


/***************************************************************************
 *                                类型定义
 ***************************************************************************/


/***************************************************************************
 *                                全局变量
 ***************************************************************************/


/***************************************************************************
 *                                 原型
 ***************************************************************************/
@interface LoadingViewController ()
{
    APPUpDateFromServer *appUpDate;
    
    NSString *platDownloadUrl;  //更新app下载
}
@end


/***************************************************************************
 *                                类的实现
 ***************************************************************************/
@implementation LoadingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

#pragma mark -
#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /* 背景图 */
    UIImageView *bgiv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WINDOWSCREEN_WIDTH, WINDOWSCREEN_HEIGHT)];
    bgiv.tag = 100001;
    bgiv.backgroundColor = [UIColor clearColor];
    
    if (VIEWSCREEN_HEIGHT == 460)
    {
        bgiv.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:LOADING_IMAGE_480 ofType:@"png"]];
    }
    else
    {
        bgiv.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:LOADING_IMAGE_568 ofType:@"png"]];
    }
    
    [self.view addSubview:bgiv];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

#pragma mark -
#pragma mark - 通知注册


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    /* 注册更新取消的通知 */
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateCancle" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateCancle:)
                                                 name: @"updateCancle"
                                               object: nil];
    
    /* 获取更新信息 */
    appUpDate = [[APPUpDateFromServer alloc] init];
    
    [appUpDate setSender_id:self];
    [appUpDate clickAppVer];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateCancle" object:nil];
}

#pragma -
#pragma mark - 更新操作
/* 取消更新 */
- (void)updateCancle:(NSNotification *)notification
{
    [self loadingDone];
}

#pragma mark -
#pragma mark - 移除loading页面

/**
 *	@brief	移除loading页面
 */
- (void)loadingDone
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [(AppDelegate *)[UIApplication sharedApplication].delegate loadLoginView];
}

@end
