//
//  TXDLoginViewController.m
//  IOSFramework
//
//  Created by 林科 on 2018/8/23.
//  Copyright © 2018年 allianture. All rights reserved.
//

#import "TXDLoginViewController.h"

#import "AppDelegate.h"

@implementation TXDLoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    /* 初始化登录按钮 */
    [self initTHC];
}

#pragma mark -
#pragma mark - 
- (void)initTHC
{
    button = [UICustomButton buttonWithType:UIButtonTypeCustom];
    
    [button createButtonWithFrame:CGRectMake(30.0, SCREEN_HEIGHT - SCREEN_HEIGHT/12 - 40, 40, 40) Title:@"CRM" TitleColor:HEXCOLOR(0X535353) Font:12.0 Target:self Selector:@selector(tHCButtonAction:) BackgroundImageNormal:@"tyb" BackgroundImageHighlighted:@"tyb"];
    //top, left, bottom, right
    [button setTitleEdgeInsets:UIEdgeInsetsMake(60, 0, 0, 0)];
    [button setHidden:YES];
    
    [button setHidden:YES];
    
    [self.view addSubview:button];
}

- (void)tHCButtonAction:(UICustomButton *)button
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark - UIWebViewDelegate
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [super webViewDidStartLoad:webView];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [super webViewDidFinishLoad:webView];
    
    [button setHidden:NO];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(nonnull NSError *)error
{
    [super webView:webView didFailLoadWithError:error];
    
    [button setHidden:NO];
    
    if ([error code] == NSURLErrorCancelled) return;
}

#pragma mark -
#pragma mark -

@end
