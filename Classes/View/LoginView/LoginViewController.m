//
//  LoginViewController.m
//  CRM
//
//  Created by 林科 on 15/12/10.
//  Copyright © 2015年 FBI01. All rights reserved.
//

#import "LoginViewController.h"

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /* 越狱判断 */
    if ([MobClick isJailbroken])
    {
        [[[ClassFactory getInstance] getInfoHUD] showHud:@"您的手机已越狱，使用此应用可能存在风险."];
    }
    
    /* 导航隐藏 */
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark -
#pragma mark - UIWebViewDelegate
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [super webViewDidStartLoad:webView];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [super webViewDidFinishLoad:webView];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(nonnull NSError *)error
{
    [super webView:webView didFailLoadWithError:error];
    
    if ([error code] == NSURLErrorCancelled) return;
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return [super webView:webView shouldStartLoadWithRequest:request navigationType:navigationType];
}

#pragma mark -
#pragma mark -

@end
